﻿namespace GI.Flash.Test
{
    partial class CTSSim
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MessageBox = new System.Windows.Forms.TextBox();
            this.portCombo = new System.Windows.Forms.ComboBox();
            this.connectBtn = new System.Windows.Forms.Button();
            this.ConnectedLbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.EjectBtn = new System.Windows.Forms.Button();
            this.ReadBtn = new System.Windows.Forms.Button();
            this.cardInfoControl = new GI.Flash.Test.CardInfoControl();
            this.CreditBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MessageBox
            // 
            this.MessageBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBox.Location = new System.Drawing.Point(13, 347);
            this.MessageBox.Margin = new System.Windows.Forms.Padding(4);
            this.MessageBox.Multiline = true;
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MessageBox.Size = new System.Drawing.Size(840, 181);
            this.MessageBox.TabIndex = 2;
            // 
            // portCombo
            // 
            this.portCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portCombo.FormattingEnabled = true;
            this.portCombo.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3"});
            this.portCombo.Location = new System.Drawing.Point(20, 95);
            this.portCombo.Name = "portCombo";
            this.portCombo.Size = new System.Drawing.Size(92, 21);
            this.portCombo.TabIndex = 5;
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(129, 91);
            this.connectBtn.Margin = new System.Windows.Forms.Padding(4);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(100, 28);
            this.connectBtn.TabIndex = 3;
            this.connectBtn.Text = "Connect";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // ConnectedLbl
            // 
            this.ConnectedLbl.BackColor = System.Drawing.Color.Red;
            this.ConnectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConnectedLbl.Location = new System.Drawing.Point(20, 28);
            this.ConnectedLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.ConnectedLbl.Name = "ConnectedLbl";
            this.ConnectedLbl.Size = new System.Drawing.Size(209, 39);
            this.ConnectedLbl.TabIndex = 4;
            this.ConnectedLbl.Text = "Disconnected";
            this.ConnectedLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ConnectedLbl);
            this.groupBox1.Controls.Add(this.portCombo);
            this.groupBox1.Controls.Add(this.connectBtn);
            this.groupBox1.Location = new System.Drawing.Point(13, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(254, 152);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Serial Port";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 200;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // EjectBtn
            // 
            this.EjectBtn.Location = new System.Drawing.Point(697, 51);
            this.EjectBtn.Margin = new System.Windows.Forms.Padding(4);
            this.EjectBtn.Name = "EjectBtn";
            this.EjectBtn.Size = new System.Drawing.Size(100, 28);
            this.EjectBtn.TabIndex = 8;
            this.EjectBtn.Text = "Eject";
            this.EjectBtn.UseVisualStyleBackColor = true;
            this.EjectBtn.Click += new System.EventHandler(this.EjectBtn_Click);
            // 
            // ReadBtn
            // 
            this.ReadBtn.Location = new System.Drawing.Point(697, 107);
            this.ReadBtn.Margin = new System.Windows.Forms.Padding(4);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(100, 28);
            this.ReadBtn.TabIndex = 10;
            this.ReadBtn.Text = "Read Card";
            this.ReadBtn.UseVisualStyleBackColor = true;
            this.ReadBtn.Click += new System.EventHandler(this.ReadBtn_Click);
            // 
            // cardInfoControl
            // 
            this.cardInfoControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardInfoControl.Location = new System.Drawing.Point(290, 13);
            this.cardInfoControl.Margin = new System.Windows.Forms.Padding(4);
            this.cardInfoControl.Name = "cardInfoControl";
            this.cardInfoControl.Size = new System.Drawing.Size(315, 316);
            this.cardInfoControl.TabIndex = 9;
            // 
            // CreditBtn
            // 
            this.CreditBtn.Location = new System.Drawing.Point(697, 163);
            this.CreditBtn.Margin = new System.Windows.Forms.Padding(4);
            this.CreditBtn.Name = "CreditBtn";
            this.CreditBtn.Size = new System.Drawing.Size(100, 28);
            this.CreditBtn.TabIndex = 11;
            this.CreditBtn.Text = "Credit $5";
            this.CreditBtn.UseVisualStyleBackColor = true;
            this.CreditBtn.Click += new System.EventHandler(this.CreditBtn_Click);
            // 
            // CTSSim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 541);
            this.Controls.Add(this.CreditBtn);
            this.Controls.Add(this.ReadBtn);
            this.Controls.Add(this.cardInfoControl);
            this.Controls.Add(this.EjectBtn);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.MessageBox);
            this.Name = "CTSSim";
            this.Text = "CTSSim";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MessageBox;
        private System.Windows.Forms.ComboBox portCombo;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.Label ConnectedLbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button EjectBtn;
        private CardInfoControl cardInfoControl;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Button CreditBtn;
    }
}

