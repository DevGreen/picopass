﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.IO.Ports;


using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public partial class CTSSim : Form
    {

        CTSReaderProtocol ctsReader = null;
 

        public CTSSim()
        {
            InitializeComponent();

            this.portCombo.BeginUpdate();
            this.portCombo.Items.Clear();

            string[] ports = SerialPort.GetPortNames();

            Array.Sort(ports);

            this.portCombo.Items.AddRange(ports);
            this.portCombo.SelectedIndex = 0;
            this.portCombo.EndUpdate();
        }

        private void DisplayMsg(string fmt, params object[] parms)
        {
            this.MessageBox.Text += string.Format(fmt, parms) + "\r\n";
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader == null)
                {
                    string portName = this.portCombo.SelectedItem as string;

                    this.ctsReader = new CTSReaderProtocol(portName);
                    this.ctsReader.Open();
                    this.DisplayMsg("Connected to Port: {0}", portName);

                    this.connectBtn.Text = "Disconnect";

                    this.ConnectedLbl.Text = "Connected";
                    this.ConnectedLbl.BackColor = Color.Green;

                }
                else
                {
                    this.ctsReader.Close();
                    this.ctsReader = null;
                    this.connectBtn.Text = "Connect";

                    this.DisplayMsg("Disconnected");

                    this.ConnectedLbl.Text = "Disconnected";
                    this.ConnectedLbl.BackColor = Color.Red;

                }
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
        }

        private void InsertedBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader.IsCardInserted())
                    this.DisplayMsg("Card Inserted");
                else
                    this.DisplayMsg("Card Not Inserted");
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader != null)
                {

                    if (this.ctsReader.IsCardInserted())
                    {
                        this.cardInfoControl.SetCardPresent(true);
                    }
                    else
                    {
                        this.cardInfoControl.SetCardPresent(false);
                    }
                }
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
        }

        private void EjectBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader != null)
                {
                    this.ctsReader.Eject();
                    this.cardInfoControl.Clear();
                }

            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }

        }

        private void ReadBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader != null)
                {
                    FlashCardInfo cardInfo = new FlashCardInfo();

                    cardInfo.CustomerID = this.ctsReader.ReadCustomerID();

                    cardInfo.LocationID = this.ctsReader.ReadLocationID();

                    cardInfo.SerialNumber = this.ctsReader.ReadSerialNumber();

                    cardInfo.PurseValue = this.ctsReader.ReadPurse();

                    this.cardInfoControl.SetCard(cardInfo);

                }

            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }

        }

        private void CreditBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ctsReader != null)
                {

                    this.ctsReader.CreditPurse(500);

                }

            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
        }
    }
}
