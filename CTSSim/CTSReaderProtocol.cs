﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

using System.IO.Ports;
using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public class CTSReaderProtocol
    {
        SerialASCIIProtocol sp;
        SerialPort port;


        public CTSReaderProtocol(string portName)
        {
            this.port = new SerialPort(portName, 4800, Parity.None, 8, StopBits.Two);
            this.sp = new SerialASCIIProtocol(this.port);
        }

        public void Open()
        {
            this.port.Open();
        }

        public void Close()
        {
            this.port.Close();
        }


        private string SendCommand(string cmdCode)
        {
            if (!sp.SendCommand(cmdCode, null))
                throw new Exception(string.Format("CTSProtocol.SendCommand[{0}]: No Reply from Reader.Did not receive Ack.", cmdCode));

            string replyCmd;
            string[] replyParms;

            if (sp.ReadResponse(out replyCmd, out replyParms))
            {
                if (replyCmd == (cmdCode + "P"))
                {
                    if (replyParms == null || replyParms.Length==0)
                        return string.Empty;
                    else
                        return replyParms[0];
                }
                else if (replyCmd == cmdCode + "F")
                    return null;
                else
                    throw new Exception(string.Format("CTSProtocol.SendCommand[{0}]: Unexpected Reply: {1} ", cmdCode, replyCmd));
            }
            else
                throw new Exception(string.Format("CTSProtocol.SendCommand[{0}]:: No reply from Reader",cmdCode));             
        }


        public bool IsCardInserted()
        {
            return this.SendCommand("I") != null;
        }

        public void Eject()
        {
            this.SendCommand("E");
        }


        public string ReadLocationID()
        {
            return this.Param2String(this.SendCommand("L"));
        }

        public ushort ReadCustomerID()
        {
            return this.Param2UShort(this.SendCommand("C"));
        }

        public byte[] ReadSerialNumber()
        {
            return this.Param2Bytes(this.SendCommand("N"));
        }

        public ushort ReadPurse()
        {
            return this.Param2UShort(this.SendCommand("R"));
        }

        public bool CreditPurse(ushort creditAmount)
        {
            string creditString = string.Format("{0:X4}", creditAmount);

            if (!sp.SendCommand("W", new string[] { creditString }))
                throw new Exception("CTSProtocol.CreditPurse:  Error sending Command");



            string replyCmd;
            string[] replyParms;

            if (sp.ReadResponse(out replyCmd, out replyParms))
            {
                if (replyCmd == "WP")
                    return true;
                else if (replyCmd == "WF")
                    return false;
                else
                    throw new Exception(string.Format("CTSProtocol.CreditPurse: Unexpected Reply: {0} ", replyCmd));
            }
            else
                throw new Exception("CTSProtocol.CreditPurse: No reply from CTS");             

        }

        private byte[] Param2Bytes(string ctsParam)
        {
            int byteLen = ctsParam.Length / 2;

            byte[] rval = new byte[byteLen];

            for (int i = 0; i < byteLen; i++)
            {
                string hexString = ctsParam.Substring(i * 2, 2);

                byte bVal;
                if (byte.TryParse(hexString, System.Globalization.NumberStyles.HexNumber, NumberFormatInfo.InvariantInfo, out bVal))
                    rval[i] = bVal;
                else
                    throw new Exception("CTSProtocol.Convert2Bytes: Unexpected hex Value: " + hexString);

            }
            return rval;
        }


        private string Param2String(string ctsParam)
        {
            return Encoding.ASCII.GetString(this.Param2Bytes(ctsParam));
        }
        private ushort Param2UShort(string ctsParam)
        {
            byte[] bytes = this.Param2Bytes(ctsParam);
            return BitConverter.ToUInt16(new byte[] { bytes[1], bytes[0]},0);
        }


    }
}
