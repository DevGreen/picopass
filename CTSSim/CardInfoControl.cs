﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public partial class CardInfoControl : UserControl
    {
        public CardInfoControl()
        {
            InitializeComponent();
        }

        private byte[] serialNumber;

        public byte[] SerialNumber
        {
            get
            {
                return serialNumber;
            }
        }

        public void SetCardPresent(ReadResult readResult)
        {
            switch (readResult)
            {
                case ReadResult.InvalidCard:
                case ReadResult.ReadError:
                case ReadResult.Success:
                    SetCardPresent(true);
                    break;
                case ReadResult.NoCard:
                default:
                    SetCardPresent(false);
                    break;
            }
        }

        public void SetCardPresent(VendResult vendResult)
        {
            switch (vendResult)
            {
                case VendResult.InsufficientFunds:
                case VendResult.InvalidCard:
                case VendResult.ReadError:
                case VendResult.Success:
                case VendResult.WriteError:
                case VendResult.WrongCard:
                    SetCardPresent(true);
                    break;
                default:
                case VendResult.NoCard:
                    SetCardPresent(false);
                    break;
            }
        }


        public void SetCardPresent(AddValueResult addResult)
        {
            switch (addResult)
            {
                case AddValueResult.PurseFull:
                case AddValueResult.RechargeExpired:
                case AddValueResult.InvalidCard:
                case AddValueResult.ReadError:
                case AddValueResult.Success:
                case AddValueResult.WriteError:
                case AddValueResult.WrongCard:
                    SetCardPresent(true);
                    break;
                default:
                case AddValueResult.NoCard:
                    SetCardPresent(false);
                    break;
            }
        }


        public void SetCardPresent(bool cardPresent)
        {
            if (cardPresent)
            {
                this.cardLbl.BackColor = Color.Green;
                this.cardLbl.Text = "Card Present";
            }
            else
            {
                this.cardLbl.BackColor = Color.Red;
                this.cardLbl.Text = "Card Not Present";
            }


        }

        public bool HasCard
        {
            get
            {
                return serialNumber!=null;
            }
        }

        public void Clear()
        {
            this.serialLbl.Text = string.Empty;
            this.custLbl.Text = string.Empty;
            this.locLbl.Text = string.Empty;
            this.purseLbl.Text = string.Empty;
            serialNumber=null;
        }

        public void SetPurse(ushort purseValue)
        {
            float purseFloat = Convert.ToSingle(purseValue) / 100.0f;
            this.purseLbl.Text = string.Format("$ {0:0.00}", purseFloat);
        }

        public string GetPurseString()
        {
            return purseLbl.Text;
        }

        public string GetSerialString()
        {
            if (serialNumber == null)
                return "NULL";
            else
            {
                StringBuilder sb = new StringBuilder();

                foreach (byte b in serialNumber)
                {
                    sb.AppendFormat("{0:X2}", b);
                }
                return sb.ToString();
            }
        }
        public void SetCard(FlashCardInfo cardInfo)
        {
            this.serialLbl.Text = HexString(cardInfo.SerialNumber);

            if (cardInfo.CustomerID == 0xFF36)
                this.custLbl.Text = "GI Development";
            else
                this.custLbl.Text = string.Format("0x{0:X4}", cardInfo.CustomerID);

            this.locLbl.Text = cardInfo.LocationID;

            this.SetPurse(cardInfo.PurseValue);

            this.serialNumber = cardInfo.SerialNumber;
        }

        public void InvalidCard(byte[] serialNumber)
        {
            this.Clear();

            if(serialNumber!=null)
                this.serialLbl.Text = HexString(serialNumber);
            this.custLbl.Text = "Invalid Card";
        }

        public void WrongCard()
        {
            this.Clear();
            this.custLbl.Text = "Wrong Card";
        }

        public void PurseProblem(ushort newPurse, string msg)
        {
            this.SetPurse(newPurse);
            this.custLbl.Text =msg;
        }

        public static string HexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();


            for (int i = 0; i < bytes.Length; i++)
            {
                if (i == 0)
                    sb.AppendFormat("{0:X2}", bytes[i]);
                else
                    sb.AppendFormat(" {0:X2}", bytes[i]);
            }
            return sb.ToString();
        }

    }
}
