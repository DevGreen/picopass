﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;


namespace GI.Flash.Test
{


    public partial class ModeControl : UserControl
    {
        //private ushort priceControl_CashPrice = 0;
        //private string priceControl_PriceString = string.Empty;
        //private bool priceControl_Enabled = false;

        public ModeControl()
        {
            InitializeComponent();

            this.portCombo.BeginUpdate();
            this.portCombo.Items.Clear();

            string[] ports = SerialPort.GetPortNames();

            Array.Sort(ports);


            this.portCombo.Items.AddRange(ports);
            this.portCombo.SelectedIndex = 0;
            this.portCombo.EndUpdate();
            
            this.valueCombo.BeginUpdate();
            this.valueCombo.Items.Clear();
            this.valueCombo.Items.Add(new CashPrice(0));
            this.valueCombo.Items.Add(new CashPrice(300));
            this.valueCombo.Items.Add(new CashPrice(1000));
            this.valueCombo.Items.Add(new CashPrice(2000));
            this.valueCombo.Items.Add(new CashPrice(3000));
            this.valueCombo.Items.Add(new CashPrice(4000));
            this.valueCombo.Items.Add(new CashPrice(5000));
            this.valueCombo.Items.Add(new CashPrice(10000));
            this.valueCombo.Items.Add(new CashPrice(50000));

            this.valueCombo.SelectedIndex = 0;
            this.valueCombo.EndUpdate();

            // this.pmrvControl.Value = 0;
        }

        public void SetHasCard(bool hasCard)
        {
            this.hasCard = hasCard;

            if (mode == Mode.Reading)
            {
                this.priceControl.Enabled = hasCard;
                this.vendBtn.Enabled = hasCard;
                this.addValueBtn.Enabled = hasCard;
            }
        }

        private ushort pmrvValue;

        public void SetPmrvValue(ushort pmrvValue)
        {
            this.pmrvControl.Value = pmrvValue * (ushort)25;
        }

        public ushort GetPmrvValue()
        {
            return ((ushort)(this.pmrvControl.Value / 25));
        }

        public event Action<VendInfo> StartVend;
        public event Action CancelVend;



        private bool hasCard = false;

        private Mode mode;

        public Mode Mode
        {
            get { return mode; }
        }

        public void SetMode(Mode mode)
        {
            this.mode = mode;
            switch (mode)
            {
                default:
                case Mode.Disconnected:
                    this.modeLbl.Text = "Disconnected";
                    this.modeLbl.BackColor = Color.Red;
                    this.vendBtn.Text = "Vend";
                    this.vendBtn.Enabled = false;

                    this.addValueBtn.Text = "Add Value";
                    this.addValueBtn.Enabled = false;
                    this.valueCombo.Enabled = false;
                    this.connectBtn.Text = "Connect";

                    this.priceControl.Enabled = false;
                    break;

                case Mode.Reading:
                    this.modeLbl.Text = "Reading";
                    this.modeLbl.BackColor = Color.Green;
                    this.vendBtn.Text = "Vend";
                    this.vendBtn.Enabled = hasCard;

                    this.addValueBtn.Text = "Add Value";
                    this.addValueBtn.Enabled = hasCard;

                    this.connectBtn.Text = "Disconnect";
                    this.valueCombo.Enabled = true;
                    this.priceControl.Enabled = true;

                    break;

                case Mode.Vending:
                    this.modeLbl.Text = "Vending " + this.priceControl.VendInfo.ToString();
                    this.modeLbl.BackColor = Color.Yellow;

                    this.vendBtn.Text = "Cancel Vend";
                    this.vendBtn.Enabled = true;
                    this.priceControl.Enabled = false;

                    this.addValueBtn.Enabled = false;

                    this.connectBtn.Text = "Disconnect";

                    break;

                case Mode.AddingValue:
                    this.modeLbl.Text = "Adding Value " + this.valueCombo.SelectedItem.ToString();
                    this.modeLbl.BackColor = Color.Yellow;
                    this.addValueBtn.Text = "Cancel Add Value";
                    this.addValueBtn.Enabled = true;
                    this.valueCombo.Enabled = false;
                    this.priceControl.Enabled = false;

                    this.vendBtn.Enabled = false;

                    this.connectBtn.Text = "Disconnect";

                    break;

            }
        }


        private void vendBtn_Click(object sender, EventArgs e)
        {
            if (mode == Mode.Vending)
            {
                if (this.CancelVend != null)
                    this.CancelVend();
            }
            else
            {
                if (this.StartVend != null)
                {
                    this.StartVend(this.priceControl.VendInfo);
                }
            }
        }


        public event Action<ushort> StartAddValue;
        public event Action CancelAddValue;


        private void addValueBtn_Click(object sender, EventArgs e)
        {
            if (mode == Mode.AddingValue)
            {
                if (this.CancelAddValue != null)
                    this.CancelAddValue();
            }
            else
            {
                if (this.StartAddValue != null)
                {
                    CashPrice price = this.valueCombo.SelectedItem as CashPrice;

                    if (price == null)
                        throw new Exception("ModeControl.addValueBtn_Click: Value not selected");

                    this.StartAddValue(price.Price);
                }
            }
        }

        public event Action<string> Connect;
        public event Action Disconnect;

        private void connectBtn_Click(object sender, EventArgs e)
        {
            if (this.mode == Mode.Disconnected)
            {

                if (this.Connect != null)
                {
                    string portName = this.portCombo.SelectedItem as string;

                    this.Connect(portName);
                }
            }
            else
            {
                if (this.Disconnect != null)
                {
                    this.Disconnect();
                }
            }
        }

        private void pmrvControl_ValueChanged(object sender, EventArgs e)
        {
            pmrvValue = (ushort)(this.pmrvControl.Value / 25);
            
        }

    }
    public enum Mode { Disconnected, Reading, Vending, AddingValue }



    public delegate void Action();
}
