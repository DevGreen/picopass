﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;


using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public class FlashCard_CTS:IFlashCard
    {
        FlashCTSProtocol reader;
        SerialPort port;

        public FlashCard_CTS(string portName)
        {
            this.port = new SerialPort(portName, 9600);
            this.reader = new FlashCTSProtocol(port);
        }


        #region IFlashCard Members

        public ReadResult ReadCard(out FlashCardInfo cardInfo)
        {

            reader.SendReadCommand(50);

            ReadResult result;

            if(!reader.GetReadResponse(out result, out cardInfo))
                return ReadResult.ReadError;
            else
                return result;
        }

        public VendResult VendCard(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue)
        {
            throw new NotImplementedException();
        }

        public AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, out ushort newPurseValue)
        {
            reader.SendAddValueCommand(50, targetSerialNumber, creditAmount);

            AddValueResult result;

            if(!reader.GetAddValueResponse(out result,out newPurseValue))
                return AddValueResult.ReadError;
            else
                return result;
        }

        public bool ImplementsVendCard
        {
            get { return false; }
        }

        public bool ImplementsAddValue
        {
            get { return true; }
        }

        public void Disconnect()
        {
            this.port.Close();
        }

        public void Connect()
        {
            this.port.Open();           
        }

        #endregion

        #region IFlashCard Members


        public VendResult VendCardCash(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue, ushort pointsEarned)
        {
            throw new NotImplementedException();
        }

        public VendResult VendCardPoints(byte[] targetSerialNumber, ushort pointsDebitAmount, out ushort newPointsValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public ReadResult ReadAndFixCard(out FlashCardInfo cardInfo)
        {
            throw new NotImplementedException();
        }

        public AddValueResult AddValueWithBackup(byte[] targetSerialNumber, ushort creditAmount, out ushort newPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public ReadResult ReadAndFixCard(out FlashCardInfo cardInfo, ushort signWithCustomerID, string signWithLocationID, ushort signWithPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public AddValueResult AddValueWithBackup(byte[] targetSerialNumber, ushort creditAmount, ushort pinMateRedeemAmount, out ushort newPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public ReadResult ReadCardCTS(out FlashCardInfo cardInfo, ushort signWithCustomerID, string signWithLocationID, ushort signWithPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, ushort pinMateRedeemAmount, out ushort newPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
