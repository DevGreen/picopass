﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace GI.Flash.Test
{
    public partial class VendPriceControl : UserControl
    {
        public VendPriceControl()
        {
            InitializeComponent();

            this.cashPriceCombo.BeginUpdate();
            this.cashPriceCombo.Items.Clear();
            this.cashPriceCombo.Items.Add(new CashPrice(25));
            this.cashPriceCombo.Items.Add(new CashPrice(50));
            this.cashPriceCombo.Items.Add(new CashPrice(75));
            this.cashPriceCombo.Items.Add(new CashPrice(100));
            this.cashPriceCombo.Items.Add(new CashPrice(150));
            this.cashPriceCombo.Items.Add(new CashPrice(200));
            this.cashPriceCombo.Items.Add(new CashPrice(250));
            this.cashPriceCombo.Items.Add(new CashPrice(275));
            this.cashPriceCombo.Items.Add(new CashPrice(500));
            this.cashPriceCombo.Items.Add(new CashPrice(1000));
            this.cashPriceCombo.Items.Add(new CashPrice(2000));
            this.cashPriceCombo.Items.Add(new CashPrice(5000));
            this.cashPriceCombo.SelectedIndex = 3;
            this.cashPriceCombo.EndUpdate();

            this.cashPriceCombo.SelectedIndex = 3;

            this.cashRadio.Checked = true;
            this.cashRadio_CheckedChanged(this, EventArgs.Empty);

        }



       

        public VendInfo VendInfo
        {
            get
            {
                if (cashRadio.Checked)
                    return new VendInfo(((CashPrice)this.cashPriceCombo.SelectedItem).Price, Convert.ToUInt16(this.pointsEarnedSpin.Value));
                else
                    return new VendInfo(Convert.ToUInt16(this.pointsPriceSpin.Value));

            }
        }

        public bool Enabled
        {
            get
            {
                return this.typeGroup.Enabled;
            }

            set
            {
                this.typeGroup.Enabled = value;

                if (value)
                    this.cashRadio_CheckedChanged(this, EventArgs.Empty);
                else
                {
                    this.cashGroup.Enabled = false;
                    this.pointsGroup.Enabled = false;
                }
            }
        }

        private void cashRadio_CheckedChanged(object sender, EventArgs e)
        {
            this.cashGroup.Enabled = this.cashRadio.Checked;
            this.pointsGroup.Enabled = this.pointsRadio.Checked;
        }



    }


}
