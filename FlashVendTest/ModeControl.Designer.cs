﻿namespace GI.Flash.Test
{
    partial class ModeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.modeLbl = new System.Windows.Forms.Label();
            this.vendBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pmrvControl = new System.Windows.Forms.NumericUpDown();
            this.priceControl = new GI.Flash.Test.VendPriceControl();
            this.valueCombo = new System.Windows.Forms.ComboBox();
            this.addValueBtn = new System.Windows.Forms.Button();
            this.portCombo = new System.Windows.Forms.ComboBox();
            this.connectBtn = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmrvControl)).BeginInit();
            this.SuspendLayout();
            // 
            // modeLbl
            // 
            this.modeLbl.BackColor = System.Drawing.Color.Red;
            this.modeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeLbl.Location = new System.Drawing.Point(19, 29);
            this.modeLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.modeLbl.Name = "modeLbl";
            this.modeLbl.Size = new System.Drawing.Size(209, 39);
            this.modeLbl.TabIndex = 1;
            this.modeLbl.Text = "Disconnected";
            this.modeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // vendBtn
            // 
            this.vendBtn.Enabled = false;
            this.vendBtn.Location = new System.Drawing.Point(66, 359);
            this.vendBtn.Margin = new System.Windows.Forms.Padding(4);
            this.vendBtn.Name = "vendBtn";
            this.vendBtn.Size = new System.Drawing.Size(100, 28);
            this.vendBtn.TabIndex = 0;
            this.vendBtn.Text = "Vend";
            this.vendBtn.UseVisualStyleBackColor = true;
            this.vendBtn.Click += new System.EventHandler(this.vendBtn_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pmrvControl);
            this.groupBox1.Controls.Add(this.priceControl);
            this.groupBox1.Controls.Add(this.valueCombo);
            this.groupBox1.Controls.Add(this.addValueBtn);
            this.groupBox1.Controls.Add(this.portCombo);
            this.groupBox1.Controls.Add(this.connectBtn);
            this.groupBox1.Controls.Add(this.modeLbl);
            this.groupBox1.Controls.Add(this.vendBtn);
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(250, 464);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PicoRead Coupler";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(142, 436);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 16);
            this.label1.TabIndex = 7;
            this.label1.Text = "Set PMRV";
            // 
            // pmrvControl
            // 
            this.pmrvControl.Increment = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.pmrvControl.Location = new System.Drawing.Point(27, 430);
            this.pmrvControl.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.pmrvControl.Name = "pmrvControl";
            this.pmrvControl.Size = new System.Drawing.Size(92, 22);
            this.pmrvControl.TabIndex = 6;
            this.pmrvControl.ValueChanged += new System.EventHandler(this.pmrvControl_ValueChanged);
            // 
            // priceControl
            // 
            this.priceControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.priceControl.Location = new System.Drawing.Point(8, 116);
            this.priceControl.Margin = new System.Windows.Forms.Padding(4);
            this.priceControl.Name = "priceControl";
            this.priceControl.Size = new System.Drawing.Size(230, 235);
            this.priceControl.TabIndex = 5;
            // 
            // valueCombo
            // 
            this.valueCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.valueCombo.FormattingEnabled = true;
            this.valueCombo.Items.AddRange(new object[] {
            "$ 1.50",
            "$ 1.75",
            "$ 2.00",
            "$ 2.25",
            "$ 5.00"});
            this.valueCombo.Location = new System.Drawing.Point(27, 399);
            this.valueCombo.Name = "valueCombo";
            this.valueCombo.Size = new System.Drawing.Size(92, 24);
            this.valueCombo.TabIndex = 4;
            // 
            // addValueBtn
            // 
            this.addValueBtn.Enabled = false;
            this.addValueBtn.Location = new System.Drawing.Point(128, 395);
            this.addValueBtn.Margin = new System.Windows.Forms.Padding(4);
            this.addValueBtn.Name = "addValueBtn";
            this.addValueBtn.Size = new System.Drawing.Size(100, 28);
            this.addValueBtn.TabIndex = 3;
            this.addValueBtn.Text = "Add Value";
            this.addValueBtn.UseVisualStyleBackColor = true;
            this.addValueBtn.Click += new System.EventHandler(this.addValueBtn_Click);
            // 
            // portCombo
            // 
            this.portCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portCombo.FormattingEnabled = true;
            this.portCombo.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3"});
            this.portCombo.Location = new System.Drawing.Point(23, 81);
            this.portCombo.Name = "portCombo";
            this.portCombo.Size = new System.Drawing.Size(92, 24);
            this.portCombo.TabIndex = 2;
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(128, 78);
            this.connectBtn.Margin = new System.Windows.Forms.Padding(4);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(100, 28);
            this.connectBtn.TabIndex = 0;
            this.connectBtn.Text = "Connect";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // ModeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ModeControl";
            this.Size = new System.Drawing.Size(262, 476);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmrvControl)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label modeLbl;
        private System.Windows.Forms.Button vendBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox portCombo;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.ComboBox valueCombo;
        private System.Windows.Forms.Button addValueBtn;
        private VendPriceControl priceControl;
        private System.Windows.Forms.NumericUpDown pmrvControl;
        private System.Windows.Forms.Label label1;
    }
}
