﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Flash.Test
{

    public class CashPrice
    {
        public ushort Price;

        public CashPrice(ushort price)
        {
            this.Price = price;
        }
        public override string ToString()
        {
            float displayPrice = Convert.ToSingle(this.Price) / 100f;
            return string.Format("$ {0:0.00}", displayPrice);
        }
    }

    public enum VendType { Cash = 1, Points = 2 };

    public class VendInfo
    {
        private VendType vendType;
        private ushort pointsEarned;
        private ushort cashPrice;
        private ushort pointsPrice;

        public VendInfo(ushort cashPrice, ushort pointsEarned)
        {
            this.vendType = VendType.Cash;
            this.cashPrice = cashPrice;
            this.pointsEarned = pointsEarned;
        }

        public VendInfo(ushort pointsPrice)
        {
            this.vendType = VendType.Points;
            this.pointsPrice = pointsPrice;
            this.cashPrice = 0;
            this.pointsEarned = 0;

        }

        public VendType VendType
        {
            get { return this.vendType; }
        }

        public ushort CashPrice
        {
            get { return this.cashPrice; }
        }

        public ushort PointsPrice
        {
            get { return this.pointsPrice; }
        }

        public ushort PointsEarned
        {
            get { return this.pointsEarned; }
        }

        public override string ToString()
        {
            if (this.VendType == VendType.Cash)
            {
                CashPrice cp = new CashPrice(this.cashPrice);
                return string.Format("Cash: {0}", cp.ToString());
            }
            else
                return string.Format("Points:{0}", this.pointsPrice);
        }

    }
}