﻿namespace GI.Flash.Test
{
    partial class FlashVendTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MessageBox = new System.Windows.Forms.TextBox();
            this.readTimer = new System.Windows.Forms.Timer(this.components);
            this.FixCheck = new System.Windows.Forms.CheckBox();
            this.cardInfoControl = new GI.Flash.Test.CardInfoControl();
            this.modeControl = new GI.Flash.Test.ModeControl();
            this.SuspendLayout();
            // 
            // MessageBox
            // 
            this.MessageBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageBox.Location = new System.Drawing.Point(22, 544);
            this.MessageBox.Margin = new System.Windows.Forms.Padding(4);
            this.MessageBox.Multiline = true;
            this.MessageBox.Name = "MessageBox";
            this.MessageBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.MessageBox.Size = new System.Drawing.Size(622, 158);
            this.MessageBox.TabIndex = 1;
            // 
            // readTimer
            // 
            this.readTimer.Enabled = true;
            this.readTimer.Interval = 250;
            this.readTimer.Tick += new System.EventHandler(this.readTimer_Tick);
            // 
            // FixCheck
            // 
            this.FixCheck.AutoSize = true;
            this.FixCheck.Location = new System.Drawing.Point(258, 6);
            this.FixCheck.Name = "FixCheck";
            this.FixCheck.Size = new System.Drawing.Size(100, 20);
            this.FixCheck.TabIndex = 5;
            this.FixCheck.Text = "Fix and Sign";
            this.FixCheck.UseVisualStyleBackColor = true;
            // 
            // cardInfoControl
            // 
            this.cardInfoControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardInfoControl.Location = new System.Drawing.Point(329, 22);
            this.cardInfoControl.Margin = new System.Windows.Forms.Padding(4);
            this.cardInfoControl.Name = "cardInfoControl";
            this.cardInfoControl.Size = new System.Drawing.Size(315, 514);
            this.cardInfoControl.TabIndex = 4;
            // 
            // modeControl
            // 
            this.modeControl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeControl.Location = new System.Drawing.Point(22, 22);
            this.modeControl.Margin = new System.Windows.Forms.Padding(4);
            this.modeControl.Name = "modeControl";
            this.modeControl.Size = new System.Drawing.Size(262, 495);
            this.modeControl.TabIndex = 3;
            this.modeControl.Load += new System.EventHandler(this.modeControl_Load);
            this.modeControl.Disconnect += new GI.Flash.Test.Action(this.modeControl_Disconnect);
            this.modeControl.StartVend += new System.Action<GI.Flash.Test.VendInfo>(this.modeControl_StartVend);
            this.modeControl.CancelAddValue += new GI.Flash.Test.Action(this.modeControl_CancelAddValue);
            this.modeControl.Connect += new System.Action<string>(this.modeControl_Connect);
            this.modeControl.CancelVend += new GI.Flash.Test.Action(this.modeControl_CancelVend);
            this.modeControl.StartAddValue += new System.Action<ushort>(this.modeControl_StartAddValue);
            // 
            // FlashVendTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 723);
            this.Controls.Add(this.FixCheck);
            this.Controls.Add(this.cardInfoControl);
            this.Controls.Add(this.modeControl);
            this.Controls.Add(this.MessageBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FlashVendTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MessageBox;
        private System.Windows.Forms.Timer readTimer;
        private GI.Flash.Test.CardInfoControl cardInfoControl;
        private GI.Flash.Test.ModeControl modeControl;
        private System.Windows.Forms.CheckBox FixCheck;
    }
}

