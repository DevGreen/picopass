﻿namespace GI.Flash.Test
{
    partial class CardInfoControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.purseLbl = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.serialLbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.custLbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.locLbl = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.pmrvLbl = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pointsLbl = new System.Windows.Forms.Label();
            this.cardNumLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cardLbl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // purseLbl
            // 
            this.purseLbl.BackColor = System.Drawing.Color.Silver;
            this.purseLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.purseLbl.Location = new System.Drawing.Point(9, 325);
            this.purseLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.purseLbl.Name = "purseLbl";
            this.purseLbl.Size = new System.Drawing.Size(285, 32);
            this.purseLbl.TabIndex = 9;
            this.purseLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Serial Number:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 309);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Purse:";
            // 
            // serialLbl
            // 
            this.serialLbl.BackColor = System.Drawing.Color.Silver;
            this.serialLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialLbl.Location = new System.Drawing.Point(10, 91);
            this.serialLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.serialLbl.Name = "serialLbl";
            this.serialLbl.Size = new System.Drawing.Size(285, 32);
            this.serialLbl.TabIndex = 8;
            this.serialLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 190);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(84, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Customer ID:";
            // 
            // custLbl
            // 
            this.custLbl.BackColor = System.Drawing.Color.Silver;
            this.custLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.custLbl.Location = new System.Drawing.Point(9, 206);
            this.custLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.custLbl.Name = "custLbl";
            this.custLbl.Size = new System.Drawing.Size(285, 32);
            this.custLbl.TabIndex = 11;
            this.custLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 251);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 16);
            this.label7.TabIndex = 12;
            this.label7.Text = "Location ID:";
            // 
            // locLbl
            // 
            this.locLbl.BackColor = System.Drawing.Color.Silver;
            this.locLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.locLbl.Location = new System.Drawing.Point(9, 267);
            this.locLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.locLbl.Name = "locLbl";
            this.locLbl.Size = new System.Drawing.Size(285, 32);
            this.locLbl.TabIndex = 13;
            this.locLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.pmrvLbl);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.pointsLbl);
            this.groupBox1.Controls.Add(this.cardNumLbl);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cardLbl);
            this.groupBox1.Controls.Add(this.locLbl);
            this.groupBox1.Controls.Add(this.serialLbl);
            this.groupBox1.Controls.Add(this.purseLbl);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.custLbl);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 493);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contactless Card";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 427);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "PMRV";
            // 
            // pmrvLbl
            // 
            this.pmrvLbl.BackColor = System.Drawing.Color.Silver;
            this.pmrvLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pmrvLbl.Location = new System.Drawing.Point(5, 443);
            this.pmrvLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.pmrvLbl.Name = "pmrvLbl";
            this.pmrvLbl.Size = new System.Drawing.Size(285, 32);
            this.pmrvLbl.TabIndex = 18;
            this.pmrvLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 371);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 16);
            this.label6.TabIndex = 17;
            this.label6.Text = "Loyalty Points:";
            // 
            // pointsLbl
            // 
            this.pointsLbl.BackColor = System.Drawing.Color.Silver;
            this.pointsLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pointsLbl.Location = new System.Drawing.Point(8, 387);
            this.pointsLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.pointsLbl.Name = "pointsLbl";
            this.pointsLbl.Size = new System.Drawing.Size(285, 32);
            this.pointsLbl.TabIndex = 16;
            this.pointsLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cardNumLbl
            // 
            this.cardNumLbl.BackColor = System.Drawing.Color.Silver;
            this.cardNumLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardNumLbl.Location = new System.Drawing.Point(8, 149);
            this.cardNumLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cardNumLbl.Name = "cardNumLbl";
            this.cardNumLbl.Size = new System.Drawing.Size(285, 32);
            this.cardNumLbl.TabIndex = 15;
            this.cardNumLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 133);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 14;
            this.label1.Text = "Card Number:";
            // 
            // cardLbl
            // 
            this.cardLbl.BackColor = System.Drawing.Color.Red;
            this.cardLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardLbl.Location = new System.Drawing.Point(10, 27);
            this.cardLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cardLbl.Name = "cardLbl";
            this.cardLbl.Size = new System.Drawing.Size(285, 39);
            this.cardLbl.TabIndex = 3;
            this.cardLbl.Text = "Card Not Present";
            this.cardLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CardInfoControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CardInfoControl";
            this.Size = new System.Drawing.Size(315, 499);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label purseLbl;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label serialLbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label custLbl;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label locLbl;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label cardLbl;
        private System.Windows.Forms.Label cardNumLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label pointsLbl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label pmrvLbl;
    }
}
