﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public class FlashCard_C:IFlashCard
    {
        private Port comPort;

        public FlashCard_C(string comPortName)
        {
            comPort=(Port)Enum.Parse(typeof(Port), comPortName);
        }
        #region IFlashCard Members

        public ReadResult ReadCard(out FlashCardInfo cardInfo)
        {
            cardInfo =null;

            CardData cardData = new CardData();
            FlashCode rc = PP_ReadCard(ref cardData);



            switch (rc)
            {
                case FlashCode.Success:
                    cardInfo = new FlashCardInfo();
                    cardInfo.CustomerID = cardData.customerID;
                    cardInfo.LocationID = Encoding.ASCII.GetString(cardData.locationID).Trim();
                    cardInfo.SerialNumber = cardData.serialNumber;
                    cardInfo.PurseValue = cardData.purseValue;
                    cardInfo.CardNumber = cardData.cardNumber;
                    return ReadResult.Success;
                case FlashCode.NoCardPresent:
                    return ReadResult.NoCard;

                case FlashCode.InvalidCard:
                    cardInfo = new FlashCardInfo();
                    cardInfo.SerialNumber = cardData.serialNumber;
                    return ReadResult.InvalidCard;

                default:
                case FlashCode.ReadError:
                    return ReadResult.ReadError;
            }
        }

        public VendResult VendCard(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue)
        {
            CardData cardData = new CardData();
            cardData.serialNumber = targetSerialNumber;

            FlashCode rc = PP_DebitCard(ref cardData, debitAmount, out newPurseValue);

            switch (rc)
            {
                case FlashCode.Success:
                    return VendResult.Success;
                case FlashCode.NoCardPresent:
                    return VendResult.NoCard;
                case FlashCode.WrongCard:
                    return VendResult.WrongCard;
                case FlashCode.InsufficientFunds:
                    return VendResult.InsufficientFunds;
                default:
                case FlashCode.ReadError:
                    return VendResult.ReadError;
                case FlashCode.WriteError:
                    return VendResult.WriteError;

            }
        }

        public void Disconnect()
        {
            PP_CloseUART((int)comPort);

        }

        public void Connect()
        {
            FlashCode rc=PP_InitUART((int)comPort);

            if (rc != FlashCode.Success)
                throw new Exception(string.Format("FlashCard_C.Connect: Error intializing UART: {0}. RC: {1}", comPort, rc));

            rc=PP_InitCardReader((int)comPort, null);

            if (rc != FlashCode.Success)
                throw new Exception(string.Format("FlashCard_C.Connect: Error Initializing Card Reader: {0}",rc));

        }

        #endregion


        public enum FlashCode
        {
            Success = 0,
            NoCardPresent = -1,
            InvalidCard = -2,
            WrongCard = -3,
            InsufficientFunds = -4,
            ReadError = -5,
            WriteError = -6,
            PurseFull = -7,
            RechargeExpired = -8,
        }

        public enum Port
        {
            COM1 = 4,
            COM2 = 5,
            COM3 = 6,
            COM4 = 7,
            COM5 = 8,
            COM6 = 9
        }

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_InitUART(int uart);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_CloseUART(int uart);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_InitCardReader(int uart, byte[] keys);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_ReadCard(ref CardData cardData);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_DebitCard(ref CardData cardData, ushort vendPrice, out ushort newPurseValue);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_AddValue(ref CardData cardData, ushort valueAddAmount, out ushort newPurseValue);


        [StructLayout(LayoutKind.Sequential)]
        public struct CardData
        {
            public ushort customerID;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
            public byte[] locationID;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public byte[] serialNumber;
            public ushort purseValue;
            public uint cardNumber;
        }

        #region IFlashCard Members


        public AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, out ushort newPurseValue)
        {

            CardData cardData = new CardData();
            cardData.serialNumber = targetSerialNumber;

            FlashCode rc = PP_AddValue(ref cardData, creditAmount, out newPurseValue);

            switch (rc)
            {
                case FlashCode.Success:
                    return AddValueResult.Success;
                case FlashCode.NoCardPresent:
                    return AddValueResult.NoCard;
                case FlashCode.WrongCard:
                    return AddValueResult.WrongCard;
                case FlashCode.PurseFull:
                    return AddValueResult.PurseFull;
                case FlashCode.RechargeExpired:
                    return AddValueResult.RechargeExpired;
                default:
                case FlashCode.ReadError:
                    return AddValueResult.ReadError;
                case FlashCode.WriteError:
                    return AddValueResult.WriteError;

            }

        }

        public bool ImplementsVendCard
        {
            get { return true; }
        }

        public bool ImplementsAddValue
        {
            get {return true; }
        }

        #endregion

        #region IFlashCard Members


        public VendResult VendCardCash(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue, ushort pointsEarned)
        {
            throw new NotImplementedException();
        }

        public VendResult VendCardPoints(byte[] targetSerialNumber, ushort pointsDebitAmount, out ushort newPointsValue)
        {
            throw new NotImplementedException();
        }

        #endregion

     


        #region IFlashCard Members

        ReadResult IFlashCard.ReadCard(out FlashCardInfo cardInfo)
        {
            throw new NotImplementedException();
        }


        VendResult IFlashCard.VendCardCash(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue, ushort pointsEarned)
        {
            throw new NotImplementedException();
        }

        VendResult IFlashCard.VendCardPoints(byte[] targetSerialNumber, ushort pointsDebitAmount, out ushort newPointsValue)
        {
            throw new NotImplementedException();
        }


        bool IFlashCard.ImplementsVendCard
        {
            get { throw new NotImplementedException(); }
        }

        bool IFlashCard.ImplementsAddValue
        {
            get { throw new NotImplementedException(); }
        }

        void IFlashCard.Disconnect()
        {
            throw new NotImplementedException();
        }

        void IFlashCard.Connect()
        {
            throw new NotImplementedException();
        }

        #endregion


        #region IFlashCard Members


        public ReadResult ReadCardCTS(out FlashCardInfo cardInfo, ushort signWithCustomerID, string signWithLocationID, ushort signWithPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IFlashCard Members


        public AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, ushort pinMateRedeemAmount, out ushort newPurseValue)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
