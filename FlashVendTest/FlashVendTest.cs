﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;

using GI.Flash.Pico;



namespace GI.Flash.Test
{
    public partial class FlashVendTest : Form
    {
        private IFlashCard flash = null;

        public FlashVendTest()
        {
            InitializeComponent();

            //this.vendCombo.SelectedIndex = 0;

            this.modeControl.SetMode(Mode.Disconnected);
            this.cardInfoControl.Clear();

        }

        void modeControl_Connect(string portName)
        {
            try
            {

                if (portName == null || portName == string.Empty)
                {
                    this.DisplayMsg("No Serial Port Selected");
                    return;
                }

                this.flash = new FlashCard(portName);
//                this.flash = new FlashCard_C(portName);
//                                this.flash = new FlashCard_CTS(portName);


                this.flash.Connect();

                this.modeControl.SetMode(Mode.Reading);
                this.modeControl.SetHasCard(this.cardInfoControl.HasCard);


            }

            catch (Exception expt)
            {
                this.modeControl.SetMode(Mode.Disconnected);
                this.flash = null;
                this.DisplayMsg(expt.ToString());
            }
        }

        void modeControl_Disconnect()
        {
            try
            {
                this.flash.Disconnect();
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
            finally
            {
                this.flash = null;
                this.modeControl.SetMode(Mode.Disconnected);

            }
        }

        private VendInfo vendInfo;

        private void modeControl_StartVend(VendInfo vendInfo)
        {
            try
            {
                this.vendInfo = vendInfo;
                this.modeControl.SetMode(Mode.Vending);
                this.DisplayMsg("Vend Start    : Card[{0}] Balance: Cash:{1} Points: {2}", this.cardInfoControl.GetSerialString(), this.cardInfoControl.GetPurseString(),this.cardInfoControl.PointsValue);
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
        }

        private void modeControl_CancelVend()
        {
            try
            {
                this.modeControl.SetMode(Mode.Reading);
                this.DisplayMsg("Vend Cancelled: User Cancelled.");
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }

        }

        private void DisplayMsg(string fmt, params object[] parms)
        {
            this.MessageBox.Text += string.Format(fmt, parms) + "\r\n";
        }

        private int justSawOne = 0;


        private void readTimer_Tick(object sender, EventArgs e)
        {
            if (this.flash == null)
                return;
            try
            {
                this.readTimer.Enabled = false;

                switch (this.modeControl.Mode)
                {
                    default:
                    case Mode.Disconnected:
                        break;
                    case Mode.Reading:
                        this.DoRead();
                        break;
                    case Mode.Vending:
                        this.DoVend();
                        break;
                    case Mode.AddingValue:
                        this.DoAddValue();
                        break;

                }
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());

                if (this.modeControl.Mode == Mode.Vending
                    || this.modeControl.Mode == Mode.AddingValue)
                {
                    this.modeControl.SetMode(Mode.Reading);
                }

            }
            finally
            {
                this.readTimer.Enabled = true;
            }
        }
        private static ReadResult lastReadResult = ReadResult.NoCard;

        private void DoRead()
        {
            FlashCardInfo cardInfo;

            ReadResult readResult;

            if (this.FixCheck.Checked)
//                readResult = this.flash.ReadCardCTS(out cardInfo,0xFF36,"GREENWALD",0);
                //readResult = this.flash.ReadCardCTS(out cardInfo,0xF005,"FREEDRY001",100); 
                readResult = this.flash.ReadCardCTS(out cardInfo,0xF02F,"REITHO0000",100); 
            else
                readResult = this.flash.ReadCard(out cardInfo);

            this.cardInfoControl.SetCardPresent(readResult);


//            this.DisplayMsg("Read:{0}", readResult);

            //Allow a grace period after seeing a card
            //to hide Errors that will occur when card is being removed
            //if (this.justSawOne > 0)
            //{
            //    this.justSawOne--;
            //    return;
            //}

            switch (readResult)
            {
                case ReadResult.Success:
                    this.cardInfoControl.SetCard(cardInfo);
                    this.modeControl.SetHasCard(true);
                    if ( lastReadResult.Equals(ReadResult.NoCard) && readResult.Equals(ReadResult.Success))
                    {
                        this.modeControl.SetPmrvValue(cardInfo.PinMateRedeemedValue);
                    }
                    this.justSawOne = 5;
                    break;
                case ReadResult.InvalidCard:
                    this.cardInfoControl.InvalidCard(cardInfo.SerialNumber);
                    this.modeControl.SetHasCard(false);
                    this.justSawOne = 5;
                    break;
                case ReadResult.CreditLock:
                    this.cardInfoControl.CreditLock(cardInfo);
                    this.modeControl.SetHasCard(false);
                    this.justSawOne = 5;
                    break;

                case ReadResult.Unsigned:
                    this.cardInfoControl.UnsignedCard(cardInfo.SerialNumber);
                    this.modeControl.SetHasCard(false);
                    this.justSawOne = 5;
                    break;

                case ReadResult.NoCard:
                    this.justSawOne = 0;
                    break;
                default:
                case ReadResult.ReadError:
                    break;
            }
            lastReadResult = readResult;
        }


        private void DoVend()
        {
            ushort newPurseValue=0;
            ushort newPointsValue = 0;

            VendResult vendResult;

            if(this.vendInfo.VendType==VendType.Cash)
                vendResult = this.flash.VendCardCash(cardInfoControl.SerialNumber, this.vendInfo.CashPrice, out newPurseValue, this.vendInfo.PointsEarned);
            else
                vendResult = this.flash.VendCardPoints(cardInfoControl.SerialNumber, this.vendInfo.PointsPrice, out newPointsValue);


            this.cardInfoControl.SetCardPresent(vendResult);

            switch (vendResult)
            {
                default:
                case VendResult.NoCard:
                    return;
                case VendResult.ReadError:
                    this.DisplayMsg("Read Error");
                    return;
                case VendResult.WriteError:
                    this.DisplayMsg("Write Error");
                    return;

                case VendResult.Success:

                    if (this.vendInfo.VendType == VendType.Cash)
                        this.cardInfoControl.SetPurse( newPurseValue,
                                                       (ushort)(this.cardInfoControl.PointsValue + this.vendInfo.PointsEarned), 
                                                       this.cardInfoControl.Pmrv );
                    else
                        this.cardInfoControl.SetPurse( this.cardInfoControl.PurseValue, newPointsValue, this.cardInfoControl.Pmrv );

                        this.DisplayMsg("Vend Complete : Card[{0}] Balance: Cash: {1} Points: {2}", this.cardInfoControl.GetSerialString(), this.cardInfoControl.GetPurseString(),this.cardInfoControl.PointsValue);

                    break;
                case VendResult.WrongCard:
                    this.cardInfoControl.WrongCard();
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Vend Cancelled: Wrong Card");

                    break;
                case VendResult.InvalidCard:
                    this.cardInfoControl.InvalidCard(null);
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Vend Cancelled: Invalid Card");

                    break;
                case VendResult.InsufficientFunds:

                    if(this.vendInfo.VendType==VendType.Cash)
                        this.cardInfoControl.PurseProblem(newPurseValue,this.cardInfoControl.PointsValue,"Insufficient Funds: Cash");
                    else
                        this.cardInfoControl.PurseProblem(this.cardInfoControl.PurseValue, newPointsValue, "Insufficient Funds: Points");

                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Vend Cancelled: Insufficient Funds");

                    break;
            }

            this.modeControl.SetMode(Mode.Reading);
            this.justSawOne = 5;
        }



        private void DoAddValue()
        {
            ushort newPurseValue;

//            AddValueResult addResult = this.flash.AddValue(cardInfoControl.SerialNumber, this.value2Add, out newPurseValue);
            AddValueResult addResult = this.flash.AddValue(cardInfoControl.SerialNumber, this.value2Add, modeControl.GetPmrvValue(), out newPurseValue);

            this.cardInfoControl.SetCardPresent(addResult);

            switch (addResult)
            {
                default:
                case AddValueResult.NoCard:
                    return;
                case AddValueResult.ReadError:
                    this.DisplayMsg("Read Error");
                    return;
                case AddValueResult.WriteError:
                    this.DisplayMsg("Write Error");
                    return;

                case AddValueResult.Success:
                    this.cardInfoControl.SetPurse( newPurseValue, this.cardInfoControl.PointsValue, this.cardInfoControl.Pmrv );
                    this.DisplayMsg("Add Value Complete : Card[{0}] Balance:{1}", this.cardInfoControl.GetSerialString(), this.cardInfoControl.GetPurseString());
                    break;
                case AddValueResult.WrongCard:
                    this.cardInfoControl.WrongCard();
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Add Value Cancelled: Wrong Card");

                    break;
                case AddValueResult.InvalidCard:
                    this.cardInfoControl.InvalidCard(null);
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Add Value Cancelled: Invalid Card");

                    break;
                case AddValueResult.PurseFull:
                    this.cardInfoControl.PurseProblem(newPurseValue,this.cardInfoControl.PointsValue, "Purse Full");
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Add Value Cancelled: Purse Full");
                    break;

                case AddValueResult.RechargeExpired:
                    this.cardInfoControl.PurseProblem(newPurseValue,this.cardInfoControl.PointsValue, "Recharge Expired");
                    this.modeControl.SetHasCard(false);
                    this.DisplayMsg("Add Value Cancelled: Recharge Expired");
                    break;

            }

            this.modeControl.SetMode(Mode.Reading);
            this.justSawOne = 5;
        }


        private ushort value2Add;

        private void modeControl_StartAddValue(ushort value2Add)
        {

            try
            {
                this.value2Add = value2Add;
                this.modeControl.SetMode(Mode.AddingValue);

                this.DisplayMsg("Add Value Start    : Card[{0}] Balance:{1}", this.cardInfoControl.GetSerialString(), this.cardInfoControl.GetPurseString());
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }


        }

        private void modeControl_CancelAddValue()
        {
            try
            {
                this.modeControl.SetMode(Mode.Reading);
                this.DisplayMsg("Add Value Cancelled: User Cancelled.");
            }
            catch (Exception expt)
            {
                this.DisplayMsg(expt.ToString());
            }
        }

        private void modeControl_Load(object sender, EventArgs e)
        {

        }

    }
}