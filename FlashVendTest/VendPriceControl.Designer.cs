﻿namespace GI.Flash.Test
{
    partial class VendPriceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cashRadio = new System.Windows.Forms.RadioButton();
            this.pointsRadio = new System.Windows.Forms.RadioButton();
            this.cashPriceCombo = new System.Windows.Forms.ComboBox();
            this.pointsEarnedSpin = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.pointsPriceSpin = new System.Windows.Forms.NumericUpDown();
            this.typeGroup = new System.Windows.Forms.GroupBox();
            this.cashGroup = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pointsGroup = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pointsEarnedSpin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointsPriceSpin)).BeginInit();
            this.typeGroup.SuspendLayout();
            this.cashGroup.SuspendLayout();
            this.pointsGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // cashRadio
            // 
            this.cashRadio.AutoSize = true;
            this.cashRadio.Checked = true;
            this.cashRadio.Location = new System.Drawing.Point(34, 22);
            this.cashRadio.Margin = new System.Windows.Forms.Padding(4);
            this.cashRadio.Name = "cashRadio";
            this.cashRadio.Size = new System.Drawing.Size(57, 20);
            this.cashRadio.TabIndex = 8;
            this.cashRadio.TabStop = true;
            this.cashRadio.Text = "Cash";
            this.cashRadio.UseVisualStyleBackColor = true;
            this.cashRadio.CheckedChanged += new System.EventHandler(this.cashRadio_CheckedChanged);
            // 
            // pointsRadio
            // 
            this.pointsRadio.AutoSize = true;
            this.pointsRadio.Location = new System.Drawing.Point(129, 22);
            this.pointsRadio.Margin = new System.Windows.Forms.Padding(4);
            this.pointsRadio.Name = "pointsRadio";
            this.pointsRadio.Size = new System.Drawing.Size(63, 20);
            this.pointsRadio.TabIndex = 9;
            this.pointsRadio.TabStop = true;
            this.pointsRadio.Text = "Points";
            this.pointsRadio.UseVisualStyleBackColor = true;
            // 
            // cashPriceCombo
            // 
            this.cashPriceCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cashPriceCombo.FormattingEnabled = true;
            this.cashPriceCombo.Items.AddRange(new object[] {
            "$ 1.50",
            "$ 1.75",
            "$ 2.00",
            "$ 2.25",
            "$ 5.00"});
            this.cashPriceCombo.Location = new System.Drawing.Point(108, 29);
            this.cashPriceCombo.Name = "cashPriceCombo";
            this.cashPriceCombo.Size = new System.Drawing.Size(101, 24);
            this.cashPriceCombo.TabIndex = 2;
            // 
            // pointsEarnedSpin
            // 
            this.pointsEarnedSpin.Location = new System.Drawing.Point(109, 68);
            this.pointsEarnedSpin.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pointsEarnedSpin.Name = "pointsEarnedSpin";
            this.pointsEarnedSpin.Size = new System.Drawing.Size(100, 22);
            this.pointsEarnedSpin.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Points Earned:";
            // 
            // pointsPriceSpin
            // 
            this.pointsPriceSpin.Location = new System.Drawing.Point(109, 25);
            this.pointsPriceSpin.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.pointsPriceSpin.Name = "pointsPriceSpin";
            this.pointsPriceSpin.Size = new System.Drawing.Size(100, 22);
            this.pointsPriceSpin.TabIndex = 7;
            this.pointsPriceSpin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // typeGroup
            // 
            this.typeGroup.Controls.Add(this.cashRadio);
            this.typeGroup.Controls.Add(this.pointsRadio);
            this.typeGroup.Location = new System.Drawing.Point(3, 3);
            this.typeGroup.Name = "typeGroup";
            this.typeGroup.Size = new System.Drawing.Size(220, 55);
            this.typeGroup.TabIndex = 10;
            this.typeGroup.TabStop = false;
            this.typeGroup.Text = "Vend Type";
            // 
            // cashGroup
            // 
            this.cashGroup.Controls.Add(this.label2);
            this.cashGroup.Controls.Add(this.cashPriceCombo);
            this.cashGroup.Controls.Add(this.pointsEarnedSpin);
            this.cashGroup.Controls.Add(this.label1);
            this.cashGroup.Location = new System.Drawing.Point(3, 64);
            this.cashGroup.Name = "cashGroup";
            this.cashGroup.Size = new System.Drawing.Size(220, 100);
            this.cashGroup.TabIndex = 11;
            this.cashGroup.TabStop = false;
            this.cashGroup.Text = "Cash Vend";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Price:";
            // 
            // pointsGroup
            // 
            this.pointsGroup.Controls.Add(this.label3);
            this.pointsGroup.Controls.Add(this.pointsPriceSpin);
            this.pointsGroup.Location = new System.Drawing.Point(3, 170);
            this.pointsGroup.Name = "pointsGroup";
            this.pointsGroup.Size = new System.Drawing.Size(220, 64);
            this.pointsGroup.TabIndex = 12;
            this.pointsGroup.TabStop = false;
            this.pointsGroup.Text = "Points Vend";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Price:";
            // 
            // VendPriceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pointsGroup);
            this.Controls.Add(this.cashGroup);
            this.Controls.Add(this.typeGroup);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "VendPriceControl";
            this.Size = new System.Drawing.Size(230, 246);
            ((System.ComponentModel.ISupportInitialize)(this.pointsEarnedSpin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pointsPriceSpin)).EndInit();
            this.typeGroup.ResumeLayout(false);
            this.typeGroup.PerformLayout();
            this.cashGroup.ResumeLayout(false);
            this.cashGroup.PerformLayout();
            this.pointsGroup.ResumeLayout(false);
            this.pointsGroup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton cashRadio;
        private System.Windows.Forms.RadioButton pointsRadio;
        private System.Windows.Forms.ComboBox cashPriceCombo;
        private System.Windows.Forms.NumericUpDown pointsEarnedSpin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown pointsPriceSpin;
        private System.Windows.Forms.GroupBox typeGroup;
        private System.Windows.Forms.GroupBox cashGroup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox pointsGroup;
        private System.Windows.Forms.Label label3;
    }
}
