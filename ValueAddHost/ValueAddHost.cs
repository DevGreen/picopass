﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.IO.Ports;


namespace GI.Flash.Pico
{
    public class ValueAddHost
    {
        static void Main(string[] args)
        {

            FlashCTSService ctsService=null;


            try
            {


                ctsService = new FlashCTSService("COM1", "COM2");


                ctsService.Listen();

            }
            catch (Exception expt)
            {
                Console.WriteLine(expt.ToString());
            }
            finally
            {
                if (ctsService != null)
                    ctsService.Close();
            }
        }
    }
}
