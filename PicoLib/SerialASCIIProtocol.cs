﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.IO.Ports;


namespace GI.Flash.Pico
{
    public class SerialASCIIProtocol
    {
        private SerialPort port;

        public enum ReturnCode  {Success,Failure};

        private byte lrc = 0;

        private enum Control: byte
        {
                STX=0x02,
                ETX=0x03,
                FS = 0x1C,
                ACK = 0x06,
                NACK = 0x15,
                EOT = 0x04
        }

        public SerialASCIIProtocol(SerialPort port)
        {
            this.port = port;

            this.port.ReadTimeout = 500;
            this.port.WriteTimeout = 500;

        }

        private void Write(byte[] data)
        {
            foreach (byte b in data)
            {
                this.lrc ^= b;

                //Console.WriteLine("Write: byte:{0:X2}, lrc:{1:X2}", b, this.lrc);
            }

            port.Write(data, 0, data.Length);
        }

        private void Write(byte b)
        {
            this.lrc ^= b;
            //Console.WriteLine("Write: byte:{0:X2}, lrc:{1:X2}", b, this.lrc);

            byte[] data = new byte[] { b };
            port.Write(data, 0, 1);
        }

        private void Write(Control control)
        {
            this.Write((byte)control);
        }

        private void Write(string value)
        {
            byte[] ascii = Encoding.ASCII.GetBytes(value);
            this.Write(ascii);
        }

        public bool SendCommand(string cmd,string[] parameters)
        {

            //Clear receive buffer before sending to 
            //remove any leftovers (such as an EOT)
            //this.port.ReadExisting();

            while (port.BytesToRead > 0)
                port.ReadByte();


            Write(Control.STX);
            this.lrc = 0x00;
            Write(cmd);

            if (parameters != null)
            {
                foreach (string param in parameters)
                {
                    Write(Control.FS);

                    Write(param);
                }
            }

            Write(Control.ETX);
            Write(this.lrc);

            return this.GetAck();
        }

        private bool GetAck()
        {

            int timeout = 50;

            while (port.BytesToRead < 1 && (timeout--) > 0)
                System.Threading.Thread.Sleep(10);

            if (port.BytesToRead < 1)
            {
                //Console.WriteLine("Timed-out Waiting for ACK");
                return false;
            }

            int ack=this.port.ReadByte();


            //Console.WriteLine("Received Ack: {0:X2}", ack);


            if ((byte)ack == (byte)Control.ACK)
                return true;
            else
                return false;
//                throw new Exception(string.Format("Unexpected ack byte:{0:X2}", ack));
        }


        private bool GetEOT()
        {

            int timeout = 5;

            while (port.BytesToRead < 1 && (timeout--) > 0)
                System.Threading.Thread.Sleep(10);

            if (port.BytesToRead < 1)
            {
                //Console.WriteLine("Timed-out Waiting for EOT");
                return false;
            }

            int eot = this.port.ReadByte();


            //Console.WriteLine("Received EOT: {0:X2}", ack);

            return (byte)eot == (byte)Control.EOT;
        }



        private int ReadControl(Control stopChar, byte[] buffer)
        {
            int readVal;

            int count=0;

            int timeout = 500;

            while (port.BytesToRead < 1 && (timeout--) > 0)
                System.Threading.Thread.Sleep(10);

            if (port.BytesToRead < 1)
                return -1;


            while( (readVal=port.ReadByte()) > 0)
            {
                byte b = (byte)readVal;

                this.lrc ^= b;

                //Console.WriteLine("Read: byte:{0:X2}[{1}], lrc:{2:X2}", b,Encoding.ASCII.GetString(new byte[]{b}), this.lrc);


                if (b == (byte)stopChar)
                    return count;

                if (buffer != null)
                {
                    if (count >= buffer.Length)
                        return -1;

                    buffer[count++] = b;
                }
                else
                    count++;


                //Are there more bytes?

                timeout = 50;

                while (port.BytesToRead < 1 && (timeout--) > 0)
                    System.Threading.Thread.Sleep(10);

                if (port.BytesToRead < 1)
                    return -1;

            }
            return -1;
        }



        public bool ReadCommand(out string cmd,out string[] parameters)
        {
            cmd=null;
            parameters = null;

            //Console.WriteLine("Reading Command");

            if (ReadControl(Control.STX, null) < 0)
                return false;

            //Console.WriteLine("Got STX");

            this.lrc = 0;

            byte[] buffer = new byte[256];
            int rc;


            rc = ReadControl(Control.ETX, buffer);

            //Console.WriteLine("Got Message[{0}]:{1}",rc, Encoding.ASCII.GetString(buffer, 0, 4));

            int lrcRead=port.ReadByte();

            //Console.WriteLine("Got LRC:{0:X2} Expecting: {1:X2}", lrcRead, this.lrc);

            //Console.WriteLine("LRC Read:{0:X2}", lrcRead);
            //Console.WriteLine("LRC Calc:{0:X2}", this.lrc);

            if ((this.lrc ^ lrcRead) != 0)
            {
                //Console.WriteLine("Sending NACK");
                this.Write(Control.NACK);
                return false;
            }
            else
            {
                //Console.WriteLine("Sending ACK");
                this.Write(Control.ACK);
            }

            //Parse buffer

            List<string> fields = new List<string>();


            int startPos = 0;

            for (int i = 0; i < rc; i++)
            {
                if (buffer[i] == (byte)Control.FS)
                {
                    fields.Add(Encoding.ASCII.GetString(buffer, startPos, i - startPos));
                    startPos = i+1;
                }
            }

            fields.Add(Encoding.ASCII.GetString(buffer, startPos, rc - startPos));

            if (fields.Count > 0)
                cmd = fields[0];

            if (fields.Count > 1)
            {
                parameters = new string[fields.Count - 1];

                for(int i=0;i<(fields.Count-1);i++)
                {
                    parameters[i]=fields[i+1];
                }
            }
            return true;
        }


        public bool SendResponse(string cmd,string[] parameters)
        {
            return this.SendCommand(cmd, parameters);
        }
        public bool ReadResponse(out string cmd,out string[] parameters)
        {
            bool success=this.ReadCommand(out cmd, out parameters);

            this.GetEOT();

            return success;

        }

    }
}
