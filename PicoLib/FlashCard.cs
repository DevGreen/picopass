﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using GI.Utility;

namespace GI.Flash.Pico
{

    public class FlashCardInfo
    {
        public byte[] SerialNumber;
        public ushort CustomerID;
        public string LocationID;
        public ushort PurseValue;
        public CardType CardType;
        public uint CardNumber;
        public ushort LoyaltyPoints;
        public ushort PinMateRedeemedValue;
    }

    public class SignInfo
    {
        public ushort CustomerID;
        public string LocationID;
        public ushort InitialPurseValue;
    }


    public enum VendResult
    {
                Success             = 0,
                NoCard              = -1,
                InvalidCard         = -2,
                WrongCard           = -3,
                InsufficientFunds   = -4,
                ReadError           = -5,
                WriteError          = -6
    };

    public enum ReadResult
    {
                Success             = 0,
                NoCard              = -1,
                InvalidCard         = -2,
                ReadError           = -5,
                CreditLock          = -9,
                Unsigned            = -10,
    };


    public enum AddValueResult
    {
                Success             = 0,
                NoCard              = -1,
                InvalidCard         = -2,
                WrongCard           = -3,
                ReadError           = -5,
                WriteError          = -6,
                PurseFull           = -7,
                RechargeExpired     = -8
    };


    public interface IFlashCard
    {
        ReadResult ReadCard(out FlashCardInfo cardInfo);
        ReadResult ReadCardCTS(out FlashCardInfo cardInfo, ushort signWithCustomerID, string signWithLocationID, ushort signWithPurseValue);

        VendResult VendCardCash(byte[] targetSerialNumber, ushort debitAmount, out ushort newPurseValue,ushort pointsEarned);
        VendResult VendCardPoints(byte[] targetSerialNumber, ushort pointsDebitAmount, out ushort newPointsValue);

//        AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, out ushort newPurseValue);
        AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, ushort pinMateRedeemAmount, out ushort newPurseValue);


        bool ImplementsVendCard { get; }
        bool ImplementsAddValue { get; }

        void Disconnect();
        void Connect();
    }



    public class FlashCard:IFlashCard
    {
        private PicoPass pico;

        public FlashCard(string comPort)
        {
            this.pico = new PicoPass(comPort);

        }

        public void Connect()
        {
            this.pico.Connect();
        }

        public void Disconnect()
        {
            this.pico.Disconnect();
        }



        //public ReadResult ReadCard(out FlashCardInfo cardInfo)
        //{
        //    byte[] serialNumber;

        //    cardInfo=null;

        //    if(!this.pico.SelectCard(PicoPass.Auth.AuthKd, out serialNumber))
        //        return ReadResult.NoCard;


        //    cardInfo = new FlashCardInfo();

        //    cardInfo.SerialNumber = serialNumber;


        //    byte[] hBlock = this.pico.Read4Block(6);

        //    if (hBlock == null)
        //        return ReadResult.ReadError;


        //    PicoCardHeader header = new PicoCardHeader(hBlock);

        //    //Test for Unsigned Card
        //    if (header.CardType == CardType.Unsigned)
        //    {
        //        cardInfo.CardNumber = header.GetCardNumberFromUnsigned();
        //        return ReadResult.Unsigned;
        //    }
            

        //    if ( !header.Verify(serialNumber))
        //            return ReadResult.InvalidCard;


        //    cardInfo.CardType = header.CardType;
        //    cardInfo.CustomerID = header.CustomerID;
        //    cardInfo.LocationID = header.LocationID;
        //    cardInfo.CardNumber = header.CardNumber;


        //    if (cardInfo.CardType == CardType.User)
        //    {
        //        LoyaltyBlock lb = LoyaltyBlock.Read(pico);

        //        if (lb == null)
        //            return ReadResult.ReadError;

        //        cardInfo.LoyaltyPoints = lb.Purse1;
        //        cardInfo.PinMateRedeemedValue = lb.Purse2;

        //        int purse = pico.ReadPurse();

        //        if (purse < 0)
        //            return ReadResult.ReadError;

        //        else if (purse == 0xFFFF)
        //            return ReadResult.CreditLock;
        //        else
        //            cardInfo.PurseValue = Convert.ToUInt16(purse);


        //    }

        //    return ReadResult.Success;
        //}


        public ReadResult ReadCard(out FlashCardInfo cardInfo)
        {
            return DoReadCard(out cardInfo, false,0,null,0);
        }

        public ReadResult ReadCardCTS(out FlashCardInfo cardInfo,ushort signWithCustomerID,string signWithLocationID,ushort signWithPurseValue)
        {
            return DoReadCard(out cardInfo, true,signWithCustomerID,signWithLocationID,signWithPurseValue);
        }

        private ReadResult DoReadCard(out FlashCardInfo cardInfo, bool fixCreditLock, ushort signWithCustomerID, string signWithLocationID, ushort signWithPurseValue)
        {

            byte[] serialNumber;

            cardInfo = null;

            if (!this.pico.SelectCard(PicoPass.Auth.AuthKd, out serialNumber))
                return ReadResult.NoCard;

            cardInfo = new FlashCardInfo();
            cardInfo.SerialNumber = serialNumber;


            byte[] hBlock = this.pico.Read4Block(6);

            if (hBlock == null)
                return ReadResult.ReadError;


            PicoCardHeader header = new PicoCardHeader(hBlock);

            //Test for Unsigned Card
            if (header.CardType == CardType.Unsigned)  //Auto-magically sign the card
            {
                if (signWithCustomerID == 0)
                    return ReadResult.Unsigned;
                else
                {
                    cardInfo.CardType = CardType.User;
                    cardInfo.CustomerID = signWithCustomerID;
                    cardInfo.LocationID = signWithLocationID;
                    cardInfo.PurseValue = signWithPurseValue;
                    cardInfo.CardNumber = CardNumber.AppendCheckDigit(header.GetCardNumberFromUnsigned());

                    return this.SignCard(cardInfo);

                }
            }

            if (!header.Verify(serialNumber))
                return ReadResult.InvalidCard;


            cardInfo.CardType = header.CardType;
            cardInfo.CustomerID = header.CustomerID;
            cardInfo.LocationID = header.LocationID;
            cardInfo.CardNumber = header.CardNumber;


            if (cardInfo.CardType == CardType.User)
            {
                LoyaltyBlock lb = LoyaltyBlock.Read(pico);

                if (lb == null)
                    return ReadResult.ReadError;

                cardInfo.LoyaltyPoints = lb.Purse1;
                cardInfo.PinMateRedeemedValue = lb.Purse2;

                int purse = pico.ReadPurse();

                if (purse < 0)
                    return ReadResult.ReadError;

                else if (purse == 0xFFFF)  //Credit Locked
                {
                    if (fixCreditLock)
                        return this.RestorePurse(cardInfo);
                    else
                    {
                        //For FlashVend Test Only
                        //Read Backup Purse

                        PurseBackup backup=PurseBackup.Read(this.pico);

                        if (backup == null || !backup.Valid)
                            cardInfo.PurseValue=0;
                        else
                            cardInfo.PurseValue=backup.Value;

                        return ReadResult.CreditLock;
                    }
                }
                else
                    cardInfo.PurseValue = Convert.ToUInt16(purse);

            }

            return ReadResult.Success;
        }

        private ReadResult SignCard(FlashCardInfo cardInfo)
        {
            cardInfo.LoyaltyPoints = 0;
            cardInfo.PinMateRedeemedValue = 0;
 
            //Assume card is currently selected with debit key.

            PicoCardHeader newHeader = new PicoCardHeader();

            newHeader.CardType = cardInfo.CardType;
            newHeader.CardNumber = cardInfo.CardNumber;
            newHeader.CardVersion = 0x01;
            newHeader.CustomerID = cardInfo.CustomerID;
            newHeader.LocationID = cardInfo.LocationID;
            newHeader.Sign(cardInfo.SerialNumber);

            if(!this.pico.Write4Block(6,new ArraySegment<byte>(newHeader.GetBytes()),true))
                return ReadResult.ReadError;


            PurseBackup purseBack = new PurseBackup();
            purseBack.Value = cardInfo.PurseValue;

            if (!purseBack.Write(this.pico))
                return ReadResult.ReadError;

           
            //Re-select with credit key

            byte[] serialNumber;

            if (!this.pico.SelectCard(PicoPass.Auth.AuthKc, out serialNumber))
                return ReadResult.ReadError;

            if (!this.CompareSN(serialNumber, cardInfo.SerialNumber))
                return ReadResult.ReadError;


            //Clear Purse
            if (this.pico.ClearPurse() != PicoPass.CreditResult.Success)
                return ReadResult.ReadError;

            PicoPass.CreditResult cResult;


            //Load Initial Value

            if (cardInfo.PurseValue > 0)
            {
                ushort newPurseValue;
                ushort newRechargeValue;

                cResult=this.pico.CreditPurse(cardInfo.PurseValue, out newPurseValue, out newRechargeValue);

                if (cResult != PicoPass.CreditResult.Success)
                    return ReadResult.ReadError;
            }

            return ReadResult.Success;
        }


        private ReadResult RestorePurse(FlashCardInfo cardInfo)
        {
            //Assume debit is already selected

            PurseBackup backup=PurseBackup.Read(this.pico);

            if (backup == null)
                return ReadResult.ReadError;
            else if (!backup.Valid)
                return ReadResult.CreditLock;


            //A valid purse backup was found.  Restore it!!
            byte[] serialNumber;


            if (!this.pico.SelectCard(PicoPass.Auth.AuthKc, out serialNumber))
                return ReadResult.ReadError;

            if (!CompareSN(serialNumber, cardInfo.SerialNumber))
                return ReadResult.ReadError;


            ushort newRechargeValue;

            PicoPass.CreditResult cres = this.pico.RestorePurse(backup.Value, out newRechargeValue);

            if (cres != PicoPass.CreditResult.Success)
                return ReadResult.ReadError;

            cardInfo.PurseValue = backup.Value;

            return ReadResult.Success;

        }


        public VendResult VendCardCash(byte[] targetSerialNumber,ushort debitAmount,out ushort newPurseValue, ushort pointsEarned)
        {
            byte[] serialNumber;

            newPurseValue = 0;

            if (!this.pico.SelectCard(PicoPass.Auth.AuthKd, out serialNumber))
                return VendResult.NoCard;

            if(!CompareSN(serialNumber,targetSerialNumber))
                return VendResult.WrongCard;

            byte[] hBlock=this.pico.Read4Block(6);

            if(hBlock==null)
                return VendResult.ReadError;

            PicoCardHeader header=new PicoCardHeader(hBlock);

            if(!header.Verify(serialNumber) || !(header.CardType==CardType.User))
                return VendResult.InvalidCard;

            if (pointsEarned > 0)
            {
                LoyaltyBlock lb = LoyaltyBlock.Read(pico);

                if (lb == null)
                    return VendResult.ReadError;

                lb.Purse1 += pointsEarned;

                if (!lb.Write(pico))
                    return VendResult.WriteError;  

            }
            // KL 11/11/2010
            //Note: I have chosen to write loyalty points before debiting the purse.
            // If debiting the purse fails, the system will try again and may
            // credit loyalty points twice.

            // This is better than the alternative 
            // which is to have the loyalty points fail to write after successfully debiting the purse.
            // That situation could end up with a double debit of the cash purse.


            PicoPass.DebitResult debitResult = this.pico.DebitPurse(debitAmount, out newPurseValue);

            switch(debitResult)
            {
                case PicoPass.DebitResult.Success:
                    return VendResult.Success;
                case PicoPass.DebitResult.ReadError:
                    return VendResult.ReadError;
                case PicoPass.DebitResult.WriteError:
                    return VendResult.WriteError;
                case PicoPass.DebitResult.InsufficientFunds:
                    {
                        //In case of insuff funds, attempt to "take back" loyalty points that were already credited
                        if (pointsEarned > 0)
                        {
                            LoyaltyBlock lb = LoyaltyBlock.Read(pico);

                            if (lb != null)
                                lb.Purse1 -= pointsEarned;

                            lb.Write(pico);
                        }
                        return VendResult.InsufficientFunds;
                    }
                default:
                    throw new Exception(string.Format("FlashCard.VendCard: Unexpected DebitResult: {0}",debitResult.ToString()));
            }


            //int confirmPurse=this.pico.ReadPurse();

            //if (confirmPurse < 0)
            //    return VendResult.BrokenVend;

            //if (confirmPurse != newPurseValue)
            //    throw new Exception("FlashCard.VendCard: Bad New Purse");


            //return VendResult.Success;
        }


        public VendResult VendCardPoints(byte[] targetSerialNumber, ushort pointsDebitAmount, out ushort newPointsValue)
        {
            byte[] serialNumber;

            newPointsValue = 0;

            if (!this.pico.SelectCard(PicoPass.Auth.AuthKd, out serialNumber))
                return VendResult.NoCard;

            if (!CompareSN(serialNumber, targetSerialNumber))
                return VendResult.WrongCard;

            byte[] hBlock = this.pico.Read4Block(6);

            if (hBlock == null)
                return VendResult.ReadError;

            PicoCardHeader header = new PicoCardHeader(hBlock);

            if (!header.Verify(serialNumber))
                return VendResult.InvalidCard;

             LoyaltyBlock lb = LoyaltyBlock.Read(pico);


             if (lb == null)
                  return VendResult.ReadError;


             if (pointsDebitAmount > lb.Purse1)
                 return VendResult.InsufficientFunds;

             lb.Purse1 -= pointsDebitAmount;

             newPointsValue=lb.Purse1;


            if (!lb.Write(pico))
                    return VendResult.WriteError;

            return VendResult.Success;
        }




        private bool CompareSN(byte[] serialNumber, byte[] targetSerialNumber)
        {
            if (serialNumber.Length != 8 || targetSerialNumber.Length != 8)
                throw new Exception("FlashCard.CompareSN: Bad Serial Number");

            for (int i = 0; i < 8; i++)
            {
                if (serialNumber[i] != targetSerialNumber[i])
                    return false;

            }

            return true;
        }

    
        //public AddValueResult AddValue(byte[] targetSerialNumber,ushort creditAmount,out ushort newPurseValue)
        //{
        //    byte[] serialNumber;

        //    newPurseValue = 0;

        //    if (!this.pico.SelectCard(PicoPass.Auth.AuthKc, out serialNumber))
        //        return AddValueResult.NoCard;

        //    if(!CompareSN(serialNumber,targetSerialNumber))
        //        return AddValueResult.WrongCard;


        //    // Cannot read header block while using the  credit key
        //    // Thus we cannot verify the header again
        //    // We've already checked that the serial number
        //    // matches the s/n from the first pass in which we verified the header

        //    //byte[] hBlock=this.pico.Read4Block(6);

        //    //if(hBlock==null)
        //    //    return AddValueResult.ReadError;

        //    //PicoCardHeader header=new PicoCardHeader(hBlock);

        //    //if(!header.Verify(serialNumber))
        //    //    return AddValueResult.InvalidCard;

        //    ushort newRechargeValue = 0;

        //    PicoPass.CreditResult creditResult = this.pico.CreditPurse(creditAmount, out newPurseValue, out newRechargeValue);

        //    switch (creditResult)
        //    {
        //        case PicoPass.CreditResult.Success:
        //            return AddValueResult.Success;
        //        case PicoPass.CreditResult.ReadError:
        //            return AddValueResult.ReadError;
        //        case PicoPass.CreditResult.WriteError:
        //            return AddValueResult.WriteError;
        //        case PicoPass.CreditResult.PurseFull:
        //            return AddValueResult.PurseFull;
        //        case PicoPass.CreditResult.RechargeExpired:
        //            return AddValueResult.RechargeExpired;           
        //        default:
        //            throw new Exception(string.Format("FlashCard.AddValue: Unexpected CreditResult: {0}", creditResult.ToString()));
        //    }
        //}

        public AddValueResult AddValue(byte[] targetSerialNumber, ushort creditAmount, ushort pinMateRedeemAmount,out ushort newPurseValue)
        {
            byte[] serialNumber;

            newPurseValue = 0;


            //First, Select the card for debit
            if (!this.pico.SelectCard(PicoPass.Auth.AuthKd, out serialNumber))
                return AddValueResult.NoCard;

            if (!CompareSN(serialNumber, targetSerialNumber))
                return AddValueResult.WrongCard;


            // Check for valid GI Header  (Maybe we don't need to do this if time becomes an issue)

            byte[] hBlock=this.pico.Read4Block(6);

            if(hBlock==null)
                return AddValueResult.ReadError;

            PicoCardHeader header=new PicoCardHeader(hBlock);

            if(!header.Verify(serialNumber))
                return AddValueResult.InvalidCard;


            //Read, existing purse value

            int oldPurseValue=this.pico.ReadPurse();

            if (oldPurseValue < 0)
                return AddValueResult.ReadError;

            // Make sure credit amount is valid
            if ((oldPurseValue + creditAmount) > 0xFFFE)
            {
                newPurseValue = (ushort)oldPurseValue;
                return AddValueResult.PurseFull;
            }

            // Next, Write Purse backup

            PurseBackup purseBack = new PurseBackup();
            purseBack.Value = (ushort)(oldPurseValue + creditAmount);

            if (!purseBack.Write(this.pico))
                return AddValueResult.WriteError;


            // Update PinMateRedeemedValue if necessary
            if (pinMateRedeemAmount > 0)
            {
                LoyaltyBlock lb = LoyaltyBlock.Read(this.pico);
                // TT - chaged the LOC below to be a set operation, not an add.
                // lb.Purse2 += pinMateRedeemAmount;
                lb.Purse2 = pinMateRedeemAmount;
                lb.Write(this.pico);
            }

            // Now, re-select for Credit
            
            if (!this.pico.SelectCard(PicoPass.Auth.AuthKc, out serialNumber))
                return AddValueResult.ReadError;

            if (!CompareSN(serialNumber, targetSerialNumber))
                return AddValueResult.WrongCard;

            //And, apply the credit

            ushort newRechargeValue = 0;

            PicoPass.CreditResult creditResult = this.pico.CreditPurse(creditAmount, out newPurseValue, out newRechargeValue);

            switch (creditResult)
            {
                case PicoPass.CreditResult.Success:
                    return AddValueResult.Success;
                case PicoPass.CreditResult.ReadError:
                    return AddValueResult.ReadError;
                case PicoPass.CreditResult.WriteError:
                    return AddValueResult.WriteError;
                case PicoPass.CreditResult.PurseFull:
                    return AddValueResult.PurseFull;
                case PicoPass.CreditResult.RechargeExpired:
                    return AddValueResult.RechargeExpired;
                default:
                    throw new Exception(string.Format("FlashCard.AddValue: Unexpected CreditResult: {0}", creditResult.ToString()));
            }
        }


        #region IFlashCard Members


        public bool ImplementsVendCard
        {
            get { return true; }
        }

        public bool ImplementsAddValue
        {
            get { return true; }
        }

        #endregion
    }

}
