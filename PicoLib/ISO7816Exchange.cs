﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Diagnostics;


using System.IO.Ports;

namespace GI.Flash.Pico
{
    public class ISO7816Exchange
    {
        private SerialPort port;
        private int timeout=100;

        public enum Status:ushort {OK=0x9000, NoReply=0xFFFF,BadInput=0xEEEE};

        public ISO7816Exchange(SerialPort port)
        {
            this.port = port;
        }

        private void SendCommand(byte cls,byte ins,byte p1,byte p2,byte p3)
        {
            byte[] command=new byte[5];

            command[0] = cls;
            command[1] = ins;
            command[2] = p1;
            command[3] = p2;
            command[4] = p3;
            //Console.WriteLine("Send: {0:X2} {1:X2} {2:X2} {3:X2} {4:X2}", cls, ins, p1, p2, p3);

            //Debug.WriteLine(string.Format("Send: {0:X2} {1:X2} {2:X2} {3:X2} {4:X2}", cls, ins, p1, p2, p3));

            port.Write(command, 0, 5);
        }

        private bool WaitForBytes(ArraySegment<byte> dataOut,int timeout)
        {
            while (port.BytesToRead < dataOut.Count && timeout > 0)
            {
                System.Threading.Thread.Sleep(10);
                timeout -= 10;
            }

            if (port.BytesToRead >= dataOut.Count)
            {
                int nRead=port.Read(dataOut.Array,dataOut.Offset, dataOut.Count);

                if (nRead == dataOut.Count)
                    return true;
            }

          return false;
        }

        public Status GetStatus()
        {
            byte[] status = new byte[2];
            ArraySegment<byte> data=new ArraySegment<byte>(status);

            if (this.WaitForBytes(data, this.timeout))
            {
                //Console.WriteLine("Status: {0:X2} {1:X2}", status[0], status[1]);
                //Debug.WriteLine(string.Format("Status: {0:X2} {1:X2}", status[0], status[1]));

                return (Status)MakeStatus(status[0], status[1]);
            }
            else
            {
                //Console.WriteLine("Status: No Reply");
                //Debug.WriteLine(string.Format("Status: No Reply"));
                return Status.NoReply;
            }
        }

        private ushort MakeStatus(byte b1, byte b2)
        {
            int word =  b1 << 8;
            word |= b2;
            return Convert.ToUInt16(word);
        }

        private Status GetAck(byte instruction)
        {
            byte[] status = new byte[2];

            ArraySegment<byte> ack = new ArraySegment<byte>(status, 0, 1);

            if (this.WaitForBytes(ack, this.timeout))
            {
                if (status[0] == instruction)
                {
                    //Console.WriteLine("Ack: OK");
                    //Debug.WriteLine("Ack: OK");
                    return Status.OK;
                }
                else
                {
                    ack = new ArraySegment<byte>(status, 1, 1);


                    if (this.WaitForBytes(ack, this.timeout))
                    {
                        //Console.WriteLine("Ack: Fail {0:X2} {1:X2}", status[0], status[1]);
                        //Debug.WriteLine(string.Format("Ack: Fail {0:X2} {1:X2}", status[0], status[1]));
                        return (Status)MakeStatus(status[0], status[1]);
                    }
                }
            }

            return Status.NoReply;
        }


        public Status ExchangeNone(byte _class,byte instruction,byte parm1,byte parm2,byte parm3)
        {
            this.SendCommand(_class, instruction, parm1, parm2, parm3);

            return this.GetStatus();
        }

        //void DisplayBlock(string name, ArraySegment<byte> block)
        //{
        //    //Console.Write("{0} ", name);
        //    //Debug.Write(name);
        //    Debug.Write(" ");

        //    for (int i = 0; i < block.Count; i++)
        //        //Debug.Write(string.Format("{0:X2} ", block.Array[i + block.Offset]));

        //    //Debug.WriteLine(string.Empty);

        //}
        public Status ExchangeOut(byte _class,byte instruction,byte parm1,byte parm2,ArraySegment<byte> dataOut)
        {
            if (dataOut.Count > 255 || dataOut.Count < 0)
                return Status.BadInput;

            byte dataLength = Convert.ToByte(dataOut.Count);

            this.SendCommand(_class, instruction, parm1, parm2, dataLength);

            Status ack = this.GetAck(instruction);

            if (ack != Status.OK)
                return ack;

            if (this.WaitForBytes(dataOut, this.timeout))
            {
                //this.DisplayBlock("Read", dataOut);
                return this.GetStatus();
            }
            else
            {
                //Console.WriteLine("Read: No Reply");
                //Debug.WriteLine("Read: No Reply");
                return Status.NoReply;
            }
        }


        public Status ExchangeIn(byte _class, byte instruction, byte parm1, byte parm2, ArraySegment<byte> dataIn)
        {
            if (dataIn.Count > 255||dataIn.Count<0)
                return Status.BadInput;

            byte dataLength = Convert.ToByte(dataIn.Count);

            this.SendCommand(_class, instruction, parm1, parm2, dataLength);

            Status ack = this.GetAck(instruction);

            if (ack != Status.OK)
                return ack;

            //DisplayBlock("Write", dataIn);
            port.Write(dataIn.Array, dataIn.Offset, dataIn.Count);

            return this.GetStatus();
        }

        public Status ExchangeInOut(byte _class, byte instruction, byte parm1,ArraySegment<byte> dataIn,ArraySegment<byte> dataOut)
        {
            if (    dataIn.Count > 255 || dataIn.Count < 0
                 || dataOut.Count > 255 || dataOut.Count < 0)
                return Status.BadInput;

            byte dataInLength = Convert.ToByte(dataIn.Count);
            byte dataOutLength = Convert.ToByte(dataOut.Count);

            this.SendCommand(_class, instruction, parm1, dataOutLength, dataInLength);

            Status ack = this.GetAck(instruction);

            if (ack != Status.OK)
                return ack;

            //DisplayBlock("Write", dataIn);
            port.Write(dataIn.Array, dataIn.Offset, dataIn.Count);

            ack = this.GetAck(instruction);

            if (ack != Status.OK)
                return ack;

            if (this.WaitForBytes(dataOut, this.timeout))
            {
                //this.DisplayBlock("Read", dataOut);

                return this.GetStatus();
            }
            else
            {
                //Console.WriteLine("Read:No Reply");
                //Debug.WriteLine("Read: No Reply");

                return Status.NoReply;
            }
        }
    }
}
