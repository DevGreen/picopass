﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace GI.Flash.Pico
{

    public class PicoPass
    {
        private PicoRead reader;


        public enum Auth : byte
        {
            NoAuth = 0x00,
            AuthKc = 0x10,
            AuthKd = 0x30
        }


        public PicoPass(string comPort)
        {
            this.reader = new PicoRead(comPort);
        }

        public void Connect()
        {
            this.reader.Open();
            if (!this.reader.ResetField())
            {
                this.reader.Close();
                throw new Exception("PicoPass.Connect: Unable to Reset Field");
            }
        }

        public void Disconnect()
        {
            this.reader.Close();
        }

        public bool SelectCard(Auth auth,out byte[] serialNumber)
        {
            if (auth == Auth.AuthKd)
            {
                //Console.WriteLine("SelectKey");

                this.reader.SelectCurrentKey(PicoRead.Key.Kd0);
            }
            else if (auth == Auth.AuthKc)
            {
                //Console.WriteLine("SelectKey");

                this.reader.SelectCurrentKey(PicoRead.Key.Kc0);
            }

            //Console.WriteLine("SelectCard");

            byte[] result=this.reader.SelectCard((PicoRead.SelectMode)((byte)auth), PicoRead.ChipType.PicoTag);

            if (result == null)
            {
                serialNumber = null;
                return false;
            }
            else
            {
                serialNumber = new byte[8];

                Buffer.BlockCopy(result, 1, serialNumber, 0, 8);

                return true;
            }
        }

        private enum Stage { Left, Right }

        private Stage DetermineStage(byte[] purseBlock)
        {
            if (   purseBlock[0] == 0xFF && purseBlock[1] == 0xFF
                && purseBlock[2] == 0xFF && purseBlock[3] == 0xFF)
                return Stage.Right;
            else
                return Stage.Left;
        }

        private ushort GetCounter(Stage stage, byte[] purseBlock)
        {
            int offset = (stage == Stage.Left) ? 0 : 4;
            return Convert.ToUInt16((purseBlock[0+offset]) + (purseBlock[1+offset] << 8));
        }

        private void PutCounter(Stage stage, byte[] purseBlock,ushort counter)
        {
            int offset = (stage == Stage.Left) ? 0 : 4;

            purseBlock[0 + offset] = Convert.ToByte(counter & 0xFF);
            purseBlock[1 + offset] = Convert.ToByte( (counter>>8) & 0xFF);
        }

        private ushort GetRecharge(Stage stage, byte[] purseBlock)
        {
            int offset = (stage == Stage.Left) ? 0 : 4;
            return Convert.ToUInt16((purseBlock[2 + offset]) + (purseBlock[3 + offset] << 8));
        }

        private void PutRecharge(Stage stage, byte[] purseBlock, ushort recharge)
        {
            int offset = (stage == Stage.Left) ? 0 : 4;

            purseBlock[2 + offset] = Convert.ToByte(recharge & 0xFF);
            purseBlock[3 + offset] = Convert.ToByte((recharge >> 8) & 0xFF);
        }


        public enum CreditResult { Success, ReadError, WriteError, PurseFull,RechargeExpired };

        public CreditResult SetPurse( ushort purseValue, out ushort newPurseValue, out ushort newRechargeValue )
        {
           byte[] purseBlock = this.ReadBlock( 0x02 );

           if ( purseBlock == null || purseBlock.Length < 8 )
           {
              newPurseValue = 0;
              newRechargeValue = 0;
              return CreditResult.ReadError;
           }

           Stage stage = this.DetermineStage( purseBlock );

           ushort counter = this.GetCounter( stage, purseBlock );
           ushort recharge = this.GetRecharge( stage, purseBlock );

           if (( recharge < 1 ))
           {
              newPurseValue = counter;
              newRechargeValue = recharge;
              return CreditResult.RechargeExpired;
           }
           // else if (( creditAmount + counter) > 0xFFFE)
           else if (( purseValue ) > 0xFFFE )
           {
              newPurseValue = counter;
              newRechargeValue = recharge;
              return CreditResult.PurseFull;
           }
           else
           {
              newPurseValue = ( ushort )( purseValue );
              newRechargeValue = ( ushort )( recharge - 1 );
           }

           byte[] newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

           //First Write recharge into current stage
           this.PutRecharge( stage, newPurseBlock, newRechargeValue );

           if ( this.WriteBlock( 0x02, new ArraySegment<byte>( newPurseBlock ), true ))
           {
              //Now write counter and recharge into other stage;

              //toggle stage
              stage = ( stage == Stage.Left ) ? Stage.Right : Stage.Left;

              newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

              this.PutCounter( stage, newPurseBlock, newPurseValue );
              this.PutRecharge( stage, newPurseBlock, newRechargeValue );

              if ( this.WriteBlock( 0x02, new ArraySegment<byte>( newPurseBlock ), true ))
              {
                 return CreditResult.Success;
              }
              else
              {
                 return CreditResult.WriteError;
              }
           }
           else
           {
              return CreditResult.WriteError;
           }
        }

        public CreditResult CreditPurse(ushort creditAmount, out ushort newPurseValue, out ushort newRechargeValue)
        {
            //Console.WriteLine("CreditPurse");

            byte[] purseBlock = this.ReadBlock(0x02);

            if (purseBlock == null || purseBlock.Length < 8)
            {
                newPurseValue = 0;
                newRechargeValue = 0;
                return CreditResult.ReadError;
            }

            Stage stage = this.DetermineStage(purseBlock);

            ushort counter = this.GetCounter(stage, purseBlock);
            ushort recharge = this.GetRecharge(stage, purseBlock);


            if ((recharge < 1))
            {
                newPurseValue = counter;
                newRechargeValue = recharge;
                return CreditResult.RechargeExpired;
            }
            else if ((creditAmount + counter) > 0xFFFE)
            {
                newPurseValue = counter;
                newRechargeValue = recharge;
                return CreditResult.PurseFull;
            }
            else
            {
                newPurseValue = (ushort)(counter + creditAmount);
                newRechargeValue =(ushort) (recharge-1);
            }

            byte[] newPurseBlock = new byte[] { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};


            //First Write recharge into current stage
            this.PutRecharge(stage, newPurseBlock, newRechargeValue);

            if (this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
            {
                //Now write counter and recharge into other stage;

                //toggle stage
                stage = (stage == Stage.Left) ? Stage.Right : Stage.Left;

                newPurseBlock = new byte[] { 0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};

                this.PutCounter(stage, newPurseBlock, newPurseValue);
                this.PutRecharge(stage, newPurseBlock, newRechargeValue);

                if(this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
                    return CreditResult.Success;
                else
                    return CreditResult.WriteError;
            }
            else
                return CreditResult.WriteError;
        }

        public CreditResult RestorePurse(ushort newPurseValue, out ushort newRechargeValue)
        {
            //Console.WriteLine("CreditPurse");

            byte[] purseBlock = this.ReadBlock(0x02);

            if (purseBlock == null || purseBlock.Length < 8)
            {
                newPurseValue = 0;
                newRechargeValue = 0;
                return CreditResult.ReadError;
            }

            Stage stage = this.DetermineStage(purseBlock);

            ushort counter = this.GetCounter(stage, purseBlock);
            ushort recharge = this.GetRecharge(stage, purseBlock);


            if (counter != 0xFFFF) //Card is not credit locked.  Cannot restore
            {
                newRechargeValue = recharge;
                return CreditResult.WriteError;
            }
            else if ((recharge < 1))
            {
                newRechargeValue = recharge;
                return CreditResult.RechargeExpired;
            }
            else if (newPurseValue > 0xFFFE)
            {
                newRechargeValue = recharge;
                return CreditResult.PurseFull;
            }
            else
            {
                newRechargeValue = (ushort)(recharge - 1);
            }

            byte[] newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };


            //First Write recharge into current stage
            this.PutRecharge(stage, newPurseBlock, newRechargeValue);

            if (this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
            {
                //Now write counter and recharge into other stage;

                //toggle stage
                stage = (stage == Stage.Left) ? Stage.Right : Stage.Left;

                newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

                this.PutCounter(stage, newPurseBlock, newPurseValue);
                this.PutRecharge(stage, newPurseBlock, newRechargeValue);

                if (this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
                    return CreditResult.Success;
                else
                    return CreditResult.WriteError;
            }
            else
                return CreditResult.WriteError;
        }


        public CreditResult ClearPurse()
        {
            //Console.WriteLine("CreditPurse");

            byte[] purseBlock = this.ReadBlock(0x02);

            if (purseBlock == null || purseBlock.Length < 8)
                return CreditResult.ReadError;

            Stage stage = this.DetermineStage(purseBlock);

            ushort counter = this.GetCounter(stage, purseBlock);
            ushort recharge = this.GetRecharge(stage, purseBlock);


            if ((recharge < 1))
            {
                return CreditResult.RechargeExpired;
            }

            ushort newPurseValue = 0;
            ushort newRechargeValue = (ushort)(recharge - 1);

            byte[] newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };


            //First Write recharge into current stage
            this.PutRecharge(stage, newPurseBlock, newRechargeValue);

            if (this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
            {
                //Now write counter and recharge into other stage;

                //toggle stage
                stage = (stage == Stage.Left) ? Stage.Right : Stage.Left;

                newPurseBlock = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

                this.PutCounter(stage, newPurseBlock, newPurseValue);
                this.PutRecharge(stage, newPurseBlock, newRechargeValue);

                if (this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
                    return CreditResult.Success;
                else
                    return CreditResult.WriteError;
            }
            else
                return CreditResult.WriteError;
        }

        public enum DebitResult  { Success,ReadError,WriteError,InsufficientFunds};

        public DebitResult DebitPurse(ushort debitAmount, out ushort newPurseValue)
        {
            byte[] purseBlock=this.ReadBlock(0x02);

            //Console.WriteLine("DebitPurse");

            if (purseBlock == null || purseBlock.Length < 8 )
            {
                newPurseValue = 0;
                return DebitResult.ReadError;
            }

            Stage stage=this.DetermineStage(purseBlock);

            ushort counter=this.GetCounter(stage,purseBlock);
            ushort recharge=this.GetRecharge(stage,purseBlock);


            if (debitAmount > counter)
            {
                newPurseValue = counter;
                return DebitResult.InsufficientFunds;
            }
            else
                newPurseValue = (ushort)(counter - debitAmount);

            byte[] newPurseBlock = new byte[] { 0xFF,0xFF,0xFF,0xFF,
                                                0xFF,0xFF,0xFF,0xFF};

            this.PutCounter(stage, newPurseBlock, newPurseValue);
            this.PutRecharge(stage, newPurseBlock, recharge);

            if(this.WriteBlock(0x02, new ArraySegment<byte>(newPurseBlock), true))
                return DebitResult.Success;
            else
                return DebitResult.WriteError;
        }

        public int ReadPurse()
        {
            //Console.WriteLine("ReadPurse");

            byte[] block2 = this.ReadBlock(2);

            if (block2 == null || block2.Length < 8)
                return -1;
            
            int stagePosition=0;

            if (block2[0] == 0xFF
                && block2[1] == 0xFF
                && block2[2] == 0xFF
                && block2[3] == 0xFF)
                stagePosition = 4;

            int rval = block2[stagePosition];
            rval+= block2[stagePosition + 1]<<8;


            return rval;
        }

        public byte[] ReadBlock(byte blockIndex)
        {

            byte[] result=new byte[8];

            if (this.ReadBlock(blockIndex, new ArraySegment<byte>(result)))
                return result;
            else
                return null;
        }

        public bool ReadBlock(byte blockIndex,ArraySegment<byte> blockOut)
        {
            //Console.WriteLine("ReadBlock");

            if (blockOut.Count < 8 || blockOut.Array.Length <(8+blockOut.Offset))
                throw new Exception("PicoPass.ReadBlock: Destination buffer to small: " + blockOut.Count.ToString());

            if (blockOut.Count != 8)
                blockOut = new ArraySegment<byte>(blockOut.Array, blockOut.Offset, 8);


            byte[] command = new byte[] { 0x0C, blockIndex };

            bool read = reader.Transmit(PicoRead.Protocol.Pico15693,
                            PicoRead.CRC.SendAndVerify,
                            PicoRead.Timeout.Mult1,
                            false,
                            new ArraySegment<byte>(command),
                            blockOut);

            return read;
        }

        public byte[] Read4Block(byte blockIndex)
        {
            byte[] result = new byte[32];

            if (this.Read4Block(blockIndex, new ArraySegment<byte>(result)))
                return result;
            else
                return null;
        }

        public bool Read4Block(byte startBlockIndex, ArraySegment<byte> blockOut)
        {
            //Console.WriteLine("Read4Block");

            if (blockOut.Count < 32 || blockOut.Array.Length < (32 + blockOut.Offset))
                throw new Exception("PicoPass.Read4Block: Destination buffer to small: " + blockOut.Count.ToString());

            if (blockOut.Count != 32)
                blockOut = new ArraySegment<byte>(blockOut.Array, blockOut.Offset, 32);


            byte[] command = new byte[] { 0x06, startBlockIndex };

            bool read = reader.Transmit(PicoRead.Protocol.Pico15693,
                            PicoRead.CRC.SendAndVerify,
                            PicoRead.Timeout.Mult1,
                            false,
                            new ArraySegment<byte>(command),
                            blockOut);

            return read;
        }



        public bool WriteBlock(byte blockIndex, ArraySegment<byte> blockIn,bool auth)
        {
            //Console.WriteLine("WriteBlock");

            PicoRead.CRC crc=PicoRead.CRC.SendAndVerify;
            bool signature=false;

            if(auth)  //Send signature instead of CRC
            {
                signature=true;
                crc=PicoRead.CRC.Verify;
            }

            if (blockIn.Count < 8 || blockIn.Array.Length < (8 + blockIn.Offset))
                throw new Exception("PicoPass.WriteBlock: Source buffer to small: " + blockIn.Count.ToString());

            if (blockIn.Count != 8)
                blockIn = new ArraySegment<byte>(blockIn.Array, blockIn.Offset, 8);


            byte[] dataIn = new byte[10];
            byte[] dataOut = new byte[8];

            dataIn[0] = 0x87;
            dataIn[1]=blockIndex;
            Buffer.BlockCopy(blockIn.Array, blockIn.Offset, dataIn, 2, 8);

            bool success = reader.Transmit(PicoRead.Protocol.Pico15693,
                            crc,
                            PicoRead.Timeout.Mult40,
                            signature,
                            new ArraySegment<byte>(dataIn),
                            new ArraySegment<byte>(dataOut));

            return success;
        }


        public bool Write4Block(byte blockIndex, ArraySegment<byte> blockIn, bool auth)
        {
            //Console.WriteLine("Write4Block");


            PicoRead.CRC crc = PicoRead.CRC.SendAndVerify;
            bool signature = false;

            if (auth)  //Send signature instead of CRC
            {
                signature = true;
                crc = PicoRead.CRC.Verify;
            }

            if (blockIn.Count < 32 || blockIn.Array.Length < (32 + blockIn.Offset))
                throw new Exception("PicoPass.Write4Block: Source buffer to small: " + blockIn.Count.ToString());

            if (blockIn.Count != 32)
                blockIn = new ArraySegment<byte>(blockIn.Array, blockIn.Offset, 32);


            byte[] dataIn = new byte[10];
            byte[] dataOut = new byte[8];

            dataIn[0] = 0x87;
            dataIn[1] = blockIndex;

            for (int i = 0; i < 4; i++)
            {
                dataIn[1] = Convert.ToByte(blockIndex+i);

                Buffer.BlockCopy(blockIn.Array, blockIn.Offset+(i*8), dataIn, 2, 8);

                bool success = reader.Transmit(PicoRead.Protocol.Pico15693,
                                crc,
                                PicoRead.Timeout.Mult40,
                                signature,
                                new ArraySegment<byte>(dataIn),
                                new ArraySegment<byte>(dataOut));


                if (!success)
                    return false;
            }
            return true;
        }

     
    }
}
