﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Flash.Pico
{
    public class HashedBlock
    {
        public ushort Value1;
        public ushort Value2;
        public ushort Value3;

        public bool Valid;

        public HashedBlock()
        {
            Value1 = 0;
            Value2 = 0;
            Value3 = 0;
            Valid = false;
        }

        private static bool VerifyHash(byte[] block)
        {
            return ((block[0] ^ block[2] ^ block[4] ^ block[6]) == 0xBE
                    && (block[1] ^ block[3] ^ block[5] ^ block[7]) == 0xEF);
        }

        private static void SetHash(byte[] block)
        {
            block[6] = (byte)(block[0] ^ block[2] ^ block[4] ^ 0xBE);
            block[7] = (byte)(block[1] ^ block[3] ^ block[5] ^ 0xEF);
        }


        public static HashedBlock Read(PicoPass pico,byte blockIndex)
        {
            byte[] block = pico.ReadBlock(blockIndex);

            if (block == null || block.Length < 8)
                return null;

            HashedBlock hb = new HashedBlock();

            if (!VerifyHash(block))  //Block was not valid.  Return Invalid Block
                return hb;


            hb.Value1 = (ushort)(block[0] + (block[1] << 8));  //little endian to match PicoPass purse
            hb.Value2 = (ushort)(block[2] + (block[3] << 8));
            hb.Value3 = (ushort)(block[4] + (block[5] << 8));

            hb.Valid = true;

            return hb;
        }



        public bool Write(PicoPass pico,byte blockIndex)
        {
            byte[] block = new byte[8];

            block[0] = (byte)(this.Value1 & 0xFF);
            block[1] = (byte)((this.Value1 >> 8) & 0xFF);
            block[2] = (byte)(this.Value2 & 0xFF);
            block[3] = (byte)((this.Value2 >> 8) & 0xFF);
            block[4] = (byte)(this.Value3 & 0xFF);
            block[5] = (byte)((this.Value3 >> 8) & 0xFF);

            SetHash(block);

            return pico.WriteBlock(blockIndex, new ArraySegment<byte>(block), true);
        }



    }
}
