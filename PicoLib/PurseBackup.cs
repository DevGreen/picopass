﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Flash.Pico
{
    public class PurseBackup
    {
        private HashedBlock hashBlock;

        public PurseBackup()
        {
            this.hashBlock = new HashedBlock();
        }

        private PurseBackup(HashedBlock hb)
        {
            this.hashBlock = hb;
        }
        public bool Valid
        {
            get { return this.hashBlock.Valid; }
        }
        public ushort Value
        {
            get { return this.hashBlock.Value1; }
            set { this.hashBlock.Value1 = value; }
        }
        public static PurseBackup Read(PicoPass pico)
        {
            HashedBlock hb = HashedBlock.Read(pico, 11);

            if (hb == null)
                return null;
            else
                return new PurseBackup(hb);
        }

        public bool Write(PicoPass pico)
        {
            return this.hashBlock.Write(pico, 11);
        }
    }
}
