﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO.Ports;


namespace GI.Flash.Pico
{
    public class FlashCTSProtocol
    {
        private SerialASCIIProtocol sap;

        public FlashCTSProtocol(SerialPort port)
        {
            sap = new SerialASCIIProtocol(port);
        }

        //CTS side

        public bool SendReadCommand(int timeout)
        {
            return sap.SendCommand("READ", new string[] { timeout.ToString() });
        }

        public bool GetReadResponse(out ReadResult result,out FlashCardInfo cardInfo)
        {
            result = ReadResult.ReadError;
            cardInfo = null;


            string cmd;
            string[] parms;

            if (sap.ReadResponse(out cmd, out parms))
            {
                if (cmd != "READ")
                {
                    result = ReadResult.ReadError;
                    return true;
                }

                int resultCode;

                if (!int.TryParse(parms[0], out resultCode))
                    result = ReadResult.ReadError;
                else
                {
                    result = (ReadResult)resultCode;


                    cardInfo = new FlashCardInfo();

                    cardInfo.SerialNumber = ParseHexString(parms[1]);

                    if (!ushort.TryParse(parms[2], System.Globalization.NumberStyles.HexNumber, null, out cardInfo.CustomerID))
                        cardInfo.CustomerID = 0;

                    cardInfo.LocationID = parms[3].Trim();

                    if (!ushort.TryParse(parms[4], out cardInfo.PurseValue))
                        cardInfo.PurseValue = 0;
                }

                return true;
            }
            else
                return false;
        }


        public bool SendAddValueCommand(int timeout, byte[] serialNumber, ushort creditAmount)
        {
            string snString = this.MakeHexString(serialNumber);
            return sap.SendCommand("ADDV", new string[] { timeout.ToString(), snString, creditAmount.ToString() });
        }

        public bool GetAddValueResponse(out AddValueResult result, out ushort newPurseValue)
        {
            result = AddValueResult.ReadError;
            newPurseValue = 0;

            string cmd;
            string[] parms;

            if (sap.ReadResponse(out cmd, out parms))
            {
                if (cmd != "ADDV")
                {
                    result = AddValueResult.ReadError;
                    return true;
                }

                if (parms.Length != 2)
                    return false;

                int resultCode;

                if (!int.TryParse(parms[0], out resultCode))
                    result = AddValueResult.ReadError;
                else
                {
                    result = (AddValueResult)resultCode;
                                      
                    if (!ushort.TryParse(parms[1],  out newPurseValue))
                        newPurseValue = 0;
                }

                return true;
            }
            else
                return false;
        }

        // Reader side

        public bool SendReadResponse(ReadResult result, FlashCardInfo cardInfo)
        {
            string resultCode = ((int)result).ToString();

            if (cardInfo != null)
            {
                return sap.SendResponse("READ", new string[] 
                {resultCode,
                 MakeHexString(cardInfo.SerialNumber),
                 cardInfo.CustomerID.ToString("X4"),
                 cardInfo.LocationID.PadRight(10,' '),
                 cardInfo.PurseValue.ToString()});
            }
            else
            {
                return sap.SendResponse("READ", new string[] { resultCode, string.Empty, string.Empty, string.Empty, string.Empty });
            }

        }



        public bool SendAddValueResponse(AddValueResult result, ushort newPurseValue)
        {
            string resultCode = ((int)result).ToString();
            return sap.SendResponse("ADDV",new string[]{resultCode,newPurseValue.ToString()});
        }



        private byte[] ParseHexString(string hex)
        {
            byte[] result = new byte[hex.Length / 2];

            for (int i = 0; i < result.Length; i++)
            {
                if (!byte.TryParse(hex.Substring(2 * i, 2), System.Globalization.NumberStyles.HexNumber, null, out result[i]))
                    result[i] = 0;
            }
            return result;
        }

        private string MakeHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            return sb.ToString();
        }


        public enum Command { Read, AddValue, Error };

        public Command GetCommand(out int timeout,out byte[] serialNumber,out ushort creditAmount)
        {
            string cmdString;
            string[] parms;

            timeout=0;
            serialNumber = null;
            creditAmount = 0;


            if (sap.ReadCommand(out cmdString, out parms))
            {
                switch (cmdString)
                {
                    case "READ":
                        if (!int.TryParse(parms[0], out timeout))
                            return Command.Error;
                        else
                            return Command.Read;
                    case "ADDV":
                        if (!int.TryParse(parms[0], out timeout))
                            return Command.Error;

                        serialNumber = this.ParseHexString(parms[1]);

                        if (!ushort.TryParse(parms[2], out creditAmount))
                            return Command.Error;

                        return Command.AddValue;
                    default:
                        return Command.Error;
                }
            }
            else
                return Command.Error;
        }

    }
}
