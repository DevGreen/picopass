﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace GI.Flash.Pico
{
    public enum CardType : byte
    {
        Admin              = 0xC4,
        CashRemoved        = 0xD9,
        CycleKill          = 0xE3,
        DisplayCounters    = 0xD4,
        ClearCounters      = 0xD1,        
        RecentTransaction  = 0xD0,
        SetPrice           = 0xB0,
        Unsigned           = 0xAA,
        User               = 0xAC,
        Service            = 0xE1,
        UseRemainingBalance = 0xE4,   // added for carnival 6/15/15 EM
        FactoryTest        = 0xFF     // added 7/5/17 EM
    }

    public class PicoCardHeader
    {
        byte[] data;
        public PicoCardHeader(byte[] header4Block)
        {
            this.data = header4Block;
            if (this.data.Length != 32)
                throw new Exception("PicoCardHeader.ctr: HeaderBlock must be 32 bytes");

        }

        public byte[] GetBytes()
        {
            return data;
        }

        public PicoCardHeader()
        {
            this.data = new byte[32];
        }

        public CardType CardType
        {
            get{return (CardType)data[0];}
            set { data[0] = (byte)value; }
        }

        public uint GetCardNumberFromUnsigned()
        {
            byte[] cnBytes = new byte[4];

            //Source is Big Endian

            cnBytes[3] = data[1];
            cnBytes[2] = data[2];
            cnBytes[1] = data[3];
            cnBytes[0] = data[4];

            return BitConverter.ToUInt32(cnBytes, 0);
        }

        public static PicoCardHeader MakeUnsigned(uint cardNumber)
        {
            PicoCardHeader header = new PicoCardHeader();
            header.data[0] = (byte)CardType.Unsigned;

            byte[] cnBytes = BitConverter.GetBytes(cardNumber);

            header.data[1]=cnBytes[3];
            header.data[2] = cnBytes[2];
            header.data[3] = cnBytes[1];
            header.data[4] = cnBytes[0];

            return header;

        }

        public byte CardVersion
        {
            get { return data[1]; }
            set { data[1] = value; }
        }

        public ushort CustomerID
        {
            get
            {
                return Convert.ToUInt16((data[2]<<8) + data[3]);
            }
            set
            {
                data[2] = Convert.ToByte((value >> 8) & 0xFF);
                data[3] = Convert.ToByte(value & 0xFF);
            }
        }

        public uint CardNumber
        {
            get
            {
                byte[] cnBytes = new byte[4];

                //Source is Big Endian

                cnBytes[3]=data[16];
                cnBytes[2]=data[17];
                cnBytes[1]=data[18];
                cnBytes[0]=data[19];

                return BitConverter.ToUInt32(cnBytes,0);
           }
            set
            {
                byte[] cnBytes = BitConverter.GetBytes(value);

                 data[16]=cnBytes[3];
                 data[17]=cnBytes[2];
                 data[18]=cnBytes[1];
                 data[19]=cnBytes[0];
            }
        }


        public string LocationID
        {
            get
            {
                return Encoding.ASCII.GetString(data, 0x04, 10).Trim();
            }
            set
            {
                byte[] loc = Encoding.ASCII.GetBytes(value.PadRight(10));

                Buffer.BlockCopy(loc, 0, data, 0x04, 10);
            }
        }

        public void Sign(byte[] serialNumber)
        {
            byte magicNumber = 8675309 %256;

            for (int i = 0; i < 8; i++)
            {
                magicNumber = Convert.ToByte((data[i] + data[i + 8] + data[i + 16] + serialNumber[i] + magicNumber) % 256);

                data[i+24] =Convert.ToByte(data[i] ^ data[i + 8] ^ data[i + 16] ^ serialNumber[i]^magicNumber);
            }
        }

        public bool Verify(byte[] serialNumber)
        {
            byte magicNumber = 8675309 % 256;

            for (int i = 0; i < 8; i++)
            {
                magicNumber = Convert.ToByte((data[i] + data[i + 8] + data[i + 16] + serialNumber[i]+magicNumber) % 256);

                if (data[i + 24] != Convert.ToByte(data[i] ^ data[i + 8] ^ data[i + 16] ^ serialNumber[i] ^ magicNumber))
                    return false;
            }
            return true;
        }

    }
}
