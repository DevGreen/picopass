﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.IO.Ports;


namespace GI.Flash.Pico
{
    public class FlashCTSService
    {
        FlashCTSProtocol cts;
        SerialPort ctsSerialPort;

        FlashCard flash;

        public FlashCTSService(string picoPortName,string ctsPortName)
        {
            ctsSerialPort = new SerialPort(ctsPortName, 9600);
            cts = new FlashCTSProtocol(ctsSerialPort);

            flash = new FlashCard(picoPortName);
        }

        public void Close()
        {
            if (ctsSerialPort != null)
                ctsSerialPort.Close();

            if(flash!=null)
                flash.Disconnect();
        }

        public void Listen()
        {
            ctsSerialPort.Open();
            flash.Connect();

            while (true)
            {
                int timeout;
                byte[] serialNumber;
                ushort creditAmount;

                FlashCTSProtocol.Command cmd=cts.GetCommand(out timeout, out serialNumber,out creditAmount);

                switch (cmd)
                {
                    case FlashCTSProtocol.Command.Read:
                        this.Read(timeout);
                        break;
                    case FlashCTSProtocol.Command.AddValue:
                        this.AddValue(timeout, serialNumber, creditAmount);
                        break;
                    case FlashCTSProtocol.Command.Error:
                        break;
                }
            }
        }

        private const int SleepTime=50;

        private void Read(int timeout)
        {
            FlashCardInfo cardInfo;

            bool noCard = false;

            int counter = timeout / SleepTime;
            if (counter < 1)
                counter = 1;


            while ((counter--) > 0)
            {
                ReadResult read = flash.ReadCard(out cardInfo);

                if (read == ReadResult.Success || read == ReadResult.InvalidCard)
                {
                    cts.SendReadResponse(read, cardInfo);

                    
                    Console.WriteLine("Read :{0} Purse:{1}",read,cardInfo==null?"NULL":cardInfo.PurseValue.ToString());
                    return;
                }

                if (read == ReadResult.NoCard)
                    noCard = true;

                if(counter>0)
                    System.Threading.Thread.Sleep(SleepTime);
            }

            cts.SendReadResponse(noCard ? ReadResult.NoCard : ReadResult.ReadError, null);
            Console.WriteLine("Read :{0}", noCard ? ReadResult.NoCard : ReadResult.ReadError);

        }

        private void AddValue(int timeout, byte[] serialNumber, ushort creditAmount)
        {
            ushort newPurseValue;

            bool noCard = false;

            int counter = timeout / SleepTime;
            if (counter < 1)
                counter = 1;


            while ((counter--) > 0)
            {
                AddValueResult result = flash.AddValue(serialNumber, creditAmount,0, out newPurseValue);


                if (
                            result == AddValueResult.Success
                        || result == AddValueResult.InvalidCard
                        || result == AddValueResult.PurseFull
                        || result == AddValueResult.RechargeExpired
                        || result == AddValueResult.WriteError
                        || result == AddValueResult.WrongCard                    
                    )
                {

                    cts.SendAddValueResponse(result, newPurseValue);
                    return;
                }

                if (result == AddValueResult.NoCard)
                    noCard = true;

                if (counter > 0)
                    System.Threading.Thread.Sleep(SleepTime);
            }

            cts.SendAddValueResponse(noCard ? AddValueResult.NoCard : AddValueResult.ReadError, 0);
        }
    }
}
