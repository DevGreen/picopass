﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Flash.Pico
{
    public class LoyaltyBlock
    {
        private HashedBlock hashBlock;

        public LoyaltyBlock()
        {
            this.hashBlock = new HashedBlock();
        }

        private LoyaltyBlock(HashedBlock hashBlockArg)
        {
            this.hashBlock = hashBlockArg;
        }

        public bool Valid
        {
            get { return this.hashBlock.Valid; }
        }

        public ushort Purse1
        {
            get { return this.hashBlock.Value1; }
            set { this.hashBlock.Value1 = value; }
        }

        public ushort Purse2
        {
            get { return this.hashBlock.Value2; }
            set { this.hashBlock.Value2 = value; }
        }
        public ushort Purse3
        {
            get { return this.hashBlock.Value3; }
            set { this.hashBlock.Value3 = value; }
        }

        public static LoyaltyBlock Read(PicoPass pico)
        {
            HashedBlock hb=HashedBlock.Read(pico, 10);

            if (hb == null)   //Error reading
                return null;
            else
                return new LoyaltyBlock(hb);
        }

        public bool Write(PicoPass pico)
        {
            return this.hashBlock.Write(pico, 10);
        }
    }

}
