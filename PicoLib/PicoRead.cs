﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO.Ports;
//using System.Diagnostics;


namespace GI.Flash.Pico
{
    public class PicoRead
    {
        private SerialPort port;
        private ISO7816Exchange iso;

        public enum SelectMode : byte
        {
            NoAuth = 0x00,
            AuthKc = 0x10,
            AuthKd = 0x30,
            Loop = 0x01,
            Halt = 0x02,
            Pre = 0x08
        }

        public enum ChipType : byte
        {
            PicoPass = 0x01,
            PicoTag = 0x02,
            PicoCrypt = 0x04,
            User = 0x08,
            All = 0x0F
        }

        public enum Protocol : byte
        {
            Pico14443B = 0x00,
            Pico15693 = 0x01,
            Pico14443A = 0x02,
            User = 0x03
        }

        public enum CRC : byte
        {
            NoCRC = 0x00,
            Send = 0x80,
            Verify = 0x40,
            SendAndVerify = 0xC0
        }

        public enum Timeout : byte
        {
            Mult1 = 0x00,
            Mult4 = 0x10,
            Mult40 = 0x20,
            Mult100 = 0x30
        }

        public enum Key : byte
        {
            Kd0 = 0x01,
            Kc0 = 0x02,
            Kd1 = 0x03,
            Kc1 = 0x04,
            Kd2 = 0x05,
            Kc2 = 0x06,
            Kd3 = 0x07,
            Kc3 = 0x08,
            Kd4 = 0x09,
            Kc4 = 0x0A,
            Kd5 = 0x0B,
            Kc5 = 0x0C,
            Kd6 = 0x0D,
            Kc6 = 0x0E,
            Kd7 = 0x0F,
            Kc7 = 0x10
        }

        public static bool IsDebitKey(PicoRead.Key key)
        {
            switch (key)
            {
                case PicoRead.Key.Kd0:
                case PicoRead.Key.Kd1:
                case PicoRead.Key.Kd2:
                case PicoRead.Key.Kd3:
                case PicoRead.Key.Kd4:
                case PicoRead.Key.Kd5:
                case PicoRead.Key.Kd6:
                case PicoRead.Key.Kd7:
                    return true;
                case PicoRead.Key.Kc0:
                case PicoRead.Key.Kc1:
                case PicoRead.Key.Kc2:
                case PicoRead.Key.Kc3:
                case PicoRead.Key.Kc4:
                case PicoRead.Key.Kc5:
                case PicoRead.Key.Kc6:
                case PicoRead.Key.Kc7:
                    return false;
                default:
                    throw new Exception("Unknown Key: " + key.ToString());
            }
        }
        

        public PicoRead(string comPortName)
        {
            this.port = new SerialPort(comPortName, 9600, Parity.Even, 8,StopBits.Two);
            
            this.iso = new ISO7816Exchange(port);
        }

        public void Open()
        {
            this.port.Open();
            if (!this.Synchronize())
            {
                this.port.Close();
                throw new Exception("PicoRead.Synchronize: Could not sync with coupler.");
            }

        }
        public void Close()
        {
            this.port.Close();
        }

        private bool Synchronize()
        {
            byte[] zero=new byte[] { 0x00 };

            for (int i = 0; i < 6; i++)
            {
                this.port.Write(zero, 0, 1);
                //Console.WriteLine("Bytes to Read: {0}", port.BytesToRead);

                ushort status = (ushort)this.iso.GetStatus();

                //Console.WriteLine("Status: {0:X4}", status);

                //System.Threading.Thread.Sleep(100);

                if (status == 0x6B00
                    || status == 0x6D00
                    || status == 0x6E00
                    || status == 0x9835
                    || status == 0x6A82)
                    return true;
            }
            return false;
        }

        public byte[] SelectCard(SelectMode selectMode, ChipType chipTypes)
        {
            byte[] dataOut = new byte[9];

            ISO7816Exchange.Status status=iso.ExchangeOut(0x80, 0xA4, (byte)selectMode, (byte)chipTypes, new ArraySegment<byte>(dataOut));

            if (status == ISO7816Exchange.Status.OK)
                return dataOut;
            else
                return null;
        }

        public bool ResetField()
        {
            byte[] dataIn = new byte[] { 0x00};


            ISO7816Exchange.Status status = iso.ExchangeIn(0x80, 0xF4, 0x40, 0x00, new ArraySegment<byte>(dataIn));

            return (status == ISO7816Exchange.Status.OK);
        }

        public bool Transmit(Protocol protocol,CRC crc,Timeout timeout,bool signature,ArraySegment<byte> dataIn,ArraySegment<byte> dataOut)
        {

            byte p1 = 0x04;  //ISO type IN-OUT

            p1 |= (byte) timeout;
            p1 |= (byte)crc;
            p1 |= (byte)protocol;

            if (signature)
                p1 |= 0x08;

            ISO7816Exchange.Status status = iso.ExchangeInOut(0x80, 0xC2, p1, dataIn, dataOut);

            //Console.WriteLine("Transmit:({0:X2},in:{1},out:{2}):{3:X2}", p1, dataIn.Count, dataOut.Count,(ushort)status);
            return (status == ISO7816Exchange.Status.OK);
        }

        public bool SelectCurrentKey(Key key)
        {
            byte[] dataIn = new byte[8];

            ISO7816Exchange.Status status = iso.ExchangeIn(0x80, 0x52, 0x00, (byte)key, new ArraySegment<byte>(dataIn));

            return (status == ISO7816Exchange.Status.OK);

        }

        public byte[] AskRandom()
        {
            byte[] dataOut = new byte[8];
            ISO7816Exchange.Status status = iso.ExchangeOut(0x80, 0x84, 0x00, 0x00, new ArraySegment<byte>(dataOut));

            if (status == ISO7816Exchange.Status.OK)
                return dataOut;
            else
                return null;
        }

        private byte[] PermuteKey(byte[] key)
        {
            //Transpose the 8x8 matrix of bits
            byte[] permutedKey = new byte[8];
            byte checkByte = 0xFF;

            for (int i = 0; i < 7; i++)
            {
                permutedKey[i] = 0;

                for (int j = 0; j < 8; j++)
                {
                    bool bitSet=( key[j] & (0x80 >>i)) !=0;

                    if (bitSet)
                        permutedKey[i] |= (byte)(0x01 << j);
                }
                checkByte ^= permutedKey[i];
            }

            permutedKey[7] = checkByte;

            return permutedKey;
        }

        private byte[] XorKeys(byte[] key1, byte[] key2)
        {
            byte[] result = new byte[8];
            for (int i = 0; i < 8; i++)
                result[i] = (byte)(key1[i] ^ key2[i]);
            return result;
        }

        private byte[] CalcCheckSum(byte[] permutedKey, Key keyLocation)
        {
            byte[] checkSum = new byte[4];

            checkSum[0] = (byte)(permutedKey[0] ^ permutedKey[4] ^ 0x80 ^ 0x0C);
            checkSum[1] = (byte)(permutedKey[1] ^ permutedKey[5] ^ 0xD8);
            checkSum[2] = (byte)(permutedKey[2] ^ permutedKey[6]);
            checkSum[3] = (byte)(permutedKey[3] ^ permutedKey[7] ^ ((byte)keyLocation));

            return checkSum;
        }


        public bool LoadKey(Key keyLocation, byte[] exchangeKey, byte[] newKey)
        {
            byte[] permutedExchange = this.PermuteKey(exchangeKey);
            byte[] random = this.AskRandom();

            if (random == null)
                return false;

            byte[] sessionkey = this.XorKeys(permutedExchange, random);

            byte[] permutedNewKey = this.PermuteKey(newKey);
            byte[] cryptNewKey = this.XorKeys(permutedNewKey, sessionkey);

            byte[] checkSum = this.CalcCheckSum(permutedNewKey, keyLocation);

            byte[] cmdData = new byte[12];

            Buffer.BlockCopy(cryptNewKey, 0, cmdData, 0, 8);
            Buffer.BlockCopy(checkSum, 0, cmdData, 8, 4);

            ISO7816Exchange.Status result = this.iso.ExchangeIn(0x80, 0xD8, 0x00,(byte) keyLocation, new ArraySegment<byte>(cmdData));

            if (result == ISO7816Exchange.Status.OK)
                return true;
            else
                return false;

        }


        public byte[] GetDiversifiedKey(Key key, byte[] serialNumber)
        {
            ISO7816Exchange.Status status = iso.ExchangeIn(0x80, 0x52, 0x00, (byte)key, new ArraySegment<byte>(serialNumber));

            if (status != ISO7816Exchange.Status.OK)
                return null;


            byte[] dataOut = new byte[8];

            status = iso.ExchangeOut(0x80, 0xC0, 0x00, 0x00, new ArraySegment<byte>(dataOut));


            if (status == ISO7816Exchange.Status.OK)
                return dataOut;
            else
                return null;
        }
    }
}
