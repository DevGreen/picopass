﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Utility
{
    public class CardNumber
    {

        public static string Format(UInt32 baseNumber)
        {
            List<Digit> digits = new List<Digit>();

            if (baseNumber > 99999999)
                throw new Exception("CardNumber.Format: Invalid Base Number: " + baseNumber.ToString());

            for(int i=0;i<8;i++)
            {
                digits.Add((Digit)(baseNumber % 10));
                baseNumber /= 10;
            }

            digits.Reverse();

            Verhoeff.AppendCheckDigit(digits);


            return Digits2String(digits).Insert(3, "-").Insert(7, "-");
        }

        public static UInt32 AppendCheckDigit(UInt32 baseNumber)
        {
            List<Digit> digits = new List<Digit>();

            if (baseNumber > 99999999)
                throw new Exception("CardNumber.AppendCheckDigit: Invalid Base Number: " + baseNumber.ToString());

            for (int i = 0; i < 8; i++)
            {
                digits.Add((Digit)(baseNumber % 10));
                baseNumber /= 10;
            }

            digits.Reverse();

            Verhoeff.AppendCheckDigit(digits);

            digits.Reverse();


            uint cardNumber = 0;
            uint radix = 1;


            for (int i = 0; i < 9; i++)
            {
                cardNumber += ((uint)digits[i]) * radix;
                radix *= 10;
            }

            return cardNumber;
        }


        public static bool Validate(string cardNumber)
        {
            List<Digit> digits = String2Digits(cardNumber.Replace("-", string.Empty));

            if (digits.Count != 9)
                return false;

            return Verhoeff.VerifyCheckDigit(digits);
        }


        private static string Digits2String(List<Digit> digits)
        {
            StringBuilder sb = new StringBuilder();
            foreach (Digit d in digits)
                sb.Append((int)d);
            return sb.ToString();
        }

        private static List<Digit> String2Digits(string value)
        {
            List<Digit> digits = new List<Digit>();

            foreach (char c in value.ToCharArray())
            {
                if (char.IsDigit(c))
                    digits.Add((Digit)int.Parse(new string(c, 1)));
                else
                    return null;
            }
            return digits;
        }

    }


}
