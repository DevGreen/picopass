﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using GI.Flash.Pico;
using System.Media;
using System.Data.SqlClient;
using System.Data.SqlServerCe;
using System.Runtime.InteropServices;
using SlpApiDll;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        //Struct to pass args to the BackgroundWorker
        public struct workerArg
        {
            public string comPort;
            public string programAction;
        }

        //************************
        //**  Static Variables  **
        //************************
        //Static variable to count the number of cards programmed in a selectLoop
        public static int cardCounter=0;

        //Static variable for the Background Worker Progress Changed function
        private static string previousCard = "";

        //Static variables for InitSigning
        private static PicoCardHeader signingHeader = null;
        private static ushort signingValue = 0;

        //*************************
        //**  Startup Functions  **
        //*************************
        //Performs the very first actions when the program is started
        public Form1()
        {
            InitializeComponent();

            InitializeBackgroundWorker();

            string[] ds = databaseGetIDs();
            idBox.Items.AddRange(ds);
            printingIDBox.Items.AddRange(ds);
        }

        //Performs the startup actions
        //Sets the necessary labels to empty
        private void Form1_Load(object sender, EventArgs e)
        {
            statusLabel.Text = "";
            statusLabel2.Text = "";
            statusLabel3.Text = "";
            statusLabel4.Text = "";
            statusLabel5.Text = "Select settings before programming";
            successLabel.Text = "";
            successLabel2.Text = "";
            stopButton.Enabled = false;
            balanceBox.Enabled = false;
            balanceBox.Visible = false;
            label6.Enabled = false;
            label6.Visible = false;
            printButton.Enabled = false;
            printButton.Visible = false;
            tableLayoutPanel4.Visible = false;
            printingStatusLabel.Text = "Select settings before printing";
            partNoBox.Enabled = false; // Remove this if/when part number support is added
        }

        //Creates the Background Worker to handle selectLoop calls
        private void InitializeBackgroundWorker()
        {
            backgroundWorker1.DoWork += new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
        }

        //************************
        //**  Database Support  **
        //************************
        //Pulls the customerIDs from the database
        private string[] databaseGetIDs()
        {
            List<string> dataSet = new List<string>();
            using (SqlCeConnection con = new SqlCeConnection(@"Data Source=CardData.sdf"))
            {
                con.Open();
                string query = "SELECT IDs FROM customerID";
                using (SqlCeCommand command = new SqlCeCommand(query, con))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataSet.Add(reader.GetString(0));
                        }
                    }
                }
            }
            return dataSet.ToArray();
        }

        //Pulls the customerID x type truth value from the database
        private bool[] databaseGetTypeValid(string dbType)
        {
            List<bool> dataSet = new List<bool>();
            using (SqlCeConnection con = new SqlCeConnection(@"Data Source=CardData.sdf"))
            {
                con.Open();
                try
                {
                    string query = "SELECT " + dbType + " FROM customerID";
                    using (SqlCeCommand command = new SqlCeCommand(query, con))
                    {
                        using (SqlCeDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dataSet.Add(reader.GetBoolean(0));
                            }
                        }
                    }
                }
                catch
                {
                    string query = "SELECT [" + dbType + "] FROM customerID";
                    using (SqlCeCommand command = new SqlCeCommand(query, con))
                    {
                        using (SqlCeDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                dataSet.Add(reader.GetBoolean(0));
                            }
                        }
                    }
                }
            }
            return dataSet.ToArray();
        }

        //Pulls the customerID strings from the database
        private string[] databaseGetStrings()
        {
            List<string> dataSet = new List<string>();
            using (SqlCeConnection con = new SqlCeConnection(@"Data Source=CardData.sdf"))
            {
                con.Open();
                string query = "SELECT [String Name] FROM customerID";
                using (SqlCeCommand command = new SqlCeCommand(query, con))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataSet.Add(reader.GetString(0));
                        }
                    }
                }
            }
            return dataSet.ToArray();
        }

        //Pulls the customerID words from the database
        private int[] databaseGetWords()
        {
            List<int> dataSet = new List<int>();
            using (SqlCeConnection con = new SqlCeConnection(@"Data Source=CardData.sdf"))
            {
                con.Open();
                string query = "SELECT Word FROM customerID";
                using (SqlCeCommand command = new SqlCeCommand(query, con))
                {
                    using (SqlCeDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dataSet.Add(reader.GetInt32(0));
                        }
                    }
                }
            }
            return dataSet.ToArray();
        }

        //************************
        //**   Layout Support   **
        //************************
        
        //Allows for part number selection functionality in the future
        private void partNoBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (partNoBox.Text == "Not Specified" || string.IsNullOrEmpty(partNoBox.Text))
            {
                typeBox.Enabled = true;
                idBox.Enabled = true;
            }
            else
            {
                typeBox.Enabled = false;
                idBox.Enabled = false;
            }

            switch (partNoBox.Text)
            {
                //Add cases to specify the typeBox value and 
                //idBox value based on the part number
                //
                //This "Test Case" will never occur, but it
                //can act as a template for future real cases
                case ("Test Case"):
                    idBox.SelectedIndex = 0;
                    typeBox.SelectedIndex = 0;
                    break;
                default:
                    break;
            }
        }

        //Does nothing. Simply supports the combobox's existence
        private void actionBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            
        }

        //Removes the ability to print from the Card Programming
        //page if the typeBox is changed
        private void typeBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (typeBox.Text == "User")
            {
                balanceBox.Enabled = true;
                balanceBox.Visible = true;
                label6.Enabled = true;
                label6.Visible = true;
            }
            else
            {
                balanceBox.Enabled = false;
                balanceBox.Visible = false;
                label6.Enabled = false;
                label6.Visible = false;
            }

            if (printButton.Enabled || printButton.Visible)
            {
                printButton.Enabled = false;
                printButton.Visible = false;
                successLabel2.Text = "Value changed without printing";
            }
        }

        //Removes the ability to print from the Card Programming
        //page if the idBox is changed
        private void idBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            if (printButton.Enabled == true)
            {
                printButton.Enabled = false;
                printButton.Visible = false;
                successLabel2.Text = "Value changed without printing";
            }
        }

        //Does nothing. Simply supports the combobox's existence
        private void comPortBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //************************
        //**   Button Support   **
        //************************
        //Handles when the Stop button is clicked
        private void stopButton_Click_1(object sender, EventArgs e)
        {
            stopButton.Enabled = false;
            backgroundWorker1.CancelAsync();
        }

        //Handles when the Program button is clicked
        private void startButton_Click_1(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(comPortBox.Text) ||
                string.IsNullOrEmpty(typeBox.Text) ||
                string.IsNullOrEmpty(idBox.Text) ||
                string.IsNullOrEmpty(actionBox.Text) ||
                (typeBox.Text=="User" && string.IsNullOrEmpty(balanceBox.Text)))
            {
                statusLabel5.Text = "Please fill all available fields";
            }
            else
            {
                if (comPortTest(comPortBox.Text))
                {
                    if (testLegitimacy(typeBox.Text))
                    {
                        stopButton.Enabled = true;
                        startButton.Enabled = false;
                        typeBox.Enabled = false;
                        idBox.Enabled = false;
                        comPortBox.Enabled = false;
                        actionBox.Enabled = false;
                        balanceBox.Enabled = false;
                        printButton.Enabled = false;
                        printButton.Visible = false;
                        tableLayoutPanel4.Visible = true;
                        successLabel.Text = "";
                        successLabel2.Text = "";
                        printingIDBox.SelectedIndex = idBox.SelectedIndex;
                        printingTypeBox.SelectedIndex = typeBox.SelectedIndex;
                        workerArg workerArg1 = new workerArg();
                        workerArg1.comPort = comPortBox.Text.ToString();
                        workerArg1.programAction = actionBox.Text.ToString();
                        backgroundWorker1.RunWorkerAsync(workerArg1);
                        statusLabel2.Text = "";
                        statusLabel.Text = "Place card for programming";
                        if (actionBox.Text == "New Cards")
                        {
                            statusLabel5.Text = "Total cards programmed: " + cardCounter;
                        }
                        else if (actionBox.Text == "Recycle Cards")
                        {
                            statusLabel5.Text = "Total cards recycled: " + cardCounter;
                        }
                    }
                    else
                    {
                        statusLabel5.Text = "Not a valid card setup";
                    }
                }
                else
                {
                    statusLabel5.Text = "Not a valid COM port";
                }
            }
        }

        //Handles when the Print button on the Card Programming tab is clicked
        private void printButton_Click(object sender, EventArgs e)
        {
            if (printLabel(idBox.Text.Trim(), typeBox.Text, cardCounter))
            {
                if (cardCounter == 1)
                {
                    successLabel2.Text = "Printed " + cardCounter + " label";
                }
                else
                {
                    successLabel2.Text = "Printed " + cardCounter + " labels";
                }
                printButton.Enabled = false;
                printButton.Visible = false;
            }
            else
            {
                successLabel2.Text = "Please ensure that exactly 1 printer is connected";
            }
        }

        //Handles when the Print button on the Printing tab is clicked
        private void printingPrintButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(printingTypeBox.Text) ||
                string.IsNullOrEmpty(printingIDBox.Text))
            {
                printingStatusLabel.Text = "Please fill all available fields";
            }
            else
            {
                if (printLabel(printingIDBox.Text.Trim(), printingTypeBox.Text, (int)labelCount.Value))
                {
                    if (labelCount.Value == 1)
                    {
                        printingStatusLabel.Text = "Printed " + labelCount.Value + " label";
                    }
                    else
                    {
                        printingStatusLabel.Text = "Printed " + labelCount.Value + " labels";
                    }
                }
                else
                {
                    printingStatusLabel.Text = "Please ensure that exactly 1 printer is connected";
                }
            }
        }
        
        //*********************************
        //** Background Worker Functions **
        //*********************************
        //Calls selectLoop in a Background Worker
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            workerArg workerArg1 = (workerArg)e.Argument;
            if (workerArg1.programAction == "New Cards")
            {
                SelectLoop(PicoPass.Auth.AuthKd, new CardAction(SignCard), workerArg1.comPort, worker, e);
            }
            else if (workerArg1.programAction == "Recycle Cards")
            {
                SelectLoop(PicoPass.Auth.AuthKd, new CardAction(MakeUnsignedCard), workerArg1.comPort, worker, e);
            }
        }

        //Updates the readout labels when the 
        //Background Worker makes progress
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (statusLabel2.Text != e.UserState.ToString())
            {
                if (e.UserState.ToString() != previousCard)
                {
                    if ((!e.UserState.ToString().Contains("Not a valid GI card") &&
                        !e.UserState.ToString().Contains("Not an unsigned card")) ||
                        (statusLabel2.Text == "" || statusLabel2.Text.Contains("No Card") ||
                        (statusLabel2.Text.Contains("Card :") && !statusLabel2.Text.Contains("Signed"))))
                    {

                        if (statusLabel3.Text != "")
                        {
                            statusLabel4.Text = statusLabel3.Text;
                            statusLabel3.Text = statusLabel2.Text;
                        }
                        if (statusLabel2.Text != "")
                        {
                            statusLabel3.Text = statusLabel2.Text;
                        }
                        statusLabel2.Text = e.UserState.ToString();
                        if (e.UserState.ToString().Contains("Card :") &&
                            !e.UserState.ToString().Contains("Signed"))
                        {
                            previousCard = e.UserState.ToString();
                        }
                    }
                }
            }
            if (e.ProgressPercentage == 1)
            {
                successLabel.Text = "Successfully Programmed Card :";
                successLabel2.Text = previousCard.Remove(0,7);
                if (actionBox.Text == "New Cards")
                {
                    statusLabel5.Text = "Total cards programmed: " + cardCounter;
                }
                else if (actionBox.Text == "Recycle Cards")
                {
                    statusLabel5.Text = "Total cards recycled: " + cardCounter;
                }
            }
        }

        //Runs when the Background Worker completes running
        //Called when the Stop button is clicked
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            startButton.Enabled = true;
            typeBox.Enabled = true;
            idBox.Enabled = true;
            comPortBox.Enabled = true;
            actionBox.Enabled = true;
            stopButton.Enabled = false;
            tableLayoutPanel4.Visible = false;
            if (typeBox.Text == "User")
            {
                balanceBox.Enabled = true;
            }
            successLabel.Text = "";
            successLabel2.Text = "";
            statusLabel4.Text = "";
            statusLabel3.Text = "";
            statusLabel2.Text = "";
            statusLabel.Text = "Programming stopped";
            if (actionBox.Text == "Recycle Cards")
            {
                if (cardCounter == 1)
                    statusLabel5.Text = "Successfully recycled " + cardCounter.ToString() + " card";
                else
                    statusLabel5.Text = "Successfully recycled " + cardCounter.ToString() + " cards";
            }
            else if (actionBox.Text == "New Cards")
            {
                if (cardCounter == 1)
                    statusLabel5.Text = "Successfully programmed " + cardCounter.ToString() + " card";
                else
                    statusLabel5.Text = "Successfully programmed " + cardCounter.ToString() + " cards";
                if (cardCounter > 0)
                {
                    printButton.Enabled = true;
                    printButton.Visible = true;
                    if (cardCounter == 1)
                        successLabel2.Text = "Would you like to print " + cardCounter + " label?";
                    else
                        successLabel2.Text = "Would you like to print " + cardCounter + " labels?";
                }
            }
        }

        //************************
        //**  PicoPass Support  **
        //************************
        //Supporting functions for card programming
        public delegate bool CardAction(PicoPass pp, byte[] serialNumber, BackgroundWorker worker, DoWorkEventArgs e);
        public static string HexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                if (i == 0)
                    sb.AppendFormat("{0:X2}", bytes[i]);
                else
                    sb.AppendFormat(" {0:X2}", bytes[i]);
            }
            return sb.ToString();
        }

        //Function to construct and write an unsigned header
        public static bool WriteUnsignedHeader(PicoPass pp, uint serialNumber, BackgroundWorker worker, DoWorkEventArgs e) // byte[] serialNumber)
        {
            PicoCardHeader header = PicoCardHeader.MakeUnsigned(serialNumber);

            int p = 0;
            object param;

            byte[] hBlock = header.GetBytes();

            if (pp.Write4Block(6, new ArraySegment<byte>(hBlock), true))
            {
                //Console.WriteLine("Wrote Header");
                param = "Wrote Header";
                worker.ReportProgress(p, param);
                return true;
            }
            else
            {
                //Console.WriteLine("Write Header Failed");
                param = "Write Header Failed";
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return false;
            }
        }

        //Function to create an unsigned card
        public static bool MakeUnsignedCard(PicoPass pp, byte[] serialNumber, BackgroundWorker worker, DoWorkEventArgs e)
        {
            int p = 0;
            object param;

            byte[] hBlock = pp.Read4Block(6);
            if (hBlock == null)
            {
                //Console.WriteLine("Bad Header Read");
                param = "Bad Header Read";
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return true;
            }
            PicoCardHeader header = new PicoCardHeader(hBlock);
            if (!header.Verify(serialNumber))
            {
                //Console.WriteLine("Not a valid GI card");
                param = "Not a valid GI card";
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return true;
            }
            else
            {
                if (!WriteUnsignedHeader(pp, header.CardNumber / 10, worker, e))
                    return false;
            }

            byte[] block = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            byte i;

            for (i = 7; i < 32; i++)
            {
                if (!pp.WriteBlock(i, new ArraySegment<byte>(block), true))
                {
                    return false;
                }
            }

            //Console.WriteLine("Created Unsigned card");
            param = "Created Unsigned card";
            p = 1;
            worker.ReportProgress(p, param);
            p = 0;
            cardCounter++;
            SystemSounds.Asterisk.Play();

            return true;
        }

        //Function to create a signed card
        public static bool SignCard(PicoPass pp, byte[] serialNumber, BackgroundWorker worker, DoWorkEventArgs e)
        {
            int p = 0;
            object param;

            byte[] hBlock = pp.Read4Block(6);

            if (hBlock == null)
            {
                //Console.WriteLine("Bad Header Read");
                param = "Bad Header Read";
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return false;
            }

            PicoCardHeader header = new PicoCardHeader(hBlock);

            if (header.CardType != CardType.Unsigned)
            {
                //Console.WriteLine("Not an unsigned card: {0}", header.CardType);
                param = "Not an unsigned card: " + header.CardType.ToString();
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return false;
            }


            uint cardNumber = header.GetCardNumberFromUnsigned();

            //Console.WriteLine("Card Number: {0}", cardNumber);
            param = "Card Number: " + cardNumber.ToString();
            worker.ReportProgress(p, param);

            uint cardNumberWithCheck = GI.Utility.CardNumber.AppendCheckDigit(cardNumber);


            signingHeader.CardNumber = cardNumberWithCheck;
            signingHeader.Sign(serialNumber);
            byte[] writeBlock = signingHeader.GetBytes();

            if (pp.Write4Block(6, new ArraySegment<byte>(writeBlock), true))
            {
                //Console.WriteLine("Signed Card : {0}", cardNumberWithCheck);
                param = "Signed Card : " + cardNumberWithCheck.ToString();
                worker.ReportProgress(p, param);

                if (signingHeader.CardType == CardType.User)
                {
                    //Initialize Purse
                    byte[] newSN;
                    if (pp.SelectCard(PicoPass.Auth.AuthKc, out newSN))
                    {
                        ushort newPurseValue = 0;

                        PicoPass.CreditResult result = pp.ClearPurse();

                        if (result != PicoPass.CreditResult.Success)
                        {
                            //Console.WriteLine("Error Clearing Purse: {0}", result);
                            param = "Error Clearing Purse: " + result.ToString();
                            worker.ReportProgress(p, param);
                            SystemSounds.Hand.Play();
                            return false;
                        }

                        if (signingValue > 0)
                        {
                            ushort newRechargeValue = 0;
                            result = pp.CreditPurse(signingValue, out newPurseValue, out newRechargeValue);

                            if (result != PicoPass.CreditResult.Success)
                            {
                                //Console.WriteLine("Error Crediting Purse: {0}", result);
                                param = "Error Crediting Purse: " + result.ToString();
                                worker.ReportProgress(p, param);
                                SystemSounds.Hand.Play();
                                return false;
                            }

                        }

                        //Console.WriteLine("Initial Value: {0}", newPurseValue);
                        param = "Initial Value: " + newPurseValue.ToString();
                        p = 1;
                        worker.ReportProgress(p, param);
                        p = 0;
                        cardCounter++;
                        SystemSounds.Asterisk.Play();
                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("Error re-selecting Card.");
                        param = "Error re-selecting Card";
                        worker.ReportProgress(p, param);
                        SystemSounds.Hand.Play();
                        return false;
                    }
                }
                else
                {
                    p = 1;
                    worker.ReportProgress(p, param);
                    p = 0;
                    cardCounter++;
                    SystemSounds.Asterisk.Play();
                    return true;
                }
            }
            else
            {
                //Console.WriteLine("Write Header Failed");
                param = "Write Header Failed";
                worker.ReportProgress(p, param);
                SystemSounds.Hand.Play();
                return false;
            }
        }

        //Constructs a PicoCardHeader
        public static void InitSigning(ushort customerID, string locationID, CardType cardType, ushort purseValue)
        {
            if (signingHeader == null)
                signingHeader = new PicoCardHeader();

            signingHeader.CustomerID = customerID;
            signingHeader.LocationID = locationID;
            signingHeader.CardType = cardType;
            signingHeader.CardVersion = 0x01;


            signingValue = purseValue;
        }

        //Waits for a card and then programs the card
        //Called by the Background Worker
        public static void SelectLoop(PicoPass.Auth auth, CardAction action, string comPort, BackgroundWorker worker, DoWorkEventArgs e)
        {
            PicoPass pp = new PicoPass(comPort);

            previousCard = "";

            int p = 0;
            object param;
            cardCounter = 0;

            pp.Connect();

            while (!worker.CancellationPending)
            {
                byte[] serialNumber;
                if (pp.SelectCard(auth, out serialNumber))
                {
                    //Console.WriteLine();
                    //Console.WriteLine("Card : {0}", HexString(serialNumber));
                    param = "Card : " + HexString(serialNumber);
                    worker.ReportProgress(p, param);

                    if (action(pp, serialNumber, worker, e))
                    {
                        //Beep(440, 250);
                    }

                    //Console.WriteLine();

                    System.Threading.Thread.Sleep(1000);
                }
                else
                {
                    //Console.WriteLine("No Card");
                    param = "No Card";
                    worker.ReportProgress(p, param);
                }

                if (!worker.CancellationPending)
                {
                    System.Threading.Thread.Sleep(500);
                }
            }
            pp.Disconnect();
        }

        //*************************
        //** UI Validity Testing **
        //*************************
        //Tests if the selected card setup is valid and 
        //calls InitSigning if it is
        private bool testLegitimacy(string type)
        {            
            bool[] validList = databaseGetTypeValid(type);

            string[] stringList = databaseGetStrings();
            int[] wordList = databaseGetWords();

            string chosenStr = stringList[idBox.SelectedIndex];
            int tempWord = wordList[idBox.SelectedIndex];
            ushort chosenWord = (ushort)tempWord;

            int tempWallet;
            ushort walletLoader;
            if (string.IsNullOrEmpty(balanceBox.Text))
            {
                walletLoader = 0;
            }
            else
            {
                tempWallet = int.Parse(balanceBox.Text);
                walletLoader = (ushort)tempWallet;
            }

            switch (type)
            {
                case "Recent Transaction":
                    InitSigning(chosenWord, chosenStr, CardType.RecentTransaction, 0);
                    return validList[idBox.SelectedIndex];
                case "Service":
                    InitSigning(chosenWord, chosenStr, CardType.Service, 0);
                    return validList[idBox.SelectedIndex];
                case "Cash Removed":
                    InitSigning(chosenWord, chosenStr, CardType.CashRemoved, 0);
                    return validList[idBox.SelectedIndex];
                case "User":
                    InitSigning(chosenWord, chosenStr, CardType.User, walletLoader);
                    return validList[idBox.SelectedIndex];
                case "Admin":
                    InitSigning(chosenWord, chosenStr, CardType.Admin, 0);
                    return validList[idBox.SelectedIndex];
                case "Cycle Kill":
                    InitSigning(chosenWord, chosenStr, CardType.CycleKill, 0);
                    return validList[idBox.SelectedIndex];
                case "Use Remaining Balance":
                    InitSigning(chosenWord, chosenStr, CardType.UseRemainingBalance, 0);
                    return validList[idBox.SelectedIndex];
                case "Unsigned":
                    InitSigning(chosenWord, chosenStr, CardType.Unsigned, 0);
                    return validList[idBox.SelectedIndex];
                case "Set Price":
                    InitSigning(chosenWord, chosenStr, CardType.SetPrice, 0);
                    return validList[idBox.SelectedIndex];
                case "Clear Counters":
                    InitSigning(chosenWord, chosenStr, CardType.ClearCounters, 0);
                    return validList[idBox.SelectedIndex];
                case "Factory Test":
                    InitSigning(chosenWord, chosenStr, CardType.FactoryTest, 0);
                    return validList[idBox.SelectedIndex];
                default:
                    return false;
            }
        }

        //Checks if the selected COM Port is connected to a reader
        private bool comPortTest(string comPort)
        {
            PicoPass pp = new PicoPass(comPort);

            try
            {
                pp.Connect();
                pp.Disconnect();
                return true;
            }
            catch
            {
                return false;
            }
        }

        //************************
        //**  Printing Support  **
        //************************
        //Uses the SlpApi to connect to the label printer
        private static string SelectPrinter()
        {
            string strOut = "";
            bool bFindAllPrinters = false;
            int nMaxChars = 128;
            StringBuilder strPrinterName = new StringBuilder(nMaxChars);
            int length;

            int n = SlpApi.SlpFindPrinters(bFindAllPrinters);
            
            if (n == 1)
            {
                length = SlpApi.SlpGetPrinterName(0, strPrinterName, nMaxChars);
                strOut = strPrinterName.ToString(0, length - 1);
            }
            else
            {
                strOut = "";
            }

            return strOut;
        }

        //Function to print (count) labels 
        private static bool printLabel(string customer, string type, int count)
        {
            //SlpApi.SlpDebugMode(SlpApi.Debug_Log | SlpApi.Debug_Virtual_Print);

            string strPrinterName = SelectPrinter();

            if (strPrinterName == "")
            {
                return false;
            }
            else
            {
                for (int i = 0; i < count; i++)
                {
                    if (SlpApi.SlpOpenPrinter(strPrinterName, SlpApi.Size_Multipurpose, SlpApi.Orient_Landscape) == false)
                    {
                        return false;
                    }
                    else
                    {
                        SlpApi.SlpStartLabel();

                        int nLabelWidth = SlpApi.SlpGetLabelWidth();
                        int nLabelHeight = SlpApi.SlpGetLabelHeight();
                        int nTextWidth;
                        int nTextHeight;
                        IntPtr iFont;
                        int fontSize = 14;
                        do
                        {
                            iFont = SlpApi.SlpCreateFont("Tahoma", fontSize, SlpApi.Attr_Bold);
                            nTextWidth = SlpApi.SlpGetTextWidth(iFont, customer);
                            nTextHeight = SlpApi.SlpGetTextHeight(iFont, customer);
                            if (nTextWidth >= nLabelWidth)
                            {
                                fontSize -= 2;
                                SlpApi.SlpDeleteFont(iFont);
                            }
                        } while (nTextWidth >= nLabelWidth);
                        SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight / 9, iFont, customer);
                        SlpApi.SlpDeleteFont(iFont);

                        SlpApi.SlpDrawRectangle(((nLabelWidth - nTextWidth) / 2) - 20, nLabelHeight / 9, nTextWidth + 40, nTextHeight, 2);

                        iFont = SlpApi.SlpCreateFont("Tahoma", 12, 0);

                        switch (type)
                        {
                            case "Recent Transaction":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "MOST RECENT");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "MOST RECENT");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) - (nTextHeight / 2), iFont, "MOST RECENT");
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "TRANSACTION Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "TRANSACTION Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) + (nTextHeight / 2), iFont, "TRANSACTION Card");
                                break;
                            case "Service":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "SERVICE Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "SERVICE Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "SERVICE Card");
                                break;
                            case "Cash Removed":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "CASH REMOVED");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "CASH REMOVED");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) - (nTextHeight / 2), iFont, "CASH REMOVED");
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "MARKER Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "MARKER Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) + (nTextHeight / 2), iFont, "MARKER Card");
                                break;
                            case "User":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "USER Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "USER Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "USER Card");
                                break;
                            case "Admin":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "ADMIN Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "ADMIN Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "ADMIN Card");
                                break;
                            case "Cycle Kill":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "CYCLE KILL Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "CYCLE KILL Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "CYCLE KILL Card");
                                break;
                            case "Use Remaining Balance":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "USE REMAINING");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "USE REMAINING");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) - (nTextHeight / 2), iFont, "USE REMAINING");
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "BALANCE Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "BALANCE Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) + (nTextHeight / 2), iFont, "BALANCE Card");
                                break;
                            case "Unsigned":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "UNSIGNED Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "UNSIGNED Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "UNSIGNED Card");
                                break;
                            case "Set Price":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "SET PRICE Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "SET PRICE Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, nLabelHeight * 5 / 9, iFont, "SET PRICE Card");
                                break;
                            case "Clear Counters":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "CLEAR");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "CLEAR");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) - (nTextHeight / 2), iFont, "CLEAR");
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "COUNTERS Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "COUNTERS Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) + (nTextHeight / 2), iFont, "COUNTERS Card");
                                break;
                            case "Factory Test":
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "FACTORY");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "FACTORY");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) - (nTextHeight / 2), iFont, "FACTORY");
                                nTextWidth = SlpApi.SlpGetTextWidth(iFont, "TEST Card");
                                nTextHeight = SlpApi.SlpGetTextHeight(iFont, "TEST Card");
                                SlpApi.SlpDrawTextXY((nLabelWidth - nTextWidth) / 2, (nLabelHeight * 5 / 9) + (nTextHeight / 2), iFont, "TEST Card");
                                break;
                            default:
                                break;
                        }
                        SlpApi.SlpDeleteFont(iFont);
                        SlpApi.SlpEndLabel();
                        SlpApi.SlpClosePrinter();
                    }
                }
                return true;
            }
        }

    }
}
