﻿using System;
using System.Text;
using System.Runtime.InteropServices;

namespace SlpApiDll
{
    class SlpApi
    {
        //
        // Declarations for SLPAPI DLL subroutines and functions.
        //
        // Use this module when compiling 32-bit/64-bit projects using
        // one of the following development environments:
        //
        // - Visual C# 2010
        // - Or, any newer version of Visual C#.
        //

        //////////////////////////////////////////////////////////////////
        // Sample code for a SLPSDK demo program. This code is intended //
        // for the Smart Label Printer 620/650 printers.                //
        //                                                              //
        // NOTE: Be sure to include the file SplApi.cs in your          //
        //       project. This sample code uses definitions contained   //
        //       in that file.                                          //
        //////////////////////////////////////////////////////////////////

        //
        // Debug flag
        //
        public const int Debug_Off = 0;                         // No debug output
        public const int Debug_On = 1;                          // Backward compatible output (Log + Virtual_Print + Enter_Exit)
        public const int Debug_Log = 2;                         // Write debug messages to debug log file
        public const int Debug_Virtual_Print = 4;               // Output label image to clipboard, instead of printer
        public const int Debug_Msg_Box = 8;                     // Enables debug message boxes (with OK button)
        public const int Debug_Out_Dbg_Str = 16;                // Write debug messages to debugger ("OutputDebugString")
        public const int Debug_Enter_Exit = 32;                 // Log entry into and exit from API functions

        //
        // Constant definitions
        //
        public const int Attr_Normal = 0;                       // Font normal
        public const int Attr_Bold = 1;                         // Font bold
        public const int Attr_Italic = 2;                       // Font italic
        public const int Attr_Underline = 4;                    // Font underline

        public const bool Orient_Landscape = false;             // Label landscape
        public const bool Orient_Portrait = true;               // Label portrait

        //
        // Label sizes
        //
        public const int Size_Standard = 1;                     // Address (SLP-100RL, -1RLB, -2RL, -2RLH, -4AST, -R2RL, -1RL, -TRL,-1BLB, -1GLB, -1OLB, -1PLB, -1YLB, -1RLC, -2RLC)
        public const int Size_Shipping = 2;                     // Shipping (SLP-SRL, -RSRL, -SRLB, -SRLC, -OPSRL)
        public const int Size_Diskette = 3;                     // Diskettel (SLP-DRL)
        public const int Size_Large = 4;                        // Large Address (SLP-2RLE)
        public const int Size_Multipurpose = 5;                 // Multipurpose (SLP-MRL, -MRLB, -RMRL, -TMRL, -MRLC, -OPMRL)
        public const int Size_VHS_Spine = 7;                    // VHS Spine (SLP-VSL)
        public const int Size_VHS_Face = 8;                     // VHS Face (SLP-VTL)
        public const int Size_8mm_Spine = 9;                    // DAT cassettes Spine (SLP-27210)
        public const int Size_Folder = 10;                      // File Folder (SLP-4AFL, -FLW, -FLB, -FLG, -FLR)
        public const int Size_35mm = 11;                        // 35mm Slide (SLP-35L, -35LY)
        public const int Size_Badge = 12;                       // Name Badge (SLP-NB, -NR, -NWB)
        public const int Size_Euro_N = 13;                      // Folder/Binder (SLP-FN)
        public const int Size_Euro_W = 14;                      // Folder/Binder (SLP-FW)
        public const int Size_ZipDisk = 15;                     // Zip Disk Label (SLP-ZIP)
        public const int Size_Jewelry = 17;                     // Jewellery Label (SLP-JWL)
        public const int Size_Euro_Name_Tag = 18;               // Name Badge Paper (SLP-ENT)
        public const int Size_Hanging_Folder_3 = 19;            // (欠番)
        public const int Size_Hanging_Folder_5 = 20;            // File Folder Paper (SLP-5HFL)
        public const int Size_Euro_Hanging_Folder = 21;         // (欠番)
        public const int Size_Fanfold_Card_Stock = 22;          // Appointment-, Business Card (SLP-FCS2)
        public const int Size_Euro_Name_Tag_Large = 23;         // Name Badge Paper (SLP-ENTL)
        public const int Size_Retail_Label = 24;                // Retail Label (SLP-RTL)
        public const int Size_Security_Paper = 25;              // Vouchers with hologram (SLP-DIA)
        public const int Size_Topcoated_Paper = 26;             // Receipt Paper (SLP-P150)
        public const int Size_Return_Address = 27;              // Return (SLP-RTN)
        public const int Size_Round = 28;                       // Round (SLP-RND)
        public const int Size_Multi_Part = 29;                  // Multi Part (SLP-MPL4)
        public const int Size_Clear_Return_Address = 30;        // Return (SLP-RTNC)
        public const int Size_Tamper_Proof_Label = 31;          // Security (SLP-TP)
        public const int Size_Postage_Label = 32;               // Digital stamps (SLP-STAMP1)
        public const int Size_Large_Postage_Label = 33;         // Digital stamps + logo (SLP-STAMP2)
        public const int Size_Round_2 = 34;                     // Round (SLP-RNO)

        //
        // Error Code
        //
        public const int SlpApi_Error_No_Error = 0;                    // No error
        public const int SlpApi_Error_No_Dc = -1;                      // Device context cannot create.
        public const int SlpApi_Error_Generic_Error = -2;              // An unexpected WIN32API error occurred.
        public const int SlpApi_Error_Bad_Label_Type = -3;             // Bad label type.
        public const int SlpApi_Error_Bad_Font_Handle = -4;            // The font handle is invalid.
        public const int SlpApi_Error_Bad_Thickness = -5;              // The parameter is incorrect.
        public const int SlpApi_Error_Bad_BarCode_Style = -6;          // The parameter of barcode style is invalid.
        public const int SlpApi_Error_Bad_Printer_Type = -7;           // Bad printer type.
        public const int SlpApi_Error_Invalid_Port = -8;               // Invalid port.
        public const int SlpApi_Error_Invalid_Label_Type = -9;         // The current label type is invalid.
        public const int SlpApi_Error_Invalid_BarCode_Handle = -10;    // A sequence error occurred while drawing the barcode.
        public const int SlpApi_Error_Invalid_Bitmap_Handle = -11;     // The file-path specified is invalid.
        public const int SlpApi_Error_No_BarCode_Lib = -12;            // The barcode library cannot open.
        public const int SlpApi_Error_Invalid_Library_Function = -13;  // Invalid library function.
        public const int SlpApi_Error_Printer_Open_Error = -14;        // The printer specified cannot open.
        public const int SlpApi_Error_Invalid_Image_File = -15;        // The image file is broken or bad format.
        public const int SlpApi_Error_Image_Error = -16;               // A error occurred while processing the image.
        public const int SlpApi_Error_Buffer_Too_Small = -17;          // The buffer-size specified is too small.
        public const int SlpApi_Error_Invalid_Error = -18;             // Invalid error code.
        public const int SlpApi_Error_Out_Of_Printable_Area = -19;     // A size specified is out of printable area.

        //
        // Bar Code Symbologies
        //
        public const int SLP_BC_CODE39 = 1;                            // Code 39
        public const int SLP_BC_CODE2OF5 = 2;                          // I-2 of 5
        public const int SLP_BC_CODABAR = 3;                           // Codabar
        public const int SLP_BC_CODE128 = 4;                           // Code 128
        public const int SLP_BC_UPC = 5;                               // UPC-A
        public const int SLP_BC_UPCE = 6;                              // UPC-E
        public const int SLP_BC_EAN13 = 7;                             // EAN-13
        public const int SLP_BC_POSTNET = 8;                           // POSTNET
        public const int SLP_BC_RM4SCC = 9;                            // RM4SCC
        public const int SLP_BC_EAN8 = 10;                             // EAN-8
        public const int SLP_BC_EAN128 = 11;                           // EAN128
        public const int SLP_BC_INTELLIGENT_MAIL_BARCODE = 12;         // Intelligent Mail Barcode
        public const int SLP_BC_MAXICODE = 20;                         // Maxicode
        public const int SLP_BC_PDF417 = 21;                           // PDF417
        public const int SLP_BC_DATAMATRIX = 22;                       // DataMatrix
        public const int SLP_BC_QR = 23;                               // QR Code
        public const int SLP_BC_GS1DB_OMNIDIRECTIONAL = 30;            // GS1 DataBar Omnidirectional
        public const int SLP_BC_GS1DB_TRUNCATED = 31;                  // GS1 DataBar Truncated
        public const int SLP_BC_GS1DB_STACKED = 32;                    // GS1 DataBar Stacked
        public const int SLP_BC_GS1DB_STACKED_OMNIDIRECTIONAL = 33;    // GS1 DataBar Stacked Omnidirectional
        public const int SLP_BC_GS1DB_LIMITED = 34;                    // GS1 DataBar Limited
        public const int SLP_BC_GS1DB_EXPANDED = 35;                   // GS1 DataBar Expanded
        public const int SLP_BC_GS1DB_EXPANDED_STACKED = 36;           // GS1 DataBar Expanded Stacked

        //
        // SlpApi Function
        //
        [DllImport("SlpApi.dll")]
        public extern static int SlpGetErrorCode();

        [DllImport("SlpApi.dll")]
        public extern static int SlpDeleteFont(IntPtr hFont);

        [DllImport("SlpApi.dll")]
        public extern static int SlpGetLabelHeight();

        [DllImport("SlpApi.dll")]
        public extern static int SlpGetLabelWidth();

        [DllImport("SlpApi.dll")]
        public extern static bool SlpDrawRectangle(int x, int y, int width, int height, int thickness);

        [DllImport("SlpApi.dll")]
        public extern static bool SlpDrawLine(int xStart, int yStart, int xEnd, int yEnd, int thickness);

        [DllImport("SlpApi.dll")]
        public extern static bool SlpStartLabel();

        [DllImport("SlpApi.dll")]
        public extern static bool SlpEndLabel();

        [DllImport("SlpApi.dll")]
        public extern static void SlpClosePrinter();

        [DllImport("SlpApi.dll")]
        public extern static void SlpCopyLabelToClipboard();

        [DllImport("SlpApi.dll")]
        public extern static void SlpDebugMode(uint dwMode);

        [DllImport("SlpApi.dll")]
        public extern static int SlpSetRotation(int nAngle);

        [DllImport("SlpApi.dll")]
        public extern static int SlpFindPrinters(bool bAllPrinters);

        [DllImport("SlpApi.dll")]
        public extern static int SlpGetPrinterDPI();

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static void SlpComment(string wszComment);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static bool SlpOpenPrinter(string szPrinterName, int nID, bool fPortrait);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static IntPtr SlpCreateFont(string wszName, int nPoints, int nAttributes);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static bool SlpDrawTextXY(int x, int y, IntPtr hFont, string wszText);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetTextWidth(IntPtr hFont, string wszText);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetTextHeight(IntPtr hFont, string wszText);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static bool SlpDrawBarCode(int nLeft, int nTop, int nRight, int nBottom,
                                                      string wszText);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static bool SlpDrawPicture(int nLeft, int nTop, int nRight, int nBottom,
                                                      string wszPath);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static bool SlpSetBarCodeStyle(int nSymbology, int nRatio, int nMode,
                                                          int nSecurity, bool bReadableText,
                                                          int nFontHeight, int nFontAttributes,
                                                          string wszFaceName);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetVersion(StringBuilder wszVersion);

        [DllImport("SlpApi.dll", CharSet = CharSet.Ansi)]
        public extern static int SlpGetVersionEx(StringBuilder wszVersion, int nMaxLength);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetPrinterName(int nIndex, StringBuilder wszPrinterName, int nMaxLength);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetErrorString(StringBuilder wszText, int nMaxLength);

        [DllImport("SlpApi.dll", CharSet = CharSet.Unicode)]
        public extern static int SlpGetBarCodeWidth(int nLeft, int nTop, int nRight, int nBottom, string lpText);
    }
}
