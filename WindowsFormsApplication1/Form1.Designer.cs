﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cardDataDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cardDataDataSet = new WindowsFormsApplication1.CardDataDataSet();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.actionBox = new System.Windows.Forms.ComboBox();
            this.idBox = new System.Windows.Forms.ComboBox();
            this.typeBox = new System.Windows.Forms.ComboBox();
            this.comPortBox = new System.Windows.Forms.ComboBox();
            this.startButton = new System.Windows.Forms.Button();
            this.partNoBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            this.statusLabel2 = new System.Windows.Forms.Label();
            this.statusLabel3 = new System.Windows.Forms.Label();
            this.statusLabel4 = new System.Windows.Forms.Label();
            this.successLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.balanceBox = new System.Windows.Forms.TextBox();
            this.printButton = new System.Windows.Forms.Button();
            this.successLabel2 = new System.Windows.Forms.Label();
            this.statusLabel = new System.Windows.Forms.Label();
            this.statusLabel5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.printingTypeBox = new System.Windows.Forms.ComboBox();
            this.printingIDBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.printingPrintButton = new System.Windows.Forms.Button();
            this.printingStatusLabel = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.labelCount = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.cardDataDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardDataDataSet)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cardDataDataSetBindingSource
            // 
            this.cardDataDataSetBindingSource.DataSource = this.cardDataDataSet;
            this.cardDataDataSetBindingSource.Position = 0;
            // 
            // cardDataDataSet
            // 
            this.cardDataDataSet.DataSetName = "CardDataDataSet";
            this.cardDataDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(465, 322);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(457, 296);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Card Programming";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 76F));
            this.tableLayoutPanel2.Controls.Add(this.actionBox, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.idBox, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.typeBox, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.comPortBox, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.startButton, 5, 12);
            this.tableLayoutPanel2.Controls.Add(this.partNoBox, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.stopButton, 4, 12);
            this.tableLayoutPanel2.Controls.Add(this.successLabel, 0, 12);
            this.tableLayoutPanel2.Controls.Add(this.label6, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.balanceBox, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.printButton, 3, 12);
            this.tableLayoutPanel2.Controls.Add(this.successLabel2, 0, 13);
            this.tableLayoutPanel2.Controls.Add(this.statusLabel, 3, 10);
            this.tableLayoutPanel2.Controls.Add(this.statusLabel5, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 3, 7);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 14;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(451, 290);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // actionBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.actionBox, 2);
            this.actionBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.actionBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.actionBox.FormattingEnabled = true;
            this.actionBox.Items.AddRange(new object[] {
            "Recycle Cards",
            "New Cards"});
            this.actionBox.Location = new System.Drawing.Point(3, 123);
            this.actionBox.Name = "actionBox";
            this.actionBox.Size = new System.Drawing.Size(219, 21);
            this.actionBox.TabIndex = 6;
            // 
            // idBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.idBox, 4);
            this.idBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.idBox.DropDownHeight = 410;
            this.idBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.idBox.FormattingEnabled = true;
            this.idBox.IntegralHeight = false;
            this.idBox.Location = new System.Drawing.Point(228, 73);
            this.idBox.MaxDropDownItems = 31;
            this.idBox.Name = "idBox";
            this.idBox.Size = new System.Drawing.Size(220, 21);
            this.idBox.TabIndex = 0;
            this.idBox.SelectedIndexChanged += new System.EventHandler(this.idBox_SelectedIndexChanged_1);
            // 
            // typeBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.typeBox, 2);
            this.typeBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.typeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.typeBox.FormattingEnabled = true;
            this.typeBox.Items.AddRange(new object[] {
            "Recent Transaction",
            "Service",
            "Cash Removed",
            "User",
            "Admin",
            "Cycle Kill",
            "Use Remaining Balance",
            "Unsigned",
            "Set Price",
            "Clear Counters",
            "Factory Test"});
            this.typeBox.Location = new System.Drawing.Point(3, 73);
            this.typeBox.Name = "typeBox";
            this.typeBox.Size = new System.Drawing.Size(219, 21);
            this.typeBox.TabIndex = 2;
            this.typeBox.SelectedIndexChanged += new System.EventHandler(this.typeBox_SelectedIndexChanged_1);
            // 
            // comPortBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.comPortBox, 4);
            this.comPortBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.comPortBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comPortBox.FormattingEnabled = true;
            this.comPortBox.Items.AddRange(new object[] {
            "COM3",
            "COM5",
            "COM8"});
            this.comPortBox.Location = new System.Drawing.Point(228, 23);
            this.comPortBox.Name = "comPortBox";
            this.comPortBox.Size = new System.Drawing.Size(220, 21);
            this.comPortBox.TabIndex = 3;
            // 
            // startButton
            // 
            this.startButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.startButton.Location = new System.Drawing.Point(379, 264);
            this.startButton.Name = "startButton";
            this.tableLayoutPanel2.SetRowSpan(this.startButton, 2);
            this.startButton.Size = new System.Drawing.Size(69, 23);
            this.startButton.TabIndex = 1;
            this.startButton.Text = "Program";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click_1);
            // 
            // partNoBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.partNoBox, 2);
            this.partNoBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.partNoBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.partNoBox.FormattingEnabled = true;
            this.partNoBox.Items.AddRange(new object[] {
            "Not Specified"});
            this.partNoBox.Location = new System.Drawing.Point(3, 23);
            this.partNoBox.Name = "partNoBox";
            this.partNoBox.Size = new System.Drawing.Size(219, 21);
            this.partNoBox.TabIndex = 7;
            this.partNoBox.SelectedIndexChanged += new System.EventHandler(this.partNoBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label1, 2);
            this.label1.Location = new System.Drawing.Point(3, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Part #:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label2, 4);
            this.label2.Location = new System.Drawing.Point(228, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "COM Port:";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label3, 4);
            this.label3.Location = new System.Drawing.Point(228, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Customer ID:";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label4, 2);
            this.label4.Location = new System.Drawing.Point(3, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Card Type:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label5, 2);
            this.label5.Location = new System.Drawing.Point(3, 107);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Operation:";
            // 
            // stopButton
            // 
            this.stopButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.stopButton.Location = new System.Drawing.Point(303, 264);
            this.stopButton.Name = "stopButton";
            this.tableLayoutPanel2.SetRowSpan(this.stopButton, 2);
            this.stopButton.Size = new System.Drawing.Size(69, 23);
            this.stopButton.TabIndex = 4;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click_1);
            // 
            // statusLabel2
            // 
            this.statusLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel2.AutoSize = true;
            this.statusLabel2.Location = new System.Drawing.Point(164, 34);
            this.statusLabel2.Name = "statusLabel2";
            this.statusLabel2.Size = new System.Drawing.Size(58, 13);
            this.statusLabel2.TabIndex = 14;
            this.statusLabel2.Text = "info label 2";
            // 
            // statusLabel3
            // 
            this.statusLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel3.AutoSize = true;
            this.statusLabel3.Location = new System.Drawing.Point(164, 17);
            this.statusLabel3.Name = "statusLabel3";
            this.statusLabel3.Size = new System.Drawing.Size(58, 13);
            this.statusLabel3.TabIndex = 15;
            this.statusLabel3.Text = "info label 3";
            // 
            // statusLabel4
            // 
            this.statusLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel4.AutoSize = true;
            this.statusLabel4.Location = new System.Drawing.Point(164, 2);
            this.statusLabel4.Name = "statusLabel4";
            this.statusLabel4.Size = new System.Drawing.Size(58, 13);
            this.statusLabel4.TabIndex = 16;
            this.statusLabel4.Text = "info label 4";
            // 
            // successLabel
            // 
            this.successLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.successLabel.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.successLabel, 3);
            this.successLabel.Location = new System.Drawing.Point(3, 261);
            this.successLabel.Name = "successLabel";
            this.successLabel.Size = new System.Drawing.Size(52, 13);
            this.successLabel.TabIndex = 17;
            this.successLabel.Text = "success?";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.label6, 4);
            this.label6.Location = new System.Drawing.Point(228, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Balance to Load:";
            // 
            // balanceBox
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.balanceBox, 4);
            this.balanceBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.balanceBox.Location = new System.Drawing.Point(228, 123);
            this.balanceBox.Name = "balanceBox";
            this.balanceBox.Size = new System.Drawing.Size(220, 20);
            this.balanceBox.TabIndex = 19;
            // 
            // printButton
            // 
            this.printButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.printButton.Location = new System.Drawing.Point(228, 264);
            this.printButton.Name = "printButton";
            this.tableLayoutPanel2.SetRowSpan(this.printButton, 2);
            this.printButton.Size = new System.Drawing.Size(69, 23);
            this.printButton.TabIndex = 20;
            this.printButton.Text = "Print";
            this.printButton.UseVisualStyleBackColor = true;
            this.printButton.Click += new System.EventHandler(this.printButton_Click);
            // 
            // successLabel2
            // 
            this.successLabel2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.successLabel2.AutoSize = true;
            this.successLabel2.Location = new System.Drawing.Point(3, 276);
            this.successLabel2.Name = "successLabel2";
            this.successLabel2.Size = new System.Drawing.Size(58, 13);
            this.successLabel2.TabIndex = 21;
            this.successLabel2.Text = "success2?";
            // 
            // statusLabel
            // 
            this.statusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.statusLabel, 3);
            this.statusLabel.Location = new System.Drawing.Point(390, 231);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(58, 13);
            this.statusLabel.TabIndex = 5;
            this.statusLabel.Text = "info label 1";
            // 
            // statusLabel5
            // 
            this.statusLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.statusLabel5.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.statusLabel5, 3);
            this.statusLabel5.Location = new System.Drawing.Point(390, 247);
            this.statusLabel5.Name = "statusLabel5";
            this.statusLabel5.Size = new System.Drawing.Size(58, 13);
            this.statusLabel5.TabIndex = 22;
            this.statusLabel5.Text = "info label 5";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(485, 284);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Printing";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.printingTypeBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.printingIDBox, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.printingPrintButton, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.printingStatusLabel, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.labelCount, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(479, 278);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // printingTypeBox
            // 
            this.printingTypeBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.printingTypeBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.printingTypeBox.FormattingEnabled = true;
            this.printingTypeBox.Items.AddRange(new object[] {
            "Recent Transaction",
            "Service",
            "Cash Removed",
            "User",
            "Admin",
            "Cycle Kill",
            "Use Remaining Balance",
            "Unsigned",
            "Set Price",
            "Clear Counters",
            "Factory Test"});
            this.printingTypeBox.Location = new System.Drawing.Point(3, 23);
            this.printingTypeBox.Name = "printingTypeBox";
            this.printingTypeBox.Size = new System.Drawing.Size(233, 21);
            this.printingTypeBox.TabIndex = 0;
            // 
            // printingIDBox
            // 
            this.printingIDBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.printingIDBox.DropDownHeight = 410;
            this.printingIDBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.printingIDBox.FormattingEnabled = true;
            this.printingIDBox.IntegralHeight = false;
            this.printingIDBox.Location = new System.Drawing.Point(242, 23);
            this.printingIDBox.MaxDropDownItems = 31;
            this.printingIDBox.Name = "printingIDBox";
            this.printingIDBox.Size = new System.Drawing.Size(234, 21);
            this.printingIDBox.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Card Type:";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(242, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Customer ID:";
            // 
            // printingPrintButton
            // 
            this.printingPrintButton.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.printingPrintButton.Location = new System.Drawing.Point(401, 251);
            this.printingPrintButton.Name = "printingPrintButton";
            this.printingPrintButton.Size = new System.Drawing.Size(75, 23);
            this.printingPrintButton.TabIndex = 4;
            this.printingPrintButton.Text = "Print";
            this.printingPrintButton.UseVisualStyleBackColor = true;
            this.printingPrintButton.Click += new System.EventHandler(this.printingPrintButton_Click);
            // 
            // printingStatusLabel
            // 
            this.printingStatusLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.printingStatusLabel.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.printingStatusLabel, 2);
            this.printingStatusLabel.Location = new System.Drawing.Point(390, 231);
            this.printingStatusLabel.Name = "printingStatusLabel";
            this.printingStatusLabel.Size = new System.Drawing.Size(86, 13);
            this.printingStatusLabel.TabIndex = 5;
            this.printingStatusLabel.Text = "printing info label";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "# of Labels";
            // 
            // labelCount
            // 
            this.labelCount.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelCount.Location = new System.Drawing.Point(3, 73);
            this.labelCount.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.labelCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(233, 20);
            this.labelCount.TabIndex = 7;
            this.labelCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.statusLabel2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.statusLabel3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.statusLabel4, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(222, 44);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(225, 180);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel4, 3);
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(226, 48);
            this.tableLayoutPanel4.TabIndex = 24;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 322);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Test PicoPass";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cardDataDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardDataDataSet)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.labelCount)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.BindingSource cardDataDataSetBindingSource;
        private CardDataDataSet cardDataDataSet;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox actionBox;
        private System.Windows.Forms.ComboBox idBox;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.ComboBox comPortBox;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.ComboBox partNoBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label statusLabel;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label statusLabel2;
        private System.Windows.Forms.Label statusLabel3;
        private System.Windows.Forms.Label statusLabel4;
        private System.Windows.Forms.Label successLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox balanceBox;
        private System.Windows.Forms.Button printButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox printingTypeBox;
        private System.Windows.Forms.ComboBox printingIDBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button printingPrintButton;
        private System.Windows.Forms.Label printingStatusLabel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown labelCount;
        private System.Windows.Forms.Label successLabel2;
        private System.Windows.Forms.Label statusLabel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;


    }
}

