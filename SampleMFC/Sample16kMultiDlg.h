// Sample16kMultiDlg.h : header file
//
//{{AFX_INCLUDES()
//}}AFX_INCLUDES

#if !defined(AFX_SAMPLE16KMULTIDLG_H__2871C133_1046_491A_A27B_AA80B9823E85__INCLUDED_)
#define AFX_SAMPLE16KMULTIDLG_H__2871C133_1046_491A_A27B_AA80B9823E85__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSample16kMultiDlg dialog

class CSample16kMultiDlg : public CDialog
{
// Construction
public:
	CSample16kMultiDlg(CWnd* pParent = NULL);	// standard constructor

	boolean m_bConnected;
	int		m_iSelectedKey;
	BYTE	m_abSerialNumber[10];

// Dialog Data
	//{{AFX_DATA(CSample16kMultiDlg)
	enum { IDD = IDD_FROM_MAIN };
	CComboBox	m_cmbAuth;
	CButton	m_rdNoAuth;
	CButton	m_rdKd;
	CButton	m_rdKc;
	CComboBox	m_cmbPage;
	CComboBox	m_cmbBlock;
	CString	m_lblConfBlock;
	CString	m_lblConnectionStatus;
	CString	m_lblData;
	CString	m_lblSN;
	CString	m_txtComPort;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample16kMultiDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSample16kMultiDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	afx_msg void OnBtConnect();
	afx_msg void OnBtDisconnect();
	afx_msg void OnBtLoadKeys();
	afx_msg void OnBtRead();
	afx_msg void OnBtSelectChip();
	afx_msg void OnBtSelectPage();
	afx_msg void OnBtWrite();
	afx_msg void OnSelchangeCmbAuth();
	afx_msg void OnBtHalt();
	afx_msg void OnBtReselect();
	afx_msg void OnBtAuthentify();
	afx_msg void OnBtRead4();
	afx_msg void OnBtDisable();
	afx_msg void OnBtEnable();
	afx_msg void OnBtGetStatus();
	afx_msg void OnBtSetStatus();
	afx_msg void OnBtEasDetect();
	afx_msg void OnBtEasActivate();
	afx_msg void OnBtEasDeactivate();
	afx_msg void OnBtSetUserProtocol();
	afx_msg void OnBTDecrease();
	afx_msg void OnBtIncrease();
	afx_msg void OnChangeTxtComport();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAMPLE16KMULTIDLG_H__2871C133_1046_491A_A27B_AA80B9823E85__INCLUDED_)
