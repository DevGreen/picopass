// Sample16kMulti.h : main header file for the SAMPLE16KMULTI application
//

#if !defined(AFX_SAMPLE16KMULTI_H__C1572778_AA0A_464E_9C7F_9BB5606FFEDC__INCLUDED_)
#define AFX_SAMPLE16KMULTI_H__C1572778_AA0A_464E_9C7F_9BB5606FFEDC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSample16kMultiApp:
// See Sample16kMulti.cpp for the implementation of this class
//

class CSample16kMultiApp : public CWinApp
{
public:
	CSample16kMultiApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSample16kMultiApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSample16kMultiApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAMPLE16KMULTI_H__C1572778_AA0A_464E_9C7F_9BB5606FFEDC__INCLUDED_)
