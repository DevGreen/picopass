//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Sample16kMulti.rc
//
#define IDD_SAMPLE16KMULTI_DIALOG       102
#define IDD_FROM_MAIN                   102
#define IDR_MAINFRAME                   128
#define IDC_GP_COMMANDS                 1000
#define IDC_GP_RESULTS                  1001
#define IDC_BT_CONNECT                  1002
#define IDC_BT_DISCONNECT               1003
#define IDC_BT_LOAD_KEYS                1004
#define IDC_BT_SELECT_CHIP              1005
#define IDC_BT_SELECT_PAGE              1006
#define IDC_BT_READ                     1007
#define IDC_BT_WRITE                    1008
#define IDC_RD_NOAUTH                   1009
#define IDC_BT_HALT                     1009
#define IDC_RD_KD                       1010
#define IDC_BT_RESELECT                 1010
#define IDC_RD_KC                       1011
#define IDC_BT_AUTHENTIFY               1011
#define IDC_CMB_PAGE                    1012
#define IDC_CMB_BLOCK                   1013
#define IDC_LBL_CONNECTION_STATUS       1014
#define IDC_LBL_SN                      1015
#define IDC_LBL_CONF_BLOCK              1016
#define IDC_LBL_DATA                    1017
#define IDC_MXPANEL                     1018
#define IDC_BT_READ4                    1018
#define IDC_BT_EAS_DETECT               1019
#define IDC_BT_EAS_DEACTIVATE           1020
#define IDC_BT_EAS_ACTIVATE             1021
#define IDC_CMB_AUTH                    1022
#define IDC_BT_DISABLE                  1023
#define IDC_BT_ENABLE                   1024
#define IDC_BT_GET_STATUS               1025
#define IDC_TXT_COMPORT                 1026
#define IDC_BT_SET_STATUS               1027
#define IDC_BT_SET_USER_PROTOCOL        1028
#define IDC_BT_INCREASE                 1029
#define IDC_BT_Decrease                 1030

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1027
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
