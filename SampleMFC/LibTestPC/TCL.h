//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	TCL.c
//		High level functions for T=CL Communication and MicroPass management
//
// Documentation :
//		These functions are detailed in the MicroPass OS-J1 & OS-J2 datasheet
//
// Version :    2.0.0
// Date :       January, 15th 2006
// Author :     Frederic CAPASSO
//
//------------------------------------------------------------------

#include <math.h> // ANSI C official library for mathematical computations
#include "function.c"

// T=CL parameters structure declaration
struct TTCLConfiguration
{
	BYTE	f_iBlockNumber;
	BYTE	f_bCID;
	BYTE	f_bNAD;
	BOOL	f_bCIDSupported;
    BOOL	f_bNADSupported;
        
	BYTE	f_bRetryCount;
    int		f_eCardType,
			f_ePCDBitRate, 
			f_ePICCBitRate;

    int		f_iPICCMaximumInputFrameSize,
			f_iPCDMaximumInputFrameSize;
    
	double	f_dPICCMaximumFrameWaitingTime,
			f_dPCDWaitingTimeBeforeSendingAnotherFrame;
} g_TCLCard;


//=======================================================================================
// Private functions for T=CL blocks generation
//		DO NOT USE OUTSIDE THIS FILE!!!!
//=======================================================================================




//========================================================================
// function 	CRC16ShiftRight
// In          p_abData
//             p_iDataLen
//             p_wPreset
//             p_wPolynom
// Return      CRC16
// Usage			The CRC16 shifted by right of the p_iDataLen BYTEs contained
//					p_abData buffer.
//========================================================================
WORD CRC16ShiftRight(	BYTE	*p_abData,
						int		p_iDataLen,
						WORD	p_wPreset,
						WORD    p_wPolynom)
{
	int 	l_iI, l_iJ;
	WORD  	l_wBit0;
	WORD	l_wResult;

	//INIT RESULT WITH PRESET
   l_wResult = p_wPreset ;

   // FOR EACH VALUES IN TAB
   for( l_iI = 0 ; l_iI < p_iDataLen; l_iI++)
   {
      // RESULT EQAL AT RESULT ^ TAB ENTRY
      l_wResult = l_wResult ^ p_abData[l_iI];

      // FOR EACH BITS
      for( l_iJ = 0; l_iJ < 8 ; l_iJ++)
      {
         //GET FIRST BIT
         l_wBit0 = ( l_wResult & 0x0001 ) ;

         // GO NEXT BIT ( RIGHT SCALE)
         l_wResult = l_wResult >> 1 ;

         // IF CURRENT BIT SET ; 0
         if (l_wBit0)
            // RESULT EQUAL RESULT ^ 0
            l_wResult = l_wResult ^ p_wPolynom ;
      }
   }

   return l_wResult ;
} // CRC16ShiftRight()

//=======================================================================================
// Routine fnv_TCLFrameBuild
// In       p_abDataIn
//          p_iDataInLength
//
// Out      p_abTCLFrameIn
//          p_iTCLFrameInLength
//          p_iTCLFrameOutLength
//
// Return   - None -
//
// Usage    This function creates the TCL object and initialize its parameters.
//=======================================================================================
void fnv_TCLFrameBuild	(	BYTE	p_ePCBType,
							BYTE	*p_abCommand,
							BYTE	*p_abDataIn,
							BYTE	p_iDataInLength,
							BYTE	*p_abTCLFrameIn,
							int		*p_iTCLFrameInLength,
							int		*p_iTCLFrameOutLength)
{
	int		l_iShiftPosition;
	BYTE	l_bTemp;

	// INIT TCL FRAME IN LENGTH
	p_iTCLFrameInLength[0] = 0;

	// CHECK IF COMMAND
	if (p_abCommand != NULL)
	{
		// COPY COMMAND IN OUTPUT BUFFER
		memcpy(p_abTCLFrameIn, p_abCommand, 5);

		// SET TCL FRAME IN LENGTH
		p_iTCLFrameInLength[0] =  5 ;
	}

	// CHECK IF DATA IN
	if (p_abDataIn != NULL) 
	{
		// ADD DATA IN
		memcpy(&p_abTCLFrameIn[p_iTCLFrameInLength[0]], p_abDataIn, p_iDataInLength);

		// INIT OUT BUFFER LENGTH WITH IN BUFFER LENGTH + COMMAND LENGTH
		p_iTCLFrameInLength[0] += p_iDataInLength;
	}

	// SHIFT OF 1 BYTE TO ADD PCB
	memmove(&p_abTCLFrameIn[1], p_abTCLFrameIn, p_iTCLFrameInLength[0]);
	//memcpy(&p_abTCLFrameIn[1], p_abTCLFrameIn, p_iTCLFrameInLength[0]);

	// INC IN AND OUT TCL FRAME LENGTH
	p_iTCLFrameInLength[0]++;
	p_iTCLFrameOutLength[0]++;

	// ADD PCB
	switch (p_ePCBType)
	{
		case CST_pcbtINoChaining:
			l_bTemp = PCB_I;
			p_abTCLFrameIn[0] = (BYTE)(l_bTemp | (g_TCLCard.f_iBlockNumber & 0x01));
			break;
		case CST_pcbtIWithChaining:
			p_abTCLFrameIn[0] = (BYTE)(PCB_I | PCB_I_WITH_CHAINING | (g_TCLCard.f_iBlockNumber & 0x01));
			break;
		case CST_pcbtRACK:
			p_abTCLFrameIn[0] = (BYTE)(PCB_R | PCB_R_WITH_ACK | (g_TCLCard.f_iBlockNumber & 0x01));
			break;
		case CST_pcbtRNACK:
			p_abTCLFrameIn[0] = (BYTE)(PCB_R | PCB_R_WITH_NACK | (g_TCLCard.f_iBlockNumber & 0x01));
			break;
		case CST_pcbtSWTX:
			p_abTCLFrameIn[0] = (BYTE)(PCB_S | PCB_S_WITH_WTX);
			break;
		case CST_pcbtDESELECT:
			p_abTCLFrameIn[0] = (BYTE)(PCB_S | PCB_S_WITH_DESELECT);
			break;
	}


	// CHECK IF CID
	if (g_TCLCard.f_bCIDSupported)
	{
		// SHIFT OF 1 BYTE FOR ADDING CID
		memmove(&p_abTCLFrameIn[2], &p_abTCLFrameIn[1], p_iTCLFrameInLength[0]);
		
		// ADD CID BIT IN PCB
		p_abTCLFrameIn[0] = p_abTCLFrameIn[0] | PCB_WITH_CID;
		
		// SET CID
		p_abTCLFrameIn[1] = g_TCLCard.f_bCID;
		
		// INC OUT BUFFER LENGTH
		p_iTCLFrameInLength[0]++;
		p_iTCLFrameOutLength[0]++;
	}

	// CHECK IF NAD SUPPORTED AND NAD != 0  AND PCB TYPE IS PCB_I
	if (g_TCLCard.f_bNADSupported && (g_TCLCard.f_bNAD != 0) && ((p_ePCBType == CST_pcbtINoChaining) || (p_ePCBType == CST_pcbtIWithChaining)))
	{
		// IF CID SUPPORTED  SHIFT FROM 2 EITHER SHIFT FROM ONE
		l_iShiftPosition = 1;
		if (g_TCLCard.f_bCIDSupported) 
			l_iShiftPosition = 2;
		
		// SHIFT OUT BUFFER FOR ADDING NAD
		memmove(&p_abTCLFrameIn[l_iShiftPosition] , &p_abTCLFrameIn[l_iShiftPosition + 1], p_iTCLFrameInLength[0]);
		
		// ADD NAD BIT IN PCB
		p_abTCLFrameIn[0] = p_abTCLFrameIn[0] | PCB_I_WITH_NAD ;
		
		// SET NAD
		p_abTCLFrameIn[l_iShiftPosition] = g_TCLCard.f_bNAD ;
		
		// INC OUT BUFFER LENGTH
		p_iTCLFrameInLength[0]++;
		p_iTCLFrameOutLength[0]++;
	}

	// ADD STATUS BYTES
	p_iTCLFrameOutLength[0] += 2;
} // fnv_TCLFrameBuild()


//=======================================================================================
// Routine fnb_TCLFrameExtract
// In       p_sFrame
//
// Out      p_sData
//
// Return   none
//
// Usage    This function extracts the data from a TCL frame and returns its type.
//=======================================================================================
BYTE fnb_TCLFrameExtract(	BYTE	*p_iTCLFrameLength,
							BYTE	*p_abTCLFrame,
							BYTE	*p_abData,
							WORD	*p_wStatus)
{
	BYTE	l_abTCLFrame[255];
	WORD	l_wCRCPreset, l_wCRC;
	BOOL	l_bCRCInvert;
	int		l_iLength, l_iIndex;
	BYTE	l_bCRCLow,
			l_bCRCHigh,
			l_bPCB;

	// Transfert data
	memcpy(l_abTCLFrame, p_abTCLFrame, p_iTCLFrameLength[0]);

	// SEARCH CRC ONY IF STATUS IS STATUS_CARD_NOT_IDENTIFIED
	if (p_wStatus[0] == STATUS_CARD_NOT_IDENTIFIED)
	{
		// SET CRC PARAMETERS
		l_wCRCPreset = ISO_14443_A_CRC_PRESET;
		l_bCRCInvert = FALSE;
		if (g_TCLCard.f_eCardType == CST_tclctISO_14443_B_4) 
		{
			l_wCRCPreset = ISO_14443_B_CRC_PRESET;
			l_bCRCInvert = TRUE;
		}

		// ANALYSE FRAME
		for (l_iLength = (p_iTCLFrameLength[0]); l_iLength > 0; l_iLength--) 
		{
			// CALCULATE CRC
			l_wCRC = CRC16ShiftRight(l_abTCLFrame, l_iLength, l_wCRCPreset, ISO_14443_A_B_CRC_POLYNOM);
			
			// INVERT CRC IF NEEDED
			if (l_bCRCInvert)
				l_wCRC = l_wCRC ^ 0xFFFF;
			
			// GET CRC LOW AND HIGH
			l_bCRCLow  = (BYTE)(l_wCRC & 0xFF);
			l_bCRCHigh = (BYTE)(l_wCRC >> 8);
			
			// CHECK IF CRC IS CORRECT
			if ((l_bCRCLow == l_abTCLFrame[l_iLength]) && (l_bCRCHigh == l_abTCLFrame[l_iLength + 1])) 
				break;
		}

		// CHECK IF LENGTH EXCEEDED
		if (l_iLength == 0) 
			return CST_pcbtError;

		p_iTCLFrameLength[0] = l_iLength;
	}
	else
	  // LENGTH IS THE TCL FRAME LENGTH BECAUSE THERE ARE NO CRC BYTES
	  l_iLength = p_iTCLFrameLength[0];

	// SAVE PCB
	l_bPCB = l_abTCLFrame[0];

	// I BLOCK
	if ((l_bPCB & 0xE2) == PCB_I)
	{
		// REMOVE PCB
		memcpy(l_abTCLFrame, l_abTCLFrame + 1, --l_iLength);

		// REMOVE CID IF PRESENT
		if ((l_bPCB & 0x08) == 0x08)
		 memcpy(l_abTCLFrame, l_abTCLFrame + 1, --l_iLength);

		// REMOVE NAD IF PRESENT
		if ((l_bPCB & 0x04) == 0x04) 
		 memcpy(l_abTCLFrame, l_abTCLFrame + 1, --l_iLength);

		// CHECK IF CHAINING
		if ((l_bPCB & 0x10) == 0x10)
		{
			 // Transfer data
			 memcpy(p_abData, l_abTCLFrame, l_iLength);

			 // RESULT
			 return CST_pcbtIWithChaining ;
		}
		else 
		{
			// Transfer data
			memcpy(p_abData, l_abTCLFrame, l_iLength - 2);

			//GET STATUS
			p_wStatus[0] = (l_abTCLFrame[l_iLength - 2] << 8) | l_abTCLFrame[l_iLength - 1];

			// RESULT
			return CST_pcbtINoChaining ;
		}
	}

	// R BLOCK
	if ((l_bPCB & 0xE2) == PCB_R) 
	{
		//CHECK IF ACK OR NACK
		if ((l_bPCB & 0x10) == 0x10) 
			return CST_pcbtRNACK;
		else
			return CST_pcbtRACK ;
	}

	// S BLOCK
	if ((l_bPCB & 0xC2) == PCB_S)
	{
		// CHECK IF SWTX OR DESELECT
		if ((l_bPCB & PCB_S_WITH_WTX) == PCB_S_WITH_WTX) 
		{
			// CHECK IF CID PRESENT
			l_iIndex =  1;
			if ((l_bPCB & 0x08) == 0x08)
				l_iIndex = 2 ;
			
			// GET WTX VALUE
			memcpy(p_abData, &l_abTCLFrame[l_iIndex], 1);
			
			// SET RESULT
			return CST_pcbtSWTX ;
		}

		return CST_pcbtDESELECT;
	}

	// Default path: return error
	return CST_pcbtError;
} // fnb_TCLFrameExtract()

//=======================================================================================
// Routine fni_TCLFrameIPrologueEpilogueLength
// In       - None -
//
// Out      - None -
//
// Return   the standard I-Block length according to chip feature.
//
// Usage    This function retreives the default I Block length.
//=======================================================================================
int fni_TCLFrameIPrologueEpilogueLength()
{
	int l_iResult;	

	//PCB
	l_iResult = 1;

	//IF CID SUPPORTED
	if (g_TCLCard.f_bCIDSupported)
		l_iResult++;

	//CHECK IF NAD SUPPORTED AND NAD != 0
	if (g_TCLCard.f_bNADSupported && (g_TCLCard.f_bNAD != 0))
		l_iResult++;

	//ADD CRC BYTES
	l_iResult += 2;

	return l_iResult;
} // fni_TCLFrameIPrologueEpilogueLength()




//=======================================================================================
// T=CL functions
//=======================================================================================





//=======================================================================================
// Routine CLib_b_CardConnect
// In       p_bResetRFField
//			p_eCardType
//          p_iCID
//          p_iReaderMaxInputFrameSize
//          p_eProtocolSpeed
//
// Out      p_abSN
//			p_bSize_SN
//			p_abRATS_RATTRIB
//			p_bSize_RATS_RATTRIB
//
// Return   Result of card connection (TRUE/FALSE)
//
// Usage    This function resets the RF field is requested and try to perform the requested
//          anti-collisions ISO-14443-A/B-3, and establishes with a card a T=CL channel.
//=======================================================================================
_CDECL_EXPORT_ BOOL _CDECL_FUNCTION_ CLib_b_CardConnect(BOOL	p_bResetRFField,
													BYTE	p_eCardType,
													BYTE	p_bCID,
													int		p_iPCDMaximumInputFrameSize,
													BYTE	p_eProtocolSpeed,
													BYTE	*p_abSN,
													BYTE	*p_bSize_SN,
													BYTE	*p_abRATS_RATTRIB,
													BYTE	*p_bSize_RATS_RATTRIB)
{
	BYTE	l_abTypeSN[15];
	BYTE	l_abATQB[63];
	BYTE	l_abDataOut[63];
	BYTE	l_abData[63];

	int		l_iLength,
			l_iData;

	WORD	l_wCRC,
			l_wStatus;

	BYTE	l_bProtocolInfo1,
			l_bProtocolInfo2,
			l_bProtocolInfo3,
			l_bProtocolConfig1,
			l_bProtocolConfig2,
			l_bCRCHigh,
			l_bCRCLow;


	// Check if Reset Field is requested
	if (p_bResetRFField)
	{
		// Reset Field (low level)
		CLib_w_ResetField();
		
		// Wait 5 ms because a card needs these 5 ms to be in IDLE mode
		Sleep(5);
	}

	// Initialize T=CL parameters
	g_TCLCard.f_iBlockNumber								= 0;
	g_TCLCard.f_bNADSupported								= FALSE;
	g_TCLCard.f_bCIDSupported								= FALSE;
	g_TCLCard.f_bCID										= 0;
	g_TCLCard.f_bNAD										= 0;
	g_TCLCard.f_ePCDBitRate									= CST_tclbr106kbps;
	g_TCLCard.f_ePICCBitRate								= CST_tclbr106kbps;
	g_TCLCard.f_dPCDWaitingTimeBeforeSendingAnotherFrame	= 0;
	g_TCLCard.f_eCardType									= CST_tclctISO_14443_A_B_4;
	g_TCLCard.f_iPCDMaximumInputFrameSize					= p_iPCDMaximumInputFrameSize;

	// Check if selected protocols include ISO 14443 A-4
	if ((p_eCardType == CST_tclctISO_14443_A_B_4) || (p_eCardType == CST_tclctISO_14443_A_4))
	{
		// Set User's protocol (low level)
			l_bProtocolConfig1 = 0xFF;
			l_bProtocolConfig2 = 0x63;
			CLib_w_SetStatus(COUPLER_RAM, 0x6B, &l_bProtocolConfig1);
			CLib_w_SetStatus(COUPLER_RAM, 0x64, &l_bProtocolConfig2);
 			CLib_w_SetStatus(COUPLER_RAM, 0x65, &l_bProtocolConfig2);
		l_bProtocolConfig1 = 0x32;
		l_bProtocolConfig2 = 0x12;
		l_wStatus = CLib_w_SetStatus(COUPLER_RAM, 0x5E, &l_bProtocolConfig1);
		if (l_wStatus = STATUS_OK)
		{
			l_wStatus = CLib_w_SetStatus(COUPLER_RAM, 0x5F, &l_bProtocolConfig2);
			if (l_wStatus != STATUS_OK) return FALSE;
		}
		else
			return FALSE;

		// Try to select a card
		if (CLib_w_SelectCard(0x00, 0x08, l_abTypeSN) == STATUS_OK)
		{
			// Return Serial Number
			memcpy(p_abSN, &l_abTypeSN[1], 0x0C);
			p_bSize_SN[0] = 0x0C;
		 
			// Send RATS
			l_abData[0x00] = ISO_14443_A_RATS;
			l_abData[0x01] = (p_bCID & 0x0F);
		 
			// Add FSDI wich is the maximum frame size by the PCD
			switch (p_iPCDMaximumInputFrameSize)
			{
				case 0x10: 
					l_abData[1] = l_abData[1];
					break;
				case 0x18: 
					l_abData[1] = l_abData[1] | 0x10;
					break;
				case 0x20: 
					l_abData[1] = l_abData[1] | 0x20;
					break;
				case 0x28: 
					l_abData[1] = l_abData[1] | 0x30;
					break;
				case 0x30: 
					l_abData[1] = l_abData[1] | 0x40;
					break;
				case 0x40: 
					l_abData[1] = l_abData[1] | 0x50;
					break;
				case 0x60: 
					l_abData[1] = l_abData[1] | 0x60;
					break;
				case 0x80: 
					l_abData[1] = l_abData[1] | 0x70;
					break;
				case 0x100: 
					l_abData[1] = l_abData[1] | 0x80;
					break;
				default: 
					return FALSE;
			}
		 
			// Send RATS
			if (CLib_w_SendISOCommand(	ISO_IN_OUT,
										0x20,
										0x80,
										0xC2,
										0xF7,
										0x20,
										0x02,
										l_abData,
										l_abDataOut) == STATUS_FILE_NOT_FOUND) return FALSE;

			// Get Response
			CLib_w_GetResponse(0x20, l_abDataOut);

			// Copy out buffer 
			memcpy(l_abData, l_abDataOut, 0x20);

			// Analyze frame
			l_iLength = l_abData[0x00];
		 
			// Verify CRC
			l_wCRC		= CRC16ShiftRight(l_abData, l_iLength, ISO_14443_A_CRC_PRESET, ISO_14443_A_B_CRC_POLYNOM);
			l_bCRCLow	= (BYTE)(l_wCRC & 0xFF);
			l_bCRCHigh	= (BYTE)(l_wCRC >> 0x08);
		 
			// Check if CRC is correct
			if ((l_bCRCLow != l_abData[l_iLength]) || (l_bCRCHigh != l_abData[l_iLength + 1]))
				return FALSE;
		 
			// Assign RATS
			memcpy(p_abRATS_RATTRIB, l_abData, l_iLength);
			p_bSize_RATS_RATTRIB[0] = l_iLength;
		 
			// Set PCD waiting time before sending another frame (DEFAULT VALUE : 1172 / fc)
			g_TCLCard.f_dPCDWaitingTimeBeforeSendingAnotherFrame = 1172 / 13560000;
		 
			// Initialize ATS
			if (l_iLength > 1)
			{
				//DECODE PICC MAXIMUM INPUT FRAME SIZE
				switch (l_abData[1] >> 4) 
				{
					case 0: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x10;
						break;
					case 1:
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x18;
						break;
					case 2: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x20;
						break;
					case 3: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x28;
						break;
					case 4: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x30;
						break;
					case 5: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x40;
						break;
					case 6: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x60;
						break;
					case 7: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x80;
						break;
					case 8: 
						g_TCLCard.f_iPICCMaximumInputFrameSize = 0x100;
						break;
					default: 
						return FALSE;
				}

				// Check if TA(1) is present
				if ((l_abData[1] & 0x10) == 0x10)
				{
					// Get protocol infor 1
					l_bProtocolInfo1 = l_abData[2];

					// Detect PCD bit rate supported by PICC
					if ((l_bProtocolInfo1 & 0x80) == 0x00)
					{
					  //GET PICC BIT RATE & PCD BIT RATE ACCEPTANCE
					  if ((l_bProtocolInfo1 & 0x10) == 0x10) g_TCLCard.f_ePICCBitRate = CST_tclbr212kbps;
					  if ((l_bProtocolInfo1 & 0x20) == 0x20) g_TCLCard.f_ePICCBitRate = CST_tclbr424kbps;
					  if ((l_bProtocolInfo1 & 0x40) == 0x40) g_TCLCard.f_ePICCBitRate = CST_tclbr847kbps;
					  if ((l_bProtocolInfo1 & 0x01) == 0x01) g_TCLCard.f_ePCDBitRate  = CST_tclbr212kbps;
					  if ((l_bProtocolInfo1 & 0x02) == 0x02) g_TCLCard.f_ePCDBitRate  = CST_tclbr424kbps;
					  if ((l_bProtocolInfo1 & 0x04) == 0x04) g_TCLCard.f_ePCDBitRate  = CST_tclbr847kbps;
					}
					else 
					{
					  //PCD & PICC SAME BIT RATE
					  if ((l_bProtocolInfo1 & 0x10) == 0x10) g_TCLCard.f_ePICCBitRate = CST_tclbr212kbps;
					  if ((l_bProtocolInfo1 & 0x20) == 0x20) g_TCLCard.f_ePICCBitRate = CST_tclbr424kbps;
					  if ((l_bProtocolInfo1 & 0x40) == 0x40) g_TCLCard.f_ePICCBitRate = CST_tclbr847kbps;

					  //SET PDC BIT RATE IDENTICAL
					  g_TCLCard.f_ePCDBitRate = g_TCLCard.f_ePICCBitRate ;
					}
					
					if ((l_bProtocolInfo1 & 0x08) == 0x08) return FALSE;
				}

				// Check if TB(1) is present
				if ((l_abData[1] & 0x20) == 0x20) 
				{
					// Get protocol info 2
					l_bProtocolInfo2 = l_abData[3];

					// Get the time wich the PCD must wait before sending another frame
					g_TCLCard.f_dPCDWaitingTimeBeforeSendingAnotherFrame = (256 * 16 / 13560000) * pow(2, l_bProtocolInfo2 & 0x0F) ;

					// Get the maximum time the PCD have to wait for receiing the response of the PICC
					g_TCLCard.f_dPICCMaximumFrameWaitingTime = ((256 * 16) / 13560000 ) * pow(2, l_bProtocolInfo2 >> 4) ;
				}
				
				//CHECK IF TC(1) IS PRESENT
				if ((l_abData[1] & 0x40) == 0x40)
				{
				   //GET PROTOCOL INFO 3
				   l_bProtocolInfo3 = l_abData[4];
   
				   //CHECK IF NAD AND CID SUPPORTED
				   if ((l_bProtocolInfo3 & 0x01) == 0x01) g_TCLCard.f_bNADSupported = TRUE;
				   if ((l_bProtocolInfo3 & 0x02) == 0x02) g_TCLCard.f_bCIDSupported = TRUE;
				}
			}

			// Assign CID
			if (g_TCLCard.f_bCIDSupported) 
				g_TCLCard.f_bCID = (p_bCID & 0x0F);

			// Assign card type
			g_TCLCard.f_eCardType = CST_tclctISO_14443_A_4;

			// Return success
			return TRUE;
		}
		else 
		{
		 
			// If Type B not requested  exit with error
			if (p_eCardType != CST_tclctISO_14443_A_B_4) return FALSE;
		}
	}
	
	// Check if ISO 14443 B-4
	if ((p_eCardType == CST_tclctISO_14443_A_B_4) || (p_eCardType == CST_tclctISO_14443_B_4))
	{
		// Set User's protocol
			l_bProtocolConfig1 = 0xFF;
			l_bProtocolConfig2 = 0x42;
			CLib_w_SetStatus(COUPLER_RAM, 0x6B, &l_bProtocolConfig1);
			CLib_w_SetStatus(COUPLER_RAM, 0x6F, &l_bProtocolConfig2);
		l_bProtocolConfig1 = 0x44;
		l_bProtocolConfig2 = 0x31;
		l_wStatus = CLib_w_SetStatus(COUPLER_RAM, 0x5E, &l_bProtocolConfig1);
		if (l_wStatus == STATUS_OK)
		{
			l_wStatus = CLib_w_SetStatus(COUPLER_RAM, 0x5F, &l_bProtocolConfig2);
			if (l_wStatus != STATUS_OK) return FALSE;
		}
		else
			return FALSE;

		// Send REQB with one slot
		l_abData[0x00] = 0x05;
		l_abData[0x01] = 0x00;
		l_abData[0x02] = 0x00;
		if (CLib_w_SendISOCommand(	ISO_IN_OUT,
									0x0C,
									0x80,
									0xC2,
									0xF7,
									0x0C,
									0x03,
									l_abData,
									l_abATQB) == STATUS_OK) 
		{
			// Copy Serial Number
			memcpy(l_abTypeSN, l_abATQB, 0x0C);

			// Set Serial Number (PUPI + APPLICATION DATA + PROTOCOL INFO)
			memcpy(p_abSN, &l_abTypeSN[1], 0x0B);
			p_bSize_SN[0] = 0x0B;

			// Get protocol info bytes
			l_bProtocolInfo1 = l_abTypeSN[0x09];
			l_bProtocolInfo2 = l_abTypeSN[0x0A];
			l_bProtocolInfo3 = l_abTypeSN[0x0B];

			// Detect PCD bit rate supported by PICC
			if ((l_bProtocolInfo1 & 0x80) == 0x80) {g_TCLCard.f_ePCDBitRate = CST_tclbrPCD_PICC; g_TCLCard.f_ePICCBitRate = CST_tclbrPCD_PICC;}
			if ((l_bProtocolInfo1 & 0x10) == 0x10) g_TCLCard.f_ePICCBitRate = CST_tclbr212kbps ;
			if ((l_bProtocolInfo1 & 0x20) == 0x20) g_TCLCard.f_ePICCBitRate = CST_tclbr424kbps ;
			if ((l_bProtocolInfo1 & 0x40) == 0x40) g_TCLCard.f_ePICCBitRate = CST_tclbr847kbps ;
			if ((l_bProtocolInfo1 & 0x01) == 0x01) g_TCLCard.f_ePCDBitRate  = CST_tclbr212kbps ;
			if ((l_bProtocolInfo1 & 0x02) == 0x02) g_TCLCard.f_ePCDBitRate  = CST_tclbr424kbps ;
			if ((l_bProtocolInfo1 & 0x04) == 0x04) g_TCLCard.f_ePCDBitRate  = CST_tclbr847kbps ;
			if (l_bProtocolInfo1 == 0x00) {g_TCLCard.f_ePCDBitRate = CST_tclbr106kbps; g_TCLCard.f_ePICCBitRate = CST_tclbr106kbps;}
			if ((l_bProtocolInfo1 & 0x08) == 0x08)  return FALSE;

			// Decode PICC maximum input frame size
			switch (l_bProtocolInfo2 >> 4)
			{
				case 0: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x10;
					break;
				case 1: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x18;
					break;
				case 2: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x20;
					break;
				case 3: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x28;
					break;
				case 4: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x30;
					break;
				case 5: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x40;
					break;
				case 6: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x60;
					break;
				case 7: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x80;
					break;
				case 8: 
					g_TCLCard.f_iPICCMaximumInputFrameSize = 0x100;
					break;
				default: return FALSE;
			}

			// Check chip ISO 14443-4 compliant TCL
			if ((l_bProtocolInfo2 & 0x0F) != 0x01) return FALSE;

			// Check NAD and CID supported
			if ((l_bProtocolInfo3 & 0x02) == 0x02) g_TCLCard.f_bNADSupported = TRUE;
			if ((l_bProtocolInfo3 & 0x01) == 0x01) g_TCLCard.f_bCIDSupported = TRUE;

			// Calculate PICC frame waiting time
			g_TCLCard.f_dPICCMaximumFrameWaitingTime = ((256 * 16) / 13560000 ) * pow(2, l_bProtocolInfo2 >> 4) ;

			// Set the time wich the PCD must wait before sending another frame (Default is 10 ETU + 32/fs)
			g_TCLCard.f_dPCDWaitingTimeBeforeSendingAnotherFrame = 10 * (128/13560000) + 32 * (16 / 13560000);

			// Set ATTRIB
			l_abData[0x00] = ISO_14443_B_ATTRIB ;
			
			// Set PUPI
			l_abData[0x01] = l_abTypeSN[1];
			l_abData[0x02] = l_abTypeSN[2];
			l_abData[0x03] = l_abTypeSN[3];
			l_abData[0x04] = l_abTypeSN[4];
			
			// Set PARAM1 : Default TR0, Default TR1, SOF and EOF
			l_abData[5] = 0x00 ;

			// Set PARAM2 : maw frame size 128, Emission 106 kb/s, Reception 106 kb/s
			switch (p_iPCDMaximumInputFrameSize)
			{
				case 0x10: 
					l_abData[0x06] = 0x00;
					break;
				case 0x18 : 
					l_abData[0x06] = 0x01;
					break;
				case 0x20 : 
					l_abData[0x06] = 0x02;
					break;
				case 0x28 : 
					l_abData[0x06] = 0x03;
					break;
				case 0x30 : 
					l_abData[0x06] = 0x04;
					break;
				case 0x40 : 
					l_abData[0x06] = 0x05;
					break;
				case 0x60 : 
					l_abData[0x06] = 0x06;
					break;
				case 0x80 : 
					l_abData[0x06] = 0x07;
					break;
				case 0x100: 
					l_abData[0x06] = 0x08;
					break;
				default: return FALSE;
			}

			// Set PARAM3 : confirm ISO 14443-4 compliant PCD
			l_abData[0x07] = 0x01;

			// Set CID
			l_abData[0x08] = 0x00;
			g_TCLCard.f_bCID = 0x00 ;
			if (g_TCLCard.f_bCIDSupported) 
			{
				l_abData[0x08]	 = (p_bCID & 0x0F);
				g_TCLCard.f_bCID = (p_bCID & 0x0F);
			}

			// Send ATTRIB
			l_wStatus = CLib_w_SendISOCommand(	ISO_IN_OUT,
												0x20,
												0x80,
												0xC2,
												0xF7,
												0x20,
												0x09,
												l_abData,
												l_abDataOut);
			// Check if response
			if (l_wStatus == STATUS_FILE_NOT_FOUND) return FALSE;

			// Check if length of data receied in status, if so get data and CRC
			if ((l_wStatus & 0xFF00) == 0x9000) l_iData = (l_wStatus & 0xFF) + 2;

			// Get response
			CLib_w_GetResponse(l_iData, l_abData);

			// analyse frame
			for (l_iLength = 1; l_iLength <= l_iData - 2; l_iLength++)
			{
				// calculate CRC
				l_wCRC = CRC16ShiftRight(&l_abData[0], l_iLength, ISO_14443_B_CRC_PRESET, ISO_14443_A_B_CRC_POLYNOM);
			
				// Invert CRC for ISO 14443-B
				l_wCRC = l_wCRC ^ 0xFFFF;
				l_bCRCLow = (BYTE)(l_wCRC & 0x00FF) ;
				l_bCRCHigh = (BYTE)(l_wCRC >> 8) ;
				
				// Check if CRC is correct
				if ((l_bCRCLow == l_abData[l_iLength]) && (l_bCRCHigh == l_abData[l_iLength + 1]))
					break;
			}

			// assign RATTRIB
			memcpy(p_abRATS_RATTRIB, l_abData, l_iLength);
			p_bSize_RATS_RATTRIB[0] = l_iLength;

			// Assign card type
			g_TCLCard.f_eCardType = CST_tclctISO_14443_B_4;

			// Return success
			return TRUE;
		}
	}

	// Default path: return error
	return FALSE;
} // CLib_b_CardConnect()


//=======================================================================================
// Routine 	CLib_b_CardDisconnect
// In       none
// Out      none
// Return   true is card deselected.
// Usage    This function sends an DESELECT APDU using the ISO-14443-4 T=CL protocol.
//=======================================================================================
_CDECL_EXPORT_ BOOL _CDECL_FUNCTION_ CLib_b_CardDisconnect()
{
	BYTE	l_abTCLFrame[10];
	BYTE	l_abDataIn[10];
	BYTE	l_abDataOut[255];
	
	int		l_iRetryCount;
	WORD	l_wStatus;
	
	BYTE	l_abFrameIn[255];
	BYTE	l_abFrameOut[255];

	BYTE	l_ePCBType,
			l_eTransmitStep,
			l_bWTX;
	
	int		l_iTCLFrameInLength, 
			l_iTCLFrameOutLength;

	// Initialize retry count to TCL_RETRY_COUNT
	l_iRetryCount = TCL_RETRY_COUNT;

	//INITIALIZE STEP
	l_eTransmitStep = CST_tcltsDESELECT;
	l_iTCLFrameInLength  = 0;
	l_iTCLFrameOutLength = 0;

	//LOOP UNTIL EXCHANGE DONE
	while ((l_eTransmitStep != CST_tcltsError) && (l_eTransmitStep != CST_tcltsEnd))
	{
		// CASE STEP
		switch (l_eTransmitStep)
		{
			case CST_tcltsDESELECT:
				//BUILD S(DESELECT) TCL FRAME
				fnv_TCLFrameBuild(CST_pcbtDESELECT, NULL, NULL, 0, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

					//RECEIVE THE SAME LENGTH AS INPUT S(DESELECT)
				l_iTCLFrameOutLength = l_iTCLFrameInLength;
				break;

			case CST_tcltsRNACK:
				//BUILD RNACK TCL FRAME
				fnv_TCLFrameBuild(CST_pcbtRNACK, NULL, NULL, 0, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

				//UNKNOWN FRAME OUT LENGTH
				l_iTCLFrameOutLength = -1;
				break;


			case CST_tcltsSWTX:
				//SET WTX VALUE (THE 2 UPPER BITS SHOULD BE RESET TO ZERO)
				l_abDataIn[0] = l_bWTX & 0x3F ;

				//BUILD RNACK TCL FRAME
				fnv_TCLFrameBuild(CST_pcbtSWTX, NULL, l_abDataIn, 1, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

				//UNKNOWN FRAME OUT LENGTH
				l_iTCLFrameOutLength = -1;
				break;
		}

		// Copy data
		memcpy(l_abFrameIn, l_abTCLFrame, l_iTCLFrameInLength);

		// Execute TRANSMIT command
		if (l_iTCLFrameOutLength != -1)
		{
			//SEND IN AND WAIT DATA
			l_wStatus = CLib_w_SendISOCommand(	ISO_IN_OUT,
												l_iTCLFrameOutLength,
												0x80,
												0xC2,
												0xF7,
												(BYTE)l_iTCLFrameOutLength,
												(BYTE)l_iTCLFrameInLength,
												l_abFrameIn,
												l_abFrameOut);

			//READ OUTPUT BUFFER IF DATA RECEIVED BUT CRC ERROR CHECKING : DO THIS OPERATION BEFORE CHEKCING LENGTH RECEIVED
			//BECAUSE l_wStatus IS MODIFIED AND THEN GET RESPONSE WOULD BE REPEATED TWICE
			if (l_wStatus = STATUS_CARD_NOT_IDENTIFIED) 
				CLib_w_GetResponse(g_TCLCard.f_iPCDMaximumInputFrameSize, l_abFrameOut);

			//CHECK IF LENGTH OF DATA RECEIVED IN STATUS, IF SO THEN GET DATA AND CRC
			if ((l_wStatus & 0xFF00) == 0x9000 && (l_wStatus & 0xFF) != 0x00) 
			{
				//SET DATA TO RECEIVE
				l_iTCLFrameOutLength = (BYTE)((l_wStatus & 0xFF) + 2);

				//READ OUTPUT BUFFER
				CLib_w_GetResponse(l_iTCLFrameOutLength, l_abFrameOut);

				//SET STATUS TO STATUS_CARD_NOT_IDENTIFIED SO TCLFrameExtract WILL LOCATE THE CRC
				l_wStatus = STATUS_CARD_NOT_IDENTIFIED;
			}
		}
		else 
		{
			//THE MAXIMUM FRAME LENGHT IS PCB(1) + CID(1) + WTX(1) + CRC(2) = 5
			l_iTCLFrameOutLength = 5;

			//SEND IN
			l_wStatus = CLib_w_SendISOCommand(	ISO_IN_OUT,
												l_iTCLFrameOutLength,
												0x80,
												0xC2,
												0xF7,
												(BYTE)l_iTCLFrameOutLength,
												(BYTE)l_iTCLFrameInLength,
												l_abFrameIn,
												l_abFrameOut);

			//CHECK IF LENGTH OF DATA RECEIVED IN STATUS, IF SO THEN GET DATA AND CRC
			if ((l_wStatus & 0xFF00) == 0x9000 && (l_wStatus & 0xFF) != 0x00)
			{
				//GET LENGTH TO RETREIVE
				l_iTCLFrameOutLength = (BYTE)(l_wStatus & 0xFF);

				//READ OUTPUT BUFFER IF DATA RECEIVED
				CLib_w_GetResponse(l_iTCLFrameOutLength, l_abFrameOut);

				//SET STATUS AS IT WAS A CRC ERROR FOR RETREIVING THE GOOD POSITION OF THE DATA IN TCLFrameExtract
				l_wStatus = STATUS_CARD_NOT_IDENTIFIED;
			}
		}

		//CHECK IF DATA RECEIVED
		if (l_wStatus != STATUS_FILE_NOT_FOUND)
			l_ePCBType = fnb_TCLFrameExtract((BYTE *)&l_iTCLFrameOutLength, l_abFrameOut, l_abDataOut, &l_wStatus);
		else 
			l_ePCBType = CST_pcbtError ;

   		//CASE PCB TYPE
		switch (l_ePCBType)
		{
			case CST_pcbtDESELECT:
				//CARD DESELECTED
				l_eTransmitStep = CST_tcltsEnd;
				
				//SET RESULT
				return TRUE;
			break;

			case CST_pcbtRNACK:
				//DECREMENT RETRY COUNT
				l_iRetryCount--;

				//SEND PNE MORE TIME DESELECT
				l_eTransmitStep = CST_tcltsDESELECT ;

				//CHECK IF RETRY COUNT ELLAPSED
				if (l_iRetryCount == 0) 
					l_eTransmitStep = CST_tcltsError;
			break;

			case CST_pcbtSWTX:
				//GET SWTX
				memcpy(&l_bWTX, l_abDataOut, 2);

				//TRANSMIT SWTX
				l_eTransmitStep = CST_tcltsSWTX;
			break;

			default:
				//DECREMENT RETRY COUNT
				l_iRetryCount--;

				//CHECK IF RETRY COUNT ELLAPSED
				if (l_iRetryCount == 0)
					l_eTransmitStep = CST_tcltsError;
				else 
					l_eTransmitStep = CST_tcltsDESELECT;
		}
   }

   // Default path: return error
   return FALSE;
}

//=======================================================================================
// Routine CLib_b_CardTransmit
// In       p_iDataOutLength
//			p_iDataInLength
//          p_bClass
//          p_bIns
//          p_bP1
//          p_bP2
//          p_bP3
//          p_sDataIn
//
// Out      p_iDataOutLength
//			p_sDataOut
//
// Return   Status word
// Usage    This function sends an APDU using the ISO-14443-4 T=CL protocol.
//=======================================================================================
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_CardTransmit(	int		*p_iDataOutLength,
														int		p_iDataInLength, 
														BYTE	p_bClass,
														BYTE	p_bIns,
														BYTE	p_bP1,
														BYTE	p_bP2,
														BYTE	p_bP3,
														BYTE	*p_abDataIn,
														BYTE	*p_abDataOut)
{
	BYTE	l_abTCLFrame[255];
	BYTE	l_abCommand[4];
	BYTE	l_abDataIn[255];
	BYTE	l_abDataOut[255];
	BYTE	l_abDataTruncated[255];

	int		l_iTCLFrameInLength, 
			l_iTCLFrameOutLength,
			l_iDataInLength;
	
	BYTE	l_abFrameIn[255];
	BYTE	l_abFrameOut[255];
	
	WORD	l_wStatus;
	int		l_iLength;
	BYTE	l_ePCBType,
			l_eTransmitStep,
			l_bWTX;
	
	int		l_iRetryCount,
			l_iDataInStep,
			l_iDataInPrologueEpilogue;
	
	BYTE	l_abDataInList[255][255];
	int		l_aiDataInListLength[255];
	int		l_iCurrentInIndex;
	
	BYTE	l_abDataOutList[255][255];
	int		l_aiDataOutListLength[255];
	int		l_iCurrentOutIndex;

	int		l_iDataInIndex,
			l_iDataCursor;

	// INITIALIZE STEP
	l_eTransmitStep = CST_tcltsCommand;

	for (l_iLength = 0; l_iLength < 255; l_iLength++)
		l_aiDataOutListLength[l_iLength] = 0x00;

	// BUILD COMMAND
	l_abCommand[0] = p_bClass;
	l_abCommand[1] = p_bIns;
	l_abCommand[2] = p_bP1;
	l_abCommand[3] = p_bP2;
	l_abCommand[4] = p_bP3;

	// INITIALIZE RETRY COUNT TO TCL_RETRY_COUNT
	l_iRetryCount = TCL_RETRY_COUNT;

	// CREATE DATA IN LIST
	l_iDataInIndex = 0;

	// GET STRING
	memcpy(l_abDataIn, p_abDataIn, p_iDataInLength);
	l_iDataInStep		= 0;
	l_iDataCursor		= 0;
	l_iCurrentInIndex	= 0;
	l_iCurrentOutIndex	= 0;
	while (l_iDataCursor < p_iDataInLength)
	{
		// GET PROLOGUE EPILOGUE SIZE
		l_iDataInPrologueEpilogue = fni_TCLFrameIPrologueEpilogueLength();

		// ADD COMAND SIZE IF FIRST PASS
		if (l_iDataInStep == 0)  
			l_iDataInPrologueEpilogue += 5;
		
		l_iDataInStep = 1;
		
		// GET SIZE
		if ((p_iDataInLength - l_iDataCursor + l_iDataInPrologueEpilogue) > g_TCLCard.f_iPICCMaximumInputFrameSize)
		{
			l_iLength = g_TCLCard.f_iPICCMaximumInputFrameSize - l_iDataInPrologueEpilogue;
			memcpy(l_abDataTruncated, l_abDataIn, l_iLength);
			l_iDataCursor += l_iLength;
		}
		else
		{
			l_iLength = p_iDataInLength - l_iDataCursor;
			memcpy(l_abDataTruncated, l_abDataIn, l_iLength);
			l_iDataCursor = p_iDataInLength;
		}

		// ADD TO LIST
		memcpy(l_abDataInList[l_iCurrentInIndex], l_abDataTruncated, l_iLength);
		l_aiDataInListLength[l_iCurrentInIndex] = l_iLength;
		l_iCurrentInIndex++;
		//l_oDataInList.Add(l_sDataTruncated);
	}

	// LOOP UNTIL EXCHANGE DONE
	while ((l_eTransmitStep != CST_tcltsError) && (l_eTransmitStep != CST_tcltsEnd))
	{
		// CASE STEP
		switch (l_eTransmitStep)
		{
			case CST_tcltsCommand:
				// ASSIGN FRAME OUT
				l_iTCLFrameOutLength = p_iDataOutLength[0];

				// COPY DATA IN
				l_iDataInLength = 0;
				if (l_iCurrentInIndex > 0)
				{
					l_iDataInLength = l_aiDataInListLength[0];
					memcpy(l_abDataIn, l_abDataInList[0], l_iDataInLength);
				}

				// SET CHAINING IF MORE THAN ONE DATA IN LIST
				l_ePCBType = CST_pcbtINoChaining;
				if (l_iCurrentInIndex > 1)
					l_ePCBType = CST_pcbtIWithChaining;

				//BUILD I TCL FRAME
				l_iTCLFrameInLength = 0;
				l_iTCLFrameOutLength = 0;
				fnv_TCLFrameBuild(l_ePCBType, l_abCommand, l_abDataIn, l_iDataInLength, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);
			break;

			case CST_tcltsChaining:
				// CONVERT DATA IN
				l_iDataInLength = l_aiDataInListLength[l_iDataInIndex];
				memcpy(l_abDataIn, l_abDataInList[l_iDataInIndex], l_iDataInLength);

				// SET CHAINING BIT IF STRING LEFT IN LIST
				l_ePCBType = CST_pcbtINoChaining;
				if (l_iDataInIndex < (l_iCurrentInIndex - 1)) 
					l_ePCBType = CST_pcbtIWithChaining;

				// BUILD I TCL FRAME
				fnv_TCLFrameBuild(l_ePCBType, NULL, l_abDataIn, l_iDataInLength, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

				//UNKNOWN FRAME OUT LENGTH
				l_iTCLFrameOutLength = -1;
			break;

			case CST_tcltsRACK:
			   // BUILD RACK TCL FRAME
			   fnv_TCLFrameBuild(CST_pcbtRACK, NULL, NULL, 0, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

			   // UNKNOWN FRAME OUT LENGTH
			   l_iTCLFrameOutLength = -1;
			break;

			case CST_tcltsRNACK:
			   // BUILD RNACK TCL FRAME
			   fnv_TCLFrameBuild(CST_pcbtRNACK, NULL, NULL, 0, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

			   // UNKNOWN FRAME OUT LENGTH
			   l_iTCLFrameOutLength = -1;
			break;


			case CST_tcltsSWTX:
			   // SET WTX VALUE (THE 2 UPPER BITS SHOULD BE RESET TO ZERO)
			   l_abDataIn[0] = l_bWTX & 0x3F;

			   // BUILD RNACK TCL FRAME
			   fnv_TCLFrameBuild(CST_pcbtSWTX, NULL, l_abDataIn, 1, l_abTCLFrame, &l_iTCLFrameInLength, &l_iTCLFrameOutLength);

			   // UNKNOWN FRAME OUT LENGTH
			   l_iTCLFrameOutLength = -1;
		}

		// Copy buffer
		memcpy(l_abFrameIn, l_abTCLFrame, l_iTCLFrameInLength);

		//TRANSMIT
		if (l_iTCLFrameOutLength != -1) 
		{
			//SEND IN AND WAIT DATA
			l_wStatus = CLib_w_SendISOCommand(	ISO_IN_OUT,
												l_iTCLFrameOutLength,
												0x80,
												0xC2,
												0xF7,
												g_TCLCard.f_iPCDMaximumInputFrameSize,
												//(BYTE)l_iTCLFrameOutLength,
												(BYTE)l_iTCLFrameInLength,
												l_abFrameIn,
												l_abFrameOut);

			// READ OUTPUT BUFFER IF DATA RECEIVED BUT CRC ERROR CHECKING : DO THIS OPERATION BEFORE CHEKCING LENGTH RECEIVED
			// BECAUSE l_wStatus IS MODIFIED AND THEN GET RESPONSE WOULD BE REPEATED TWICE
			if (l_wStatus == STATUS_CARD_NOT_IDENTIFIED) 
			{
				l_iTCLFrameOutLength = g_TCLCard.f_iPCDMaximumInputFrameSize;
				CLib_w_GetResponse(l_iTCLFrameOutLength, l_abFrameOut);
			}

			// CHECK IF LENGTH OF DATA RECEIVED IN STATUS, IF SO THEN GET DATA AND CRC
			if ((l_wStatus & 0xFF00) == 0x9000 && (l_wStatus & 0xFF) != 0x00) 
			{
				// SET DATA TO RECEIVE
				l_iTCLFrameOutLength = (BYTE)(l_wStatus & 0xFF) + 2;

				// READ OUTPUT BUFFER
				CLib_w_GetResponse(l_iTCLFrameOutLength, l_abFrameOut);

				// SET STATUS TO STATUS_CARD_NOT_IDENTIFIED SO TCLFrameExtract WILL LOCATE THE CRC
				l_wStatus = STATUS_CARD_NOT_IDENTIFIED;
			}
		}
		else 
		{
			//SET LENGTH TO MAXIMUM PCD SIZE BECAUSE PICC WILL NEVER SEND MORE
			l_iTCLFrameOutLength = g_TCLCard.f_iPCDMaximumInputFrameSize;

			//SEND IN
			l_wStatus = CLib_w_SendISOCommand(	ISO_IN_OUT,
												l_iTCLFrameOutLength,
												0x80,
												0xC2,
												0xB7,
												(BYTE)l_iTCLFrameOutLength,
												(BYTE)l_iTCLFrameInLength,
												l_abFrameIn,
												l_abFrameOut);

			// CHECK IF LENGTH OF DATA RECEIVED IN STATUS, IF SO THEN GET DATA AND CRC
			if ((l_wStatus & 0xFF00) == 0x9000 && (l_wStatus & 0xFF) != 0x00)
			{
				// GET LENGTH TO RETREIVE
				l_iTCLFrameOutLength = (BYTE)(l_wStatus & 0xFF) + 2;

				// READ OUTPUT BUFFER IF DATA RECEIVED
				CLib_w_GetResponse(l_iTCLFrameOutLength, l_abFrameOut);
			}

				// SET STATUS AS IT WAS A CRC ERROR FOR RETREIVING THE GOOD POSITION OF THE DATA IN TCLFrameExtract
				l_wStatus = STATUS_CARD_NOT_IDENTIFIED;
		}

		// CHECK IF DATA RECEIVED
		if (l_wStatus != STATUS_FILE_NOT_FOUND)
			l_ePCBType = fnb_TCLFrameExtract((BYTE *)&l_iTCLFrameOutLength, l_abFrameOut, l_abDataOut, &l_wStatus);
		else 
			l_ePCBType = CST_pcbtError;

		// CASE PCB TYPE
		switch (l_ePCBType)
		{
			case CST_pcbtINoChaining:
				// ADD DATA TO LIST
				memcpy(l_abDataOutList[l_iCurrentOutIndex], l_abDataOut, l_iTCLFrameOutLength);
				l_aiDataOutListLength[l_iCurrentOutIndex] = l_iTCLFrameOutLength;
				l_iCurrentOutIndex++;
				//l_oDataOutList.Add(p_sDataOut);
				
				// INCREMENT BLOCK COUNTER
				g_TCLCard.f_iBlockNumber++;
				
				// END
				l_eTransmitStep = CST_tcltsEnd;
			break;

			case CST_pcbtIWithChaining:
				// RESET RETRY COUNT
				l_iRetryCount = TCL_RETRY_COUNT;

				// ADD DATA TO LIST
				memcpy(l_abDataOutList[l_iCurrentOutIndex], l_abDataOut, l_iTCLFrameOutLength);
				l_aiDataOutListLength[l_iCurrentOutIndex] = l_iTCLFrameOutLength;
				l_iCurrentOutIndex++;
				//l_oDataOutList.Add(p_sDataOut);

				// INCREMENT BLOCK COUNTER
				g_TCLCard.f_iBlockNumber++;

				// SEND RACK FOR GETTING THE DATA
				l_eTransmitStep = CST_tcltsRACK;
			break;

			case CST_pcbtRACK:
				// SET CHAINING
				l_iDataInIndex++;
				if (l_iDataInIndex <= (l_iCurrentInIndex - 1)) 
				{
					// SEND REMAINING DATA
					l_eTransmitStep = CST_tcltsChaining ;
					
					// NEXT BLOCK
					g_TCLCard.f_iBlockNumber++;
				}
				else 
				  l_eTransmitStep = CST_tcltsEnd;
			break;

			case CST_pcbtRNACK:
				// DECREMENT RETRY COUNT
				l_iRetryCount--;

				// CHECK IF RETRY COUNT ELLAPSED
				if (l_iRetryCount == 0) 
					l_eTransmitStep = CST_tcltsError ;
			break;

			case CST_pcbtSWTX:
				// GET SWTX
				memcpy(&l_bWTX, p_abDataOut, 1);
				//ConvertHexaToBuffer(p_sDataOut,@l_bWTX,f_oPanelMX.MxHexaSymbol);
				
				// TRANSMIT SWTX
				l_eTransmitStep = CST_tcltsSWTX;
			break;

			case CST_pcbtError:
				// DECREMENT RETRY COUNT
				l_iRetryCount--;

				//CHECK IF RETRY COUNT ELLAPSED
				if (l_iRetryCount == 0)
					l_eTransmitStep = CST_tcltsError;
				else 
					l_eTransmitStep = CST_tcltsRNACK;
		}
	}

	//ASSIGN DATA OUT
	l_iDataInStep = 0;
	for (l_iLength = 0; l_iLength < l_iCurrentOutIndex; l_iLength++)
	{
		memcpy(&p_abDataOut[l_iDataInStep], l_abDataOutList[l_iLength], l_aiDataOutListLength[l_iLength]);
		l_iDataInStep += l_aiDataOutListLength[l_iLength];
	}

	// Assign data out size
	if (l_iDataInStep == 0)
		p_iDataOutLength[0] = 0;
	else
		p_iDataOutLength[0] = l_iDataInStep - 4;

	//SET RESULT
	if (l_eTransmitStep == CST_tcltsError) 
		l_wStatus = STATUS_FILE_NOT_FOUND;

	return l_wStatus;
}

