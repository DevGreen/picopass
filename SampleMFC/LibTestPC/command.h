//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	Command.c
//		Basic coupler commands
//
// Documentation :
//		These commands are detailed in the coupler
//		Datasheet
//
// Version :    2.0.0
// Date :       January, 15th 2006
// Author :     Frederic CAPASSO
//
//------------------------------------------------------------------


#ifdef __CLIB_DLL_EXPORT__
	#define _CDECL_EXPORT_ _declspec(dllexport)
	#define _CDECL_FUNCTION_ _cdecl
#else
	#include "com.h"

	#define _CDECL_EXPORT_ 
	#define _CDECL_FUNCTION_ 
#endif

//------------------------------------------------------------------
// Function : CLib_w_SelectCard
//
// In		: p_bSelectMode		-> Authntication type 
//			  p_bChipTypes		-> Protocols to scan	
// Out      : p_abTypeSN		-> Chip type and Serial Number
// Return   : Status word from the coupler
// Usage    : Select a PICO family card
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SelectCard(BYTE p_bSelectMode, BYTE p_bChipTypes, BYTE *p_abTypeSN)
{
	BYTE	l_abISOCommand[5];

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xA4;
	l_abISOCommand[CMD_P1]  = p_bSelectMode;
	l_abISOCommand[CMD_P2]  = p_bChipTypes;
	l_abISOCommand[CMD_P3]  = 0x09;

	// Send ISO Command 
	return(IsoExchange(ISO_OUT, l_abISOCommand, 0, NULL, 9, p_abTypeSN));
} // CLib_w_SelectCard()


//------------------------------------------------------------------
// Function : CLib_w_Halt
//
// In		: BYTE p_bProtocol	-> Command protocol
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Put current Selected card in Halted mode
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_Halt(BYTE p_bProtocol)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataIn  = 0x00;
	BYTE	l_abDataOut = 0x00;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xC2;
	l_abISOCommand[CMD_P1] = 0x30 + p_bProtocol;
	l_abISOCommand[CMD_P2] = 0x00;
	l_abISOCommand[CMD_P3] = 0x01;

	// Send ISO Command 
	return(IsoExchange(ISO_IN, l_abISOCommand, 1, &l_abDataIn, 0, NULL));
} // CLib_w_Halt()


//------------------------------------------------------------------
// Function : CLib_w_ReSelect
//
// In		: p_bProtocol		-> Command proocol
//			  p_abSerialNumber	-> Chip Serial Number
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Select a chip in Halted mode (with its Serial Number)
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_ReSelect(BYTE p_bProtocol, BYTE *p_abSerialNumber)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataIn[10];
	BYTE	l_abDataOut[8];
	
	int l_indData;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xC2;
	l_abISOCommand[CMD_P1]  = 0x54 + p_bProtocol;
	l_abISOCommand[CMD_P2]  = 0x08;
	l_abISOCommand[CMD_P3]  = 0x09;

	// Prepare Data in
	l_abDataIn[0] = 0x81;
	for (l_indData = 0; l_indData < 8; l_indData++)
		l_abDataIn[l_indData + 1] = p_abSerialNumber[l_indData];

	// Send ISO Command 
	return(IsoExchange(ISO_IN_OUT, l_abISOCommand, 9, l_abDataIn, 8, l_abDataOut));
} // CLib_w_ReSelect()


//------------------------------------------------------------------
// Function : CLib_w_EASDetect
//
// In		: p_bProtocol		-> Command proocol
// Out      : p_abSerialNumber	-> Chip Serial Number
// Return   : Status word from the coupler
// Usage    : Use chip EAS detection (for fast anti-collision)
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_EASDetect(BYTE p_bProtocol, BYTE *p_abSerialNumber)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataIn;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xC2;
	l_abISOCommand[CMD_P1] = 0x74 + p_bProtocol;
	l_abISOCommand[CMD_P2] = 0x08;
	l_abISOCommand[CMD_P3] = 0x01;

	// Prepare Data in
	l_abDataIn = 0x0F;

	// Send ISO Command 
	return(IsoExchange(ISO_IN_OUT, l_abISOCommand, 1, &l_abDataIn, 8, p_abSerialNumber));
} // CLib_w_EASDetect()


//------------------------------------------------------------------
// Function : CLib_w_AskRandom
//
// In       : - None-
// Out      : p_abRandom	-> Random sequence returned
// Return   : Status word from the coupler
// Usage    : ask for a 8-byte random number from the coupler
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_AskRandom(BYTE *p_abRandom)
{
	BYTE	l_abISOCommand[5];

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0x84;
	l_abISOCommand[CMD_P1]  = 0x00;
	l_abISOCommand[CMD_P2]  = 0x00;
	l_abISOCommand[CMD_P3]  = 0x08;

	// Send ISO Command 
	return(IsoExchange(ISO_OUT, l_abISOCommand, 0, NULL, 8, p_abRandom));
} // CLib_w_AskRandom()


//------------------------------------------------------------------
// Function : CLib_w_Transmit
//
// In       : p_bP1			-> Command parameter 1 value
//            p_bP2			-> Command parameter 2 value
//            p_bP3			-> Command parameter 3 value
//			  p_abDataIn	-> Command to send to the chip
// Out      : p_abDataOut -> Response from the chip
// Return   : Status word from the coupler
// Usage    : Transmit data from the coupler to the chip
//            and read back data from the chip
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_Transmit(BYTE p_bP1, BYTE p_bP2, BYTE p_bP3, BYTE *p_abDataIn,BYTE *p_abDataOut)
{
	BYTE	l_abISOCommand[5];

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xC2;
	l_abISOCommand[CMD_P1]  = p_bP1;
	l_abISOCommand[CMD_P2]  = p_bP2;
	l_abISOCommand[CMD_P3]  = p_bP3;

	// Send ISO Command for chip selection
	return(IsoExchange(ISO_IN_OUT, l_abISOCommand, p_bP3, p_abDataIn, p_bP2, p_abDataOut));
} // CLib_w_Transmit()


//------------------------------------------------------------------
// Function : GetResponse
//
// In       : p_iOutLength	-> Size of the data out Buffer
// Out      : p_abResponse	-> Data in the coupler buffer
// Return   : Status word from the coupler
// Usage    : Read the coupler buffer
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_GetResponse(int p_iOutLength, BYTE *p_abResponse)
{
	BYTE l_BCommand[5];
	BYTE l_bSentData = 0x00;

	// write command
	l_BCommand[CMD_CLA] = 0x80 ;
	l_BCommand[CMD_INS] = 0xC0;
	l_BCommand[CMD_P1]  = 0x00;
	l_BCommand[CMD_P2]  = 0x00;
	l_BCommand[CMD_P3]  = p_iOutLength;

	// Send ISO Command
	return IsoExchange (ISO_OUT, l_BCommand, 0, (BYTE *)l_bSentData, p_iOutLength, p_abResponse) ;
} // CLib_w_GetResponse()


//------------------------------------------------------------------
// Function : CLib_w_SendISOCommand
//
// In       : p_iISOType	-> Command ISO direction (In;Out;InOut)
//            p_iInLength	-> Length of the data in buffer
//			  p_bClass		-> Command Class value
//			  p_bINS		-> Command INS value
//			  p_bP1			-> Command parameter 1 value
//            p_bP2			-> Command parameter 2 value
//            p_bP3			-> Command parameter 3 value
//			  p_abDataIn	-> Command to send to the chip
// Out      : p_abDataOut -> Response from the chip
// Return   : Status word from the coupler
// Usage    : Send Low-Level command to the coupler and get result
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SendISOCommand(BYTE p_iISOType, int p_iOutLength, BYTE p_bClass, BYTE p_bINS, BYTE p_bP1, BYTE p_bP2, BYTE p_bP3, BYTE *p_abDataIn, BYTE *p_abDataOut)
{
	BYTE	l_abISOCommand[5];
	int		p_iInLength = 0;

	if ((p_iISOType == ISO_IN) || (p_iISOType == ISO_IN_OUT)) 
		p_iInLength = p_bP3;


	// Prepare the command
	l_abISOCommand[CMD_CLA] = p_bClass;
	l_abISOCommand[CMD_INS] = p_bINS;
	l_abISOCommand[CMD_P1]  = p_bP1;
	l_abISOCommand[CMD_P2]  = p_bP2;
	l_abISOCommand[CMD_P3]  = p_bP3;

	// Send ISO Command
	return(IsoExchange(p_iISOType, l_abISOCommand, p_iInLength, p_abDataIn, p_iOutLength, p_abDataOut));
} // CLib_a_SendISOCommand()


//------------------------------------------------------------------
// Function : CLib_w_SelectCurrentKey
//
// In       : p_bKey	-> number of the key to select
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Set key as default key (for authentication procedures)
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SelectCurrentKey(BYTE p_bKey)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataIn[8];
	BYTE	l_abDataOut = 0x00;

	int		l_indData;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0x52;
	l_abISOCommand[CMD_P1]  = 0x00;
	l_abISOCommand[CMD_P2]  = p_bKey;
	l_abISOCommand[CMD_P3]  = 0x08;

	// Prepare buffer data in
	for (l_indData = 0; l_indData < 8; l_indData++)
		l_abDataIn[l_indData] = 0x00;

	// Send ISO Command 
	return(IsoExchange(ISO_IN, l_abISOCommand, 8, l_abDataIn, 0, (BYTE *)l_abDataOut));
} // CLib_w_SelectCurrentKey()


//------------------------------------------------------------------
// Function : CLib_w_DiversifyKey
//
// In       : p_bKey		-> number of the key to select
//			  p_abSN		-> Chip Serial Number
// Out      : p_abKeyDiv	-> Value of diversified chip
// Return   : Status word from the coupler
// Usage    : Select choosen key as default one
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_DiversifyKey(BYTE p_bKey, BYTE *p_abSN, BYTE *p_abKeyDiv)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataOut = 0x00;
	WORD	l_wCommandStatus;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0x52;
	l_abISOCommand[CMD_P1]  = 0x00;
	l_abISOCommand[CMD_P2]  = p_bKey;
	l_abISOCommand[CMD_P3]  = 0x08;

	// Send ISO Command 
	l_wCommandStatus = (IsoExchange(ISO_IN, l_abISOCommand, 8, p_abSN, 0, (BYTE *)l_abDataOut));
	if (l_wCommandStatus != 0x9000) return l_wCommandStatus;
	// Get diversification value (ONLY FOR PERSONLIZATION COUPLERS)
	return (CLib_w_GetResponse(8, p_abKeyDiv));
	
} // CLib_w_DiversifyKey()


//------------------------------------------------------------------
// Function : CLib_w_Enable
//
// In       : - None -
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Enable the coupler
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_Enable(void)
{
	BYTE	l_abISOCommand[5];
	WORD	l_wStatus = 0xFFFF;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xAE;
	l_abISOCommand[CMD_P1]  = 0xDA;
	l_abISOCommand[CMD_P2]  = 0xBC;
	l_abISOCommand[CMD_P3]  = 0x00;

	// Send ISO Command
	l_wStatus = IsoExchange(ISO_NONE, l_abISOCommand, 0, NULL, 0, NULL);
	return(l_wStatus);
} // CLib_w_Enable()


//------------------------------------------------------------------
// Function :  CLib_w_Disable
//
// In       : - None -
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Disable the coupler which not not respond to any
//            other command except to ENABLE_COUPLER
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_Disable(void)
{
	BYTE	l_abISOCommand[5];
	WORD	l_wStatus = 0xFFFF;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xAD;
	l_abISOCommand[CMD_P1]  = 0xBC;
	l_abISOCommand[CMD_P2]  = 0xDA;
	l_abISOCommand[CMD_P3]  = 0x00;

	// Send ISO Command
	l_wStatus = IsoExchange(ISO_NONE, l_abISOCommand, 0, NULL, 0, NULL);
	return(l_wStatus);
} // CLib_w_Disable()


//------------------------------------------------------------------
// Function :	CLib_w_ResetField
// In       :	- None -
// Out      :	- None -
// Return   :  Status word from the coupler
// Usage    :  Cut the RF Field and set it back
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_ResetField(void)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataIn[2] = {0x00, 0x00}; // Data in initialisation

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xF4;
	l_abISOCommand[CMD_P1]  = 0x40;
	l_abISOCommand[CMD_P2]  = 0x00;
	l_abISOCommand[CMD_P3]  = 0x01;

	// Send ISO Command
	return(IsoExchange(ISO_IN, l_abISOCommand, 1, l_abDataIn, 0, NULL));
} // CLib_w_ResetField()


//------------------------------------------------------------------
// Function :  CLib_w_GetStatus
//
// In       :  p_bLocation	-> Reading Location(EEPROM, RAM...)
//             p_BAddress	-> Address to read
// Out      :  p_bData		-> Data received from the command
// Return   :  Status word from the coupler
// Usage    :  read the coupler memory parameter or the external
//             EEPROM memory
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_GetStatus(BYTE p_bLocation, BYTE p_bAddress, BYTE *p_bData)
{
	BYTE	l_abISOCommand[5];

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xF2;
	l_abISOCommand[CMD_P1]  = p_bLocation;
	l_abISOCommand[CMD_P2]  = p_bAddress;
	l_abISOCommand[CMD_P3]  = 0x01;

	// Send ISO Command
	return(IsoExchange(ISO_OUT, l_abISOCommand, 0, NULL, 1, p_bData));
} // CLib_w_GetStatus()


//------------------------------------------------------------------
// Function : CLib_w_SetStatus
//
// In       : p_bLocation	-> Updateing Location
//            p_BAddress	-> Address to read
//            p_bData		-> Data to update 
// Out      : - None -
// Return   : Status word from the coupler
// Usage    : Set a data in the coupler memory parameter or external
//            EEPROM memory
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SetStatus(BYTE p_bLocation, BYTE p_bAddress, BYTE *p_bData)
{
	BYTE	l_abISOCommand[5];
	BYTE	l_abDataOut = 0x00;

	// Prepare the command
	l_abISOCommand[CMD_CLA] = 0x80;
	l_abISOCommand[CMD_INS] = 0xF4;
	l_abISOCommand[CMD_P1]  = p_bLocation;
	l_abISOCommand[CMD_P2]  = p_bAddress;
	l_abISOCommand[CMD_P3]  = 0x01;

	// Send ISO Command
	return(IsoExchange(ISO_IN, l_abISOCommand, 1, p_bData, 0, NULL));
} // CLib_w_GetStatus()


//------------------------------------------------------------------
// Function : CLib_w_LoadKeyFile
//
// In       : p_bKeyOperation	-> type of operation
//            p_bKeyNumber		-> number of the key to be altered
//            p_abSentData		-> key + checksum
// Out      : - None-
// Return   : Status word from the coupler
// Usage    : Load the encrypted master keys into the the coupler
//-------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_LoadKeyFile(BYTE p_bKeyOperation ,BYTE p_bKeyNumber , BYTE *p_abSentData)
{
	BYTE l_abCommand[5];
	BYTE l_bResponse = 0x00;

	l_abCommand[CMD_CLA] = 0x80;
	l_abCommand[CMD_INS] = 0xD8;
	l_abCommand[CMD_P1]  = p_bKeyOperation;
	l_abCommand[CMD_P2]  = p_bKeyNumber;
	l_abCommand[CMD_P3]  = 0x0C;

	return  IsoExchange (ISO_IN, l_abCommand, 12, p_abSentData, 0, NULL);
}
