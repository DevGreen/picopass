//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	Function.c
//		High level function for PICO family couplers and chips
//
// Documentation :
//		These function are detailed in the coupler
//		Datasheet and in the PICOTAG datasheet
//
// Version :    2.0.0
// Date :       January, 15th 2006
// Author :     Frederic CAPASSO
//
//------------------------------------------------------------------

#include "command.h"

//-----------------------------------------------------------------------
// HIGH LEVEL COMMAND TO THE COUPLER
//
// LoadEncryptedKey : change keys value in the coupler
// ReadBlock : Read a blockin the chip memory
// UpdateBlock : Update a block in a non secured chip memory
// UpdateBlock_S : Update a block in a secured chip memory
// Increase : Set the counter value
// Decrease : Decrease the counter value
//------------------------------------------------------------------------


//------------------------------------------------------------------------
// Function : CLib_w_KeyLoading
//
// In		: p_bKeyNum			-> Key number
//			  p_bLoadingMode	-> Loading type (on echange or previous key)
//			  p_abExchangeKey	-> Exchange Key value
//			  p_abNewKey		-> New Key value
//
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: This function loads a new crypted key into the reader
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_KeyLoading(BYTE p_bKeyNum, BYTE p_bLoadingMode, BYTE *p_abExchangeKey, BYTE *p_abNewKey)
{
	BYTE	l_abTempPC[8];
	BYTE	l_abKeyEncrypted[8];
	BYTE	l_abCheckSum[4];
	BYTE	l_abKeySession[8];
	BYTE	l_abCommand[8];
	BYTE	l_abRandom[8];
	BYTE	l_abDataIn[12];
	
	BYTE	l_iByte;
	WORD	l_wStatus;
	BYTE	l_bByteAdd;
	BYTE	l_bBitAdd;
	BYTE	l_bCheckSum;
	int		l_iIndice;

	// COMMAND DEFINITION
	l_abCommand[0] = 0x80;
	l_abCommand[1] = 0xD8;
	l_abCommand[2] = 0x00;
	l_abCommand[3] = p_bKeyNum;
	l_abCommand[4] = 0x0C;
	l_abCommand[5] = 0x00;// BYTE 5, 6, 7 will be use in the encrypted key calculation
	l_abCommand[6] = 0x00;
	l_abCommand[7] = 0x00;

	// 1. ASK RANDOM TO THE COUPLER
	//-----------------------------
	l_wStatus = CLib_w_AskRandom(l_abRandom);
	if (l_wStatus != STATUS_OK)
		return(l_wStatus);


	// 2. CALCULATE THE SESSION KEY
	//-----------------------------
	// Initialize temporary buffer
	for (l_iIndice = 0; l_iIndice < 8; l_iIndice++)
		l_abTempPC[l_iIndice] = 0x00;
	
	// 2.1 KEY PERMUTATION
	for (l_bBitAdd = 0; l_bBitAdd < 7; l_bBitAdd++)
	{
		for (l_bByteAdd = 0; l_bByteAdd < 8; l_bByteAdd++)
			l_abTempPC[l_bBitAdd] = (l_abTempPC[l_bBitAdd] >> 1) | ((p_abExchangeKey[l_bByteAdd] << l_bBitAdd) & 0x80);
	}

	// 2.2 REPLACE 8th BYTE WITH CHECKSUM
	//loop on 7 high bytes
	l_bCheckSum = 0x00;
	for (l_iIndice = 0; l_iIndice < 7; l_iIndice++)
		l_bCheckSum = l_bCheckSum ^ (*(l_abTempPC + l_iIndice));

	//COMPLEMENT THE CHECKSUM
	l_bCheckSum = l_bCheckSum ^ 0xFF ;

	//ASSIGN CHECK SUM TO THE POINTER
	l_abTempPC[7] = l_bCheckSum ;


	// 2.3 CALCULATE SESSION KEY Kse
	for (l_iByte = 0; l_iByte < 8; l_iByte++)
		l_abKeySession[l_iByte]  =	l_abTempPC[l_iByte] ^ l_abRandom[l_iByte] ;
									//ExchangeKey permuted ^ Random

	// 3. CALCULATION OF THE ENCRYPTED KEY
	// ------------------------------------
	// 3.1 PERMUTE NEW KEY
	// Init permuted key to zero
	for(l_iIndice = 0; l_iIndice < 8; l_iIndice = l_iIndice + 1)
		l_abTempPC[l_iIndice] = 0;

	// For the key permutation method used in the digicash 8051 DES implementation
	for (l_bBitAdd = 0; l_bBitAdd < 7; l_bBitAdd++)
	{
		for (l_bByteAdd = 0; l_bByteAdd < 8; l_bByteAdd++)
			l_abTempPC[l_bBitAdd] = (l_abTempPC[l_bBitAdd] >> 1) | ((p_abNewKey[l_bByteAdd]<<l_bBitAdd) & 0x80);
	}

	// 3.2 REPLACE 8th BYTE WITH CHECKSUM
	// Loop on the 7 high byte
	l_bCheckSum = 0x00;
	for (l_iIndice = 0; l_iIndice < 7; l_iIndice++)
		l_bCheckSum = l_bCheckSum ^ (*(l_abTempPC + l_iIndice)) ;

	// Compliment the checksum
	l_bCheckSum = l_bCheckSum ^ 0xFF ;

	// Assign checksum to the pointer
	l_abTempPC[7] = l_bCheckSum ;



	// 3.3 CALCUL ENCRYPTED KEY Kenc.MK is the master key application: CK = f(SK,MK)
	//       Encrypted key = Session Key ^ New Key permuted
	for (l_iByte = 0; l_iByte < 8; l_iByte++)
		l_abKeyEncrypted[l_iByte] = l_abKeySession[l_iByte] ^ l_abTempPC[l_iByte] ;


	// 4. CALCULATE THE FINAL CHECK SUM
	// ---------------------------------
	for (l_iByte = 0; l_iByte < 4; l_iByte++)
		l_abCheckSum[l_iByte] = l_abCommand[l_iByte] ^ l_abCommand[l_iByte + 4] ^ l_abTempPC[l_iByte] ^ l_abTempPC[l_iByte + 4];


	// 5. SEND COMMAND TO THE READER
	//-------------------------------
	l_abDataIn[0]  = l_abKeyEncrypted[0];
	l_abDataIn[1]  = l_abKeyEncrypted[1];
	l_abDataIn[2]  = l_abKeyEncrypted[2];
	l_abDataIn[3]  = l_abKeyEncrypted[3];
	l_abDataIn[4]  = l_abKeyEncrypted[4];
	l_abDataIn[5]  = l_abKeyEncrypted[5];
	l_abDataIn[6]  = l_abKeyEncrypted[6];
	l_abDataIn[7]  = l_abKeyEncrypted[7];
	l_abDataIn[8]  = l_abCheckSum[0];
	l_abDataIn[9]  = l_abCheckSum[1];
	l_abDataIn[10] = l_abCheckSum[2];
	l_abDataIn[11] = l_abCheckSum[3];

	switch (p_bLoadingMode)
	{
		case KEY_LOADING_XOR_KE:
			l_wStatus = CLib_w_LoadKeyFile(0x00, p_bKeyNum, l_abDataIn);
			return(l_wStatus);
		break;
		case KEY_LOADING_XOR_KO:
			l_wStatus = CLib_w_LoadKeyFile(0x40, p_bKeyNum, l_abDataIn);
			return(l_wStatus);
		break;
		default:
			return(0x6D00);
		break;
	}
}


//------------------------------------------------------------------------
// Function : CLib_w_SelectAuthPage
//
// In		: p_bNumKey		-> Key number for authentication 
//			  p_bPage		-> Page to select
//			  p_bProtocol	-> Command protocol
//			  p_abSN		-> Chip Serial Number
// Out		: p_abConfBlock	-> Page configuration block
// Return	: Status word from the coupler
// Usage	: Select a page on multi-application chip
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SelectAuthPage(BYTE p_bKeyNum, BYTE p_bPage, BYTE p_bProtocol, BYTE *p_abSN, BYTE *p_abConfBlock)
{
	BYTE l_abDataReceived[8];
	BYTE l_bP1Value;
	BYTE l_bP2Value;
	BYTE l_bDataIn = 0x00;
	WORD l_wStatusWord;
	
	// Test security
	if (p_bKeyNum != KEY_NO_AUTH)
	{
		// Diversify choosen key (to set it as default key)
		l_wStatusWord = CLib_w_DiversifyKey(p_bKeyNum, p_abSN, l_abDataReceived);
		if (l_wStatusWord != STATUS_OK) 
			return(l_wStatusWord);
		// Set secured parameters
		l_bP1Value = 0x0C + p_bProtocol;
		l_bP2Value = (p_bKeyNum - 1) << 4 ;
		l_bP2Value |= p_bPage;
	}
	else
	{
		// Set unsecured parameters
		l_bP1Value = 0x04 | p_bProtocol;
		l_bP2Value = p_bPage;
	}

	// Initialize page selection command
	return CLib_w_SendISOCommand(ISO_OUT, 8, 0x80, 0xA6, l_bP1Value, l_bP2Value, 0x08, NULL, p_abConfBlock);
}


//------------------------------------------------------------------------
// Function : CLib_w_Authentify
//
// In		: p_bKeyNum		-> Key number for authentication 
//			  p_bProtocol	-> Command protocol
//			  p_abSN		-> Chip Serial Number
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: Authentify a page with choosen key
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_Authentify(BYTE p_bKeyNum, BYTE p_bProtocol, BYTE *p_abSN)
{
	BYTE l_abDataReceived[8];
	BYTE p_abConfBlock[8];
	BYTE l_bP1Value;
	BYTE l_bP2Value;
	WORD l_wStatusWord;
	
	// Diversify choosen key (to set it as default key)
	l_wStatusWord = CLib_w_DiversifyKey(p_bKeyNum, p_abSN, l_abDataReceived);
	if (l_wStatusWord != 0x9000) return l_wStatusWord;

	// Initialize page selection command
	l_bP1Value = 0x08 | p_bProtocol;
	l_bP2Value = ((p_bKeyNum - 1) << 4);
	return CLib_w_SendISOCommand(ISO_OUT, 8, 0x80, 0xA6, l_bP1Value, l_bP2Value, 0x08, NULL, p_abConfBlock);
}


//------------------------------------------------------------------------
// Function : CLib_w_ReadBlock
//
// In		: p_bNumBlock	-> First block to read
//			  p_bNbBlocks	-> Number of blocks to read
//			  l_bProtocol	-> Command protocol
// Out		: p_abData		-> Data on the chip
// Return	: Status word from the coupler
// Usage	: Read blocks in chip memory
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_ReadBlock(BYTE p_bNumBlock, BYTE p_bNbBlocks, BYTE l_bProtocol, BYTE *p_abData)
{
	BYTE l_abChipCommand[2];
	WORD l_wStatus = 0xFFFF;
	int	l_indBlock;

	// Initialize read loop
	l_abChipCommand[0] = 0x0C ; // Chip read command
	for (l_indBlock = 0; l_indBlock < p_bNbBlocks; l_indBlock++)
	{
		// Initialize current command
		l_abChipCommand[1] = p_bNumBlock + l_indBlock;
		// Send ISO Command
		l_wStatus = CLib_w_Transmit((BYTE)(0xC4 + l_bProtocol), 0x08, 0x02, l_abChipCommand , p_abData + (l_indBlock * 8));
		// Test the last command status word (exit if error)
		if (l_wStatus != STATUS_OK)
			return(l_wStatus);
	}

	// Return last Status Word
	return(l_wStatus);
} // CLib_w_ReadBlock()


//------------------------------------------------------------------------
// Function : CLib_w_ReadBlockBy4
//
// In		: p_bNumBlock	-> First block to read
//			  p_bNbBlocks	-> Number of blocks to read
//			  l_bProtocol	-> Command protocol
// Out		: p_abData		-> Data on the chip
// Return	: Status word from the coupler
// Usage	: Read blocks in chip memory, use 4 blocks reading command
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_ReadBlockBy4(BYTE p_bNumBlock, BYTE p_bNbBlocks, BYTE l_bProtocol, BYTE *p_abData)
{
	BYTE l_abChipCommand[2];
	WORD l_wStatus = 0xFFFF;

	int	l_indBlock;

	// Initialize read loop
	l_abChipCommand[0] = 0x06 ; // Chip read4 command
	for (l_indBlock = 0; l_indBlock < p_bNbBlocks; l_indBlock += 4)
	{
		// Initialize current command
		l_abChipCommand[1] = p_bNumBlock + l_indBlock;
		// Send ISO Command
		l_wStatus = CLib_w_Transmit((BYTE)(0xC4 + l_bProtocol), 0x20, 0x02, l_abChipCommand , p_abData + (l_indBlock * 8));
		// Test the last command status word (exit if error)
		if (l_wStatus != STATUS_OK)
			return(l_wStatus);
   }

	// Return last Status Word
	return(l_wStatus);
} // CLib_w_ReadBlock()

//------------------------------------------------------------------------
// Function : CLib_w_WriteBlock
//
// In		: p_bNumBlock	-> First block to update
//			  p_bNbBlocks	-> Number of block to update
//			  p_bProtocol	-> Command protcol
//			  l_bAuth		-> Is update secured?
//			  p_abData		-> Data to Write
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: Update data in chip memory
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_WriteBlock(BYTE p_bNumBlock, BYTE p_bNbBlocks, BYTE p_bProtocol, BOOL p_bAuth, BYTE *p_abData)
{
	BYTE l_abDataOut[8]; 
	BYTE l_abDataIn[10];
	WORD l_wStatus;
   
	int	 l_indByte;
	int	 l_indBlock;

	// Initialize data loop
	l_abDataIn[0] = 0x87 ; // Chip Update command
	for (l_indBlock = 0; l_indBlock < p_bNbBlocks; l_indBlock++)
	{
		// Initialisation
		l_abDataIn[1] = p_bNumBlock + l_indBlock;  // Current write Address

		for (l_indByte = 0; l_indByte < 8; l_indByte++) 
		{
			l_abDataIn[l_indByte + 2] = p_abData[(8 * l_indBlock) + l_indByte];
		}
		// Send ISO Command
		if (p_bAuth)
			l_wStatus = CLib_w_Transmit((BYTE)(0x6C + p_bProtocol), 0x08, 0x0A, l_abDataIn , l_abDataOut);
		else
			l_wStatus = CLib_w_Transmit((BYTE)(0xE4 + p_bProtocol), 0x08, 0x0A, l_abDataIn , l_abDataOut);
		// Test the last command status word (exit if error)
		if (l_wStatus != STATUS_OK) 
			return(l_wStatus);
	}

	// Return last Status Word
	return(l_wStatus);
} // CLib_w_WriteBlock()


//------------------------------------------------------------------------
// Function : CLib_w_DecreaseCounter
//
// In		: p_wUnitToDecrease	-> units to decrease from the counter
//			  p_bProtocol		-> Command protocol
// Out		: p_wCounterNew		-> New value of the counter
// Return	: Status word from the coupler
// Usage	: Decrease the counter of the transmitted number
//
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_DecreaseCounter(WORD p_wUnitToDecrease, BYTE p_bProtocol, WORD *p_wCounterNew)
{
	BYTE l_abCounterData[10];
	BYTE l_abRechargerValue[3];
	BYTE l_abCounterValue[3];
	WORD l_wStatus;
	WORD l_wCounterValue;
	BYTE l_bStagePosition;

	l_bStagePosition = 0;

	// Read Block 2 to determine current value and which stage to write
	l_wStatus = CLib_w_ReadBlock(0x02, 1, p_bProtocol, l_abCounterData);
	if (l_wStatus != STATUS_OK) 
		return(l_wStatus);

	if (  (l_abCounterData[0] == 0xFF) 
		& (l_abCounterData[1] == 0xFF)
		& (l_abCounterData[2] == 0xFF) 
		& (l_abCounterData[3] == 0xFF))
			l_bStagePosition = 4;

	l_abCounterValue[0]   = l_abCounterData[0 + l_bStagePosition];
	l_abCounterValue[1]   = l_abCounterData[1 + l_bStagePosition];
	l_abRechargerValue[0] = l_abCounterData[2 + l_bStagePosition];
	l_abRechargerValue[1] = l_abCounterData[3 + l_bStagePosition];


	// Calculate new value
	l_wCounterValue = (l_abCounterValue[1] << 8) + (l_abCounterValue[0] & 0xFF);
	l_wCounterValue -= p_wUnitToDecrease;
	l_abCounterValue[0]  = (BYTE)(l_wCounterValue & 0xFF);
	l_abCounterValue[1]  = (BYTE)(l_wCounterValue >> 8);
	
	// Write new value in the counter
	l_abCounterData[0 + l_bStagePosition] = l_abCounterValue[0];
	l_abCounterData[1 + l_bStagePosition] = l_abCounterValue[1];
	l_abCounterData[2 + l_bStagePosition] = l_abRechargerValue[0];
	l_abCounterData[3 + l_bStagePosition] = l_abRechargerValue[1];
	l_abCounterData[4 - l_bStagePosition] = 0xFF;
	l_abCounterData[5 - l_bStagePosition] = 0xFF;
	l_abCounterData[6 - l_bStagePosition] = 0xFF;
	l_abCounterData[7 - l_bStagePosition] = 0xFF;

	// Write new value and return resuls
	*p_wCounterNew = l_wCounterValue;
	return(CLib_w_WriteBlock(0x02, 0x01, p_bProtocol, TRUE, l_abCounterData));
} // CLib_w_Decrease()


//------------------------------------------------------------------------
// Function : CLib_w_IncreaseCounter
//
// In		: p_wNewValue				-> New value of the counter
//			  p_bProtocol				-> Command Protocol
// Out		: p_wRechargingCounterNew	-> New value of the recharging counter
// Return	: Status word from the coupler
// Usage	: Increase the counter value and decrease the
//				recharging counter of 1 unit
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_IncreaseCounter(WORD p_wNewValue, BYTE p_bProtocol, WORD *p_wRechargingCounterNew)
{
	BYTE l_abCounterData[10] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
	BYTE l_abCounterValue[3];
	BYTE l_abRechargerValue[3];
	WORD l_wRechargerValue;
	WORD l_wStatus;
	int l_iStagePosition = 0;


	// Read Block 2 to determine current value and which stage to write
	l_wStatus = CLib_w_ReadBlock (0x02, 0x01, p_bProtocol, l_abCounterData);
	if (l_wStatus != STATUS_OK) 
		return(l_wStatus);

	if (	(l_abCounterData[0] == 0xFF)  
		&	(l_abCounterData[1] == 0xFF)
		&	(l_abCounterData[2] == 0xFF) 
		&	(l_abCounterData[3] == 0xFF))
			l_iStagePosition = 4;

	l_abCounterValue[0]    = l_abCounterData[0 + l_iStagePosition];
	l_abCounterValue[1]    = l_abCounterData[1 + l_iStagePosition];
	l_abRechargerValue[0] = l_abCounterData[2 + l_iStagePosition];
	l_abRechargerValue[1] = l_abCounterData[3 + l_iStagePosition];


	// Calculate new value
	l_wRechargerValue = (l_abRechargerValue[1] << 8) + (l_abRechargerValue[0] & 0xFF);;
	l_wRechargerValue -= 1;
	l_abRechargerValue[0]  = (BYTE)(l_wRechargerValue & 0xFF);
	l_abRechargerValue[1]  = (BYTE)(l_wRechargerValue >> 8);

	l_abCounterValue[0]  = p_wNewValue & 0xFF;
	l_abCounterValue[1]  = p_wNewValue >> 8;

	// Write new value in the counter
	l_abCounterData[4 - l_iStagePosition] = 0xFF;
	l_abCounterData[5 - l_iStagePosition] = 0xFF;
	l_abCounterData[6 - l_iStagePosition] = 0xFF;
	l_abCounterData[7 - l_iStagePosition] = 0xFF;
	l_abCounterData[0 + l_iStagePosition] = 0xFF;
	l_abCounterData[1 + l_iStagePosition] = 0xFF;
	l_abCounterData[2 + l_iStagePosition] = l_abRechargerValue[0];
	l_abCounterData[3 + l_iStagePosition] = l_abRechargerValue[1];

	l_wStatus = CLib_w_WriteBlock(0x02, 0x01, p_bProtocol, TRUE, l_abCounterData);

	if (l_wStatus == STATUS_OK)
	{
		// Write new value in the counter
		l_abCounterData[0 + l_iStagePosition] = 0xFF;
		l_abCounterData[1 + l_iStagePosition] = 0xFF;
		l_abCounterData[2 + l_iStagePosition] = 0xFF;
		l_abCounterData[3 + l_iStagePosition] = 0xFF;
		l_abCounterData[4 - l_iStagePosition] = l_abCounterValue[0];
		l_abCounterData[5 - l_iStagePosition] = l_abCounterValue[1];
		l_abCounterData[6 - l_iStagePosition] = l_abRechargerValue[0];
		l_abCounterData[7 - l_iStagePosition] = l_abRechargerValue[1];
		*p_wRechargingCounterNew = l_wRechargerValue;
		l_wStatus = CLib_w_WriteBlock(0x02, 0x01, p_bProtocol, TRUE, l_abCounterData);
	}

	return(l_wStatus);
} // CLib_w_Increase()


//------------------------------------------------------------------------
// Function : CLib_EASActivate
//
// In		: p_bProtocol	-> Command Protocol
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: Activate EAS functionality (for fast anti-collision)
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_EASActivate(BYTE p_bProtocol)
{
	WORD	l_wStatusWord;
	BYTE	l_abConfBlock[8];

	// Grab chip Configuration block
	l_wStatusWord = CLib_w_ReadBlock(0x01, 0x01, p_bProtocol, l_abConfBlock);
	// Verify function result (exit if not OK) 
	if (l_wStatusWord != STATUS_OK)
		return l_wStatusWord;

	// Test Current EAS
	if (l_abConfBlock[6] == 0xFF)
		// Return error Status Word
		return (0xFFFF);
	
	// Activate EAS and return Status Word
	l_abConfBlock[6] = 0xFF;
	return CLib_w_WriteBlock(0x01, 0x01, p_bProtocol, TRUE, l_abConfBlock);
} // CLib_EASActivate()


//------------------------------------------------------------------------
// Function : CLib_w_EASDeactivate
//
// In		: p_bProtocol	-> Command Protocol
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: Deactivate EAS functionality (slower anti-collision)
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_EASDeactivate(BYTE l_bProtocol)
{
	WORD	l_wStatusWord;
	BYTE	l_abConfBlock[8];

	// Grab chip Configuration block
	l_wStatusWord = CLib_w_ReadBlock(0x01, 0x01, l_bProtocol, l_abConfBlock);
	// Verify function result (exit if not OK) 
	if (l_wStatusWord != STATUS_OK)
		return l_wStatusWord;

	// Test Current EAS
	if (l_abConfBlock[6] == 0x7F)
		// Return error Status Word
		return (0xFFFF);
	
	// Deactivate EAS and return Status Word
	l_abConfBlock[6] = 0x7F;
	return CLib_w_WriteBlock(0x01, 0x01, l_bProtocol, TRUE, l_abConfBlock);
} // CLib_w_EASDeactivate()


//------------------------------------------------------------------------
// Function : CLib_w_SetUserProtocol
//
// In		: l_bUserProtocol	-> Final User Protocol
// Out		: - None -
// Return	: Status word from the coupler
// Usage	: Change protocol #3 configuration (user protocol)
//------------------------------------------------------------------------
_CDECL_EXPORT_ WORD _CDECL_FUNCTION_ CLib_w_SetUserProtocol(BYTE l_bUserProtocol)
{
	WORD	l_wStatusWord;
	BYTE	l_bParameter1;
	BYTE	l_bParameter2;

	// Test choosen user protocol
	switch (l_bUserProtocol)
	{
	case USER_PROTOCOL_15693_3 :
		// Complete command set
		l_bParameter1 = 0x21;
		l_bParameter2 = 0x31;
		break;
	case USER_PROTOCOL_14443_A_3 :
		// Complete command set
		l_bParameter1 = 0x32;
		l_bParameter2 = 0x12;
		break;
	case USER_PROTOCOL_14443_B_3 :
		// Complete command set
		l_bParameter1 = 0x44;
		l_bParameter2 = 0x31;
		break;
	default : return(0xFFFF); // Returns error
	}

	// Send first Setup Command
	l_wStatusWord = CLib_w_SetStatus(COUPLER_RAM, 0x5E, &l_bParameter1);
	if (l_wStatusWord == STATUS_OK)
		// Send second Setup Command
		return CLib_w_SetStatus(COUPLER_RAM, 0x5F, &l_bParameter2);
	else
		// Return error status
		return l_wStatusWord;
} // CLib_w_SetUserProtocol()
