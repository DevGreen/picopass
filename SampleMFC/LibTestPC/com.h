//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	Com.c
//			IMPLEMENTED VERSION FOR WINDOWS OS / VC++ 6
//		Low level function that manage communication
//		with the coupler and the ISO 7816 T=0
//		protocol.
//
// Documentation :
//		The T=0 protocol is detailed in the coupler
//		Datasheet.
//
// Version :    1.5.8
// Date :       June, 22th 2004
// Author :     Joris JOURDAIN / Frederic CAPASSO
//
//------------------------------------------------------------------

#include <windows.h>
#include "it_const.h"

// Global variables declaration
HANDLE	g_hndCOMPort;

//-----------------------------------------------------------------------------
// Function name : v_sendByte
//-----------------------------------------------------------------------------
// Description : Send a byte to the host following the interface defined
//               when a command has been received (parallel or serial).
//
// IN       :   byte        byte to send
// OUT      :   - none -
// RETURN   :   - none -
//
// Notes    :
//-----------------------------------------------------------------------------
void v_sendByte(BYTE byte)
{

	BYTE		l_abBuffer[256];
	DWORD		l_iAckLength;
	
    //--------------------------------
    // Send byte to RS232 port buffer
    //
	l_abBuffer[0] = byte;
	WriteFile(g_hndCOMPort, l_abBuffer, 1, &l_iAckLength, NULL);
}	



//-----------------------------------------------------------------------------
// Function name :  b_getByte

//-----------------------------------------------------------------------------
// Description : Get a byte from the host.
//
// IN       :
// OUT      :
// RETURN   :   - RXREG -
//
// Notes    :
//-----------------------------------------------------------------------------
BYTE b_getByte()
{
	BYTE		l_abBuffer[2];
	DWORD		l_iAckLength = 1;

    //--------------------------------
    // Get byte from RS232 port buffer
    //
	if (ReadFile(g_hndCOMPort, l_abBuffer, 1, &l_iAckLength, NULL))
		return(l_abBuffer[0]);
	else
		return(0x00);
}


//-----------------------------------------------------------------------------
// Function name : GetStatus

//-----------------------------------------------------------------------------
// Description : Get the 2 status bytes from the coupler
//
// IN       :
// OUT      :
// RETURN   :   Status word
//
// Notes    :
//-----------------------------------------------------------------------------
WORD GetStatus()
{
   WORD l_wStatus;

   l_wStatus = (b_getByte() << 8);
   l_wStatus |= b_getByte();

   return l_wStatus;
}


//-----------------------------------------------------------------------------
// Function name : b_ConnectCoupler

//-----------------------------------------------------------------------------
// Description : Synchronize host and coupler buffers
//
// IN       :	- None -
// OUT      :	- None -
// RETURN   :   True if succeeded, False if failed
//
// Notes    :	You have to connect RS232 port before launching this procedure. 
//				RS232 Connection function v_InitComHost has to be implemented.
//-----------------------------------------------------------------------------
BOOL b_ConnectCoupler(void)
{
   WORD l_wStatus;
   int	l_indSynchro;
   BOOL	l_bSynchro;
   
   // Initialize procedure variables
   l_wStatus = 0;
   l_bSynchro = FALSE;

   // Loop until synchrinisation reached or coupler did not respond after 12 attempts
   for (l_indSynchro = 1; l_indSynchro < 6; l_indSynchro++)
   {
	   	// Send 0x00 to the coupler
		v_sendByte(0);
		// Get Status Word
		l_wStatus = GetStatus();
		// Test Status Word
		if ((l_wStatus == 0x6B00) || 
			(l_wStatus == 0x6D00) || 
			(l_wStatus == 0x6E00) ||
			(l_wStatus == 0x9835) ||
			(l_wStatus == 0x6A82))
		{
		   // Status Word returned : synchro reached
		   l_bSynchro = TRUE;
		   break;
		}
   } 
   
   // Return result (synchronized or not)
   return l_bSynchro;
}


//-----------------------------------------------------------------------------
// Function name : b_InitComHost
//-----------------------------------------------------------------------------
// Description : Initialise the internal UART to the defined baudrate.
//
// IN       :   - none -
// OUT      :   - none -
// RETURN   :   - none -
//
// Notes    :	You will have to launch b_ConnectCoupler after this procedure.
//-----------------------------------------------------------------------------
BOOL b_InitComHost(LPCTSTR p_szComPortName)
{
	BOOL			l_bConnection;
	COMMTIMEOUTS	CommTimeOuts = {0};
	DCB				CommParameters = {0};

	//--------------------------------
    // Initialise Serial communication
    //
    CommTimeOuts.ReadIntervalTimeout = 0;
    CommTimeOuts.ReadTotalTimeoutConstant = 20;

	// Open serial port and get handler
	g_hndCOMPort =	CreateFile( 
						p_szComPortName, 
						GENERIC_READ | GENERIC_WRITE,					// In/Out mode
						0,												// exclusive access
						NULL,											// no security attrs
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						NULL);

	// Build DCB
	BuildCommDCB("9600,e,8,2", &CommParameters);
    SetCommState(g_hndCOMPort, &CommParameters);
    SetCommTimeouts(g_hndCOMPort, &CommTimeOuts);

	// Syncrhonize buffer
	l_bConnection = b_ConnectCoupler();
	if (!l_bConnection )
		g_hndCOMPort = INVALID_HANDLE_VALUE;
		
    CommTimeOuts.ReadIntervalTimeout = 100;
    CommTimeOuts.ReadTotalTimeoutConstant = 100;
    SetCommTimeouts(g_hndCOMPort, &CommTimeOuts);

	// return synchronisation result
	return(l_bConnection); 
}


BOOL b_Disconnect(void)
{
	BOOL l_bDiscon;
	l_bDiscon = CloseHandle(g_hndCOMPort);
	if (l_bDiscon) g_hndCOMPort = INVALID_HANDLE_VALUE;

	return(l_bDiscon);
}
//------------------------------------------------------------------------
// Function IsoExchange
//------------------------------------------------------------------------

// In       IsoType			: Identify the type of the exchange
//          p_buffer	   	: Buffer containing instruction, data
//          p_iLength      : Length of the data to receive
//
// Out      p_buffer       : contain the answer of the coupler
//
// Return   OK or ERROR
//
// Usage    This function execute the ISO7816 data exchange with the coupler.
//------------------------------------------------------------------------
WORD IsoExchange(BYTE p_bIsoType, BYTE *p_abCommand, int p_iInLength, BYTE *p_abSentData, int p_iOutLength, BYTE *p_abResponse)
{
	// Local variable declaration
	BYTE ACK;
	WORD l_wStatus;

	int i; //for counter

	// Send command
	v_sendByte(p_abCommand[CMD_CLA]);
	v_sendByte(p_abCommand[CMD_INS]);
	v_sendByte(p_abCommand[CMD_P1]);
	v_sendByte(p_abCommand[CMD_P2]);
	v_sendByte(p_abCommand[CMD_P3]);

	switch (p_bIsoType)
	{
	case ISO_NONE :
		// Neither data sent nor received
		return GetStatus();

	case ISO_IN :
		// Verify ACK
		ACK = b_getByte();
		if (ACK != p_abCommand[CMD_INS])
		{
			l_wStatus = (ACK<<8);
			l_wStatus = l_wStatus | b_getByte();
			return l_wStatus;
		}
		// Send buffer data
		for (i = 0; i < p_iInLength ; i++)
		{
			v_sendByte(p_abSentData[i]);
		}
		return GetStatus();

	case ISO_OUT :
		// Verify ACK
		ACK = b_getByte();
		if (ACK != p_abCommand[CMD_INS])
		{
			l_wStatus = (ACK << 8);
			l_wStatus |= b_getByte();
			return l_wStatus;
		}
		// Grab buffer data
		for (i = 0; i < p_iOutLength; i++)
		{
			p_abResponse[i] = b_getByte();
		}
		return GetStatus();

	case ISO_IN_OUT :
		// Verify ACK
		ACK = b_getByte();
		if (ACK != p_abCommand[CMD_INS])
		{
			l_wStatus = (ACK<<8);
			l_wStatus = l_wStatus | b_getByte();
			return l_wStatus;
		}
		// Send buffer data in
		for (i = 0; i < p_iInLength ; i++)
		{
			v_sendByte(p_abSentData[i]);
		}
		// Verify ACK
		ACK = b_getByte();
		if (ACK != p_abCommand[CMD_INS])
		{
			l_wStatus = (ACK<<8);
			l_wStatus = l_wStatus | b_getByte();
			return l_wStatus;
		}
		// Grab buffer data out
		for (i = 0; i < p_iOutLength; i++)
		{
			p_abResponse[i] = b_getByte();
		}
		return GetStatus();
	default : return(0x0000);
	}
} // IsoExchange()

