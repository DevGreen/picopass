//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	Function.c
//		High level function for PICO family couplers and chips
//
//
// Version :    2.0.0
// Date :       January, 15th 2006
// Author :     Frederic CAPASSO
//
//------------------------------------------------------------------

//#define BYTE 	int	
typedef unsigned char       BYTE;
typedef unsigned short      WORD;


// STATUS DEFINITION
#define	STATUS_OK					0x9000
#define	STATUS_CARD_NOT_IDENTIFIED	0x6982
#define	STATUS_FILE_NOT_FOUND		0x6A82

// ISO 7816-4 command set format
#define	CMD_CLA	0
#define	CMD_INS	1
#define	CMD_P1	2
#define	CMD_P2	3
#define	CMD_P3	4

// T=0 protocol direction
#define	ISO_NONE		0x00
#define	ISO_IN  		0x01
#define	ISO_OUT 		0x02
#define	ISO_IN_OUT		0x03

// Selection Options (for CLib_w_SelectCard) 
#define SELECT_NO_AUTH	0x00
#define SELECT_AUTH_KC	0x10
#define SELECT_AUTH_KD	0x30
#define SELECT_LOOP		0x01
#define SELECT_HALT		0x02
#define SELECT_PRE		0x08

// Chip types to select (for CLib_w_SelectCard)
#define SELECT_CHIP_PICOPASS	0x01
#define SELECT_CHIP_PICOTAG		0x02
#define SELECT_CHIP_PICOCRYPT	0x04
#define SELECT_CHIP_USER		0x08
#define SELECT_CHIP_ALL			0x0F

// Authentification key (for page selection, authentication and key loading)
#define KEY_EXCHANGE	0x00
#define	KEY_PAGE0_KD	0x01
#define	KEY_PAGE0_KC	0x02
#define	KEY_PAGE1_KD	0x03
#define	KEY_PAGE1_KC	0x04
#define	KEY_PAGE2_KD	0x05
#define	KEY_PAGE2_KC	0x06
#define	KEY_PAGE3_KD	0x07
#define	KEY_PAGE3_KC	0x08
#define	KEY_PAGE4_KD	0x09
#define	KEY_PAGE4_KC	0x0A
#define	KEY_PAGE5_KD	0x0B
#define	KEY_PAGE5_KC	0x0C
#define	KEY_PAGE6_KD	0x0D
#define	KEY_PAGE6_KC	0x0E
#define	KEY_PAGE7_KD	0x0F
#define	KEY_PAGE7_KC	0x10
#define	KEY_TRANSPORT	0x11
#define KEY_NO_AUTH		0x18

// Protocol (for High-level functions & commands)
#define PROTOCOL_PICO_14443_B	0x00
#define PROTOCOL_PICO_15693		0x01
#define PROTOCOL_PICO_14443_A	0x02
#define PROTOCOL_USER			0x03 // Not available for SelectAuthPage and Authentify

// USer Protocol (for CLib_w_SetUserProtocol)
#define USER_PROTOCOL_15693_3	0x00
#define USER_PROTOCOL_14443_A_3	0x01
#define USER_PROTOCOL_14443_B_3	0x02

// Key Loading Mode (for CLib_w_KeyLoading)
#define KEY_LOADING_XOR_KE	0x00
#define KEY_LOADING_XOR_KO	0x01

// Data Location (for coupler Status handling)
#define COUPLER_EEPROM	0x00
#define COUPLER_IO		0x01
#define COUPLER_RAM		0x03

// T=CL dependant declarations declaration
#define TCL_RETRY_COUNT				3; // Change this value to set the retry count for T=CL command execution

#define CST_pcbtINoChaining			0x00
#define CST_pcbtIWithChaining		0x01
#define CST_pcbtRACK				0x02
#define CST_pcbtRNACK				0x03
#define CST_pcbtSWTX				0x04
#define CST_pcbtDESELECT			0x05
#define CST_pcbtError				0x06

#define CST_tclbr106kbps			0x00
#define CST_tclbr212kbps			0x01
#define CST_tclbr424kbps			0x02
#define CST_tclbr847kbps			0x03
#define CST_tclbrPCD_PICC			0x04

#define CST_tclctISO_14443_A_B_4	0x00
#define CST_tclctISO_14443_A_4		0x01
#define CST_tclctISO_14443_B_4		0x02

#define CST_tcltsCommand			0x00
#define CST_tcltsRACK				0x01
#define CST_tcltsRNACK				0x02
#define CST_tcltsSWTX				0x03
#define CST_tcltsChaining			0x04
#define CST_tcltsDESELECT			0x05
#define CST_tcltsError				0x06
#define CST_tcltsEnd				0x07

const	BYTE PCB_I					= 0x02;
const	BYTE PCB_R					= 0xA2;
const	BYTE PCB_S					= 0xC2;

const	BYTE PCB_WITH_CID			= 0x08;

const	BYTE PCB_I_WITH_NAD			= 0x04;
const	BYTE PCB_I_WITH_CHAINING	= 0x10;

const	BYTE PCB_R_WITH_ACK			= 0x00;
const	BYTE PCB_R_WITH_NACK		= 0x10;

const	BYTE PCB_S_WITH_DESELECT	= 0x00;
const	BYTE PCB_S_WITH_WTX			= 0x30;

const	BYTE ISO_14443_A_RATS 		= 0xE0;
const	BYTE ISO_14443_B_ATTRIB		= 0x1D;

const	WORD ISO_14443_A_CRC_PRESET		= 0x6363;
const	WORD ISO_14443_B_CRC_PRESET		= 0xFFFF;
const	WORD ISO_14443_A_B_CRC_POLYNOM	= 0x8408;
