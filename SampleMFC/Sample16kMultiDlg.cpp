// Sample16kMultiDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Sample16kMulti.h"
#include "Sample16kMultiDlg.h"

#include "LibTestPC\function.h"

#include <comdef.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// XMXPanel enumerative replacement (only used enumerative values are replaced)
#define mpkExchange	0
#define mpkP0Kd		1
#define mpkP0Kc		2
#define mpkNoAuth	24

#define mklmXorKe	0
#define mklmXorKo	1


/////////////////////////////////////////////////////////////////////////////
// CSample16kMultiDlg dialog

CSample16kMultiDlg::CSample16kMultiDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSample16kMultiDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSample16kMultiDlg)
	m_lblConfBlock = _T("");
	m_lblConnectionStatus = _T("");
	m_lblData = _T("");
	m_lblSN = _T("");
	m_txtComPort = _T("");
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CSample16kMultiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSample16kMultiDlg)
	DDX_Control(pDX, IDC_CMB_AUTH, m_cmbAuth);
	DDX_Control(pDX, IDC_CMB_PAGE, m_cmbPage);
	DDX_Control(pDX, IDC_CMB_BLOCK, m_cmbBlock);
	DDX_Text(pDX, IDC_LBL_CONF_BLOCK, m_lblConfBlock);
	DDX_Text(pDX, IDC_LBL_CONNECTION_STATUS, m_lblConnectionStatus);
	DDX_Text(pDX, IDC_LBL_DATA, m_lblData);
	DDX_Text(pDX, IDC_LBL_SN, m_lblSN);
	DDX_Text(pDX, IDC_TXT_COMPORT, m_txtComPort);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CSample16kMultiDlg, CDialog)
	//{{AFX_MSG_MAP(CSample16kMultiDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BT_CONNECT, OnBtConnect)
	ON_BN_CLICKED(IDC_BT_DISCONNECT, OnBtDisconnect)
	ON_BN_CLICKED(IDC_BT_LOAD_KEYS, OnBtLoadKeys)
	ON_BN_CLICKED(IDC_BT_READ, OnBtRead)
	ON_BN_CLICKED(IDC_BT_SELECT_CHIP, OnBtSelectChip)
	ON_BN_CLICKED(IDC_BT_SELECT_PAGE, OnBtSelectPage)
	ON_BN_CLICKED(IDC_BT_WRITE, OnBtWrite)
	ON_CBN_SELCHANGE(IDC_CMB_AUTH, OnSelchangeCmbAuth)
	ON_BN_CLICKED(IDC_BT_HALT, OnBtHalt)
	ON_BN_CLICKED(IDC_BT_RESELECT, OnBtReselect)
	ON_BN_CLICKED(IDC_BT_AUTHENTIFY, OnBtAuthentify)
	ON_BN_CLICKED(IDC_BT_READ4, OnBtRead4)
	ON_BN_CLICKED(IDC_BT_DISABLE, OnBtDisable)
	ON_BN_CLICKED(IDC_BT_ENABLE, OnBtEnable)
	ON_BN_CLICKED(IDC_BT_GET_STATUS, OnBtGetStatus)
	ON_BN_CLICKED(IDC_BT_SET_STATUS, OnBtSetStatus)
	ON_BN_CLICKED(IDC_BT_EAS_DETECT, OnBtEasDetect)
	ON_BN_CLICKED(IDC_BT_EAS_ACTIVATE, OnBtEasActivate)
	ON_BN_CLICKED(IDC_BT_EAS_DEACTIVATE, OnBtEasDeactivate)
	ON_BN_CLICKED(IDC_BT_SET_USER_PROTOCOL, OnBtSetUserProtocol)
	ON_BN_CLICKED(IDC_BT_Decrease, OnBTDecrease)
	ON_BN_CLICKED(IDC_BT_INCREASE, OnBtIncrease)
	ON_EN_CHANGE(IDC_TXT_COMPORT, OnChangeTxtComport)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSample16kMultiDlg message handlers

BOOL CSample16kMultiDlg::OnInitDialog()
{
	int  l_indLoop;
	char *l_sItemLabel;

	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// Set default parameter values
	m_rdKd.SetCheck(1);
	m_iSelectedKey = 1;

	// Initialize ComboBoxes items
	m_cmbPage.InitStorage(10, 100);
	m_cmbBlock.InitStorage(50, 100);

	l_sItemLabel = (char *)malloc(15);

	for (l_indLoop = 0; l_indLoop < 8; l_indLoop++)
	{
		l_sItemLabel = itoa(l_indLoop, l_sItemLabel, 10);
		//l_sItemLabel = strcat("Page ", l_sItemLabel);
		m_cmbPage.AddString(l_sItemLabel);
	}

	for (l_indLoop = 6; l_indLoop < 32; l_indLoop++)
	{
		l_sItemLabel = itoa(l_indLoop, l_sItemLabel, 10);
		//l_sItemLabel = strcat("Page ", l_sItemLabel);
		m_cmbBlock.AddString(l_sItemLabel);
	}

	m_cmbAuth.AddString("No Auth");
	m_cmbAuth.AddString("Kd Auth");
	m_cmbAuth.AddString("Kc Auth");
	m_cmbAuth.SetCurSel(1);
	
	free(l_sItemLabel);

	// Initilize application
	m_txtComPort = "COM1";
	m_bConnected = false;
	
	// Set default ComboBoxes values
	m_cmbPage.SetCurSel(0);
	m_cmbBlock.SetCurSel(0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSample16kMultiDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSample16kMultiDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CSample16kMultiDlg::OnClose() 
{
	if (this->m_bConnected)
		// Try to disconnect the coupler
		b_Disconnect();

	CDialog::OnClose();
}

void CSample16kMultiDlg::OnBtConnect() 
{
	// Get if coupler already connected
	if (this->m_bConnected)
		MessageBox("Coupler already connected");
	else
	{
		if (b_InitComHost(this->m_txtComPort))
		{
			this->m_bConnected = true;
			// Reset field
			CLib_w_ResetField();
			// Update command access and display defaults
			this->m_lblConnectionStatus = "Coupler state : Connected";
			this->m_lblConfBlock = "Page conf block : ";
			this->m_lblSN = "Serial Number : ";
			this->m_lblData = "Data :";
			UpdateData(FALSE);
		}
		else
		{
			this->m_bConnected = false;
			MessageBox("Connection failed");
		}
	}
}

void CSample16kMultiDlg::OnBtDisconnect() 
{
	// Get if no coupler is connected
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Try to disconnect the coupler
		this->m_bConnected = (!b_Disconnect());
		if (this->m_bConnected)
			MessageBox("Disconnection failed");
		else
		{
			// Update command access and display defaults
			this->m_lblConnectionStatus = "Coupler state : Disconnected";
			this->m_lblConfBlock = "Page conf block : ";
			this->m_lblSN = "Serial Number : ";
			this->m_lblData = "Data :";
		}
	}
	
	UpdateData(FALSE);
}

void CSample16kMultiDlg::OnBtLoadKeys() 
{
	BYTE	l_abKeValue[8]	  = {0x5C, 0xBC ,0xF1, 0xDA, 0x45, 0xD5, 0xFB, 0x5F};
	BYTE	l_abKeNewValue[8] = {0x5C, 0xBC, 0xF1, 0xDA, 0x45, 0xD5, 0xFB, 0x5F};
	BYTE	l_abKdNewValue[8] = {0xF0, 0xE1, 0xD2, 0xC3, 0xB4, 0xA5, 0x96, 0x87};
	BYTE	l_abKcNewValue[8] = {0x76, 0x65, 0x54, 0x43, 0x32, 0x21, 0x10, 0x00};
	//BYTE	l_abKdNewValue[8] = {0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11};
	//BYTE	l_abKcNewValue[8] = {0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22};

	char	l_bstrDataReceived[80];
	char	l_acByte[3];
	WORD	l_wStatus;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Load exchange key
		l_wStatus = CLib_w_KeyLoading(KEY_EXCHANGE, KEY_LOADING_XOR_KE, l_abKeValue, l_abKeNewValue);
		if (l_wStatus != STATUS_OK)
		{
			strcpy(l_bstrDataReceived, "Exchange Key loading error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
			return;
		}

		// Load Kc and Kd
		l_wStatus = CLib_w_KeyLoading(KEY_PAGE0_KD, KEY_LOADING_XOR_KE, l_abKeNewValue, l_abKdNewValue);
		if (l_wStatus != STATUS_OK)
		{
			strcpy(l_bstrDataReceived, "Debit Key loading error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
			return;
		}
		
		l_wStatus = CLib_w_KeyLoading(KEY_PAGE0_KC, KEY_LOADING_XOR_KE, l_abKeNewValue, l_abKcNewValue);
		if (l_wStatus != STATUS_OK)
		{
			strcpy(l_bstrDataReceived, "Credit Key loading error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
			return;
		}
    
		MessageBox("Keys loaded successfully");
	}
}

void CSample16kMultiDlg::OnBtRead() 
{
	BYTE	l_abDataReceived[100];
	int		l_iBlockToRead;
	WORD	l_wStatus;

	char	l_bstrDataReceived[300];
	char	l_acByte[3];
	int		l_indByte;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Retreive selected block
		l_iBlockToRead = this->m_cmbBlock.GetCurSel() + 6;
		// READ content of selected block
		for (l_indByte = 0; l_indByte < 32; l_indByte++)
			l_abDataReceived[l_indByte] = 0x00;

		l_wStatus = CLib_w_ReadBlock(l_iBlockToRead, 4, PROTOCOL_PICO_15693, l_abDataReceived);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "Data : ");
			for (l_indByte = 0; l_indByte < 32; l_indByte++)
			{
				itoa(l_abDataReceived[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
			}
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Read error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtSelectChip() 
{
	BYTE	l_sSN[15];
	WORD	l_wStatus;
	int		l_indByte;
	char	l_bstrDataReceived[255] = "\0";
	char	l_acByte[4];

	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// SELECT regarding to the security
		switch (this->m_iSelectedKey)
		{
			case KEY_NO_AUTH: 
				l_wStatus = CLib_w_SelectCard(SELECT_NO_AUTH, SELECT_CHIP_PICOTAG, l_sSN);
			break;
			case KEY_PAGE0_KD:
				CLib_w_SelectCurrentKey(this->m_iSelectedKey);
				l_wStatus = CLib_w_SelectCard(SELECT_AUTH_KD, SELECT_CHIP_PICOTAG, l_sSN);
			break;
			case KEY_PAGE0_KC:
				CLib_w_SelectCurrentKey(this->m_iSelectedKey);
				l_wStatus = CLib_w_SelectCard(SELECT_AUTH_KC, SELECT_CHIP_PICOTAG, l_sSN);
			break;
			default:
				l_wStatus = 0xFFFF;
			break;
		}
		
		if (l_wStatus == STATUS_OK)
		{
			strcpy(l_bstrDataReceived, "Serial Number :");
			for (l_indByte = 0; l_indByte < 9; l_indByte++)
			{
				itoa(l_sSN[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
				this->m_abSerialNumber[l_indByte] = l_sSN[l_indByte + 1];
			}
			this->m_lblSN = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Selection error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtSelectPage() 
{
	BYTE	l_abDataReceived[9];
	int		l_iPage;
	WORD	l_wStatus;

	char	l_bstrDataReceived[80];
	char	l_acByte[3];
	int		l_indByte;

	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Grab choosen page
		l_iPage = this->m_cmbPage.GetCurSel();

		// Select choosen page
		l_wStatus = CLib_w_SelectAuthPage(this->m_iSelectedKey, l_iPage, PROTOCOL_PICO_15693, this->m_abSerialNumber, l_abDataReceived);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "Page conf block : ");
			for (l_indByte = 0; l_indByte < 8; l_indByte++)
			{
				itoa(l_abDataReceived[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
			}
			this->m_lblConfBlock = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Page selection error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtWrite() 
{
	BYTE	l_sData[9] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08};
	WORD	l_wStatus;
	int		l_iBlockToWrite;

	char	l_bstrDataReceived[80];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Retreive selected block
		l_iBlockToWrite = this->m_cmbBlock.GetCurSel() + 6;
		// Try to write selected block
		if (this->m_iSelectedKey == KEY_NO_AUTH)
			l_wStatus = CLib_w_WriteBlock(l_iBlockToWrite, 1, 0x01, FALSE, l_sData);
		else
			l_wStatus = CLib_w_WriteBlock(l_iBlockToWrite, 1, 0x01, TRUE, l_sData);
		if (l_wStatus == STATUS_OK)
		{
			strcpy(l_bstrDataReceived, "Data wrote on block ");
			strcat(l_bstrDataReceived, " : $01$02$03$04$05$06$07$08");
			m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Write error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnSelchangeCmbAuth() 
{
	// Get security
	switch (this->m_cmbAuth.GetCurSel())
	{
		case 0:	this->m_iSelectedKey = KEY_NO_AUTH;
				break;
		case 1:	this->m_iSelectedKey = KEY_PAGE0_KD;
				break;
		case 2:	this->m_iSelectedKey = KEY_PAGE0_KC;
				break;
		default:this->m_iSelectedKey = KEY_NO_AUTH;
				break;
	}
}


void CSample16kMultiDlg::OnBtHalt() 
{
	char	l_bstrDataReceived[80];
	char	l_acByte[3];
	WORD	l_wStatus;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Try to halt chip
		l_wStatus = CLib_w_Halt(PROTOCOL_PICO_15693);
		if (l_wStatus == STATUS_OK)
		{	
			m_lblData = "Chip Halted";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Halt error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtReselect() 
{
	char	l_bstrDataReceived[80];
	char	l_acByte[3];
	int		l_indByte;

	WORD	l_wStatus;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Try to halt chip
		l_wStatus = CLib_w_ReSelect(PROTOCOL_PICO_15693, this->m_abSerialNumber);
		if (l_wStatus == STATUS_OK)
		{	
			// Update result display
			strcpy(l_bstrDataReceived, "Chip reselected : ");
			for (l_indByte = 0; l_indByte < 8; l_indByte++)
			{
				itoa(this->m_abSerialNumber[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
			}
			m_lblSN = l_bstrDataReceived;
			m_lblData = "Data :";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "ReSelect error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

BOOL CSample16kMultiDlg::DestroyWindow() 
{
	// Disconnect coupler (if connected)
	if (this->m_bConnected) 
		b_Disconnect();
	
	return CDialog::DestroyWindow();
}

void CSample16kMultiDlg::OnBtAuthentify() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[80];
	char	l_acByte[3];

	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Authentify choosen page
		l_wStatus = CLib_w_Authentify(this->m_iSelectedKey, PROTOCOL_PICO_15693, this->m_abSerialNumber);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "Page authentified");
			this->m_lblConfBlock = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Authentication error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtRead4() 
{
	BYTE	l_abDataReceived[100];
	int		l_iBlockToRead;
	WORD	l_wStatus;

	char	l_bstrDataReceived[200];
	char	l_acByte[3];
	int		l_indByte;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Retreive selected block
		l_iBlockToRead = this->m_cmbBlock.GetCurSel() + 6;
		
		// READ content of the 4 selected block
		l_wStatus = CLib_w_ReadBlockBy4(l_iBlockToRead, 4, PROTOCOL_PICO_15693, l_abDataReceived);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "Data : ");
			for (l_indByte = 0; l_indByte < 32; l_indByte++)
			{
				itoa(l_abDataReceived[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
			}
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Read4 error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtDisable() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Disable Coupler
		l_wStatus = CLib_w_Disable();
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			this->m_lblData = "Coupler Disabled";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Disable error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtEnable() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Enable Coupler
		l_wStatus = CLib_w_Enable();
		l_wStatus = CLib_w_Enable();
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			this->m_lblData = "Coupler Enabled";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "Enable error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtGetStatus() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];
	BYTE	l_bEEPROMVal;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Get EEPROM value at address 0x70 (free)
		l_bEEPROMVal = 0xFF;
		l_wStatus = CLib_w_GetStatus(COUPLER_EEPROM, 0x70, &l_bEEPROMVal);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "RAM Address 0x70 Value :");
			itoa(l_bEEPROMVal, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "GetStatus error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtSetStatus() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];
	BYTE	l_bEEPROMVal = 0x52;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Set EEPROM value at address 0x70 (free)
		l_wStatus = CLib_w_SetStatus(COUPLER_EEPROM, 0x70, &l_bEEPROMVal);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			itoa(l_bEEPROMVal, l_acByte, 16);
			strcpy(l_bstrDataReceived, "0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			strcat(l_bstrDataReceived, " put in RAM Address 0x70");
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "GetStatus error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtEasDetect() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];
	BYTE	l_abSN[10];
	int		l_indByte;

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Detect a chip using EAS
		l_wStatus = CLib_w_EASDetect(PROTOCOL_PICO_15693, l_abSN);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "EAS Number :");
			for (l_indByte = 0; l_indByte < 8; l_indByte++)
			{
				itoa(l_abSN[l_indByte], l_acByte, 16);
				strcat(l_bstrDataReceived, " 0x");
				strcat(l_bstrDataReceived, CharUpper(l_acByte));
				this->m_abSerialNumber[l_indByte] = l_abSN[l_indByte];
			}
			this->m_lblSN = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "EASDetect error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtEasActivate() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Activate EAS
		l_wStatus = CLib_w_EASActivate(PROTOCOL_PICO_15693);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			this->m_lblData = "EAS Activated";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "EASActivate error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtEasDeactivate() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Deactivate EAS
		l_wStatus = CLib_w_EASDeactivate(PROTOCOL_PICO_15693);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			this->m_lblData = "EAS Deactivated";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "EASDeactivate error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtSetUserProtocol() 
{
	WORD	l_wStatus;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Set ISO-A standard protocol
		l_wStatus = CLib_w_SetUserProtocol(USER_PROTOCOL_14443_B_3);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			this->m_lblData = "User protocol changed";
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "SetUserProtocol error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBTDecrease() 
{
	WORD	l_wStatus;
	WORD	l_wNewCounter = 0x0000;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Decrease counter by 1
		l_wStatus = CLib_w_DecreaseCounter(1, PROTOCOL_PICO_15693, &l_wNewCounter);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "New counter value : ");
			itoa(l_wNewCounter, l_acByte, 10);
			strcat(l_bstrDataReceived, l_acByte);
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "DecreaseCounter error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnBtIncrease() 
{
	WORD	l_wStatus;
	WORD	l_wNewRecharger = 0x0000;
	char	l_bstrDataReceived[200];
	char	l_acByte[3];

	// Check coupler connection
	if (!this->m_bConnected)
		MessageBox("No coupler connected");
	else
	{
		// Increase counter by 1
		l_wStatus = CLib_w_IncreaseCounter(65534, PROTOCOL_PICO_15693, &l_wNewRecharger);
		if (l_wStatus == STATUS_OK)
		{
			// Update result display
			strcpy(l_bstrDataReceived, "New recharging counter value : ");
			itoa(l_wNewRecharger, l_acByte, 10);
			strcat(l_bstrDataReceived, l_acByte);
			this->m_lblData = l_bstrDataReceived;
			UpdateData(FALSE);
		}
		else
		{
			strcpy(l_bstrDataReceived, "IncreaseCounter error :");
			itoa(l_wStatus >> 8, l_acByte, 16);
			strcat(l_bstrDataReceived, " 0x");
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			itoa(l_wStatus & 0xFF, l_acByte, 16);
			strcat(l_bstrDataReceived, CharUpper(l_acByte));
			MessageBox(l_bstrDataReceived);
		}
	}
}

void CSample16kMultiDlg::OnChangeTxtComport() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
}
