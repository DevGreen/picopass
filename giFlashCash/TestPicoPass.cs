﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using GI.Flash.Pico.Test;

namespace GI.Flash.Pico.Test
{
   public delegate bool CardAction( PicoPass pp, byte[] serialNumber, ref PicoData cardData );

   public struct PicoData
   {
      public UInt16        customerID;
      public string        locationID;
      public UInt32        cardNumber;
      public string        cardNumberStr;
      public CardType      cardType;
      public UInt16        purse;
      public CardOpReturn  retVal;
   }     
      
   public class TestPicoPass
   {     
      [DllImport( "kernel32.dll" )]
      public static extern bool Beep( int freq, int duration );

      public static uint      cardNumber = 0;
      public static PicoData  cardData;
      
      private static PicoCardHeader signingHeader  = null;
      private static ushort         signingValue   = 0;
      
      public static bool WriteUnsignedHeader( PicoPass pp, uint serialNumber )
      {
         PicoCardHeader header = PicoCardHeader.MakeUnsigned( serialNumber );

         byte[] hBlock = header.GetBytes();

         if ( pp.Write4Block( 6, new ArraySegment<byte>( hBlock ), true ) )
         {
            return true;
         }
         else
         {
            return false;
         }

      }

      public static PicoData Run( string comPort, string locID, UInt16 custID, UInt16 purseWriteVal, CardOp op )
      {
         bool     signed;
         bool     selected;
         PicoData cardData;
         PicoPass pp          = new PicoPass( comPort );
         
         // Initialize card data struct.
         cardData.customerID = 0;
         cardData.locationID = "";         
         cardData.cardNumber = 0;
         cardData.cardNumberStr = "";
         cardData.purse = 0;
         cardData.cardType = CardType.Unsigned;
         cardData.retVal = CardOpReturn.OP_NOOP;        
     
         // Initialize the card signing parameters.
         InitSigning( custID, locID, CardType.User, 0 );
         
         // Connect to the coupler.
         pp.Connect();
         
         // If the card is unsigned, it will be signed, else the signing will fail. However, the SELECT operation
         // should always succeed.
         signed = SelectLoop( pp, PicoPass.Auth.AuthKd, new CardAction( SignCard ), out selected, ref cardData );
         
         // If the card was selected OK, always read it.
         if ( selected )
         {
            SelectLoop( pp, PicoPass.Auth.AuthKd, new CardAction( ReadCard ), out selected, ref cardData );
            if ( cardData.customerID != custID )
            {
               cardData.retVal = CardOpReturn.ERR_CUSTOMER_ID_MISMATCH;
            }
         }
         else
         {
            cardData.retVal = CardOpReturn.ERR_NO_CARD;
         }
         
         // If the card was selected & read, and the operation requested is a write, do it.
         if (( cardData.retVal == CardOpReturn.OP_SUCCESS ) && ( op == CardOp.WriteSet || op == CardOp.WriteDec || op == CardOp.WriteInc )) 
         {
            switch ( op )
            {
               case CardOp.WriteSet:
                  if ((( Int32 )purseWriteVal ) <= 65534 )
                  {
                     cardData.purse = purseWriteVal;
                  }
                  else
                  {
                     cardData.retVal = CardOpReturn.WR_ERR_PURSE_OVERFLOW; 
                  }
                  break;
                  
               case CardOp.WriteInc:
                  if ((( Int32 )cardData.purse + ( Int32 )purseWriteVal ) <= 65534 )
                  {
                     cardData.purse += purseWriteVal;
                  }
                  else
                  {
                     cardData.retVal = CardOpReturn.WR_ERR_PURSE_OVERFLOW;
                  }
                  break;
                  
               case CardOp.WriteDec:
                  if ((( Int32 )cardData.purse - ( Int32 )purseWriteVal ) >= 0 )
                  {
                     cardData.purse -= purseWriteVal;
                  }
                  else
                  {
                     cardData.retVal = CardOpReturn.WR_ERR_PURSE_UNDERFLOW;
                  } 
                  break;
            }
            if ( cardData.retVal == CardOpReturn.OP_SUCCESS )
            {
               SelectLoop( pp, PicoPass.Auth.AuthKc, new CardAction( WriteCard ), out selected, ref cardData ); 
            }
         }
         
         // Disconnect the coupler.
         pp.Disconnect();
         
         return cardData;       
      }          

      public static string FormatCardNumber( uint cardNumber )
      {
         uint part1 = ( cardNumber / 1000000 );
         uint part2 = ( cardNumber / 1000 ) % 1000;
         uint part3 = cardNumber % 1000;

         return string.Format( "{0:000}-{1:000}-{2:000}", part1, part2, part3 );
      }
      
      public static bool ReadCard( PicoPass pp, byte[] serialNumber, ref PicoData cardData )
      {
         cardData.retVal = CardOpReturn.OP_SUCCESS;
         byte[] hBlock = pp.Read4Block( 6 );

         if ( hBlock == null )
         {
            cardData.retVal = CardOpReturn.RD_ERR_BAD_HEADER;
            return false;
         }

         PicoCardHeader header = new PicoCardHeader( hBlock );

         if ( !header.Verify( serialNumber ))
         {
            cardData.retVal = CardOpReturn.RD_ERR_NOT_GI_CARD;
            return false;
         }
         else
         {
            cardData.customerID = header.CustomerID;
            cardData.locationID = header.LocationID;
            cardData.cardNumber = header.CardNumber;
            cardData.cardNumberStr = FormatCardNumber( header.CardNumber );
            cardData.cardType = header.CardType;
            
            int purse = pp.ReadPurse();

            if ( purse < 0 )
            {
               cardData.retVal = CardOpReturn.RD_ERR_PURSE_INVALID;
               return false;
            }

            decimal amt = Convert.ToDecimal( purse );
            amt /= 100;

            cardData.purse = ( ushort )purse;
            return true;
         }
      }

      public static bool WriteCard( PicoPass pp, byte[] serialNumber, ref PicoData cardData )
      {
         cardData.retVal   = CardOpReturn.OP_SUCCESS;
         int cardPurse     = pp.ReadPurse();

         if ( cardPurse < 0 )
         {
            cardData.retVal = CardOpReturn.WR_ERR_PURSE_INVALID;
            return false;
         }

         ushort newPurse;
         ushort newRecharge;
            
         PicoPass.CreditResult cResult = pp.SetPurse( cardData.purse, out newPurse, out newRecharge );
           
         if ( cResult == PicoPass.CreditResult.Success )
         {
            return true;
         }
         else
         {
            cardData.retVal = CardOpReturn.WR_ERR_FAILURE;
            return false;               
         }
      }

      public static void InitSigning( ushort customerID, string locationID, CardType cardType, ushort purseValue )
      {
         if ( signingHeader == null )
         {
            signingHeader = new PicoCardHeader();
         }
         signingHeader.CustomerID   = customerID;
         signingHeader.LocationID   = locationID;
         signingHeader.CardType     = cardType;
         signingHeader.CardVersion  = 0x01;
         signingValue               = purseValue;
      }

      public static bool SignCard( PicoPass pp, byte[] serialNumber, ref PicoData cardData )
      {
         cardData.retVal = CardOpReturn.OP_SUCCESS;
         byte[] hBlock = pp.Read4Block( 6 );

         if ( hBlock == null )
         {
            cardData.retVal = CardOpReturn.SIGN_ERR_BAD_HEADER;
            return false;
         }

         PicoCardHeader header = new PicoCardHeader( hBlock );
         if ( header.CardType != CardType.Unsigned )
         {
            // This is not strictly an error; do not set retVal to an error status in this case only.
            return false;
         }

         uint cardNumber = header.GetCardNumberFromUnsigned();
         uint cardNumberWithCheck = GI.Utility.CardNumber.AppendCheckDigit( cardNumber );
         signingHeader.CardNumber = cardNumberWithCheck;
         signingHeader.Sign( serialNumber );
         byte[] writeBlock = signingHeader.GetBytes();

         if ( pp.Write4Block( 6, new ArraySegment<byte>( writeBlock ), true ))
         {
            if ( signingHeader.CardType == CardType.User )
            {
               // Initialize Purse
               byte[] newSN;
               if ( pp.SelectCard( PicoPass.Auth.AuthKc, out newSN ))
               {
                  ushort newPurseValue = 0;
                  PicoPass.CreditResult result = pp.ClearPurse();
                  if ( result != PicoPass.CreditResult.Success )
                  {
                     cardData.retVal = CardOpReturn.SIGN_ERR_CLEAR_PURSE; 
                     return false;
                  }
                  if ( signingValue > 0 )
                  {
                     ushort newRechargeValue = 0;
                     result = pp.CreditPurse( signingValue, out newPurseValue, out newRechargeValue );
                     if ( result != PicoPass.CreditResult.Success )
                     {
                        cardData.retVal = CardOpReturn.SIGN_ERR_CREDIT;
                        return false;
                     }
                  }
                  return true;
               }
               else
               {
                  cardData.retVal = CardOpReturn.SIGN_ERR_RESELECT;
                  return false;
               }
            }
            else
            {
               return true;
            }
         }
         else
         {
            return false;
         }
      }
    
      public static bool ReadPurse( PicoPass pp, byte[] serialNumber )
      {
         int purse = pp.ReadPurse();
         return true;
      }
      
      public static bool SelectLoop( PicoPass pp, PicoPass.Auth auth, CardAction action, out bool selected, ref PicoData cardData )
      {
         uint     tries          = 3;
         bool     success        = false;
         byte[]   serialNumber;
         
         do
         {
            if ( selected = pp.SelectCard( auth, out serialNumber ))
            {
               success = action( pp, serialNumber, ref cardData );
            }
         } while ( !success && ( --tries != 0 ));
         return success;
      }
   }
}
