﻿using System;
using System.Collections.Generic;
using Gnu.Getopt;
using System.Text;



namespace GI.Flash.Pico.Test
{
   public enum CardOp 
   {
      Read,
      WriteSet,
      WriteDec,
      WriteInc,
      Sign,
      Erase
   };
      
   public enum CardOpReturn
   {
      OP_SUCCESS,
      OP_NOOP,
      OP_ERR_CUST_ID_INVALID,
      OP_ERR_PURSE_VAL_INVALID,
      OP_ERR_ARG_REQUIRED,
      OP_ERR_EXTRA_ARG,
      OP_ERR_IO_EXCEPTION,         
      OP_ERR_NO_SUCH_OPT,
      SIGN_ERR_BAD_HEADER,
      SIGN_ERR_CLEAR_PURSE,
      SIGN_ERR_CREDIT,
      SIGN_ERR_RESELECT,
      RD_ERR_BAD_HEADER,
      RD_ERR_NOT_GI_CARD,
      RD_ERR_PURSE_INVALID,
      WR_ERR_BAD_HEADER,
      WR_ERR_NOT_GI_CARD,
      WR_ERR_PURSE_INVALID,
      WR_ERR_PURSE_OVERFLOW,
      WR_ERR_PURSE_UNDERFLOW,
      WR_ERR_FAILURE,
      ERR_CUSTOMER_ID_MISMATCH,
      ERR_GENERAL_EXCEPTION,
      ERR_NO_CARD  
   };
   
   class Program
   {
      public static String Name = "giFlashCash";        

      public static void PrintUsage()
      {
         Console.WriteLine( "\nGI FlashCash Utility, v1.0 (c) 2014 Greenwald Industries, Inc.\n" );
         Console.WriteLine( "Usage, where <> = required argment, [] = optional argument,\n" +
                            "              | = alternative, ... = one or more\n" );
         Console.WriteLine( "   {0} -c <customer ID> -l <location ID> -p <COM Port> [ -w <value expression> | -r | -e | -s <type> ]\n",
                            Program.Name );
         //Console.WriteLine( "   {0} [ -p <COM Port> ] [ -w <value expression> | -r | -s ]\n",
         //                   Program.Name );
         Console.WriteLine( "      -p                    = Coupler serial port, COM1, COM2, etc (default: COM1)" );                   
         Console.WriteLine( "      -r                    = Read card (default)" );
         Console.WriteLine( "      -e                    = Erase (unsign) card" );
         Console.WriteLine( "      -w <value expression> = Write card\n" );
         Console.WriteLine( "      <value expression> may be any one of:" );
         Console.WriteLine( "          <value>             SET card purse to <value>" );
         Console.WriteLine( "         +<value>             CREDIT (increment) card purse by <value>" );
         Console.WriteLine( "         -<value>             DEBIT (decrement) card purse by <value>\n" );
         Console.WriteLine( "      -s <type>             = Sign card\n" );      
         Console.WriteLine( "      <type> may be any one of:" );
         Console.WriteLine( "         admin                \"Adminstrator\" card" );
         Console.WriteLine( "         cash                 \"Cash Removed\" card" );
         Console.WriteLine( "         kill                 \"Cycle Kill\" card" );
         Console.WriteLine( "         recent               \"Recent Transaction\" card" ); 
         Console.WriteLine( "         service              \"Service\" card" );
         Console.WriteLine( "         user                 \"User\" card (default)\n" );
         
         Console.WriteLine( "   Return values indicate status as follows:" );
         Console.WriteLine( "      0   OP_SUCCESS                 : operation successful" );
         Console.WriteLine( "      1   OP_NOOP                    : no operation performed/no error" );
         Console.WriteLine( "      2   OP_ERR_CUST_ID_INVALID     : customer ID not in range 0 to 65535" );
         Console.WriteLine( "      3   OP_ERR_PURSE_VAL_INVALID   : purse value not in range 0 to 65534" );
         Console.WriteLine( "      4   OP_ERR_ARG_REQUIRED        : option requires an argument" );
         Console.WriteLine( "      5   OP_ERR_EXTRA_ARG           : extra argument supplied" );
         Console.WriteLine( "      6   OP_ERR_IO_EXCEPTION        : Windows I/O exception (check COM port)" );
         Console.WriteLine( "      7   OP_ERR_NO_SUCH_OPT         : specified option not allowed" );
         Console.WriteLine( "      8   SIGN_ERR_BAD_HEADER        : card header read error" );
         Console.WriteLine( "      9   SIGN_ERR_CLEAR_PURSE       : card purse failed to clear on signing" );
         Console.WriteLine( "     10   SIGN_ERR_CREDIT            : card purse failed initialization" );
         Console.WriteLine( "     11   SIGN_ERR_RESELECT          : card select fail on signing" );
         Console.WriteLine( "     12   RD_ERR_BAD_HEADER          : card header read error" );
         Console.WriteLine( "     13   RD_ERR_NOT_GI_CARD         : card format unknown" );
         Console.WriteLine( "     14   RD_ERR_PURSE_INVALID       : card purse format bad" );
         Console.WriteLine( "     15   WR_ERR_BAD_HEADER          : card header read error" );
         Console.WriteLine( "     16   WR_ERR_NOT_GI_CARD         : card format unknown" );
         Console.WriteLine( "     17   WR_ERR_PURSE_INVALID       : card purse format bad" );
         Console.WriteLine( "     18   WR_ERR_PURSE_OVERFLOW      : attempted purse write > 65534" );
         Console.WriteLine( "     19   WR_ERR_PURSE_UNDERFLOW     : attempted purse write < 0" );
         Console.WriteLine( "     20   WR_ERR_FAILURE             : general card write failure" );
         Console.WriteLine( "     21   ERR_CUSTOMER_ID_MISMATCH   : card customer ID does not match GI ID (65334)" );
         Console.WriteLine( "     22   ERR_GENERAL_EXCEPTION      : unhandled exception, see output" );
         Console.WriteLine( "     23   ERR_NO_CARD                : no card detected in coupler field\n" );
         Console.WriteLine( "   Output on STDOUT (delimited by CR-LF pairs):" );
         Console.WriteLine( "      Line 1  (string): Status string corresponding to return value (see above)" );
         Console.WriteLine( "      Line 2 (numeric): Customer ID in decimal" );
         Console.WriteLine( "      Line 3  (string): Location ID (10 chars max)" );
         Console.WriteLine( "      Line 4 (numeric): Card number" );
         Console.WriteLine( "      Line 5 (numeric): Purse value\n" );         
      }
      
      public static void ErrorExit( CardOpReturn retVal )
      {
         Console.WriteLine( retVal.ToString());
         Environment.Exit(( int )retVal );   
      }
      
      public static void ErrorExit( CardOpReturn retVal, Exception expt )
      {
         Console.WriteLine( retVal.ToString());
         Console.WriteLine( expt.ToString());
         Environment.Exit(( int )retVal );   
      }

               
      private static void GetOptions( string[]   args, 
                                      out string comPort,
                                      out string locID,
                                      out UInt16 custID,
                                      out UInt16 purseWriteVal,
                                      out CardOp op )
      {
         int            c;
         string         arg;
         Getopt         g        = new Getopt( Program.Name, args, ":c:l:p:w:rh" );
         //Getopt         g        = new Getopt( Program.Name, args, ":p:w:rh" );
         CardOpReturn   retVal   = CardOpReturn.OP_SUCCESS;

         // Establish defaults, then parse the command line args.
         comPort        = "COM1";
         locID          = "GREENW0000";
         custID         = 0xFF36;
         
         purseWriteVal  = 0;
         op             = CardOp.Read;
              
         // Do not report unknown option errors; this code will handle that.
         g.Opterr = false;

         while (( c = g.getopt()) != -1 )
         {
            switch ( c )
            {
              case 'c':
                  arg = g.Optarg;
                  try
                  {
                     custID = UInt16.Parse( arg );
                  }
                  catch ( Exception )
                  {
                     retVal = CardOpReturn.OP_ERR_CUST_ID_INVALID;
                  }
                  break;
                              
               case 'w':
                  CardOp tempOp = CardOp.WriteSet;
                                    
                  arg = g.Optarg;
                  if ( arg[ 0 ] == '-' )
                  {
                     tempOp = CardOp.WriteDec;
                     arg = arg.Remove( 0, 1 );
                  }
                  else if ( arg[ 0 ] == '+' )
                  {
                     tempOp = CardOp.WriteInc;
                     arg = arg.Remove( 0, 1 );
                  }      
                  try
                  {
                     purseWriteVal = UInt16.Parse( arg );
                  }
                  catch ( Exception )
                  {
                     retVal = CardOpReturn.OP_ERR_PURSE_VAL_INVALID;
                  }
                  op = tempOp;
                  break;
                  
               case 'r':
                  op = CardOp.Read;
                  break;
                  
               case 'l':
                  locID = g.Optarg;                  
                  if ( locID.Length > 10 )
                  {
                     locID = locID.Remove( 10 );
                  }
                  else if ( locID.Length < 10 )
                  {
                     locID = locID.PadRight( 10, ' ' );
                  }
                  break;
                  
               case 'p':
                  comPort = g.Optarg;
                  break;                  
                  
               case 'h': // Help/usage
                  PrintUsage();
                  retVal = CardOpReturn.OP_NOOP;
                  break;
                  
               case ':':
                  retVal = CardOpReturn.OP_ERR_ARG_REQUIRED;
                  break;

               case '?':                  
                  retVal = CardOpReturn.OP_ERR_NO_SUCH_OPT;
                  break;

               default:
                  retVal = CardOpReturn.OP_ERR_NO_SUCH_OPT;
                  break;
            }
         }
         
         if ( g.Optind < args.Length )
         {
            retVal = CardOpReturn.OP_ERR_EXTRA_ARG;
         }
         for ( int i = g.Optind; i < args.Length; i++ )
         {
         //   Console.WriteLine( args[ i ] );
         }         
         if ( retVal != CardOpReturn.OP_SUCCESS )
         {
            ErrorExit( retVal );            
         }
      }

      public static int Main( string[] args )
      {
         string   comPort;
         string   locID;
         UInt16   custID;
         UInt16   purseWriteVal;
         CardOp   op;
         PicoData cardData;
         
         cardData.customerID = 0;
         cardData.locationID = "";         
         cardData.cardNumber = 0;
         cardData.cardNumberStr = "";
         cardData.purse = 0;
         cardData.cardType = CardType.Unsigned;
         cardData.retVal = CardOpReturn.OP_NOOP;    
         
         GetOptions( args, out comPort, out locID, out custID, out purseWriteVal, out op );        
         try
         {
            cardData = TestPicoPass.Run( comPort, locID, custID, purseWriteVal, op );
         }
         catch ( System.IO.IOException )
         {
            ErrorExit( CardOpReturn.OP_ERR_IO_EXCEPTION );            
         }
         catch ( Exception expt )
         {
            ErrorExit( CardOpReturn.ERR_GENERAL_EXCEPTION, expt );
         }
         Console.WriteLine( cardData.retVal.ToString());
         if ( cardData.retVal == CardOpReturn.OP_SUCCESS )
         {
            Console.WriteLine( "{0}", cardData.customerID );
            Console.WriteLine( "{0}", cardData.locationID );
            Console.WriteLine( "{0}", cardData.cardNumber );            
            Console.WriteLine( "{0}", cardData.purse );
         }
         return ( int )cardData.retVal;
      }
   }
}
