﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO.Ports;

using GI.Flash.Pico;

namespace GI.Flash.Pico.Test
{
    public class TestSerialASCII
    {
        public static void Run()
        {
//            TestAddValueCommand();
//            TestReadCommand();

            TestPollCommand();
        }


        public static void TestPollCommand()
        {
            Console.WriteLine("Opening COM1 at 4800,None,8,2");

            SerialPort port = new SerialPort("COM1", 4800, Parity.None, 8, StopBits.Two);
            port.Open();

            SerialASCIIProtocol sp = new SerialASCIIProtocol(port);


            Console.WriteLine("Sending I Command.");
            sp.SendCommand("E", null);



            string replyCmd;
            string[] replyParms;


            if (sp.ReadResponse(out replyCmd, out replyParms))
            {
                Console.WriteLine("Reply:{0}", replyCmd);
            }
            else
                Console.WriteLine("No Reply");
        }


        public static void TestAddValueCommand()
        {
            SerialPort port = new SerialPort("COM1", 9600);

            port.Open();

            FlashCTSProtocol fp = new FlashCTSProtocol(port);

            if (fp.SendAddValueCommand(1000, new byte[] { 0xDC, 0xCA, 0x6E, 0x01, 0x08, 0x00, 0x12, 0xE0 }, 500))
            {
                Console.WriteLine("Sent Add Value Command");

                AddValueResult result;
                ushort newPurseValue;

                if (fp.GetAddValueResponse(out result, out newPurseValue))
                {
                    Console.WriteLine("Response:{0}", result);
                    Console.WriteLine("NewPurse:{0}", newPurseValue);
                }
                else
                    Console.WriteLine("Get Response Failed");

            }
            else
                Console.WriteLine("Send Add Value Failed");


            port.Close();

        }

        public static void TestReadCommand()
        {

//          SerialPort port=new SerialPort("COM1", 9600,Parity.None,8,StopBits.One);
          SerialPort port = new SerialPort("COM1", 9600);

          port.Open();

          FlashCTSProtocol fp = new FlashCTSProtocol(port);


          fp.SendReadCommand(1000);



          Console.WriteLine("Sent Read Command");

          ReadResult result;
          FlashCardInfo cardInfo;


          if (fp.GetReadResponse(out result, out cardInfo))
          {

              Console.WriteLine("Result: {0}", result);

              Console.WriteLine("SN: {0}",MakeHexString(cardInfo.SerialNumber));


              Console.WriteLine("Customer ID: {0:X4}", cardInfo.CustomerID);

              Console.WriteLine("Location ID: {0}", cardInfo.LocationID);

              Console.WriteLine("Purse Value: {0}", cardInfo.PurseValue);


          }
          else
              Console.WriteLine("Bad Reply");

          port.Close();

        }


        private static string MakeHexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            return sb.ToString();
        }

    }
}
