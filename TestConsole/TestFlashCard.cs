﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace GI.Flash.Pico.Test
{
    public class TestFlashCard
    {
        public static void Run()
        {

            FlashCard flash = new FlashCard("COM1");

            flash.Connect();


            byte[] serialNumber = new byte[8];

            serialNumber[0] = 0x6C;
            serialNumber[1] = 0xA4;
            serialNumber[2] = 0x63;
            serialNumber[3] = 0x01;
            serialNumber[4] = 0x08;
            serialNumber[5] = 0x00;
            serialNumber[6] = 0x12;
            serialNumber[7] = 0xE0;


            ushort purse;

            VendResult result=flash.VendCardCash(serialNumber, 500, out purse,0);


            if (result == VendResult.Success)
            {
                Console.WriteLine("Vend Succes: New Purse:{0}", purse);
            }
            else
                Console.WriteLine("Fail: {0}", result);

            flash.Disconnect();

        }
    }
}
