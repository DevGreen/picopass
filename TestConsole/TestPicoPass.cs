﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;



namespace GI.Flash.Pico.Test
{
    public delegate bool CardAction(PicoPass pp,byte[] serialNumber);


 

    public class TestPicoPass
    {

        [DllImport("kernel32.dll")]
        public static extern bool Beep(int freq, int duration);


        public static uint cardNumber = 882111;

        public static bool WriteHeader(PicoPass pp, byte[] serialNumber)
        {

            uint cardNumberWithCheck = GI.Utility.CardNumber.AppendCheckDigit(cardNumber);

            PicoCardHeader header = new PicoCardHeader();

//            header.CardType = CardType.Service;
//            header.CardType = CardType.User;
//            header.CardVersion = 0x01;
//            header.CustomerID = 0xFF36;
//            header.LocationID = "GREENWALD";
//            header.CardNumber = cardNumberWithCheck;

//            header.CardType = CardType.CycleKill;
            //header.CardType = CardType.Service;
            //header.CardVersion = 0x01;
            //header.CustomerID = 0xF02B;
            //header.LocationID = "GOLDST0000";
            //header.CardNumber = cardNumberWithCheck;

            //header.CardType = CardType.Service;
            header.CardType = CardType.Admin;
            header.CardVersion = 0x01;
            header.CustomerID = 0xF016;
            header.LocationID = "LAUNDR0000";
            header.CardNumber = cardNumberWithCheck;

            header.Sign(serialNumber);

            byte[] hBlock = header.GetBytes();

            if (pp.Write4Block(6, new ArraySegment<byte>(hBlock), true))
            {
                Console.WriteLine("Wrote Header : {0}", cardNumberWithCheck);

                cardNumber++;
                Console.WriteLine();

                Console.WriteLine("Next Card Number : {0}", GI.Utility.CardNumber.Format(cardNumber));

                return true;
            }
            else
            {
                Console.WriteLine("Write Header Failed");

                return false;
            }

        }

        public static bool WriteUnsignedHeader(PicoPass pp, uint serialNumber ) // byte[] serialNumber)
        {
            PicoCardHeader header = PicoCardHeader.MakeUnsigned(serialNumber);

            byte[] hBlock = header.GetBytes();

            if (pp.Write4Block(6, new ArraySegment<byte>(hBlock), true))
            {
                Console.WriteLine("Wrote Header");
                return true;
            }
            else
            {
                Console.WriteLine("Write Header Failed");

                return false;
            }

        }



        public static void Run()
        {
//            InitSigning(0xF005, "FREEDRY001", CardType.RecentTransaction,0);

//            InitSigning(0xF005, "FREEDRY001", CardType.Service,0);
//            InitSigning(0xF005, "FREEDRY001", CardType.CashRemoved, 0);
#if true
              //InitSigning(0xF005, "FREEDRY001", CardType.User, 0);

              //InitSigning( 0xF00F, "CIRCUI0000", CardType.User, 0 );
              //InitSigning( 0xF00F, "CIRCUI0000", CardType.Admin, 0 );
              //InitSigning( 0xF00F, "CIRCUI0000", CardType.Service, 0 );
              //InitSigning( 0xF00F, "CIRCUI0000", CardType.CycleKill, 0 );

              //InitSigning( 0xF010, "WASHST0000", CardType.Service, 0 );

              //InitSigning( 0xF040, "CIREUR0001", CardType.User, 0 );
              //InitSigning( 0xF040, "CIREUR0001", CardType.Admin, 0 );
              //InitSigning( 0xF040, "CIREUR0001", CardType.CashRemoved, 0 );
              //InitSigning( 0xF040, "CIREUR0001", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF040, "CIREUR0001", CardType.Service, 0 );
              
              //InitSigning( 0xF02F, "REITHO0000", CardType.User, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.UseRemainingBalance, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.Admin, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.Service, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF02F, "REITHO0000", CardType.Unsigned, 0 );

              //InitSigning( 0xF02B, "GOLDST0000", CardType.CycleKill, 0 );
              //InitSigning( 0xF02B, "GOLDST0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF02B, "GOLDST0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF02B, "GOLDST0000", CardType.Service, 0 );
              
              //InitSigning( 0xF016, "LAUNDR0000", CardType.Admin, 0 );

              //InitSigning( 0xFF70, "JETZ--0000", CardType.Admin, 0 );
              //InitSigning( 0xFF70, "JETZ--0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xFF70, "JETZ--0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xFF70, "JETZ--0000", CardType.Service, 0 );

              //InitSigning( 0xF047, "FERNAN0000", CardType.Admin, 0 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.SetPrice, 0 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.UseRemainingBalance, 0 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.User, 1000 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF047, "FERNAN0000", CardType.Service, 0 );
              
              //InitSigning( 0xFF01, "COINAM0000", CardType.User, 0 );
              //InitSigning( 0xFF01, "COINAM0000", CardType.Admin, 0 );
              //InitSigning( 0xFF01, "COINAM0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xFF01, "COINAM0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xFF01, "COINAM0000", CardType.Service, 0 );

              //InitSigning( 0xFF36, "GREENW0000", CardType.User, 1 );
              //InitSigning( 0xFF36, "GREEN0000", CardType.Admin, 0 );
              //InitSigning( 0xFF36, "GREEN0000", CardType.ClearCounters, 0 );
              //InitSigning( 0xFF36, "GREEN0000",  CardType.FactoryTest, 0 );     // temporary Factory Test Card 7/5/17 EM
              //InitSigning( 0xFF36, "GREENW0000", CardType.UseRemainingBalance, 0 );

              //InitSigning( 0xFFFF, "GREEN0000",  CardType.FactoryTest, 0 );     // temporary Factory Test Card 7/5/17 EM
              //InitSigning( 0xFFFF, "GREEN0000",  CardType.Unsigned, 1000 );             // temporary Factory Test Card 7/5/17 EM

              //InitSigning( 0xF04C, "GOODER0000", CardType.Admin, 0 );
              //InitSigning( 0xF04C, "GOODER0000", CardType.RecentTransaction, 0 );
 
              //InitSigning( 0xF032, "INTERT0000", CardType.User, 0 );              // Intertrade, Chile
//              InitSigning( 0xF032, "INTERT0000", CardType.Admin, 0 );
              //InitSigning( 0xF032, "INTERT0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF032, "INTERT0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF032, "INTERT0000", CardType.Service, 0 );

              //InitSigning( 0xF053, "PRONTO0000", CardType.User, 9000 );           // Prontomatic, Columbia
              //InitSigning( 0xF053, "PRONTO0000", CardType.Admin, 0 );
              //InitSigning( 0xF053, "PRONTO0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF053, "PRONTO0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF053, "PRONTO0000", CardType.Service, 0 );

              //InitSigning( 0xF06D, "PRPERU0000", CardType.User, 5000 );           // Prontomatic, Peru
              //InitSigning( 0xF06D, "PRPERU0000", CardType.Admin, 0 );
              //InitSigning( 0xF06D, "PRPERU0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF06D, "PRPERU0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF06D, "PRPERU0000", CardType.Service, 0 );

              //InitSigning( 0xF02D, "TYSONS0000", CardType.User, 0 );
              //InitSigning( 0xF02D, "TYSONS0000", CardType.Admin, 0 );
              //InitSigning( 0xF02D, "TYSONS0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF02D, "TYSONS0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF02D, "TYSONS0000", CardType.Service, 0 );

              //InitSigning( 0xF051, "AMITEX0000", CardType.User, 0 );
              //InitSigning( 0xF051, "AMITEX0000", CardType.Admin, 0 );
              //InitSigning( 0xF051, "AMITEX0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF051, "AMITEX0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF051, "AMITEX0000", CardType.Service, 0 );

              //InitSigning( 0xF056, "CLEANW0000", CardType.User, 0 );
              //InitSigning( 0xF056, "CLEANW0000", CardType.Admin, 0 );
              //InitSigning( 0xF056, "CLEANW0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF056, "CLEANW0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF056, "CLEANW0000", CardType.Service, 0 );

              //InitSigning( 0xF059, "PAICO_0000", CardType.User, 0 );
              //InitSigning( 0xF059, "PAICO_0000", CardType.Admin, 0 );
              //InitSigning( 0xF059, "PAICO_0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF059, "PAICO_0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF059, "PAICO_0000", CardType.Service, 0 );

              //InitSigning( 0xF05B, "BRNBAY0000", CardType.User, 0 );
              //InitSigning( 0xF05B, "BRNBAY0000", CardType.Admin, 0 );
              //InitSigning( 0xF05B, "BRNBAY0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF05B, "BRNBAY0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF05B, "BRNBAY0000", CardType.Service, 0 );

              //InitSigning( 0xFF0B, "3310      ", CardType.User, 0 );        // these are for Automatic Laundry, Newton, MA
              //InitSigning( 0xFF0B, "3310      ", CardType.Admin, 0 );
              //InitSigning( 0xFF0B, "AUTOLS0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xFF0B, "AUTOLS0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xFF0B, "AUTOLS0000", CardType.Service, 0 );

              //InitSigning( 0xFF16, "HERCUL0000", CardType.Service, 0 );       // Hercules

              //InitSigning( 0xF063, "TAS---0000", CardType.Admin, 0 );          // Todd Armstrong Shows - Carnival
              //InitSigning( 0xF063, "TAS---0000", CardType.SetPrice, 0 );
              //InitSigning( 0xF063, "TAS---0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF063, "TAS---0000", CardType.UseRemainingBalance, 0 );
              //InitSigning( 0xF063, "TAS---0000", CardType.User, 0 );
              //InitSigning( 0xF063, "TAS---0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF063, "TAS---0000", CardType.Service, 0 );

              //InitSigning( 0xF064, "HARCO-0001", CardType.User, 2000 );        // these are for Harco, Canada
              //InitSigning( 0xF064, "HARCO-0001", CardType.Admin, 0 );
              //InitSigning( 0xF064, "HARCO-0001", CardType.CashRemoved, 0 );
              //InitSigning( 0xF064, "HARCO-0001", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF064, "HARCO-0001", CardType.Service, 0 );

              //InitSigning( 0xF068, "LUND--0001", CardType.User, 2000 );        // these are for Lund, Nebraska
              //InitSigning( 0xF068, "LUND--0001", CardType.Admin, 0 );
              //InitSigning( 0xF068, "LUND--0001", CardType.CashRemoved, 0 );
              //InitSigning( 0xF068, "LUND--0001", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF068, "LUND--0001", CardType.Service, 0 );


              //InitSigning( 0xF068, "JBI---0001", CardType.User, 2000 );        // these are for Lund, Nebraska
              //InitSigning( 0xF068, "JBI---0001", CardType.Admin, 0 );
              //InitSigning( 0xF068, "JBI---0001", CardType.CashRemoved, 0 );
              //InitSigning( 0xF068, "JBI---0001", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF068, "JBI---0001", CardType.Service, 0 );

              //InitSigning( 0xF03E, "LAVATU0000", CardType.User, 5000 );

              //InitSigning( 0xFF36, "MIDWAY0000", CardType.Admin, 0 );          // Todd Armstrong Shows - Carnival
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.SetPrice, 0 );
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.UseRemainingBalance, 0 );
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.User, 0 );
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xFF36, "MIDWAY0000", CardType.Service, 0 );

              //InitSigning( 0xF06E, "COGEFI0000", CardType.User, 2000 );        // these are for Cogefimo, Canada
              //InitSigning( 0xF06E, "COGEFI0000", CardType.Admin, 0 );
              //InitSigning( 0xF06E, "COGEFI0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xF06E, "COGEFI0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xF06E, "COGEFI0000", CardType.Service, 0 );


              InitSigning( 0xFF2F, "COINOM0000", CardType.User, 2000 );        // these are for Coin-O-Maic, IL
              //InitSigning( 0xFF2F, "COINOM0000", CardType.Admin, 0 );
              //InitSigning( 0xFF2F, "COINOM0000", CardType.CashRemoved, 0 );
              //InitSigning( 0xFF2F, "COINOM0000", CardType.RecentTransaction, 0 );
              //InitSigning( 0xFF2F, "COINOM0000", CardType.Service, 0 );


              SelectLoop(PicoPass.Auth.AuthKd, new CardAction(SignCard), "COM3");
              
#else

              SelectLoop(PicoPass.Auth.AuthKd, new CardAction(ReadCard));
#endif 
//            Use for old "Demo" cards with "Operation" Key Access
//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(WriteHeader));

//            Use for new "Production" cards with "Area" Key Access
//              SelectLoop(PicoPass.Auth.AuthKd, new CardAction(WriteHeader));


//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(ClearPurse));


//            SelectLoop(PicoPass.Auth.AuthKd, new CardAction(MakeNewCard25));

//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(MakeNewCard0));

//            THIS FUNCTION "RECYCLES" A CARD (CLEARS ALL GI INITIALIZED DATA TO 0xFF)  
                   SelectLoop(PicoPass.Auth.AuthKd, new CardAction(MakeUnsignedCard), "COM3");


//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(MakePicnicCard));
//          SelectLoop(PicoPass.Auth.AuthKd, new CardAction(TestDebit));
//          SelectLoop(PicoPass.Auth.AuthKd,new CardAction(ReadPurse));
//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(TestCredit));

//            SelectLoop(PicoPass.Auth.AuthKd, new CardAction(TestWrite));
//            SelectLoop(PicoPass.Auth.AuthKd, new CardAction(TestRead4));
//            SelectLoop(PicoPass.Auth.AuthKd, new CardAction(TestWrite4));
//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(ClearPurse));

//            SelectLoop(PicoPass.Auth.AuthKc, new CardAction(WriteHeader));
        }

        public static string FormatCardNumber(uint cardNumber)
        {
            uint part1 = (cardNumber / 1000000);
            uint part2 = (cardNumber / 1000) % 1000;
            uint part3 = cardNumber % 1000;

            return string.Format("{0:000}-{1:000}-{2:000}", part1, part2, part3);

        }
        public static bool ReadCard(PicoPass pp, byte[] serialNumber)
        {
            byte[] hBlock = pp.Read4Block(6);

            if(hBlock==null)
            {
                    Console.WriteLine("Bad Header Read");
                    return true;
            }

            PicoCardHeader header = new PicoCardHeader(hBlock);

            if (!header.Verify(serialNumber))
            {
                Console.WriteLine("Not a valid GI card");
                return true;
            }
            else
            {
                Console.WriteLine("CustomerID: 0x{0:X4}", header.CustomerID);
                Console.WriteLine("LocationID: {0}", header.LocationID);
                Console.WriteLine("CardNumber: {0}", FormatCardNumber(header.CardNumber));
                Console.WriteLine("CardType: {0}", header.CardType);
                int purse = pp.ReadPurse();

                if (purse < 0)
                {
                    Console.WriteLine("Bad Read Purse");
                    return true;
                }

                decimal amt = Convert.ToDecimal(purse);
                amt /= 100;

                Console.WriteLine("Purse: $ {0:0.00}", amt);

                return true;

            }

        }

        private static PicoCardHeader signingHeader = null;
        private static ushort signingValue = 0;


        public static void InitSigning(ushort customerID,string locationID,CardType cardType,ushort purseValue)
        {
            if (signingHeader == null)
                signingHeader = new PicoCardHeader();

            signingHeader.CustomerID = customerID;
            signingHeader.LocationID = locationID;
            signingHeader.CardType = cardType;
            signingHeader.CardVersion = 0x01;


            signingValue = purseValue;
        }

        public static bool SignCard(PicoPass pp,byte[] serialNumber)
        {
            byte[] hBlock = pp.Read4Block(6);

            if (hBlock == null)
            {
                Console.WriteLine("Bad Header Read");
                return false;
            }

            PicoCardHeader header = new PicoCardHeader(hBlock);

            if (header.CardType != CardType.Unsigned)
            {
                Console.WriteLine("Not an unsigned card: {0}", header.CardType);
                return false;
            }


            uint cardNumber = header.GetCardNumberFromUnsigned();

            Console.WriteLine("Card Number: {0}", cardNumber);


            uint cardNumberWithCheck = GI.Utility.CardNumber.AppendCheckDigit(cardNumber);


            signingHeader.CardNumber = cardNumberWithCheck;
            signingHeader.Sign(serialNumber);
            byte[] writeBlock = signingHeader.GetBytes();

            if (pp.Write4Block(6, new ArraySegment<byte>(writeBlock), true))
            {
                Console.WriteLine("Signed Card : {0}", cardNumberWithCheck);


                if (signingHeader.CardType == CardType.User)
                {
                    //Initialize Purse
                    byte[] newSN;
                    if (pp.SelectCard(PicoPass.Auth.AuthKc, out newSN))
                    {
                        ushort newPurseValue = 0;

                        PicoPass.CreditResult result = pp.ClearPurse();

                        if (result != PicoPass.CreditResult.Success)
                        {
                            Console.WriteLine("Error Clearing Purse: {0}", result);
                            return false;
                        }

                        if (signingValue > 0)
                        {
                            ushort newRechargeValue = 0;
                            result = pp.CreditPurse(signingValue, out newPurseValue, out newRechargeValue);

                            if (result != PicoPass.CreditResult.Success)
                            {
                                Console.WriteLine("Error Crediting Purse: {0}", result);
                                return false;
                            }

                        }

                        Console.WriteLine("Initial Value: {0}", newPurseValue);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Error re-selecting Card.");
                        return false;
                    }
                }
                else
                    return true;
            }
            else
            {
                Console.WriteLine("Write Header Failed");
                return false;
            }
        }

        public static bool MakePicnicCard(PicoPass pp, byte[] serialNumber)
        {
            PicoCardHeader header = new PicoCardHeader();

            header.CardType = CardType.User;
            header.CardVersion = 0x01;
            header.CustomerID = 0xFF53;
            header.LocationID = "GI VEND";
//            header.LocationID = "SERVICE";
//            header.LocationID = "RESET";


            header.Sign(serialNumber);

            byte[] hBlock=header.GetBytes();

            if (!pp.Write4Block(6, new ArraySegment<byte>(hBlock), true))
            {
                Console.WriteLine("Write Header Failed");
                return false;
            }

            if (!ClearPurse(pp, serialNumber))
                return false;

//            ushort newPurse=0;
//            ushort recharge;

//            PicoPass.CreditResult result = pp.CreditPurse(500, out newPurse, out recharge);
//            Console.WriteLine("Success.  New Picnic Card: {0}",newPurse);

            return true;
        }

        public static bool MakeUnsignedCard(PicoPass pp, byte[] serialNumber)
        {
            byte[] hBlock = pp.Read4Block(6);
            if (hBlock == null)
            {
                Console.WriteLine("Bad Header Read");
                return true;
            }
            PicoCardHeader header = new PicoCardHeader(hBlock);
            if (!header.Verify(serialNumber))
            {
                Console.WriteLine("Not a valid GI card");
                return true;
            }
            else
            {
                if (!WriteUnsignedHeader(pp, header.CardNumber / 10))
                return false;
            }

            byte[] block = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
            byte i;

            for (i = 7; i < 32; i++ )
            {
                if (!pp.WriteBlock(i, new ArraySegment<byte>(block), true))
                {
                    return false;
                }
            }

          
            //if (!ClearPurse(pp, serialNumber))
            //    return false;


            Console.WriteLine("Created Unsigned card");

            return true;
        }

        public static bool MakeNewCard0(PicoPass pp, byte[] serialNumber)
        {
            if (!WriteHeader(pp, serialNumber))
                return false;

            if (!ClearPurse(pp, serialNumber))
                return false;


            Console.WriteLine("Success.  New Card Balance: 0");

            return true;
        }

        public static bool MakeNewCard25(PicoPass pp, byte[] serialNumber)
        {
            if (!WriteHeader(pp, serialNumber))
                return false;

            if (!ClearPurse(pp, serialNumber))
                return false;

            ushort newPurse;
            ushort recharge;

            PicoPass.CreditResult result=pp.CreditPurse(2500, out newPurse, out recharge);

            if (result != PicoPass.CreditResult.Success)
            {
                Console.WriteLine("Add Value Failed: {0}",result);
                return false;
            }


            Console.WriteLine("Success.  New Card Balance: {0}", newPurse);

            return true;

        }



        public static bool ReadPurse(PicoPass pp, byte[] serialNumber)
        {
            int purse = pp.ReadPurse();
            Console.WriteLine("Purse: {0}", purse);
            return true;
        }


        public static bool ClearPurse(PicoPass pp, byte[] serialNumber)
        {
            PicoPass.CreditResult result = pp.ClearPurse();

            Console.WriteLine("Clear Purse: {0}", result);

            return result == PicoPass.CreditResult.Success;
        }


        public static bool TestCredit(PicoPass pp, byte[] serialNumber)
        {
            Console.WriteLine("Credit");

            int purse = pp.ReadPurse();
            Console.WriteLine("Old Purse: {0}", purse);

            ushort newPurse;
            ushort recharge;

            if (pp.CreditPurse(5000, out newPurse, out recharge)!=PicoPass.CreditResult.Success)
                Console.WriteLine("Bad Credit");

            Console.WriteLine("New Purse: {0}", newPurse);
            Console.WriteLine("Recharge : {0}", recharge);
            return true;
        }

        public static bool TestDebit(PicoPass pp,byte[] serialNumber)
        {
            Console.WriteLine("Debit");

            int purse = pp.ReadPurse();
            Console.WriteLine("Old Purse: {0}", purse);

            ushort newPurse;
            if (pp.DebitPurse(20, out newPurse)!=PicoPass.DebitResult.Success)
                Console.WriteLine("Bad Debit");

            Console.WriteLine("New Purse: {0}", newPurse);

            return true;
        }

        public static bool TestRead4(PicoPass pp, byte[] serialNumber)
        {
            byte[] data = pp.Read4Block(6);
            Console.WriteLine("Read4: {0}", HexString(data));
            return true;
        }

        public static bool TestWrite4(PicoPass pp, byte[] serialNumber)
        {

            byte[] data = pp.Read4Block(6);
            Console.WriteLine("Read4: {0}", HexString(data));

            data = GetSampleData(32);

            Console.WriteLine("Write4: {0}", HexString(data));
            pp.Write4Block(6, new ArraySegment<byte>(data), true);

            data = pp.Read4Block(6);
            Console.WriteLine("Read4: {0}", HexString(data));


            return true;
        }


        public static bool TestWrite(PicoPass pp, byte[] serialNumber)
        {

            byte[] block7 = pp.ReadBlock(7);
            Console.WriteLine("Block7:{0}", HexString(block7));

            block7 = GetSampleData(8);

            Console.WriteLine("Write7: {0}", HexString(block7));

            if (!pp.WriteBlock(7, new ArraySegment<byte>(block7), true))
                Console.WriteLine("Bad Write");

            block7 = pp.ReadBlock(7);

            if (block7 == null)
                Console.WriteLine("Bad Read");
            else
                Console.WriteLine("Block7:{0}", HexString(block7));


            return true;
        }

        public static int counter = 82;

        public static void LogSerialNumber(byte[] sn)
        {

            string line=string.Format("{0}, {1:X2}{2:X2}{3:X2}{4:X2}{5:X2}{6:X2}{7:X2}{8:X2}\r\n",counter,sn[0],sn[1],sn[2],sn[3],sn[4],sn[5],sn[6],sn[7]);
            File.AppendAllText("serial.txt", line);
            
            Console.Write("Logged: {0}. ", counter);
            counter++;

            Console.WriteLine(" Now Expecting: {0}", counter);
        }



        public static void SelectLoop(PicoPass.Auth auth, CardAction action, string comPort)
        {
//            PicoPass pp = new PicoPass("COM8");
            PicoPass pp = new PicoPass(comPort);
//          PicoPass pp = new PicoPass("COM5");
            
            pp.Connect();

            while(true)
//            for (int i = 0; i < 10; i++)
            {
                byte[] serialNumber;
                if (pp.SelectCard(auth, out serialNumber))
                {
                    Console.WriteLine();
                    Console.WriteLine("Card : {0}", HexString(serialNumber));

                    if (action(pp, serialNumber))
                    {
                        Beep(440, 250);
//                        LogSerialNumber(serialNumber);
                    }

                    Console.WriteLine();

                    System.Threading.Thread.Sleep(1000);

                }
                else
                    Console.WriteLine("No Card");

                System.Threading.Thread.Sleep(500);
            }
            pp.Disconnect();
        }


        //public static string HexString(byte[] bytes)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    sb.Append("{");

        //    for (int i = 0; i < bytes.Length; i++)
        //    {
        //        if (i == 0)
        //            sb.AppendFormat("0x{0:X2}", bytes[i]);
        //        else
        //            sb.AppendFormat(", 0x{0:X2}", bytes[i]);
        //    }
        //    sb.Append("}");
        //    return sb.ToString();
        //}

        public static string HexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < bytes.Length; i++)
            {
                if (i == 0)
                    sb.AppendFormat("{0:X2}", bytes[i]);
                else
                    sb.AppendFormat(" {0:X2}", bytes[i]);
            }
            return sb.ToString();
        }

        static int sampleCounter = 1;

        static byte[] GetSampleData(int nBytes)
        {

            byte[] data=new byte[nBytes];

            for(int i=0;i<nBytes;i++)
                data[i] = Convert.ToByte(sampleCounter++);


            return data;
        }


    }
}
