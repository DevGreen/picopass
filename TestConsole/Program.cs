﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace GI.Flash.Pico.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
//                TestCardNumber.Run();
//                TestPicoRead.Run();
                TestPicoPass.Run();

//                TestPicoHeader.Run();
//                TestDll.Run();
//                TestSerialASCII.Run();
//                TestFlashCard.Run();
            }
            catch (Exception expt)
            {
                Console.WriteLine(expt.ToString());
            }
            Thread.Sleep(5000);
        }
    }
}
