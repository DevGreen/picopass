﻿using System;
using System.Collections.Generic;
using System.Text;


using GI.Utility;

namespace GI.Flash.Pico.Test
{
    public class TestCardNumber
    {
        public static void Run()
        {

            uint baseNum=10000;

            for (uint i = 0; i < 100; i++)
            {
                uint number = baseNum + i;

                Console.WriteLine("{0} : {1} ", number,CardNumber.AppendCheckDigit(number));

            }
        }
    }
}
