﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

namespace GI.Flash.Pico.Test
{
    public class TestPicoHeader
    {
        public static void Run()
        {
            TestSign();
        }

        
        public static void TestSign()
        {
            PicoCardHeader header = new PicoCardHeader();

            header.CardType = CardType.User;
            header.CardVersion = 0x01;
            header.CustomerID = 0xFF36;
            header.CardNumber = 8675309;
            header.LocationID = "GREENWALD";


            byte[] serialNumber=new byte[]{0x00,0x01,0x02,0x03,0x04,0x05,0x06,0x07};

            header.Sign(serialNumber);

            DisplayHeader(header);

            Console.WriteLine("Signed");
            Console.WriteLine();

            header.LocationID = "TEST";

            DisplayHeader(header);
            bool verified = header.Verify(serialNumber);
            Console.WriteLine("Verified: {0}", verified);

            Console.WriteLine();

            header.LocationID = "GREENWALD";
            DisplayHeader(header);
            verified = header.Verify(serialNumber);
            Console.WriteLine("Verified: {0}", verified);

        }

        public static void DisplayHeader(PicoCardHeader header)
        {
            Console.WriteLine("Header:");
            Console.WriteLine("CardType: {0:X2}", header.CardType);
            Console.WriteLine("CardVersion: {0:X2}", header.CardVersion);
            Console.WriteLine("CustomerID: {0:X4}", header.CustomerID);
            Console.WriteLine("CardNumber: {0:X8}", header.CardNumber);

            Console.WriteLine("LocationID: |{0}|{1}|", header.LocationID, header.LocationID.Length);
        }
    }
}
