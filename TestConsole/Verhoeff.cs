﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GI.Utility.Test
{
    public enum Digit { Zero = 0, One = 1, Two = 2, Three = 3, Four = 4, Five = 5, Six = 6, Seven = 7, Eight = 8, Nine = 9 };


    public class Verhoeff
    {
        // Performs Verhoeff Checksum Algorithm
        // returns true if the number has a valid (zero) checksum
        public static bool VerifyCheckDigit(List<Digit> digits)
        {
            Digit c = CalculateCheckDigit(digits);
            return c == Digit.Zero;
        }

        //Performs Verhoeff Checksum Algorithm
        //Appends the proper Check Digit to the end of the number
        public static void AppendCheckDigit(List<Digit> digits)
        {
            digits.Add(Digit.Zero);
            Digit c = CalculateCheckDigit(digits);
            digits[digits.Count - 1] = InverseD(c, 0);
        }


        private static Digit CalculateCheckDigit(List<Digit> digits)
        {
            List<Digit> n = new List<Digit>(digits);
            n.Reverse();

            Digit c = Digit.Zero;
            for (int i = 0; i < n.Count; i++)
            {
                c = OperatorD(c, Permute(i, n[i]));
            }

            return c;
        }

        public static Digit OperatorD(Digit j, Digit k)
        {
            return (Digit)d_table[10 * ((int)j) + ((int)k)];
        }

        public static Digit InverseD(Digit j, Digit d)
        {
            return (Digit)d_inverse[10 * ((int)j) + ((int)d)];
        }

        public static Digit Permute(int pos, Digit num)
        {
            return (Digit)p_table[10 * (pos%8) + ((int)num)];
        }


        public static Digit DePermute(int pos, Digit num)
        {
            return (Digit)p_inverse[10 * (pos%8) + ((int)num)];
        }


        private static int[] d_table =
        {                             
        //               k                    
        //  0  1  2  3  4  5  6  7  8  9     
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, // 0
            1, 2, 3, 4, 0, 6, 7, 8, 9, 5, // 1 
            2, 3, 4, 0, 1, 7, 8, 9, 5, 6, // 2 
            3, 4, 0, 1, 2, 8, 9, 5, 6, 7, // 3 
            4, 0, 1, 2, 3, 9, 5, 6, 7, 8, // 4   j 
            5, 9, 8, 7, 6, 0, 4, 3, 2, 1, // 5 
            6, 5, 9, 8, 7, 1, 0, 4, 3, 2, // 6 
            7, 6, 5, 9, 8, 2, 1, 0, 4, 3, // 7 
            8, 7, 6, 5, 9, 3, 2, 1, 0, 4, // 8 
            9, 8, 7, 6, 5, 4, 3, 2, 1, 0  // 9
        };

        private static int[] d_inverse =
        { 
        //               d                    
        //  0  1  2  3  4  5  6  7  8  9     
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, // 0
            4, 0, 1, 2, 3, 9, 5, 6, 7, 8, // 1 
            3, 4, 0, 1, 2, 8, 9, 5, 6, 7, // 2 
            2, 3, 4, 0, 1, 7, 8, 9, 5, 6, // 3 
            1, 2, 3, 4, 0, 6, 7, 8, 9, 5, // 4  j 
            5, 9, 8, 7, 6, 0, 4, 3, 2, 1, // 5 
            6, 5, 9, 8, 7, 1, 0, 4, 3, 2, // 6 
            7, 6, 5, 9, 8, 2, 1, 0, 4, 3, // 7 
            8, 7, 6, 5, 9, 3, 2, 1, 0, 4, // 8 
            9, 8, 7, 6, 5, 4, 3, 2, 1, 0  // 9
        };

        private static int[] p_table =
        {
        //               num                    
        //  0  1  2  3  4  5  6  7  8  9   
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, // 0
            1, 5, 7, 6, 2, 8, 3, 0, 9, 4, // 1 
            5, 8, 0, 3, 7, 9, 6, 1, 4, 2, // 2 
            8, 9, 1, 6, 0, 4, 3, 5, 2, 7, // 3   pos 
            9, 4, 5, 3, 1, 2, 6, 8, 7, 0, // 4 
            4, 2, 8, 6, 5, 7, 3, 9, 0, 1, // 5 
            2, 7, 9, 3, 8, 0, 6, 4, 1, 5, // 6 
            7, 0, 4, 6, 9, 1, 3, 2, 5, 8  // 7 
        };

        private static int[] p_inverse =
        {
        //               num_inv                    
        //  0  1  2  3  4  5  6  7  8  9   
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, // 0
            7, 0, 4, 6, 9, 1, 3, 2, 5, 8, // 1 
            2, 7, 9, 3, 8, 0, 6, 4, 1, 5, // 2 
            4, 2, 8, 6, 5, 7, 3, 9, 0, 1, // 3   pos
            9, 4, 5, 3, 1, 2, 6, 8, 7, 0, // 4
            8, 9, 1, 6, 0, 4, 3, 5, 2, 7, // 5
            5, 8, 0, 3, 7, 9, 6, 1, 4, 2, // 6
            1, 5, 7, 6, 2, 8, 3, 0, 9, 4, // 7
        };


    }
}
