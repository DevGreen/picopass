//******************************************************************************
//   FILE NAME: international_<project name>.c
//
//     PROJECT: (generic)
//   COMPONENT: Internationalization client module for GI embedded applications.
//
//      TARGET: GNU GAWK proprocessing -> ANSI C
//    COMPILER: ANSI/ISO C99
//
//
// DESCRIPTION:
//    This module implements a "perfect" hash function and multilingual string
//    table, machine built and human-translated for the set of string literals
//    found in the other "C" modules with which it is being linked. The
//    translated strings are accessed efficiently with a single lookup
//    operation, given a native language string as a key. The module,
//    accompanying text processing scripts written in AWK and perfect hash
//    generator GPERF comprise a lightweight internationalization (i18n) system
//    for embedded applications.
//
//    NOTES: 1> This module serves as a template and cannot be directly
//              compiled. It must be processed by the utility "intl-post-trans"
//              after the human translation phase of the internationalization
//              process is complete.
//
//           2> After processing, the name of the template module is altered
//              as follows, according to the <project-name> passed to the
//              internationalization scripts:
//
//                 international_template.c => international_<project-name>.c
//                 international_template.h => international_<project-name>.h
//
// INTERNATIONALIZATION DETAILS:
//    This template file along with its associated header are processed by the
//    GNU GAWK script "gencode.awk" to produce a syntactically correct "C"
//    module. The six scripts used in the overall process must run in the
//    following order (note that the names of generated files are denoted by
//    parens):
//
//    ==========================================================================
//    PHASE I: STRING SCAN, invoked with "intl-scan.bat"
//    ==========================================================================
//
//    First, strings in the source code are marked as translatable by passing
//    them to the function-like macros "_()" or "GSTRING()", or by directly
//    calling the translation function "INTL_<project-name>_GetStr()". Then,
//    the source is scanned for translatable strings. Note that the special
//    project files "international_<project name>.c" and "international_
//    <project name>.h" are automatically skipped to avoid re-inclusion of the
//    strings to be translated. Also, since the source is scanned before the "C"
//    preprocessor runs, any string manipulations performed by the preprocessor
//    will not be recognized.
//
//       ("C" source) -> stripcom.awk -> getstr.awk -> (*.dic)
//
//    The script "stripcom.awk" first strips all comments from the "C" source
//    code. The script "getstr.awk" then identifies all unique, non-null string
//    literals tagged for translation and outputs them, one per line, to a
//    "dictionary" file. Each line of the dictionary specifies a unique string
//    and the file and approximate line of each of its one or more instances.
//
//    The user of the "intl-scan" batch file must redirect its standard output
//    to a file with the desired name and extension, using the redirection
//    operator ">", so that the Phase II scripts can process the string data.
//
//    ==========================================================================
//    PHASE II: PRE-TRANSLATION, invoked with "intl-pre-trans.bat":
//    ==========================================================================
//
//    During this phase, the dictionary file(s) generated in Phase I are first
//    processed by the GAWK script "hashstr.awk", which formats the string data
//    in the dictionary files in a manner suitable for processing by the GPERF
//    hash generator. Directives and string IDs are also inserted into GPERF's
//    input, such that the original input strings can be correlated with the
//    lexically dissimilar but equivalent representation used in GPERF's hash
//    tables.
//
//    The GNU GPERF hash function generator reads the quoted strings produced
//    by "hashstr.awk" from standard input, and, using them as keys, creates a
//    perfect hash function. The generated "C" source for the function is
//    written to the file "hashfunc.c".
//
//    The hash function and table in "hashfunc.c" are processed by the script
//    "hashtable.awk", which extracts the mapping between the hash table indices
//    and key strings. The actual table is neither required or desired, as the
//    native language key strings are already present in the source.
//
//    Note that the hash table strings are a superset of the key strings
//    recognized; several table rows typically contain null strings (""), which
//    indicate a null mapping (i.e. the key string does not exist in the table).
//
//       (*.dic) -> hashstr.awk -> gperf -> (<project-name>-hashfunc.c) ->
//
//          hashtable.awk -> (<project-name>-hashtable.txt)
//
//    Finally, the file for the translator is created from the string dictionary
//    file by the GAWK script "transout.awk":
//
//       (*.dic) -> transout.awk -> (<project-name>-to-translator.txt)
//
//    "transout.awk" also reads two items from the command line:
//
//       * The project name.
//
//       * The supported languages (other than the default, which simply echoes
//         the code's native source strings). Language tags must be alpha-
//         numeric, containing only letters and digits without whitespace,
//         separated by colons. For example, Spanish and French could be
//         specified as:
//
//            SP:FR
//
//    At this point, a human translator must provide translations for each
//    message listed in the "<project-name>-to-translator.txt" output file.
//    After performing the translations, the file should be copied to another
//    file, "<project-name>-from-translator.txt", for final processing.
//
//    ==========================================================================
//    PHASE III: POST-TRANSLATION, invoked with "intl-post-trans.bat":
//    ==========================================================================
//
//    In the third and final "post-translation" phase of processing, the text
//    file from the translator is read, along with the hash table file, hash
//    function source and "C" template files.
//
//             (<project-name>-hashtable.txt) ->|
//                (<project-name>-hashfunc.c) ->|
//                 (international_template.h) ->|
//                 (international_template.c) ->|
//                                              |
//       (<project-name>-from-translator.txt) -> gencode.awk ->
//                                                          |->
//          (international_<project-name>_.c)
//          (international_<project-name>_.h)
//
//    Two machine generated "C" source files are produced, "international_
//    <project-name>.h" and "international_<project-name>.c". These files, along
//    with the main "C" sources "international.h" and "international.c", must be
//    compiled and linked with the application program.
//
//
//    4) The GAWK script "hashtable.awk" is run using "hashfunc.c" as input, and
//       creates a file with the default name "hashtable.txt". The purpose of
//       the hashtable script is to extract //
//       As a result, the "hashtable.txt" expresses one mapping per line, with
//       each string and its corresponding hash function index delimited by an
//       ASCII tab character. Note that //
//    5) The GAWK script "transout.awk" reads the "hashtable.txt" and
//       "dictionary.txt" files, as well as three items from the "pre-trans"
//       invocation command line:
//
//          * The project name.
//          * The supported languages other than the default, which simply
//            echoes the native source string. Language tags MUST be
//            alphanumeric strings separated by colons, containig only letters
//            and digits. For example, Spanish and French could be specified as
//
//               SP:FR
//
//            The language tags identify lines to translate in the
//            "to-translator" file, and are also employed to construct unique
//            "C" enumeration tags for each non-native language supported.
//
//          * One or more path(s) to the "C" source files to be scanned.
//
//       The script output is the file "to-translator.txt", which contains all
//       the information a translator needs to provide translations for the
//       target strings.
//
//    6) A translator reads the file "to-translator.txt" and edits it, then
//       renamed or copies it to a file "from-translator.txt".
//
//    7) The "post-trans" batch file is invoked by passing the name of the
//       translated file on the command line. The batch file executes the
//       GAWK script "gencode.awk", which reads the files:
//
//          * from-translator.txt      (for translations)
//          * hashfunc.c               (for the perfect hash function)
//          * hashtable.txt            (for the hash key string -> index map)
//          * international-template.c (for the "C" source template)
//          * international-template.h (for the "C" header template)
//
//       and produces the final output, two "C" source files named
//       "international.h" and "international.c", ready for inclusion in the
//       target application.
//
//
// CHANGE HISTORY:
//
// Version  mo/da/yr    Init  Remarks
// -------  --------    ----  -------
// 1.00     06/15/11    TT    Initial revision.
//
//******************************************************************************

//******************************************************************************
// INCLUDED FILES
//******************************************************************************

#include "international_foo.c.h"
#include <string.h>

//******************************************************************************
// PRIVATE MACRO AND TYPE DEFINITIONS
//******************************************************************************

// Locale string type.
typedef struct INTL_SREC
{
   // Maximum length for the string.
   const int maxLen;
   // Locale strings in language order.
   char     *text[ INTL_foo.c_NUM_LANG - 1 ];
} INTL_SREC_T;

//******************************************************************************
// PRIVATE FUNCTION PROTOTYPES
//******************************************************************************

//******************************************************************************
// PRIVATE DATA
//******************************************************************************

// External language-determination function.
static int ( *extLangFunc )( void ) = NULL;

// Local translation table.
static const INTL_SREC_T intlStr[] =
{
   { // Hash Index 0: 
      0,
      {
      "" // !!! WARNING: Missing translation
      }
   }
};

//******************************************************************************
// PUBLIC DATA
//******************************************************************************

//******************************************************************************
// PRIVATE FUNCTIONS
//******************************************************************************

#define TOTAL_KEYWORDS 0
#define MIN_WORD_LENGTH 0
#define MAX_WORD_LENGTH 0
#define MIN_HASH_VALUE 0
#define MAX_HASH_VALUE 0

static unsigned int
hash (register const char *str, register unsigned int len)
{
   return 0;
}

//******************************************************************************
// PUBLIC FUNCTIONS
//******************************************************************************

void INTL_foo.c_SetLangQueryFunc( int ( *langFunc )( void ))
{
   extLangFunc = langFunc;
}

char *INTL_foo.c_GetStr( char *str )
{
   register int      language = extLangFunc ? extLangFunc() : 0;
   register unsigned len      = strlen( str );
   register char     *result  = str;

   if ( language > 0 && language < INTL_foo.c_NUM_LANG &&
        len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH )
   {
      register int key = hash ( str, len );

      if ( key <= MAX_HASH_VALUE && key >= 0 )
      {
         register char *entry = intlStr[ key ].text[ language - 1 ];

         if ( entry && *entry )
         {
            result = entry;
         }
      }
   }
   return ( result );
}

char *INTL_foo.c_GetStrByIndex( int index )
{
   register int   language = extLangFunc ? extLangFunc() : 0;
   register char  *result = "";

   if ( language > 0 && language < INTL_foo.c_NUM_LANG &&
        index <= MAX_HASH_VALUE && index >= 0 )
   {
      result = intlStr[ index ].text[ language - 1 ];
   }
   return ( result );
}

int INTL_foo.c_GetMaxIndex( void )
{
   return ( MAX_HASH_VALUE );
}
