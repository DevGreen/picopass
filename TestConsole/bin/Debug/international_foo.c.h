//******************************************************************************
//   FILE NAME: international_template.h
//
//     PROJECT: (generic)
//   COMPONENT: Internationalization client module for GI embedded applications.
//
//      TARGET: GNU GAWK proprocessing -> ANSI C
//    COMPILER: ANSI/ISO C99
//
//
// DESCRIPTION:
//    This machine processed module implements a "perfect" hash function and
//    multilingual string table, machine built and human-translated for the
//    set of string literals found in the other "C" modules with which it is
//    being linked. The translated strings are accessed efficiently with a
//    single lookup operation, given a native language string as a key. The
//    module, accompanying text processing scripts written in AWK and perfect
//    hash function generator GPERF comprise a lightweight internationalization
//    (i18n) system for embedded applications.
//
//    NOTES: 1> This module serves as a template and cannot be directly
//              compiled. It must be processed by the utility "intl-post-trans"
//              after the human translation phase of the internationalization
//              process is complete.
//
//           2> After processing, the name of the template module is altered
//              as follows, according to the <project-name> passed to the
//              internationalization scripts:
//
//                 international_template.c => international_<project-name>.c
//                 international_template.h => international_<project-name>.h
//
//    Please examine the module implemetation "international_template.c" for a
//    more detailed explanation.
//
//
// CHANGE HISTORY:
//
// Version  mo/da/yr    Init  Remarks
// -------  --------    ----  -------
// 1.00     05/24/11    TT    Initial revision.
//
//******************************************************************************

#ifndef INTERNATIONAL_foo.c_H /* Guard against multiple inclusion */
#define INTERNATIONAL_foo.c_H

//******************************************************************************
// INCLUDED FILES
//******************************************************************************

//******************************************************************************
// PUBLIC MACRO AND TYPE DEFINITIONS
//******************************************************************************

// Number of alternate languages, including the default (native) language which
// is always present in the source code string literals.
#define INTL_foo.c_NUM_LANG 2

// Shorthand string reference calls.
#ifndef _
   #define _( string )        INTL_foo.c_GetStr( string )
#endif

#ifndef GSTRING
   #define GSTRING( string )  INTL_foo.c_GetStr( string )
#endif

//******************************************************************************
// PUBLIC DATA DECLARATIONS
//******************************************************************************

//******************************************************************************
// PUBLIC FUNCTION PROTOTYPES
//******************************************************************************

void INTL_foo.c_SetLangQueryFunc( int ( *langFunc )( void ));

char *INTL_foo.c_GetStr( char *str );

char *INTL_foo.c_GetStrByIndex( int index );

int INTL_foo.c_GetMaxIndex( void );

#endif /* Guard against multiple inclusion */
