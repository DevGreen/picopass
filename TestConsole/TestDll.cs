﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using System.Runtime.InteropServices;

namespace GI.Flash.Pico.Test
{
    public class TestDll
    {

        public static void Run()
        {
            TestVend();
        }
        public static void TestVend()
        {
            FlashCode rc;

            rc = PP_InitUART(1);
            Console.WriteLine("PP_InitUART: {0}", rc);

            rc = PP_InitCardReader(1, null);
            Console.WriteLine("InitCardReader: {0}", rc);

            CardData cardData = new CardData();

            cardData.serialNumber = new byte[] {0x6C,0xA4,0x63,0x01,0x08,0x00,0x12,0xE0};

            ushort newPurse;

            rc = PP_DebitCard(ref cardData, 500, out newPurse);

            Console.WriteLine("PP_DebitCard: {0}", rc);



            if (rc == FlashCode.Success)
            {

                Console.WriteLine("New Purse: {0}", newPurse);
            }



            rc = PP_CloseUART(1);
            Console.WriteLine("PP_CloseUART: {0}", rc);

        }

        public static void TestRead()
            {
            FlashCode rc;

            rc = PP_InitUART(1);
            Console.WriteLine("PP_InitUART: {0}", rc);

            rc = PP_InitCardReader(1, null);
            Console.WriteLine("InitCardReader: {0}", rc);


            CardData cardData = new CardData();

             rc=PP_ReadCard(ref cardData);

            Console.WriteLine("ReadCard: {0}", rc);



            if (rc == FlashCode.Success)
            {

                Console.WriteLine("Card: {0}", HexString(cardData.serialNumber));
                Console.WriteLine("Customer ID:{0:X4}", cardData.customerID);

                string locationID = Encoding.ASCII.GetString(cardData.locationID).Trim();
                Console.WriteLine("LocationID:{0}", locationID);

                Console.WriteLine("Purse:{0}", cardData.purseValue);

            }



            rc = PP_CloseUART(1);
            Console.WriteLine("PP_CloseUART: {0}", rc);

        }

        public static string HexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();


            for (int i = 0; i < bytes.Length; i++)
            {
                if (i == 0)
                    sb.AppendFormat("{0:X2}", bytes[i]);
                else
                    sb.AppendFormat(" {0:X2}", bytes[i]);
            }
            return sb.ToString();
        }

        public enum FlashCode
        {
            Success             = 0,
            NoCardPresent       = -1,
            InvalidCard         = -2,
            WrongCard           = -3,
            InsufficientFunds   = -4,
            ReadError           = -5,
            WriteError          = -6
        }

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_InitUART(int uart);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_CloseUART(int uart);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_InitCardReader(int uart, byte[] keys);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_ReadCard(ref CardData cardData);

        [DllImport("PicoLib_C.Dll")]
        public static extern FlashCode PP_DebitCard(ref CardData cardData, ushort vendPrice, out ushort newPurseValue);

        [StructLayout(LayoutKind.Sequential)]
        unsafe public struct CardData
        {
            public ushort customerID;
            [MarshalAs(UnmanagedType.ByValArray,SizeConst=10)]
            public byte[] locationID;
            [MarshalAs(UnmanagedType.ByValArray,SizeConst=8)]
            public byte[] serialNumber;
            public ushort purseValue;
        }

    }
}
