﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;

using GI.Flash.Pico;

namespace GI.Flash.Pico.Test
{
    public class TestPicoRead
    {
        public static void Run()
        {
//            ReadSerial();
//            TestKeyLoading();
//            TestDiversification();

//            CheckKeySigs();


        }

        public static void CheckKeySigs()
        {
            byte[] zeros = new byte[8];

            //PicoRead reader = new PicoRead("COM3");
            PicoRead reader = new PicoRead("COM6");
            reader.Open();

            PicoRead.Key[] keys = new PicoRead.Key[] { PicoRead.Key.Kd0, PicoRead.Key.Kc0, PicoRead.Key.Kd1, PicoRead.Key.Kc1 };


            foreach (PicoRead.Key key in keys)
            {

                byte[] sig = reader.GetDiversifiedKey(key, zeros);
                Console.WriteLine("{0} Sig: {1}",key.ToString(), HexString(sig));
            }

            reader.Close();

            // Signatures:
            // Each Key diversified with all zeros

            //Default Debit  Sig: {0x25, 0x62, 0x97, 0xEF, 0xE8, 0x09, 0xF0, 0xA2}
            //Default Credit Sig: {0xA5, 0xFE, 0xC9, 0xBD, 0x64, 0x68, 0x21, 0xDA}
            //GI Demo Debit  Sig: {0x32, 0xD3, 0x38, 0x43, 0x36, 0x99, 0x41, 0x4E}
            //GI Demo Credit Sig: {0x0D, 0xFE, 0x1C, 0x13, 0x10, 0xD6, 0xEF, 0xC9}
            //GI Prod Debit  Sig: {0xA8, 0xAC, 0x4B, 0xC3, 0xC9, 0x12, 0x84, 0x41}
            //GI Prod Credit Sig: {0xD6, 0xDF, 0x62, 0x23, 0x13, 0xD0, 0x02, 0xF7}
        }


        public static void TestDiversification()
        {
            PicoRead reader = new PicoRead("COM6");
            reader.Open();

            byte[] exampleSN=new byte[]{0x5C,0xB8,0x00,0x00,0x03,0x00,0x0C,0x00};

            byte[] divKey=reader.GetDiversifiedKey(PicoRead.Key.Kd0, exampleSN);

            Console.WriteLine("Div Key: {0}", HexString(divKey));

            reader.Close();
        }

        public static void TestKeyLoading()
        {
            PicoRead reader = new PicoRead("COM6");
            reader.Open();

            byte[] exchangeKey = new byte[] { 0x5C, 0xBC, 0xF1, 0xDA, 0x45, 0xD5, 0xFB, 0x5F };
            byte[] exampleDKey = new byte[] { 0x00, 0x11, 0x11, 0x11, 0x22, 0x22, 0x22, 0x88 };
            byte[] oldGIKd = new byte[] { 0x0F, 0xA3, 0x77, 0x4F, 0xBA, 0x91, 0x09, 0xF7 };
            byte[] oldGIKc = new byte[] { 0xF3, 0x4A, 0xD2, 0x19, 0x06, 0x34, 0x2B, 0x88 };

            
            bool result = reader.LoadKey(PicoRead.Key.Kd0, exchangeKey, exampleDKey);


            Console.WriteLine("Load Key Result: {0}", result);

            reader.Close();

        }

        public static void TestKeyLoadSteps()
        {
            //PicoRead reader = new PicoRead("COM6");


            //byte[] exchangeKey = new byte[] { 0x5C, 0xBC, 0xF1, 0xDA, 0x45, 0xD5, 0xFB, 0x5F };
            //byte[] random = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            //byte[] newKey = new byte[] { 0xF0, 0xE1, 0xD2, 0xC3, 0xB4, 0xA5, 0x96, 0x87 };


            //byte[] pExchange = reader.PermuteKey(exchangeKey);
            //Console.WriteLine("Permuted Exchange: {0}", HexString(pExchange));

            //byte[] sessionKey = reader.XorKeys(pExchange, random);

            //Console.WriteLine("Session Key: {0}", HexString(sessionKey));

            //byte[] pKey = reader.PermuteKey(newKey);

            //Console.WriteLine("Permuted New Key: {0}", HexString(pKey));

            //byte[] cryptKey = reader.XorKeys(pKey, sessionKey);

            //Console.WriteLine("Encrypted Key: {0}", HexString(cryptKey));

            //byte[] checkSum = reader.CalcCheckSum(pKey, PicoRead.Key.Kd0);

            //Console.WriteLine("Check Sum: {0}", HexString(checkSum));

        }

        public static void ReadSerial()
        {
            PicoRead reader = new PicoRead("COM6");

            Console.WriteLine("Reset Reader");

            reader.Open();
            reader.ResetField();
            

            System.Threading.Thread.Sleep(1000);

            Console.WriteLine("Select Card");

            for (int i = 0; i < 10; i++)
            {
                byte[] sn = reader.SelectCard(PicoRead.SelectMode.NoAuth, PicoRead.ChipType.PicoTag);

                if (sn == null)
                    Console.WriteLine("No Card");
                else
                {
                    Console.WriteLine("SerialNumber: {0}", HexString(sn));

                    byte[] command=new byte[]{0x0C,0x02};
                    byte[] result = new byte[8];

                    bool read=reader.Transmit(PicoRead.Protocol.Pico15693,
                                    PicoRead.CRC.SendAndVerify,
                                    PicoRead.Timeout.Mult1,
                                    false,
                                    new ArraySegment<byte>(command),
                                    new ArraySegment<byte>(result));

                    if (read)
                        Console.WriteLine("Read:{0}", HexString(result));
                    else
                        Console.WriteLine("No Read.");


                    break;
                }

                System.Threading.Thread.Sleep(500);
            }
        }

        public static string HexString(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{");

            for (int i = 0; i < bytes.Length; i++)
            {
                if (i == 0)
                    sb.AppendFormat("0x{0:X2}", bytes[i]);
                else
                    sb.AppendFormat(", 0x{0:X2}", bytes[i]);
            }
            sb.Append("}");
            return sb.ToString();
        }
    }
}
