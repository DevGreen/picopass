﻿namespace GI.Flash.Test
{
    partial class KeyLoadApp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.connectedLbl = new System.Windows.Forms.Label();
            this.portCombo = new System.Windows.Forms.ComboBox();
            this.connectBtn = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.creditKeyLbl = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.debitKeyLbl = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.modeLbl = new System.Windows.Forms.Label();
            this.cardNumLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cardLbl = new System.Windows.Forms.Label();
            this.serialLbl = new System.Windows.Forms.Label();
            this.selectTimer = new System.Windows.Forms.Timer(this.components);
            this.delayTimer = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.attemptBox = new System.Windows.Forms.CheckBox();
            this.reverseBox = new System.Windows.Forms.CheckBox();
            this.appLabelLbl = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.KeyAccessLbl = new System.Windows.Forms.Label();
            this.configLbl = new System.Windows.Forms.Label();
            this.fusesLbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cardNumberBox = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cardNumberBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.connectedLbl);
            this.groupBox1.Controls.Add(this.portCombo);
            this.groupBox1.Controls.Add(this.connectBtn);
            this.groupBox1.Location = new System.Drawing.Point(29, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(275, 117);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Coupler";
            // 
            // connectedLbl
            // 
            this.connectedLbl.BackColor = System.Drawing.Color.Red;
            this.connectedLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.connectedLbl.Location = new System.Drawing.Point(24, 19);
            this.connectedLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.connectedLbl.Name = "connectedLbl";
            this.connectedLbl.Size = new System.Drawing.Size(227, 35);
            this.connectedLbl.TabIndex = 2;
            this.connectedLbl.Text = "Disconnected";
            this.connectedLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // portCombo
            // 
            this.portCombo.FormattingEnabled = true;
            this.portCombo.Location = new System.Drawing.Point(24, 69);
            this.portCombo.Margin = new System.Windows.Forms.Padding(4);
            this.portCombo.Name = "portCombo";
            this.portCombo.Size = new System.Drawing.Size(119, 24);
            this.portCombo.TabIndex = 1;
            // 
            // connectBtn
            // 
            this.connectBtn.Location = new System.Drawing.Point(151, 69);
            this.connectBtn.Margin = new System.Windows.Forms.Padding(4);
            this.connectBtn.Name = "connectBtn";
            this.connectBtn.Size = new System.Drawing.Size(100, 24);
            this.connectBtn.TabIndex = 0;
            this.connectBtn.Text = "Connect";
            this.connectBtn.UseVisualStyleBackColor = true;
            this.connectBtn.Click += new System.EventHandler(this.connectBtn_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.fusesLbl);
            this.groupBox2.Controls.Add(this.configLbl);
            this.groupBox2.Controls.Add(this.KeyAccessLbl);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.appLabelLbl);
            this.groupBox2.Controls.Add(this.creditKeyLbl);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.debitKeyLbl);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.modeLbl);
            this.groupBox2.Controls.Add(this.cardNumLbl);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.cardLbl);
            this.groupBox2.Controls.Add(this.serialLbl);
            this.groupBox2.Location = new System.Drawing.Point(340, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(625, 325);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Contactless Card";
            // 
            // creditKeyLbl
            // 
            this.creditKeyLbl.BackColor = System.Drawing.Color.Silver;
            this.creditKeyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.creditKeyLbl.Location = new System.Drawing.Point(317, 271);
            this.creditKeyLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.creditKeyLbl.Name = "creditKeyLbl";
            this.creditKeyLbl.Size = new System.Drawing.Size(285, 32);
            this.creditKeyLbl.TabIndex = 26;
            this.creditKeyLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(315, 255);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "Credit Key:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(318, 202);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 24;
            this.label3.Text = "Debit Key:";
            // 
            // debitKeyLbl
            // 
            this.debitKeyLbl.BackColor = System.Drawing.Color.Silver;
            this.debitKeyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.debitKeyLbl.Location = new System.Drawing.Point(317, 218);
            this.debitKeyLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.debitKeyLbl.Name = "debitKeyLbl";
            this.debitKeyLbl.Size = new System.Drawing.Size(285, 32);
            this.debitKeyLbl.TabIndex = 23;
            this.debitKeyLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 255);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 16);
            this.label4.TabIndex = 22;
            this.label4.Text = "Mode:";
            // 
            // modeLbl
            // 
            this.modeLbl.BackColor = System.Drawing.Color.Silver;
            this.modeLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modeLbl.Location = new System.Drawing.Point(16, 271);
            this.modeLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.modeLbl.Name = "modeLbl";
            this.modeLbl.Size = new System.Drawing.Size(285, 32);
            this.modeLbl.TabIndex = 21;
            this.modeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cardNumLbl
            // 
            this.cardNumLbl.BackColor = System.Drawing.Color.Silver;
            this.cardNumLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardNumLbl.Location = new System.Drawing.Point(20, 149);
            this.cardNumLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.cardNumLbl.Name = "cardNumLbl";
            this.cardNumLbl.Size = new System.Drawing.Size(285, 32);
            this.cardNumLbl.TabIndex = 20;
            this.cardNumLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 133);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 16);
            this.label1.TabIndex = 19;
            this.label1.Text = "Card Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 16);
            this.label2.TabIndex = 17;
            this.label2.Text = "Serial Number:";
            // 
            // cardLbl
            // 
            this.cardLbl.BackColor = System.Drawing.Color.Silver;
            this.cardLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cardLbl.Location = new System.Drawing.Point(22, 27);
            this.cardLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cardLbl.Name = "cardLbl";
            this.cardLbl.Size = new System.Drawing.Size(285, 39);
            this.cardLbl.TabIndex = 16;
            this.cardLbl.Text = "Card Not Present";
            this.cardLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // serialLbl
            // 
            this.serialLbl.BackColor = System.Drawing.Color.Silver;
            this.serialLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.serialLbl.Location = new System.Drawing.Point(22, 91);
            this.serialLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.serialLbl.Name = "serialLbl";
            this.serialLbl.Size = new System.Drawing.Size(285, 32);
            this.serialLbl.TabIndex = 18;
            this.serialLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // selectTimer
            // 
            this.selectTimer.Enabled = true;
            this.selectTimer.Tick += new System.EventHandler(this.selectTimer_Tick);
            // 
            // delayTimer
            // 
            this.delayTimer.Enabled = true;
            this.delayTimer.Interval = 2000;
            this.delayTimer.Tick += new System.EventHandler(this.delayTimer_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cardNumberBox);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.reverseBox);
            this.groupBox3.Controls.Add(this.attemptBox);
            this.groupBox3.Location = new System.Drawing.Point(29, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(275, 182);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Target";
            // 
            // attemptBox
            // 
            this.attemptBox.AutoSize = true;
            this.attemptBox.Location = new System.Drawing.Point(46, 38);
            this.attemptBox.Name = "attemptBox";
            this.attemptBox.Size = new System.Drawing.Size(106, 20);
            this.attemptBox.TabIndex = 0;
            this.attemptBox.Text = "Attempt Write";
            this.attemptBox.UseVisualStyleBackColor = true;
            // 
            // reverseBox
            // 
            this.reverseBox.AutoSize = true;
            this.reverseBox.Location = new System.Drawing.Point(46, 69);
            this.reverseBox.Name = "reverseBox";
            this.reverseBox.Size = new System.Drawing.Size(82, 20);
            this.reverseBox.TabIndex = 1;
            this.reverseBox.Text = "Reverse ";
            this.reverseBox.UseVisualStyleBackColor = true;
            // 
            // appLabelLbl
            // 
            this.appLabelLbl.BackColor = System.Drawing.Color.Silver;
            this.appLabelLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appLabelLbl.Location = new System.Drawing.Point(18, 217);
            this.appLabelLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.appLabelLbl.Name = "appLabelLbl";
            this.appLabelLbl.Size = new System.Drawing.Size(285, 32);
            this.appLabelLbl.TabIndex = 27;
            this.appLabelLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 192);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 16);
            this.label7.TabIndex = 28;
            this.label7.Text = "Application Label:";
            // 
            // KeyAccessLbl
            // 
            this.KeyAccessLbl.BackColor = System.Drawing.Color.Silver;
            this.KeyAccessLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyAccessLbl.Location = new System.Drawing.Point(317, 91);
            this.KeyAccessLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.KeyAccessLbl.Name = "KeyAccessLbl";
            this.KeyAccessLbl.Size = new System.Drawing.Size(285, 32);
            this.KeyAccessLbl.TabIndex = 29;
            this.KeyAccessLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // configLbl
            // 
            this.configLbl.AutoSize = true;
            this.configLbl.Location = new System.Drawing.Point(318, 72);
            this.configLbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.configLbl.Name = "configLbl";
            this.configLbl.Size = new System.Drawing.Size(82, 16);
            this.configLbl.TabIndex = 30;
            this.configLbl.Text = "Key Access:";
            // 
            // fusesLbl
            // 
            this.fusesLbl.BackColor = System.Drawing.Color.Silver;
            this.fusesLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fusesLbl.Location = new System.Drawing.Point(317, 149);
            this.fusesLbl.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.fusesLbl.Name = "fusesLbl";
            this.fusesLbl.Size = new System.Drawing.Size(285, 32);
            this.fusesLbl.TabIndex = 31;
            this.fusesLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(318, 133);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 16);
            this.label8.TabIndex = 32;
            this.label8.Text = "Fuses:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 106);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(121, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "Next Card Number:";
            // 
            // cardNumberBox
            // 
            this.cardNumberBox.Location = new System.Drawing.Point(46, 133);
            this.cardNumberBox.Maximum = new decimal(new int[] {
            99999999,
            0,
            0,
            0});
            this.cardNumberBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.cardNumberBox.Name = "cardNumberBox";
            this.cardNumberBox.Size = new System.Drawing.Size(174, 22);
            this.cardNumberBox.TabIndex = 19;
            this.cardNumberBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // KeyLoadApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(977, 406);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "KeyLoadApp";
            this.Text = "Key Load";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cardNumberBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox portCombo;
        private System.Windows.Forms.Button connectBtn;
        private System.Windows.Forms.Label connectedLbl;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label cardNumLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label cardLbl;
        private System.Windows.Forms.Label serialLbl;
        private System.Windows.Forms.Timer selectTimer;
        private System.Windows.Forms.Timer delayTimer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label modeLbl;
        private System.Windows.Forms.Label creditKeyLbl;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label debitKeyLbl;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox attemptBox;
        private System.Windows.Forms.CheckBox reverseBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label appLabelLbl;
        private System.Windows.Forms.Label configLbl;
        private System.Windows.Forms.Label KeyAccessLbl;
        private System.Windows.Forms.Label fusesLbl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown cardNumberBox;
        private System.Windows.Forms.Label label6;
    }
}

