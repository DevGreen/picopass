﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;

using GI.Flash.Pico;


namespace GI.Flash.Test
{
    public partial class KeyLoadApp : Form
    {
        private PicoRead accesso = null;

        public KeyLoadApp()
        {
            InitializeComponent();

            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            this.portCombo.Items.AddRange(ports);
            this.portCombo.SelectedIndex = 0;
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {

            try
            {
                if (!this.Connected)
                {
                    string portName = this.portCombo.SelectedItem as string;

                    this.accesso = new PicoRead(portName);
                    this.accesso.Open();


                    if (!this.VerifyAccessoKeys())
                    {
                        this.LoadAccessoKeys();

                        if (!this.VerifyAccessoKeys())
                        {
                            MessageBox.Show(this, "Could not load keys into accesso.", "Key Loading Error");
                            this.accesso.Close();
                            this.accesso = null;
                            return;
                        }
                    }

                    this.accesso.ResetField();
                    this.Connected = true;

                }
                else
                {
                    this.accesso.Close();
                    this.accesso = null;
                    this.Connected = false;
                }
            }
            catch (Exception expt)
            {
                ExceptionHelper.Handle(expt);
                this.accesso = null;
                this.Connected = false;
            }       
          

        }

        private bool _connected = false;
        private bool Connected
        {
            set
            {
                this._connected=value;
                this.connectedLbl.Text = value ? "Connected" : "Disconnected";
                this.connectedLbl.BackColor = value ? Color.Green : Color.Red;
                this.connectBtn.Text = value ? "Disconnect" : "Connect";
            }
            get
            {
                return this._connected;
            }
        }

        private PicoPassCard _currentCard = null;

        public PicoPassCard CurrentCard
        {
            get
            {
                return _currentCard;
            }
            set
            {
                _currentCard = value;
                this.cardLbl.Text = (value==null)?"Card Not Present":(value.IsCardValid?"Valid Card":"Invalid Card");
                this.cardLbl.BackColor = (value==null)?Color.Silver:(value.IsCardValid?Color.Green:Color.Red);
                this.serialLbl.Text = (value==null)?string.Empty:value.SerialNumberString;
                this.cardNumLbl.Text = (value == null) ? string.Empty : value.CardNumber.ToString();
                this.modeLbl.Text =(value==null)?string.Empty:value.Mode.ToString();
                this.debitKeyLbl.Text = (value == null) ? string.Empty : value.DebitKey.ToString();
                this.creditKeyLbl.Text = (value == null) ? string.Empty : value.CreditKey.ToString();
                this.appLabelLbl.Text=(value==null)?string.Empty: value.ApplicationLabel;

                this.KeyAccessLbl.Text =(value==null)?string.Empty: value.KeyAccess.ToString();
                this.fusesLbl.Text = (value==null)?string.Empty:(value.FusesOK?"OK":"Incorrect");

            }
        }

        private void selectTimer_Tick(object sender, EventArgs e)
        {

            if (this.Connected)
            {
                try
                {
                    this.selectTimer.Enabled = false;

                    byte[] serialNumber = this.accesso.SelectCard(PicoRead.SelectMode.NoAuth, PicoRead.ChipType.PicoTag);


                    if (serialNumber == null)
                    {

                        this.CurrentCard = null;
                        this.selectTimer.Enabled = true;
                    }
                    else
                    {
                        serialNumber = serialNumber.Skip(1).ToArray();
                        this.CurrentCard = PicoPassCard.Evaluate(this.accesso,serialNumber);

                        if (this.attemptBox.Checked && this.CurrentCard!=null
                             && ( !this.CurrentCard.IsCardValid || reverseBox.Checked))                           
                        {
                     
                            bool config;

                            if (reverseBox.Checked)
                                config = this.CurrentCard.UnConfigureCard(this.accesso);
                            else
                            {
                                uint cardNumber = Convert.ToUInt32(this.cardNumberBox.Value);

                                config = this.CurrentCard.ConfigureCard(this.accesso,cardNumber);

                                if (config)
                                    this.cardNumberBox.Value = Convert.ToDecimal(cardNumber + 1);


                            }


                            if (config)
                                MessageBox.Show(this, "Card Configured Successfully", "Success");
                            else
                                MessageBox.Show(this, "Card could not be configured", "Failed");
                        }

                        this.delayTimer.Enabled = true;
                    }
                }
                catch (Exception expt)
                {
                    this.accesso.Close();
                    this.accesso = null;
                    this.Connected = false;

                    ExceptionHelper.Handle(expt);

                    this.delayTimer.Enabled = true;
                }
            }
        }


        private bool VerifyKey(PicoRead.Key keyLocation, byte[] expectedKeySig)
        {
            byte[] zeros = new byte[8];
            byte[] signature = this.accesso.GetDiversifiedKey(keyLocation, zeros);

            for (int i = 0; i < 8; i++)
            {
                if (signature[i] != expectedKeySig[i])
                    return false;
            }
            return true;
        }

        private bool VerifyAccessoKeys()
        {
            if (!VerifyKey(PicoRead.Key.Kd0, KeySignature.GIProductionDebit)) return false;
            if (!VerifyKey(PicoRead.Key.Kc0, KeySignature.GIProductionCredit)) return false;

//            if (!VerifyKey(PicoRead.Key.Kd0, KeySignature.GIDemoDebit)) return false;
//            if (!VerifyKey(PicoRead.Key.Kc0, KeySignature.GIDemoCredit)) return false;
            
            if (!VerifyKey(PicoRead.Key.Kd1, KeySignature.DefaultDebit)) return false;
            if (!VerifyKey(PicoRead.Key.Kc1, KeySignature.DefaultCredit)) return false;
            return true;
        }


        private bool LoadAccessoKeys()
        {
            if (!this.accesso.LoadKey(PicoRead.Key.Kd0, Key.Exchange, Key.GIProductionDebit)) return false;
            if (!this.accesso.LoadKey(PicoRead.Key.Kc0, Key.Exchange, Key.GIProductionCredit)) return false;

//            if (!this.accesso.LoadKey(PicoRead.Key.Kd0, Key.Exchange, Key.GIDemoDebit)) return false;
//            if (!this.accesso.LoadKey(PicoRead.Key.Kc0, Key.Exchange, Key.GIDemoCredit)) return false;

            if (!this.accesso.LoadKey(PicoRead.Key.Kd1, Key.Exchange, Key.DefaultDebit)) return false;
            if (!this.accesso.LoadKey(PicoRead.Key.Kc1, Key.Exchange, Key.DefaultCredit)) return false;

            return true;
        }


        private void delayTimer_Tick(object sender, EventArgs e)
        {
            this.delayTimer.Enabled = false;
            this.selectTimer.Enabled = true;
        }


    }
}
