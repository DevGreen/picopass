﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Diagnostics;

using GI.Flash.Pico;

namespace GI.Flash.Test
{
    public enum KeyType { Default = 0, Greenwald = 1, Unknown = 2 }
    public enum CardMode { Application = 0, Personalization = 1, Unknown=2 }
    public enum KeyAccess { Area=0, Operation=1,Unknown=3}

    public class PicoPassCard
    {
        public KeyType DebitKey { get; set; }
        public KeyType CreditKey { get; set; }
        public byte[] SerialNumber { get; set; }
        public uint CardNumber {get;set;}
        public string ApplicationLabel { get; set; }

        public byte[] ConfigBlock { get; set; }

        public CardMode Mode
        {
            get
            {
                if(this.ConfigBlock!=null)
                {
                    byte fuseByte = this.ConfigBlock[7];

                    if ((fuseByte & 0x80) == 0)  //Card in Application Mode
                        return CardMode.Application;
                    else
                        return CardMode.Personalization;
                }
                else
                    return CardMode.Unknown;
            }
        }

        public KeyAccess KeyAccess
        {
            get
            {
                if (this.ConfigBlock != null)
                {
                    byte memByte = this.ConfigBlock[5];

                    if ((memByte & 0x01) == 0)
                        return KeyAccess.Operation;
                    else
                        return KeyAccess.Area;
                }
                else
                    return KeyAccess.Unknown;
            }
        }


        public bool FusesOK
        {
            //bit              required value 
            //7 Fpers       -  Don't care
            //6 coding 1    -  0
            //5 coding 0    -  1
            //4 crypt 1     -  1
            //3 crypt 0     -  1
            //2 Fprod 1     -  Don't care
            //1 Fprod 0     -  Don't care
            //0 Read Access -  0

            get
            {
                if (this.ConfigBlock != null)
                {
                    byte fuseByte = this.ConfigBlock[7];
                    return (fuseByte & 0x79) == (0x38);
                }
                else
                    return false;
            }
        }

        public bool IsCardValid
        {
            get
            {
                return DebitKey == KeyType.Greenwald
                    && CreditKey == KeyType.Greenwald
                    && Mode == CardMode.Application
                    && FusesOK
                    && KeyAccess==KeyAccess.Area
                    && SerialNumber != null
                    && CardNumber > 0
                    && ApplicationLabel == "GIFLASH1";
            }
        }

        public PicoPassCard()
        {
            this.SerialNumber = null;
            this.DebitKey = KeyType.Unknown;
            this.CreditKey = KeyType.Unknown;
            this.CardNumber = 0;
            this.ApplicationLabel = string.Empty;

        }


       public string SerialNumberString
        {
            get { return string.Format("{0:X2} {1:X2} {2:X2} {3:X2} {4:X2} {5:X2} {6:X2} {7:X2}", SerialNumber[0], SerialNumber[1], SerialNumber[2], SerialNumber[3], SerialNumber[4], SerialNumber[5], SerialNumber[6], SerialNumber[7]); }
        }


        public static byte[] ReadBlock(PicoRead reader, byte blockIndex)
        {
            byte[] result = new byte[8];
            byte[] command = new byte[] { 0x0C, blockIndex };

            //Debug.WriteLine("WriteBlock");

            bool read = reader.Transmit(PicoRead.Protocol.Pico15693,
                            PicoRead.CRC.SendAndVerify,
                            PicoRead.Timeout.Mult1,
                            false,
                            new ArraySegment<byte>(command),
                            new ArraySegment<byte>(result));

            if (read)
                return result;
            else
                return null;
        }


        public static bool WriteBlock(PicoRead reader, byte blockIndex, byte[] block)
        {
            PicoRead.CRC crc = PicoRead.CRC.Verify;
            bool signature = true;

            byte[] dataIn = new byte[10];
            byte[] dataOut = new byte[8];

            dataIn[0] = 0x87;
            dataIn[1] = blockIndex;
            Buffer.BlockCopy(block, 0, dataIn, 2, 8);

            //Debug.WriteLine("WriteBlock");

            bool success = reader.Transmit(PicoRead.Protocol.Pico15693,
                            crc,
                            PicoRead.Timeout.Mult40,
                            signature,
                            new ArraySegment<byte>(dataIn),
                            new ArraySegment<byte>(dataOut));

            return success;
        }


        public void ReadConfig(PicoRead reader)
        {
            this.ConfigBlock=ReadBlock(reader, 1);

        }

        public void ReadApplicationLabel(PicoRead reader)
        {
            byte[] appLabel = ReadBlock(reader, 5);
            this.ApplicationLabel=Encoding.ASCII.GetString(appLabel);
        }

        public bool TryKey(PicoRead reader,PicoRead.Key keyLocation)
        {
            
            if(!reader.SelectCurrentKey(keyLocation))
                throw new Exception("Error selecting accesso key: "+keyLocation.ToString());

            PicoRead.SelectMode selectMode=PicoRead.SelectMode.AuthKd;

            if(    keyLocation==PicoRead.Key.Kc0
                || keyLocation==PicoRead.Key.Kc1
                || keyLocation==PicoRead.Key.Kc2
                || keyLocation==PicoRead.Key.Kc3
                || keyLocation==PicoRead.Key.Kc4)
                selectMode=PicoRead.SelectMode.AuthKc;
                   
            byte[] sn=reader.SelectCard(selectMode,PicoRead.ChipType.PicoTag);

            return sn!=null;
        }

        public void IdentifyKeys(PicoRead reader)
        {
            if(TryKey(reader,PicoRead.Key.Kd0))
                this.DebitKey=KeyType.Greenwald;
            else if(TryKey(reader,PicoRead.Key.Kd1))
                this.DebitKey=KeyType.Default;
            else
                this.DebitKey=KeyType.Unknown;

            if(TryKey(reader,PicoRead.Key.Kc0))
                this.CreditKey=KeyType.Greenwald;
            else if(TryKey(reader,PicoRead.Key.Kc1))
                this.CreditKey=KeyType.Default;
            else
                this.DebitKey=KeyType.Unknown;

        }


        public void ReadCardNumber(PicoRead reader)
        {
            //Read CardNumber from block 6
            //Block 6 is protected with the debit key
            //Must first select card with debit authentication

            if (this.DebitKey == KeyType.Unknown)
            {
                this.CardNumber = 0;
                return;
            }

            if (!reader.SelectCurrentKey(KeyLocation(this.DebitKey, true)))
                throw new Exception("Error Selecting Debit key");

            byte[] sn = reader.SelectCard(PicoRead.SelectMode.AuthKd, PicoRead.ChipType.PicoTag);

            if (sn == null)
                throw new Exception("Could not re-select card");


            byte[] block = ReadBlock(reader, 6);

            this.CardNumber = 0;

            if (block!=null && block[0] == 0xAA)
            {
                this.CardNumber += (uint)(block[1] << 24);
                this.CardNumber += (uint)(block[2] << 16);
                this.CardNumber += (uint)(block[3] <<  8);
                this.CardNumber += (uint)(block[4]) ;
            }
        }

        public static PicoPassCard Evaluate(PicoRead reader, byte[] serialNumber)
        {
            PicoPassCard card = new PicoPassCard();

            card.SerialNumber = serialNumber;

            card.ReadConfig(reader);
            card.ReadApplicationLabel(reader);

            card.IdentifyKeys(reader);

            card.ReadCardNumber(reader);

            return card;
        }


        public void WriteKey(PicoRead reader, PicoRead.Key currentKeyLocation, PicoRead.Key newKeyLocation)
        {
            //if the card's current key is the same as the target key, no write is necessary
            if (currentKeyLocation == newKeyLocation)
                return;

            // Ask reader to diversify the new key with the card's serial number
            byte[] divKey = reader.GetDiversifiedKey(newKeyLocation, this.SerialNumber);

            if (divKey == null)
                throw new Exception("Could not Get Diversified Key: " + newKeyLocation.ToString());

            // Select the card's current key as the active reader key
            if (!reader.SelectCurrentKey(currentKeyLocation))
                throw new Exception("Cannot Select Current Key: " + currentKeyLocation.ToString());

            // Re-select the card with authentication using the active reader key (which is the card's current key)
            PicoRead.SelectMode selectMode = PicoRead.IsDebitKey(currentKeyLocation) ? PicoRead.SelectMode.AuthKd : PicoRead.SelectMode.AuthKc;
            byte[] sn = reader.SelectCard(selectMode, PicoRead.ChipType.PicoTag);

            if (sn == null)
                throw new Exception("Could not re-select card");

            // Write the diversified new key into the correct key block
            byte blockIndex = PicoRead.IsDebitKey(newKeyLocation) ?(byte) 3 :(byte) 4;
            bool write = WriteBlock(reader, blockIndex, divKey);

            if (!write)
                throw new Exception("Could not write Debit Key");

            // Done, card now has new key
        }

        public PicoRead.Key KeyLocation(KeyType keyType,bool isDebit)
        {
            // Identify the reader key slot containing the requested key
            // We have preloaded the keys into the reader as follows:
            //  Kd0 : Greenwald Debit
            //  Kc0 : Greenwald Credit
            //  Kd1 : Default Debit
            //  Kc1 : Default Credit

            switch(keyType)
            {
                case KeyType.Greenwald:
                    return isDebit? PicoRead.Key.Kd0:PicoRead.Key.Kc0;
                case KeyType.Default:
                    return isDebit? PicoRead.Key.Kd1:PicoRead.Key.Kc1;
                default:
                    throw new Exception("Cannot Locate Key: "+keyType.ToString());
            }
        }


        public void WriteCardNumber(PicoRead reader,uint cardNumber)
        {
            //select Greenwald Debit Key

            if (!reader.SelectCurrentKey(KeyLocation(KeyType.Greenwald, true)))
                throw new Exception("Could not select Debit Key");

            byte[] sn=reader.SelectCard(PicoRead.SelectMode.AuthKd, PicoRead.ChipType.PicoTag);

            if(sn==null)
                throw new Exception("Could not re-select card");

            byte[] block=new byte[8];

            block[0]=0xAA;
            block[1]= (byte)((cardNumber >>24)& 0xFF);
            block[2]= (byte)((cardNumber >>16)& 0xFF);
            block[3]= (byte)((cardNumber >>8)& 0xFF);
            block[4]= (byte)((cardNumber )& 0xFF);
            block[5] = 0xFF;
            block[6] = 0xFF;
            block[7] = 0xFF;

            WriteBlock(reader,6,block);

        }


        public void ClearCardNumber(PicoRead reader)
        {
            //select Debit Key

            if (!reader.SelectCurrentKey(KeyLocation(this.DebitKey, true)))
                throw new Exception("Could not select Debit Key");

            byte[] sn = reader.SelectCard(PicoRead.SelectMode.AuthKd, PicoRead.ChipType.PicoTag);

            if (sn == null)
                throw new Exception("Could not re-select card");

            byte[] block = new byte[8];

            block[0] = 0xFF;
            block[1] = 0xFF;
            block[2] = 0xFF;
            block[3] = 0xFF;
            block[4] = 0xFF;
            block[5] = 0xFF;
            block[6] = 0xFF;
            block[7] = 0xFF;

            WriteBlock(reader, 6, block);

        }

        public void ClearFpers(PicoRead reader)
        {
            //Clear Fpers Block 1 Byte 7 Bit 7
            this.ConfigBlock[7] = (byte)(this.ConfigBlock[7] & 0x7F);

            //And Write back to the card
            WriteBlock(reader, 1, this.ConfigBlock);
        }


        public bool ConfigureCard(PicoRead reader,uint cardNumber)
        {
            if (this.Mode != CardMode.Personalization)
                return false;

            //Write Application Label
            WriteBlock(reader, 5, new byte[] { 0x47, 0x49, 0x46, 0x4C, 0x41, 0x53, 0x48, 0x31 });

            //Load Greenwald Debit Key
            WriteKey(reader, KeyLocation(this.DebitKey, true), KeyLocation(KeyType.Greenwald, true));

            //Load Greenwald Credit Key
            WriteKey(reader, KeyLocation(this.CreditKey, false), KeyLocation(KeyType.Greenwald, false));

            WriteCardNumber(reader, cardNumber);

            //If all else is successful, "Blow the fuse"
            ClearFpers(reader);

            return true;

        }


        // For testing
        public bool UnConfigureCard(PicoRead reader)
        {
            if (this.Mode != CardMode.Personalization)
                return false;

            //Write Application Label
            WriteBlock(reader, 5, new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF });

            ClearCardNumber(reader);

            //Load Default Debit Key
            WriteKey(reader, KeyLocation(this.DebitKey, true), KeyLocation(KeyType.Default, true));

            //Load Default Credit Key
            WriteKey(reader, KeyLocation(this.CreditKey, false), KeyLocation(KeyType.Default, false));

            return true;

        }





    }
}
