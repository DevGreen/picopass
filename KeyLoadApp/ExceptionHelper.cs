﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GI.Flash.Test
{
    public class ExceptionHelper
    {
        public static void Handle(Exception expt)
        {
            MessageBox.Show(expt.ToString(), "An Error Occurred");
        }
    }
}
