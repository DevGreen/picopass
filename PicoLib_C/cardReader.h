// define return codes for functions
#ifndef SUCCESS
#define SUCCESS 0
#endif

#ifndef FAILURE
#define FAILURE (-1)
#endif


#define NO_CARD_PRESENT     (-1)
#define INVALID_CARD		(-2)
#define WRONG_CARD			(-3)
#define INSUFFICIENT_FUNDS	(-4)
#define READ_ERROR			(-5)
#define WRITE_ERROR			(-6)


// add appropriate error codes (use negative integers) starting at (-2)


// other defs
#define NUM_LOC_ID_BYTES 10
#define NUM_SER_NUM_BYTES 8
#define NUM_KEYS 8


// structure defs
typedef struct cardData
	{
	unsigned short	customerID;
	unsigned char	locationID[NUM_LOC_ID_BYTES];
	unsigned char	serialNumber[NUM_SER_NUM_BYTES];
	unsigned short	purseValue;
	unsigned long   cardNumber;
	} CARDDATA;


// function prototypes
int initCardReader(int uart, unsigned char *keys);
int readCard(CARDDATA *cardData);
int debitCard(CARDDATA *cardDataPtr, unsigned short vendPrice,unsigned short *newPurseValue);
int addValue(CARDDATA *cardDataPtr, unsigned short addValueAmount,unsigned short *newPurseValue);

