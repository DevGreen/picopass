#ifndef SERIALASCII_H
#define SERIALASCII_H


#define SERIALASCII_MAX_CMD 10
#define SERIALASCII_MAX_PARM 50


#define SERIALASCII_SUCCESS 0
#define SERIALASCII_TIMEOUT -1
#define SERIALASCII_ERROR   -2

int SerialASCII_ReadCommand(int uart,char *cmdBuf,char *parmBuf,int *nParms);
int SerialASCII_SendResponse(int uart,char *cmdBuf,char *parmBuf,int nParms);

char *SerialASCII_GetParameter(char *parmBuf,int index);


#endif //SERIALASCII_H
