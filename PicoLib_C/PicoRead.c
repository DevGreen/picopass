/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : PicoRead.c
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file implements functions that are used to send commands
to an Inside Contactless PicoRead coupler as defined in the document
 'Inside Contactless PICOREAD CHIPSET REFERENCE MANUAL'

These functions rely on the functions defined in ISO7816Exchange.h
to perform input and output operations using a serial port

These functions return error codes defined in PicoError.h
*******************************************************************************/


//#include <stdio.h>
//#include <memory.h>
#include <string.h>
#include "def.h"
#include "PicoRead.h"
#include "UartExtension.h"
#include "ISO7816Exchange.h"
#include "PicoError.h"


#define CARD_NOT_FOUND 0x6A82  // PicoRead status code for Card not found

//Internal helper function to convert ISOStatus to a Pico Error code
int MakePicoRC(ISOStatus status)
{
	int rc=PICOERROR_BADINPUT;

//	printf("PicoRc:%.4X\n",status);

	switch(status)
	{
		case ISOSTATUS_OK:
			rc=SUCCESS;
			break;
		case CARD_NOT_FOUND:
			rc=PICOERROR_NOCARD;
			break;
		case ISOSTATUS_COMERROR:
			rc=PICOERROR_COM;
			break;
		case ISOSTATUS_NOREPLY:
			rc=PICOERROR_NOREPLY;
			break;
		case ISOSTATUS_BADINPUT:
			rc=PICOERROR_BADINPUT;
			break;
		default:
			rc=PICOERROR_COUPLER;
			break;
	}
	return rc;
}

// Perform the PicoRead coupler synchronization procedure.
// assumes that UART has been set up correctly
// PicoRead coupler responds to 5 byte commands.
// Send zero bytes to coupler until it responds in order
// to flush the serial line and prove that the coupler
// is responding
int PicoRead_Synchronize(int uart)
{
	int i;
	BYTE zero=0x00;
	ISOStatus status;

	//Write up to 6 "zero" bytes to the coupler one at a time
	//and check for a response after each

	for(i=0;i<6;i++)
	{
		if(UART_Write(uart,&zero,0,1)!=0)
			return PICOERROR_COM;


		status=ISOExchange_GetStatus(uart);

		// If a valid status is received, the coupler is syncrhonized. Stop procedure.
		if (status == 0x6B00
			    || status == 0x6D00
                || status == 0x6E00
                || status == 0x9835
                || status == 0x6A82)
			return SUCCESS;
	}

	//No status received after six bytes send. Syncrhonization failed.
	return PICOERROR_SYNC;
}

// Send the ResetField command
int PicoRead_ResetField(int uart)
{
	BYTE dataIn=0x00;
	ISOStatus status;

	status=ISOExchange_In(uart,0x80,0xF4,0x40,0x00,&dataIn,0,1);

	return MakePicoRC(status);
}

// Send the select card command
int PicoRead_SelectCard(int uart,PicoRead_SelectMode selectMode,PicoRead_ChipType chipTypes,BYTE *serialNumberOut,int snOffset,int snLength)
{
	ISOStatus status;

	if(snLength<9)
		return PICOERROR_BADINPUT;

	status=ISOExchange_Out(uart,0x80,0xA4,selectMode,chipTypes,serialNumberOut,snOffset,snLength);

	return MakePicoRC(status);
}

// Select the current key to be used for authentication
int PicoRead_SelectCurrentKey(int uart,PicoRead_Key key)
{
	BYTE dataIn[8];
	ISOStatus status;

	memset(dataIn,0,8);
	status=ISOExchange_In(uart,0x80,0x52,0x00,key,dataIn,0,8);

	return MakePicoRC(status);
}

// Transmit data to/from the chip  (used for all chip commands)
int PicoRead_Transmit(int uart,
					  PicoRead_Protocol protocol,
					  PicoRead_CRC crc,
					  PicoRead_Timeout timeout,
					  BOOLEAN signature,
					  BYTE *dataIn,int offsetIn,int lengthIn,
					  BYTE *dataOut,int offsetOut,int lengthOut)
{
	ISOStatus status;
	BYTE p1= 0x04; // ISO type IN-OUT

	p1 |= timeout;
	p1 |= crc;
	p1 |= protocol;

	if(signature)
		p1 |= 0x08;

	status=ISOExchange_InOut(uart,0x80,0xC2,p1,dataIn,offsetIn,lengthIn,dataOut,offsetOut,lengthOut);

	//Construct a meaningful error code from the coupler's return value
	return MakePicoRC(status);
}
