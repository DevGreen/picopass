//----------------------------------------------------------------------------------------------
//
// uart.c
//
// This is a collection of polled and eventually, interrupt driven serial I/O functions
// to run on the Microchip PIC24.  There may be platform specific details embedded in
// the code, so use on platforms other than the PIC24FJxxxGA110 should be retested.
//
// 2/25/2009 KL  Dummied up to work on Windows.  
//----------------------------------------------------------------------------------------------
#include "def.h"

#include "uart.h"
#include "winCOM.h"


//-----------------------------------------------------------------------------------------------
// 
// initUART(int uart, int BRGvalue, int numbits, int parity, int stopbits, int handshake)
//		uart = UART1, UART2...UARTx as defined in uart.h - USE THE DEFS!
//		BRGvalue = precalculated bit rate generator value from uart.h
//		parity = from uart.h.  Number of bits is combined with parity.
//		stopbits = from uart.h
//		handshake = from uart.h
//		interruptMode = UART_INTERRUPT_ON or UART_INTERRUPT_OFF from uart.h.  NOTE: INTERRUPTS_ON
//						is not supported in this version.
//
// Description - initializes a PIC24 UART as described above.  Sets a flag for read and write 
//				 functions to test before trying to operate on the uart.
//
// Returns - integer return code of SUCCESS or FAILURE, found in defs.h
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------


static int _comHandle[10];

int initUART(int uart, int BRGvalue, int parity, int stopbits, int handshake, int interruptMode)
{
	char *comPort;
	switch(uart)
	{
		default:
		case COM1:
		comPort="COM1";
			break;
		case COM2:
		comPort="COM2";
			break;
		case COM3:
		comPort="COM3";
			break;
		case COM4:
		comPort="COM4";
			break;
		case COM5:
		comPort="COM5";
			break;
		case COM6:
		comPort="COM6";
			break;
	}


	if(b_InitComHost(comPort,&_comHandle[uart]))
		return SUCCESS;
	else
		return FAILURE;	
}



//-----------------------------------------------------------------------------------------------
// 
// closeUART(int uart)
//		uart = UART1, UART2...UARTx as defined in uart.h - USE THE DEFS!
//
// Description - disables a PIC24 UART as described above.  Disables regardless of its current state.
//				 Clears a flag for read and write routines to test before operating on the uart.
//
// Returns - none
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------

void closeUART(int uart)
{
	b_Disconnect(&_comHandle[uart]);
}



//-----------------------------------------------------------------------------------------------
// 
// UARTgetchar(int uart, char *inputCharPtr)
//		uart = UART1, UART2...UARTx as defined in uart.h - USE THE DEFS!
//		*inputCharPtr = pointer to a single character to be loaded from RX register
//
// Description - initializes a PIC24 UART as described above
//
// Returns - loads a character into *inputCharPtr if one is avaiable.  Loads a NULL '\0' if no char
//			 is available or when errors occur.
//			 Returns CHAR_READY, NO_CHAR_READY or an UART_RX_error - see uart.h
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------

int UARTgetchar(int uart, char *inputCharPtr)
{
	int rc;
	int result=b_getByte(&_comHandle[uart]);

	if(result<0)
	{
		*inputCharPtr = 0x00;
		rc=NO_CHAR_READY;
	}
	else
	{
		*inputCharPtr=(char)result;
		rc=CHAR_READY;

	}
	return rc;
}



//-----------------------------------------------------------------------------------------------
// 
// UARTputchar(int uart, char outputChar)
//		uart = UART1, UART2...UARTx as defined in uart.h - USE THE DEFS!
//		outputChar = character to place in the TX register for transmission
//
// Description - transmit a character from a PIC24 UART as described above
//
// Returns - integer return code of UART_TX_OK, UART_TX_BUFFER_FULL, or an UART_TX_error found in defs.h
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------

int UARTputchar(int uart, char outputChar)
{

	v_sendByte(&_comHandle[uart],(unsigned char)outputChar);

	return UART_TX_OK;	

//			rc = UART_TX_OK;							// return OK
//			rc = UART_TX_BUFFER_FULL;					// return full
}


int UARTputstr(int uart, char *stringPtr)
{
// DECLARATIONS -------------------------------------------------------------------
	int		rc;

	BOOLEAN	errorFlag;

	errorFlag = FALSE;

	while ((*stringPtr) && (errorFlag==FALSE))
	{
		// This blocks if tx buffer remains full, need to implement a timeout - EM 1/15/09
		while ((rc=UARTputchar(uart, *stringPtr)) == UART_TX_BUFFER_FULL);	// try to send until error or OK
				
			if (rc == UART_TX_OK)
				++stringPtr;
			else
				errorFlag = TRUE;
	}
	
return rc;
}



//-----------------------------------------------------------------------------------------------
// 
// lock(void)
//
// Description - prevents remappable IO pin programming.  See reference manual for details
//
// Returns - none
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------

void lockIO(void)
{
}



//-----------------------------------------------------------------------------------------------
// 
// unlock(void)
//
// Description - allows remappable IO pin programming.  See reference manual for details
//				 Make sure that IOL1WAY in _CONFIG2 above main() is set properly, or this 
//				 cannot be unlocked after the first lock.
//
// Returns - none
//
// Revision History
// Date		Version				Description								Author
// 01/20/09	Original version											Ed Matunas
//
//----------------------------------------------------------------------------------------------

void unlockIO(void)
{
}






//---------------------------------------------------------------------------------
//
// Demo of single character and string I/O on a PIC24 Explorer16 demo board
//
// This is not intended to be rigorous, just simply examples of how one might utilize
// the functions contained in uart.c
//
//---------------------------------------------------------------------------------

void uart_test(void)
{
}
