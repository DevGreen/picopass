//----------------------------------------------------------------------------------------------
//
// uart.h
//
// This file contains the various function prototypes, and defs used in uart.c
//
//----------------------------------------------------------------------------------------------

// Function prototypes
int initUART(int port, int brgValue, int parity, int stopbits, int handshake, int interruptMode);
int UARTgetchar(int uart, char *inputCharPtr);
int UARTputchar(int uart, char outputChar);
int UARTputstr(int uart, char *stringPtr);

void closeUART(int port);
void lockIO(void);
void unlockIO(void);

void uart_test(void);

// Name the available UARTS
#define UART1 0
#define UART2 1
#define UART3 2
#define UART4 3

#define COM1 4
#define COM2 5
#define COM3 6
#define COM4 7
#define COM5 8
#define COM6 9

#define MAX_UARTS 4


// Main clock frequency, if we need it to calculate BRG values
#ifndef FOSC
#define FOSC 32000000L
#endif

// BRG values for indicated baud when BRGH=1 (1:4 divider for higher bit rates, which we will
// always use.  BRGH=0 uses a 1:16 divider.)
// This is esier than calculating the BRG and testing every time.
// Calculation is BRG = ((FOSC/2) / (4 * baudrate)) - 1.  Round result up and down,
// then test to see which has a lower error from desired baud rate (needs to be < 2%).
//
// This goes in a 16-bit register in the PIC24 so it does not need to be < 256, but needs to
// be < 64K!  Other baud rates can be calculated if needed.
#define BAUD115200 34
#define BAUD57600 70
#define BAUD38400 103
#define BAUD19200 207
#define BAUD9600 416
#define BAUD2400 1666
#define BAUD1200 3332
#define BAUD300 13332
#define BAUD110 36363

// 7-bit not supported, normally will be 8-bit unless 9-bit is selected
#define PARITY_NONE 0x0
#define PARITY_EVEN 0x1
#define PARITY_ODD 0x2
#define PARITY_NONE_9BIT 0x3

// stop bits
#define STOP_1 0x0
#define STOP_2 0x1

// RTS/CTS handshake mode
#define NO_HANDSHAKE 0x0
#define HW_HANDSHAKE_SW_CTS 0x1
#define HW_HANDSHAKE 0x2

// RTS signal mode
#define RTS_FLOW_CONTROL 0x0
#define RTS_SIMPLEX 0x1

// internal loopback for diagnostic testing
#define LOOPBACK_DISABLE 0x0
#define LOOPBACK_ENABLE 0x1

// Interrupt modes
#define UART_INTERRUPT_OFF 0
#define UART_INTERRUPT_ON 1

// Function return codes
#define UART_RX_FERR (-5)
#define UART_RX_PERR (-4)
#define UART_RX_OERR (-3)
#define UART_RX_PORT_NOT_INITIALIZED (-2)
#define UART_RX_INVALID_PORT (-1)
#define NO_CHAR_READY 0
#define CHAR_READY 1

#define UART_TX_PORT_NOT_INITIALIZED (-2)
#define UART_TX_INVALID_PORT (-1)
#define UART_TX_BUFFER_FULL 0
#define UART_TX_OK 1
