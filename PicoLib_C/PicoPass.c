/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : PicoPass.c
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file implements functions that are used to interact with an
Inside Contactless PicoPass chip via a PicoRead coupler.

These functions are not specific to Greenwald Industries usage.
These functions implement operations defined in the document
 'Inside Contactless DATASHEET PICOPASS 2KS'

 These functions rely on the functions defined in PicoRead.h for interacting with
 the PicoRead coupler.

 These functions return error codes defined in PicoError.h
*******************************************************************************/

//#include <memory.h>
#include <string.h>

#include "def.h"
#include "PicoPass.h"
#include "PicoRead.h"
#include "PicoError.h"



// This function initializes the PicoRead Coupler by carrying out the synchronization procedure.
// It assumes that the UART has previously been opened with the correct settings:
// baud:9600, Parity:Even, Bits:8, Stop Bits:2, Flow Control: None

int PicoPass_Initialize(int uart)
{
	// Perform the PicoRead coupler synchronization procedure
	if(PicoRead_Synchronize(uart)!=SUCCESS)
		return PICOERROR_SYNC;

	// Ask the coupler to Reset the field so that any chips that are in the field and already selected will
	// be reset and become available for selection
	if(PicoRead_ResetField(uart)!=SUCCESS)
		return PICOERROR_RESET;
	else
		return SUCCESS;
}

// Find's a PicoPass Chip in the coupler's field
int PicoPass_SelectCard(int uart,PicoPass_Auth auth,BYTE* serialNumberOut,int snOffset,int snLength)
{
	//Validate the length of the SN output buffer
	if(snLength!=9)
		return PICOERROR_BADINPUT;

	// If authentication is requested, select the appropriate key
	// in the coupler.  Note: Debit Key 0 and Credit Key 0 must already be 
	// loaded into the coupler.

	if(auth==PICOPASS_AUTH_AUTHKD)
	{
//		printf("SelectKey\n");

		if(PicoRead_SelectCurrentKey(uart,PICOREAD_KEY_KD0)!=SUCCESS)
			return PICOERROR_SELECTKEY;
	}
	else if(auth==PICOPASS_AUTH_AUTHKC)
	{
		//printf("SelectKey\n");
		if(PicoRead_SelectCurrentKey(uart,PICOREAD_KEY_KC0)!=SUCCESS)
			return PICOERROR_SELECTKEY;
	}

	// Select a card

	//printf("SelectCard\n");
	return PicoRead_SelectCard(uart,auth,PICOREAD_CHIPTYPE_PICOTAG,serialNumberOut,snOffset,snLength);
}


// Reads a single block (8 bytes) from chip memory
// using the PicoPass ReadBlock command
int PicoPass_ReadBlock(int uart,BYTE blockIndex,BYTE* blockOut,int offset,int length)
{
	BYTE command[2];

	//printf("ReadBlock\n");
	if(length !=8)
		return PICOERROR_BADINPUT;

	command[0]=0x0C;
	command[1]=blockIndex;

	return PicoRead_Transmit(uart,
						PICOREAD_PROTOCOL_PICO15693,
						PICOREAD_CRC_SENDANDVERIFY,
						PICOREAD_TIMEOUT_MULT1,
						FALSE,
						command,0,2,
						blockOut,offset,8);
}

// Reads 4 contiguous blocks from chip memory
// using the PicoPass Read4Block command
int PicoPass_Read4Block(int uart,BYTE startBlockIndex,BYTE* blockOut,int offset,int length)
{
	BYTE command[2];

		//printf("Read4Block\n");

	if(length !=32)
		return PICOERROR_BADINPUT;

	command[0]=0x06;
	command[1]=startBlockIndex;

	return PicoRead_Transmit(uart,
						PICOREAD_PROTOCOL_PICO15693,
						PICOREAD_CRC_SENDANDVERIFY,
						PICOREAD_TIMEOUT_MULT1,
						FALSE,
						command,0,2,
						blockOut,offset,32);
}

// Reads the PicoPass Purse
// using the procedure defined in the PicoPass manual
int PicoPass_ReadPurse(int uart,unsigned short *purseValue)
{
	BYTE block2[8];
	int rc;
	int stagePosition=0;

	//printf("ReadPurse\n");

	// Read the PicoPass purse block (block 2)
	rc=PicoPass_ReadBlock(uart,2,block2,0,8);
	
	if(rc!=SUCCESS)
		return rc;

	// Determine the "Stage"
	// (If the left half of the block is all 1's then the stage is right)
	// (and vice versa)

    if (block2[0] == 0xFF && block2[1] == 0xFF && block2[2] == 0xFF && block2[3] == 0xFF)
		stagePosition = 4;

	// Read the purse value and load it into an unsigned short
	// Note: This code is intentionally not endian specific

	*purseValue = block2[stagePosition];
    *purseValue += block2[stagePosition + 1] << 8;

	return SUCCESS;
}

// Writes a single block
// using the PicoPass writeblock command
int PicoPass_WriteBlock(int uart,BYTE blockIndex,BYTE* blockIn,int offset,int length,BOOLEAN auth)
{
	PicoRead_CRC crc=PICOREAD_CRC_SENDANDVERIFY;
	BOOLEAN signature=FALSE;
	BYTE dataIn[10];
	BYTE dataOut[8];

    //printf("WriteBlock\n");

	// If authentication is requested, configure the coupler
	// to provide a signature rather than a CRC during transmit
	if(auth)
	{
		signature=TRUE;
		crc=PICOREAD_CRC_VERIFY;
	}

	// Verify that the output buffer for the block is 8 bytes
	if(length!=8)
		return PICOERROR_BADINPUT;

	dataIn[0]=0x87;
	dataIn[1]=blockIndex;

	memcpy(dataIn+2,blockIn,8);
	memset(dataOut,0,8);

	return PicoRead_Transmit(uart,PICOREAD_PROTOCOL_PICO15693,crc,PICOREAD_TIMEOUT_MULT40,signature,
								dataIn,0,10,
								dataOut,0,8);
}


/****************************************************************************************
The following internal functions are used to carry out the two-stage debit procedure
defined by the PicoPass manual.
*****************************************************************************************/

typedef int Stage;
#define STAGE_LEFT 0
#define STAGE_RIGHT 1

// Determine the current stage of the purse block
// If the left half is all one's then the stage is right
// and vice versa
int DetermineStage(BYTE* purseBlock)
{
	if (   purseBlock[0] == 0xFF && purseBlock[1] == 0xFF
		   && purseBlock[2] == 0xFF && purseBlock[3] == 0xFF)
        return STAGE_RIGHT;
    else
		return STAGE_LEFT;

}

// Retrieve the counter value given the current stage of the purse block
unsigned short GetCounter(Stage stage,BYTE *purseBlock)
{
	int offset=(stage==STAGE_LEFT)? 0: 4;
	return purseBlock[0+offset] + (purseBlock[1+offset] <<8 );
}

// Retrieve the recharge value given the current stage of the purse block
unsigned short GetRecharge(Stage stage,BYTE *purseBlock)
{
	int offset=(stage==STAGE_LEFT)? 0: 4;
	return purseBlock[2+offset] + (purseBlock[3+offset] <<8 );
}

// Load a counter value into the correct stage of the purse block
void PutCounter(Stage stage,BYTE *purseBlock,unsigned short counter)
{
	int offset=(stage==STAGE_LEFT)? 0: 4;
	purseBlock[0+offset]= counter & 0xFF;
	purseBlock[1+offset]= (counter>>8) & 0xFF;
}

// Load a recharge value into the correct stage of the purse block
void PutRecharge(Stage stage,BYTE *purseBlock,unsigned short recharge)
{
	int offset=(stage==STAGE_LEFT)? 0: 4;
	purseBlock[2+offset]= recharge & 0xFF;
	purseBlock[3+offset]= (recharge>>8) & 0xFF;
}

// ***************** end of internal functions ********************************************



// Performs a debit of the PicoPass purse
// according to the procedure defined by the PicoPass manual
int PicoPass_DebitPurse(int uart,unsigned short debitAmount,unsigned short *newPurseValue)
{
	BYTE purseBlock[8];
	Stage stage;
	unsigned short counter;
	unsigned short recharge;
	int rc;

	//printf("DebitPurse\n");


	// Read the purse block (block 2)
	if(PicoPass_ReadBlock(uart,2,purseBlock,0,8)!=SUCCESS)
	{
		*newPurseValue=0;
		return PICOERROR_READ;
	}

	// Identify the current stage of the purse block
	stage=DetermineStage(purseBlock);

	// Read the counter and recharge value from the purse block
	counter=GetCounter(stage,purseBlock);
	recharge=GetRecharge(stage,purseBlock);


	//Make sure that the purse value is greater than the 
	// requested debit amount
	if(debitAmount >counter)
	{
		*newPurseValue=counter;
		return PICOERROR_INSFUNDS;   // The purse does not contain sufficient funds for the requested debit.
	}
	else
		*newPurseValue= counter-debitAmount;   // Subtract the debit amount from the purse

	memset(purseBlock,0xFF,8);    // Initialize the purse block to all 1's

	// Load the new counter value and original recharge value
	// into the purse block in the same stage.
	// Note: The PicoPass chip will automatically "flip" the stage
	// during the write command
	PutCounter(stage,purseBlock,*newPurseValue);   
	PutRecharge(stage,purseBlock,recharge);        

	// Write the purse block
	rc=PicoPass_WriteBlock(uart,2,purseBlock,0,8,TRUE);

	if(rc==SUCCESS)
		return SUCCESS;
	else
		return PICOERROR_WRITE;
}




// Performs a credit of the PicoPass purse
// according to the procedure defined by the PicoPass manual
int PicoPass_CreditPurse(int uart,unsigned short creditAmount,unsigned short *newPurseValue,unsigned short *newRechargeValue)
{
	BYTE purseBlock[8];
	Stage stage;
	unsigned short counter;
	unsigned short recharge;
	int rc;

	//printf("CreditPurse\n");


	// Read the purse block (block 2)
	if(PicoPass_ReadBlock(uart,2,purseBlock,0,8)!=SUCCESS)
	{
		*newPurseValue=0;
		return PICOERROR_READ;
	}

	// Identify the current stage of the purse block
	stage=DetermineStage(purseBlock);

	// Read the counter and recharge value from the purse block
	counter=GetCounter(stage,purseBlock);
	recharge=GetRecharge(stage,purseBlock);

	if(recharge <1)
	{
		*newPurseValue=counter;
		*newRechargeValue=recharge;
		return PICOERROR_RECHRGEXP;
	}
	else if((creditAmount + counter) > 0xFFFE)
	{
		*newPurseValue=counter;
		*newRechargeValue=recharge;
		return PICOERROR_PURSEFULL;
	}
	else
	{
		*newPurseValue=counter+creditAmount;
		*newRechargeValue = recharge-1;
	}

	memset(purseBlock,0xFF,8);    // Initialize the purse block to all 1's


	//First Write recharge into current stage
	PutRecharge(stage,purseBlock,*newRechargeValue);        

	//and write to card
	rc=PicoPass_WriteBlock(uart,2,purseBlock,0,8,TRUE);

	if(rc!=SUCCESS)
		return PICOERROR_WRITE;

	//Now write counter and recharge into other stage

	//toggle stage
	stage=(stage==STAGE_LEFT)?STAGE_RIGHT:STAGE_LEFT;

	memset(purseBlock,0xFF,8);    // Initialize the purse block to all 1's

	PutCounter(stage,purseBlock,*newPurseValue);   
	PutRecharge(stage,purseBlock,*newRechargeValue);        

	// Write the purse block
	rc=PicoPass_WriteBlock(uart,2,purseBlock,0,8,TRUE);

	if(rc==SUCCESS)
		return SUCCESS;
	else
		return PICOERROR_WRITE;
}


