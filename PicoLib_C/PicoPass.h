/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : PicoPass.h
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file defines functions that are used to interact with an
Inside Contactless PicoPass chip via a PicoRead coupler.
*******************************************************************************/

#ifndef PICOPASS_H
#define PICOPASS_H

// Authentication modes for communicating with the PicoPass chip
typedef unsigned char PicoPass_Auth;
#define PICOPASS_AUTH_NOAUTH 0x00			// No Authentication used
#define PICOPASS_AUTH_AUTHKC 0x10			// Use Credit Key Authentication
#define PICOPASS_AUTH_AUTHKD 0x30			// Use Debit Key Authentication

// Note: All of these functions return error codes defined in PicoError.h

// This function initializes the PicoRead Coupler by carrying out the synchronization procedure.
// It assumes that the UART has previously been opened with the correct settings:
// baud:9600, Parity:Even, Bits:8, Stop Bits:2, Flow Control: None
int PicoPass_Initialize(int uart);

// Find's a PicoPass Chip in the coupler's field
int PicoPass_SelectCard(int uart,PicoPass_Auth auth,BYTE* serialNumberOut,int snOffset,int snLength);

// Reads a single block (8 bytes) from chip memory
int PicoPass_ReadBlock(int uart,BYTE blockIndex,BYTE* blockOut,int offset,int length);

// Reads 4 contiguous blocks from chip memory
int PicoPass_Read4Block(int uart,BYTE startBlockIndex,BYTE* blockOut,int offset,int length);

// Reads the PicoPass Purse
int PicoPass_ReadPurse(int uart,unsigned short *purseValue);

// Writes a single block
int PicoPass_WriteBlock(int uart,BYTE blockIndex,BYTE* blockIn,int offset,int length,BOOLEAN auth);

// Performs a debit of the PicoPass purse
int PicoPass_DebitPurse(int uart,unsigned short debitAmount,unsigned short *newPurseValue);

// Performs a credit of the PicoPass purse
int PicoPass_CreditPurse(int uart,unsigned short creditAmount,unsigned short *newPurseValue,unsigned short *newRechargeValue);

#endif // PICOPASS_H
