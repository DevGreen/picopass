/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : PicoRead.h
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file defines functions that are used to send commands
to an Inside Contactless PicoRead coupler
*******************************************************************************/

#ifndef PICOREAD_H
#define PICOREAD_H


// PicoRead selection Modes
typedef unsigned char PicoRead_SelectMode;

#define PICOREAD_SELECTMODE_NOAUTH 0x00
#define PICOREAD_SELECTMODE_AUTHKC 0x10
#define PICOREAD_SELECTMODE_AUTHKD 0x30
#define PICOREAD_SELECTMODE_LOOP   0x01
#define PICOREAD_SELECTMODE_HALT   0x02
#define PICOREAD_SELECTMODE_PRE    0x08


// PicoRead Chip types
typedef unsigned char PicoRead_ChipType;

#define PICOREAD_CHIPTYPE_PICOPASS  0x01
#define PICOREAD_CHIPTYPE_PICOTAG   0x02
#define PICOREAD_CHIPTYPE_PICOCRYPT 0x04
#define PICOREAD_CHIPTYPE_USER      0x08
#define PICOREAD_CHIPTYPE_ALL       0x0F

// PicoRead Protocols
typedef unsigned char PicoRead_Protocol;

#define PICOREAD_PROTOCOL_PICO14443B 0x00
#define PICOREAD_PROTOCOL_PICO15693  0x01
#define PICOREAD_PROTOCOL_PICO14443A 0x02
#define PICOREAD_PROTOCOL_USER       0x03

// PicoRead CRC options
typedef unsigned char PicoRead_CRC;

#define PICOREAD_CRC_NOCRC         0x00
#define PICOREAD_CRC_SEND          0x80
#define PICOREAD_CRC_VERIFY        0x40
#define PICOREAD_CRC_SENDANDVERIFY 0xC0

// PicoRead timeout options
typedef unsigned char PicoRead_Timeout;

#define PICOREAD_TIMEOUT_MULT1   0x00
#define PICOREAD_TIMEOUT_MULT4   0x10
#define PICOREAD_TIMEOUT_MULT40  0x20
#define PICOREAD_TIMEOUT_MULT100 0x30

// PicoRead Key identifiers
typedef unsigned char PicoRead_Key;

#define PICOREAD_KEY_KD0 0x01
#define PICOREAD_KEY_KC0 0x02
#define PICOREAD_KEY_KD1 0x03
#define PICOREAD_KEY_KC1 0x04
#define PICOREAD_KEY_KD2 0x05
#define PICOREAD_KEY_KC2 0x06
#define PICOREAD_KEY_KD3 0x07
#define PICOREAD_KEY_KC3 0x08
#define PICOREAD_KEY_KD4 0x09
#define PICOREAD_KEY_KC4 0x0A
#define PICOREAD_KEY_KD5 0x0B
#define PICOREAD_KEY_KC5 0x0C
#define PICOREAD_KEY_KD6 0x0D
#define PICOREAD_KEY_KC6 0x0E
#define PICOREAD_KEY_KD7 0x0F
#define PICOREAD_KEY_KC7 0x10


// Note: All of these functions return error codes defined in PicoError.h

// Perform the PicoRead coupler synchronization procedure.
int PicoRead_Synchronize(int uart);

// Send the ResetField command
int PicoRead_ResetField(int uart);

// Send the select card command
int PicoRead_SelectCard(int uart,PicoRead_SelectMode selectMode,PicoRead_ChipType chipTypes,BYTE *serialNumberOut,int snOffset,int snLength);

// Select the current key to be used for authentication
int PicoRead_SelectCurrentKey(int uart,PicoRead_Key key);

// Transmit data to/from the chip  (used for all chip commands)
int PicoRead_Transmit(int uart,
					  PicoRead_Protocol protocol,
					  PicoRead_CRC crc,
					  PicoRead_Timeout timeout,
					  BOOLEAN signature,
					  BYTE *dataIn,int offsetIn,int lengthIn,
					  BYTE *dataOut,int offsetOut,int lengthOut);


#endif //PICOREAD_H
