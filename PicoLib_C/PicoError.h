/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : PicoError.h
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file defines error conditions that occur when interacting
with the PicoPass chipset via a PicoRead coupler
*******************************************************************************/

#ifndef PICOERROR_H
#define PICOERROR_H

#define PICOERROR_BADINPUT  (-1)	// The parameters passed to the function were not valid
#define PICOERROR_NOREPLY   (-2)    // The PicoRead coupler did not respond to the command
#define PICOERROR_NOCARD    (-3)    // The operation could not be performed because no card was present
#define PICOERROR_COM       (-4)    // An error occurred reading or writing from/to the UART
#define PICOERROR_COUPLER   (-5)    // The PICO coupler returned ana error code that was not otherwise checked for
								    // See Inside Contactless User's Guide Appendix B
#define PICOERROR_SYNC      (-6)    // The coupler synchronization procedure failed
#define PICOERROR_RESET     (-7)	// The Reset Field command failed
#define PICOERROR_SELECTKEY (-8)	// The Select Key command failed

#define PICOERROR_READ      (-9)	// An error occured while attempting to read from a card
#define PICOERROR_WRITE     (-10)	// An error occurred while attempting to write to a card
#define PICOERROR_INSFUNDS  (-11)	// A debit could not be performed because the purse had insufficient funds
#define PICOERROR_PURSEFULL (-12)	// A credit could not be performed because the purse would exceed 65535
#define PICOERROR_RECHRGEXP (-13)	// The recharge counter has reached 0. Recharge denied.


#endif //PICOERROR_H
