#include "def.h"
#include "cardReader.h"
#include "FlashCard.h"


//int currentUart=0;			// THIS SHOULD BE TESTED FOR VALID RANGE AND INITIALIATION STATE IN EACH FUNCTION.  0 is UART1!  EM 3/10/09
static int currentUart = -1;	// Also, made it static in to limit scope to this file.  EM 3/10/09

// function prototypes
int initCardReader(int uart, unsigned char *keys)
{
	currentUart=uart;
	//Note: Key loading not implemented
	//      keys is ignored for the time being

	return FlashCard_Initialize(currentUart);
}



int readCard(CARDDATA *cardData)
{
	return FlashCard_ReadCard(currentUart,(FlashCardInfo *)cardData);
}



int debitCard(CARDDATA *cardDataPtr, unsigned short vendPrice,unsigned short *newPurseValue)
{
	return FlashCard_VendCard(currentUart,cardDataPtr->serialNumber,vendPrice,newPurseValue);
}


int addValue(CARDDATA *cardDataPtr, unsigned short addValueAmount,unsigned short *newPurseValue)
{
	return FlashCard_AddValue(currentUart,cardDataPtr->serialNumber,addValueAmount,newPurseValue);
}
