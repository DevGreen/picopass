#ifndef WINCOM_H
#define WINCOM_H

void v_sendByte(int * handle,unsigned char byte);
int b_getByte(int * handle);
int b_InitComHost(char *p_szComPortName,int * handle);
int b_Disconnect(int * handle);


#endif //WINCOM_H