
#include "uart.h"
#include "def.h"

#define TIMEOUT
int UART_Write(int uart,BYTE *buffer,int offset,int length)
{
	int i;
	BYTE *nextPtr;
	int rc;
	int timeout;

	nextPtr=buffer+offset;
	
	for(i=0;i<length;i++)
	{
		rc=UART_TX_BUFFER_FULL;
		timeout=20;

		while(rc==UART_TX_BUFFER_FULL && (timeout--)>0)	         //and spin .... !!!!
		{
			rc=UARTputchar(uart,(char)*nextPtr);
		}

		if(rc!=UART_TX_OK)
			break;

		nextPtr++;
	}

	if(i==length)
		return SUCCESS;
	else
		return FAILURE;

}

//Reads a message beginning with start char and ending with stopchar
//that fits in a buffer of given length
//Returns the message (excluding start & stop chars) in buffer
//Buffer is terminated with 0x00

int UART_ReadMessage(int uart,BYTE startChar,BYTE stopChar,BYTE *buffer,int bufOffset,int bufLength,int *resultLength)
{
	int i;
	BYTE *nextPtr;
	int rc;
	int timeout;
	BYTE currentChar;

	nextPtr=buffer+bufOffset;


	//First, read and discard characters while looking for startChar
	rc=NO_CHAR_READY;
	timeout=20;

	while(1)
	{
		while(rc==NO_CHAR_READY && (timeout--)>0 )    //And Spin ...!!!
		{
			rc=UARTgetchar(uart,(char*)&currentChar);
		}

		if(rc!=CHAR_READY) //An error occurred
		{
			*resultLength=0;
			*nextPtr=0x00;
			return FAILURE;
		}

		if(currentChar==startChar)
			break;
	}


	//Found Start Char
	
	for(i=0;i<bufLength;i++)
	{
		rc=NO_CHAR_READY;
		timeout=20;

		while(rc==NO_CHAR_READY && (timeout--)>0 )    //And Spin ...!!!
		{
			rc=UARTgetchar(uart,(char*)nextPtr);
		}

		if(rc==CHAR_READY)
		{
			if(*nextPtr==stopChar)  //Found the stop char. Done!
			{
				*nextPtr=0x00;
				*resultLength=i;
				return SUCCESS;
			}
			else
				nextPtr++;
		}
		else   //An error occurred
		{
			*nextPtr=0x00;
			*resultLength=i;
			return FAILURE;
		}
	}

	//Filled up the buffer and didn't find the stop char.

	buffer[bufOffset+bufLength-1]=0x00;
	*resultLength=bufLength-1;

	return FAILURE;
}



int UART_Read(int uart,BYTE *buffer,int offset, int length)
{
	int i;
	BYTE *nextPtr;
	int rc;
	int timeout;

	nextPtr=buffer+offset;
	
	for(i=0;i<length;i++)
	{

		rc=NO_CHAR_READY;
		timeout=20;

		while(rc==NO_CHAR_READY && (timeout--)>0 )    //And Spin ...!!!
		{
			rc=UARTgetchar(uart,(char*)nextPtr);
		}

		if(rc!=CHAR_READY)
				break;

		nextPtr++;
	}

	if(i<length)
		return FAILURE;
	else
		return SUCCESS;

}