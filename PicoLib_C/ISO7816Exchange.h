/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : ISO7816Exchange.h
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file defines functions that are used to perform ISO7816 data exchange
via a serial port (UART)
*******************************************************************************/

#ifndef ISO7816EXCHANGE_H
#define ISO7816EXCHANGE_H


// Status codes
#define ISOSTATUS_OK 0x9000
#define ISOSTATUS_NOREPLY 0xFFFF
#define ISOSTATUS_BADINPUT 0xEEEE
#define ISOSTATUS_COMERROR 0xDDDD

typedef unsigned short ISOStatus;

// Reads a single status code from the device
ISOStatus ISOExchange_GetStatus(int uart);

// Perform ExchangeOut
ISOStatus ISOExchange_Out(int uart,BYTE class,BYTE instruction,BYTE parm1,BYTE parm2,BYTE* dataOut,int offset,int length);

// Perform ExchangeIn
ISOStatus ISOExchange_In(int uart,BYTE _class, BYTE instruction, BYTE parm1, BYTE parm2,BYTE* dataIn,int offset, int length);

// Perform ExchangeIn/Out
ISOStatus ISOExchange_InOut(int uart,BYTE _class, BYTE instruction, BYTE parm1,BYTE* dataIn,int offsetIn, int lengthIn,BYTE* dataOut,int offsetOut,int lengthOut);

#endif //ISO7816EXCHANGE_H
