/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : FlashCard.c
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file contains the implementation of the interaction between 
a GI reader device and Greenwald Flash Cash Cards via an Inside Contactless 
PicoRead coupler.  These functions have knowledge of the GI specific layout
and security mechansims of Flash Cash cards.
These functions rely on the functions defined in "PicoPass.h" for communicating
with the PicoPass chipset via a PicoRead coupler
*******************************************************************************/

#include "def.h"
#include "FlashCard.h"
#include "PicoPass.h"
#include "PicoError.h"


/******************************************************************************
The following internal functions are used to access elements of the 4 block (32 byte)
header of a GI Flash Card.  The structure of the header is as follows:

bytes   
0			CardType     ( 0xAC -user card)
1			CardVersion  ( 0x01 - version 1)
2-3			CustomerID   ( 0xFF36 - Greenwald Dev.)
4-13		LocationID   ( GREENWALD - padded on right with spaces. Not null terminated.)
14-23		Reserved     ( 0x00 )
24-31		Signature    ( see VerifySignature)

*******************************************************************************/

BYTE GetCardType(BYTE *header)
{
	return header[0];
}

BYTE GetCardVersion(BYTE *header)
{
	return header[1];
}

unsigned short GetCustomerID(BYTE *header)
{
	return (header[2] << 8) + header[3];
}

unsigned long GetCardNumber(BYTE *header)
{
	return (header[16] << 24) + (header[17] << 16) + (header[18] << 8) + header[19];
}

void GetLocationID(BYTE *header, unsigned char *locationID)
{	
	int i;
	for(i=0;i<10;i++)
		locationID[i]=(char)header[0x04+i];
}


// The header is signed with the card's serialNumber
// so that a card's header cannot be blindly copied to another card
// or modified by an unauthorized agent.
// Note: The current signature algorithm is cryptographically irreversible
// but it's security depends on the secrecty of the algorithm below.
// A more secure "key based" signature should be considered in the future.
// K.L.

BOOLEAN VerifyHeader(BYTE* header,BYTE *serialNumber)
{
	int i;
	BYTE magicNumber = 8675309 % 256;

    for (i = 0; i < 8; i++)
    {
		magicNumber = (header[i] + header[i + 8] + header[i + 16] + serialNumber[i]+magicNumber) % 256;

		if (header[i + 24] != (header[i] ^ header[i + 8] ^ header[i + 16] ^ serialNumber[i] ^ magicNumber))
			return FALSE;
	}
    return TRUE;
}




/******************************************************************************
FlashCard_Initialize - public

 This function initializes the PicoRead Coupler by carrying out the synchronization procedure.
 It assumes that the UART has previously been opened with the correct settings:
 baud:9600, Parity:Even, Bits:8, Stop Bits:2, Flow Control: None

*******************************************************************************/


int FlashCard_Initialize(int uart)
{



	if(PicoPass_Initialize(uart)==SUCCESS)
		return FLASHCARD_SUCCESS;
	else
		return FLASHCARD_READ_ERROR;


}

/******************************************************************************
FlashCard_ReadCard - public

 This function checks to see if a card is present and if so,
 reads its current state into the FlashCardInfo Structure.
 It may return: SUCCESS, NO_CARD, INVALID_CARD, READ_ERROR, WRITE_ERROR

*******************************************************************************/

int FlashCard_ReadCard(int uart,FlashCardInfo *cardInfo)
{
	BYTE serialNumber[9];
	BYTE hBlock[32];
	unsigned short purse;
	int rc;
	int i;


	//Look for a card in the coupler's field and select it with Debit Authentication
	rc=PicoPass_SelectCard(uart,PICOPASS_AUTH_AUTHKD,serialNumber,0,9);

	if(rc!=SUCCESS)
	{
		if(rc==PICOERROR_NOCARD)      // No card was present in the coupler's field
			return FLASHCARD_NO_CARD;
		else
			return FLASHCARD_READ_ERROR;  // The coupler returned an error
	}

	// A card was found. Pass it's serial number back to the caller
	for(i=0;i<8;i++)
		cardInfo->SerialNumber[i]=serialNumber[i+1];

	// Read the 4 block GI header from the card starting at block 6
	if(PicoPass_Read4Block(uart,6,hBlock,0,32)!=SUCCESS)
		return FLASHCARD_READ_ERROR;

	//Check for unsigned card
	if(GetCardType(hBlock)==0xAA)
		return FLASHCARD_UNSIGNED_CARD;

	// Verify that the header has a valid signature and that the card 
	// is configured as a user card.
	if(!VerifyHeader(hBlock,serialNumber+1)
		|| GetCardType(hBlock) != 0xAC
		|| GetCardVersion(hBlock) != 1)
			return FLASHCARD_INVALID_CARD;

	// Pass the card's customer ID back to the caller
	cardInfo->CustomerID=GetCustomerID(hBlock);

	// Pass the card's location ID back to the caller
	GetLocationID(hBlock,cardInfo->LocationID);

	cardInfo->CardNumber=GetCardNumber(hBlock);

	// Read the card's purse
	if(PicoPass_ReadPurse(uart,&purse)!=SUCCESS)
		return FLASHCARD_READ_ERROR;

	// Pass the purse value back to the caller
	cardInfo->PurseValue=purse;

	// The card was read successfully
	return FLASHCARD_SUCCESS;


}


/******************************************************************************
CompareSN - internal

Compare two serial numbers for equality
*******************************************************************************/

BOOLEAN CompareSN(BYTE* serialNumber,BYTE* targetSerialNumber)
{
	int i;
	for(i=0;i<8;i++)
	{
		if(serialNumber[i] != targetSerialNumber[i])
			return FALSE;
	}
	return TRUE;
}


/******************************************************************************
FlashCard_VendCard - public

 This fucntion checks to see if a card is present and if so,
 reads it's state to verify the expected "targetSerialNumber"
 and then performs a debit of the specified amount.
It may return: SUCCESS, NO_CARD, INVALID_CARD, WRONG_CARD, INSUFFICIENT_FUNDS, READ_ERROR, WRITE_ERROR

*******************************************************************************/

int FlashCard_VendCard(int uart,BYTE *targetSerialNumber,unsigned short debitAmount,unsigned short *newPurseValue)
{
	BYTE serialNumber[9];
	BYTE hBlock[32];
	int rc;

	*newPurseValue=0;

	//Look for a card in the coupler's field and select it with Debit Authentication
	rc=PicoPass_SelectCard(uart,PICOPASS_AUTH_AUTHKD,serialNumber,0,9);

	if(rc==PICOERROR_NOCARD)
		return FLASHCARD_NO_CARD;  // No card was found in the coupler's field

	if(rc!=SUCCESS)
		return FLASHCARD_READ_ERROR;   // The coupler returned an error


	// Verify that the card's serial number matches the expected serial number
	if(!CompareSN(serialNumber+1,targetSerialNumber))
		return FLASHCARD_WRONG_CARD;

	// Read the 4 block GI header from the card starting at block 6
	if(PicoPass_Read4Block(uart,6,hBlock,0,32)!=SUCCESS)
		return FLASHCARD_READ_ERROR;

	// Verify that the header has a valid signature and that the card 
	// is configured as a user card.	
	if(!VerifyHeader(hBlock,serialNumber+1)
		|| GetCardType(hBlock) != 0xAC
		|| GetCardVersion(hBlock) != 1)
			return FLASHCARD_INVALID_CARD;


	// Debit the card's purse and return the new purse value
	rc=PicoPass_DebitPurse(uart,debitAmount,newPurseValue);

	switch(rc)
	{
		case SUCCESS:
			return FLASHCARD_SUCCESS;
		default:
		case PICOERROR_READ:
			return FLASHCARD_READ_ERROR;
		case PICOERROR_WRITE:
			return FLASHCARD_WRITE_ERROR;
		case PICOERROR_INSFUNDS:
			return FLASHCARD_INSUF_FUNDS;
	}

}




/******************************************************************************
FlashCard_AddValue - public

 This fucntion checks to see if a card is present and if so,
 reads it's state to verify the expected "targetSerialNumber"
 and then performs a credit of the specified amount.
It may return: SUCCESS, NO_CARD, INVALID_CARD, WRONG_CARD, PURSE_FULL, RECHARGE_EXPIRED, READ_ERROR, WRITE_ERROR

*******************************************************************************/

int FlashCard_AddValue(int uart,BYTE *targetSerialNumber,unsigned short creditAmount,unsigned short *newPurseValue)
{
	BYTE serialNumber[9];
	int rc;
	unsigned short newRechargeValue;


	*newPurseValue=0;

	//Look for a card in the coupler's field and select it with Credit Authentication
	rc=PicoPass_SelectCard(uart,PICOPASS_AUTH_AUTHKC,serialNumber,0,9);

	if(rc==PICOERROR_NOCARD)
		return FLASHCARD_NO_CARD;  // No card was found in the coupler's field

	if(rc!=SUCCESS)
		return FLASHCARD_READ_ERROR;   // The coupler returned an error


	// Verify that the card's serial number matches the expected serial number
	if(!CompareSN(serialNumber+1,targetSerialNumber))
		return FLASHCARD_WRONG_CARD;

	// Credit the card's purse and return the new purse value
	rc=PicoPass_CreditPurse(uart,creditAmount,newPurseValue,&newRechargeValue);


	switch(rc)
	{
		case SUCCESS:
			return FLASHCARD_SUCCESS;
		default:
		case PICOERROR_READ:
			return FLASHCARD_READ_ERROR;
		case PICOERROR_WRITE:
			return FLASHCARD_WRITE_ERROR;
		case PICOERROR_PURSEFULL:
			return FLASHCARD_PURSE_FULL;
		case PICOERROR_RECHRGEXP:
			return FLASHCARD_RECHARGE_EXP;
	}
}
