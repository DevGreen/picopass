#ifndef TEST_H
#define TEST_H

void TestRead(char *msg);
void TestVend(char *msg);
void TestCommand(char *msg);
void TestCTSLoop(char *msg);

#endif //TEST_H