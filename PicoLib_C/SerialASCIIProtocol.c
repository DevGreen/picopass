#include <string.h>
//#include <stdio.h>


#include "SerialASCIIProtocol.h"

#include "def.h"
#include "UartExtension.h"


#define BUF_LEN 256

#define STX		(0x02)
#define ETX		(0x03)
#define FS		(0x1C)
#define ACK		(0x06)
#define NACK	(0x15)


static BYTE _buffer[BUF_LEN];
static BYTE _lrc;


int WriteByte(int uart,BYTE b)
{	
	_lrc ^=b;

//	printf("Write Byte:%.2X ['%c'] LRC: %.2X\n",b,b,_lrc);
	return UART_Write(uart,&b,0,1);
}


int WriteString(int uart,char *string)
{	
	int i;
	int length;

	length=strlen(string);

	for(i=0;i<length;i++)
	{
		_lrc ^=(BYTE)string[i];

//		printf("Write Byte:%.2X ['%c'] LRC: %.2X\n",(BYTE)string[i],(BYTE)string[i],_lrc);

	}

	return UART_Write(uart,string,0,length);
}

int ReadByte(int uart,BYTE *b)
{
	int rc;
	rc=UART_Read(uart,b,0,1);

	_lrc ^= *b;

//	printf("Read Byte:%.2X ['%c'] LRC: %.2X\n",*b,*b,_lrc);

	return rc;
}

int ReadMessage(int uart,int *length)
{
	int i;
	int rc;

	_lrc=0;

//	printf("Reset LRC: %.2X\n",_lrc);

	rc=UART_ReadMessage(uart,STX,ETX,_buffer,0,BUF_LEN,length);

	if(rc==SUCCESS)
	{
		for(i=0;i<(*length);i++)
		{
			_lrc ^=_buffer[i];

//			printf("Read Byte:%.2X ['%c'] LRC: %.2X\n",_buffer[i],_buffer[i],_lrc);

		}
		_lrc^=ETX;

//		printf("Read Byte:%.2X ['%c'] LRC: %.2X\n",ETX,ETX,_lrc);

	}
	return rc;
}


int SerialASCII_ReadCommand(int uart,char *cmdBuf,char *parmBuf,int *nParms)
{
	int rc;
	int length;
	BYTE *bufPtr;
	BYTE *outPtr;
	int count;
	BYTE b;

//	printf("Reading Command\n");

	//Get Message
	if(ReadMessage(uart,&length)!=SUCCESS)
		return SERIALASCII_TIMEOUT;


//	printf("Got Command: %s\n",_buffer);
	// Read LRC
	rc=ReadByte(uart,&b);

	if(rc!=SUCCESS || _lrc!=0x00)
	{
//		printf("Sending NACK\n");
		WriteByte(uart,NACK);
		return SERIALASCII_ERROR;
	}
	else  //Send Ack
	{
//		printf("Sending ACK\n");
		WriteByte(uart,ACK);
	}


	// Parse command name

	bufPtr=_buffer;
	outPtr=cmdBuf;

	count=0;

	while(*bufPtr!=FS 
			&& count<length 
			&& count<SERIALASCII_MAX_CMD)
	{
		*outPtr++=*bufPtr++;
		count++;
	}
	
	*outPtr=0x00;

	
	if(*bufPtr!=FS)//Check for no parameters
	{
		*parmBuf=0x00;
		*nParms=0;
	}
	else //copy parameters
	{	
		bufPtr++;
		count++;
		*nParms=1;

		outPtr=parmBuf;

		// Make sure remaining message is not longer than Paramter buffer
		if( (length-count) >(SERIALASCII_MAX_PARM-1))
		{
			length=count + (SERIALASCII_MAX_PARM-1);
		}

		while(count<length)
		{
			if(*bufPtr==FS)
			{
				*outPtr++=0x00;
				bufPtr++;
				(*nParms)++;
			}
			else
			{
				*outPtr++=*bufPtr++;
				count++;
			}
		}

		*outPtr=0x00;
	}
	return SUCCESS;
}


char *SerialASCII_GetParameter(char *parmBuf,int index)
{
	char *ptr=parmBuf;


	while((index--)>0)
	{
		//Advance to next zero terminated parameter

		ptr +=strlen(ptr);
		ptr++;
	}

	return ptr;
}




int SerialASCII_SendResponse(int uart,char *command,char *parameters,int nParms)
{
	int rc;
	int i;
	BYTE ack=0x00;

	//Write STX
	if(WriteByte(uart,STX)!=SUCCESS)
		return SERIALASCII_ERROR;


	_lrc=0x00;
//	printf("Reset LRC: %.2X\n",_lrc);




	//Write Command
	if(WriteString(uart,command)!=SUCCESS)
		return SERIALASCII_ERROR;

	//Write Parameters
	for(i=0;i<nParms;i++)
	{
		//Write FS
		if(WriteByte(uart,FS)!=SUCCESS)
			return SERIALASCII_ERROR;


		//Write Parameter
		if(WriteString(uart,SerialASCII_GetParameter(parameters,i))!=SUCCESS)
			return SERIALASCII_ERROR;

	}

	//Write ETX
	if(WriteByte(uart,ETX)!=SUCCESS)
		return SERIALASCII_ERROR;


	//Write LRC
	if(WriteByte(uart,_lrc)!=SUCCESS)
		return SERIALASCII_ERROR;



	//Get Ack

	rc=UART_Read(uart,&ack,0,1);


//	if(rc==SUCCESS)
//		printf("Got Ack: %.2X \n",ack);

	if(rc==SUCCESS && ack==ACK)
		return SUCCESS;
	else
		return SERIALASCII_ERROR;

}

