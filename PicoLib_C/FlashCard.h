/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : FlashCard.h
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file contains the interface used by GI reader devices to interact with
Greenwald Flash Cash cards via an Inside Contactless PicoRead coupler.
*******************************************************************************/

#ifndef FLASHCARD_H
#define FLASHCARD_H

// Return Codes for FlashCard functions
#define FLASHCARD_SUCCESS		  0   // The operation competed successfully
#define FLASHCARD_NO_CARD		(-1)  // The coupler did not detect a card present
#define FLASHCARD_INVALID_CARD	(-2)  // A card was found but it did not have a vaild GI header
#define FLASHCARD_WRONG_CARD	(-3)  // A card was found but it's serial number did not match the expected target
#define FLASHCARD_INSUF_FUNDS	(-4)  // The card did not have sufficient funds to carry out the requested debit
#define FLASHCARD_READ_ERROR	(-5)  // An error occurred while attempting to read the card.
#define FLASHCARD_WRITE_ERROR	(-6)  // An error occurred while attempting to write to the card.
#define FLASHCARD_PURSE_FULL	(-7)  // A credit could not be performed because the purse would exceed 65535
#define FLASHCARD_RECHARGE_EXP	(-8)  // The recharge counter has reached 0. Recharge denied.
#define FLASHCARD_UNSIGNED_CARD (-10)

// A structure to communicate the state of a Flash Cash Card
typedef struct _flashCardInfo
{
	unsigned short	CustomerID;
	unsigned char	LocationID[10];
	unsigned char	SerialNumber[8];
	unsigned short	PurseValue;
	unsigned long	CardNumber;
} FlashCardInfo;

// This function initializes the PicoRead Coupler by carrying out the synchronization procedure.
// It assumes that the UART has previously been opened with the correct settings:
// baud:9600, Parity:Even, Bits:8, Stop Bits:2, Flow Control: None
int FlashCard_Initialize(int uart);

// This function checks to see if a card is present and if so,
// reads its current state into the FlashCardInfo Structure.
// It may return: SUCCESS, NO_CARD, INVALID_CARD, READ_ERROR, WRITE_ERROR
int FlashCard_ReadCard(int uart,FlashCardInfo *cardInfo);

// This fucntion checks to see if a card is present and if so,
// reads it's state to verify the expected "targetSerialNumber"
// and then performs a debit of the specified amount.
// It may return: SUCCESS, NO_CARD, INVALID_CARD, WRONG_CARD, INSUFFICIENT_FUNDS, READ_ERROR, WRITE_ERROR
int FlashCard_VendCard(int uart,BYTE *targetSerialNumber,unsigned short debitAmount,unsigned short *newPurseValue);

// This fucntion checks to see if a card is present and if so,
// reads it's state to verify the expected "targetSerialNumber"
// and then performs a credit of the specified amount.
// It may return: SUCCESS, NO_CARD, INVALID_CARD, WRONG_CARD, PURSE_FULL, RECHARGE_EXPIRED, READ_ERROR, WRITE_ERROR

int FlashCard_AddValue(int uart,BYTE *targetSerialNumber,unsigned short creditAmount,unsigned short *newPurseValue);


#endif //FLASHCARD_H
