#include <stdio.h>
#include <string.h>

#include "def.h"
#include "FlashCTSProtocol.h"
#include "SerialASCIIProtocol.h"

static BYTE _buffer[256];

int ParseHexString(BYTE *dest,char *source)
{
	int len;
	int i;
	int value;
	char temp[3];

	len=strlen(source)/2;

	for(i=0;i<len;i++)
	{
		temp[0]=source[2*i];
		temp[1]=source[(2*i)+1];
		temp[2]=0;

		if(sscanf(temp,"%X",&value)!=1)
			return FAILURE;

		dest[i]=value;
	}
	return SUCCESS;
}

int FlashCTS_SendAddValueResponse(int uart,int flashCardResult,unsigned short newPurseValue)
{
	char *parmPtr;
	int nChar;

	parmPtr=_buffer;

	nChar=sprintf(parmPtr,"%i",flashCardResult);

	parmPtr+=nChar+1;

	sprintf(parmPtr,"%i",newPurseValue);

	return SerialASCII_SendResponse(uart,"ADDV",_buffer,2);
}
int FlashCTS_SendReadResponse(int uart, int flashCardResult,BYTE *serialNumber,unsigned short customerID,char *locationID,unsigned short purseValue)
{
	char *parmPtr;
	int nChar;


	parmPtr=_buffer;

	//Result Code
	nChar=sprintf(parmPtr,"%i",flashCardResult);
	parmPtr+=nChar+1;


	if(serialNumber==0)
		*parmPtr++=0x00;
	else
	{
		nChar=sprintf(parmPtr,"%.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X",
				serialNumber[0],
				serialNumber[1],
				serialNumber[2],
				serialNumber[3],
				serialNumber[4],
				serialNumber[5],
				serialNumber[6],
				serialNumber[7]);
		parmPtr+=nChar+1;
	}

	//CustomerID
	nChar=sprintf(parmPtr,"%.4X",customerID);
	parmPtr +=nChar+1;


	//LocationID
	if(locationID==0)
	{
		*parmPtr++=0x00;
	}
	else
	{
		nChar=strlen(locationID);
		if(nChar>10)
			nChar=10;

		strncpy(parmPtr,locationID,nChar);
		parmPtr[nChar]=0;

		parmPtr+=nChar+1;		
	}

	//Purse Value
	nChar=sprintf(parmPtr,"%i",purseValue);
	parmPtr+=nChar+1;

	return SerialASCII_SendResponse(uart,"READ",_buffer,5);
}


// Returns Command type (READ, ADDVALUE, or ERROR)
int FlashCTS_GetCommand(int uart,int *timeout,BYTE *serialNumber,unsigned short *creditAmount)
{
	int rc;
	BYTE *cmdPtr;
	BYTE *parmPtr;
	int nParms=0;
	int creditInt;
	

	cmdPtr=_buffer;
	parmPtr=_buffer+20;

	rc=SerialASCII_ReadCommand(uart,cmdPtr,parmPtr,&nParms);

	if(rc==SERIALASCII_TIMEOUT)
		return FLASHCTS_CMD_TIMEOUT;
	else if( rc != SERIALASCII_SUCCESS)
		return FLASHCTS_CMD_ERROR;



	if(strcmp(cmdPtr,"READ")==0 )
	{
		if(sscanf(parmPtr,"%d",timeout)==1)
			return FLASHCTS_CMD_READ;
		else
			return FLASHCTS_CMD_ERROR;
	}
	else if(strcmp(cmdPtr,"ADDV")==0)
	{
		if(sscanf(parmPtr,"%d",timeout)!=1)
			return FLASHCTS_CMD_ERROR;

		parmPtr+=strlen(parmPtr)+1;

		if(ParseHexString(serialNumber,parmPtr)!=SUCCESS)
				return FLASHCTS_CMD_ERROR;

		parmPtr+=strlen(parmPtr)+1;


		if(sscanf(parmPtr,"%d",&creditInt)!=1)
			return FLASHCTS_CMD_ERROR;

		*creditAmount=creditInt;

		return FLASHCTS_CMD_ADDVALUE;
	}
	else
		return FLASHCTS_CMD_ERROR;

}

