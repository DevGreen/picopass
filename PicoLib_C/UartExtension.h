#ifndef UARTEXTENSION_H
#define UARTEXTENSION_H

int UART_Write(int uart,BYTE *buffer,int offset,int length);
int UART_Read(int uart,BYTE *buffer,int offset, int length);

int UART_ReadMessage(int uart,BYTE startChar,BYTE stopChar,BYTE *buffer,int bufOffset,int bufLength,int *resultLength);

#endif //UARTEXTENSION_H