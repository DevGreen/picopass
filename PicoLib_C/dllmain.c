#define WIN32_LEAN_AND_MEAN          
#include <windows.h>
#include "cardReader.h"
#include "uart.h"

// EntryPoint
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

// Exports
//

__declspec(dllexport) int PP_InitUART(int uart)
{
	return initUART(uart,0,0,0,0,0);
}


__declspec(dllexport) int PP_CloseUART(int uart)
{
	closeUART(uart);
	return SUCCESS;
}



__declspec(dllexport) int PP_InitCardReader(int uart,unsigned char *keys)
{
	return initCardReader(uart,keys);
}

__declspec(dllexport) int PP_ReadCard(CARDDATA *cardData)
{
	return readCard(cardData);
}

__declspec(dllexport) int PP_DebitCard(CARDDATA *cardDataPtr,unsigned short vendPrice,unsigned short *newPurseValue)
{
	return debitCard(cardDataPtr,vendPrice,newPurseValue);
}

__declspec(dllexport) int PP_AddValue(CARDDATA *cardDataPtr,unsigned short addValueAmount,unsigned short *newPurseValue)
{
	return addValue(cardDataPtr,addValueAmount,newPurseValue);
}
