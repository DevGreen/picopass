/******************* (C) COPYRIGHT 2009 Greenwald Industries *******************
* File Name          : ISO7816Exchange.c
* Author             : K. Lange
* Date First Issued  : 03/02/2009
* Description        : 
********************************************************************************
* History:
* 03/02/2009 : Created
*******************************************************************************
This file implements functions that are used to perform ISO7816 data exchange
via a serial port (UART)

These functions rely on functions defined in UartExtension.h to perform
serial read and write operations.

*******************************************************************************/


//#include <stdio.h>

#include "def.h"
#include "UartExtension.h"
#include "ISO7816Exchange.h"


//Internal function to send a 5 byte ISO7816 command
int SendCommand(int uart,BYTE cls,BYTE ins,BYTE p1,BYTE p2, BYTE p3)
{
	BYTE command[5];

    command[0] = cls;
    command[1] = ins;
    command[2] = p1;
    command[3] = p2;
    command[4] = p3;

	//printf("Send:%.2X %.2X %.2X %.2X %.2X\n",command[0],command[1],command[2],command[3],command[4]);

	return UART_Write(uart,command,0,5);
}

// Assembles an ISO7816 status code from two bytes 
// in a non-endian dependent manner
ISOStatus MakeStatus(BYTE b1,BYTE b2)
{
	unsigned short word;
	
	word  = b1 <<8;
	word |= b2;
	return word;
}

// Retrieves an Ack from the UART
ISOStatus GetAck(int uart,BYTE instruction)
{
	BYTE status[2];

	// Read a single byte
	if(UART_Read(uart,status,0,1)==SUCCESS)
	{

		//Expect that ACK matches instruction byte passed to command
		if(status[0]==instruction)
		{
			//printf("Ack:OK\n");
			return ISOSTATUS_OK;
		}
		else
		{
			//If ACK does not match, then a second byte is read
			//and used with the first to construct a status code
			if(UART_Read(uart,status,1,1)==SUCCESS)
			{
				//printf("Ack:Fail %.2X %.2x\n",status[0],status[1]);

				return MakeStatus(status[0],status[1]);
			}
		}
	}

	return ISOSTATUS_NOREPLY;
}

// Reads two bytes from the UART and assembles a status code
ISOStatus ISOExchange_GetStatus(int uart)
{
	BYTE status[2];

	if(UART_Read(uart,status,0,2)==SUCCESS)
	{
		//printf("Status: %.2X %.2X\n",status[0],status[1]);
		return MakeStatus(status[0],status[1]);
	}
	else
	{
		//printf("Status: No Reply\n");
		return ISOSTATUS_NOREPLY;
	}

}

//void DisplayBlock(char *name,BYTE *block,int length)
//{	
//	int i;
//
//	printf("%s[%i]: ",name,length);
//	for(i=0;i<length;i++)
//	{
//		printf("%.2X ",block[i]);
//	}
//	printf("\n");
//}

// Performs the ExchangeOut procedure
ISOStatus ISOExchange_Out(int uart,BYTE _class,BYTE instruction,BYTE parm1,BYTE parm2,BYTE* dataOut,int offset,int length)
{
	BYTE dataLength;
	ISOStatus ack;

	if(length >255 || length <0)
		return ISOSTATUS_BADINPUT;

	dataLength=(BYTE)length;

	if(SendCommand(uart,_class,instruction,parm1,parm2,dataLength)!=SUCCESS)
		return ISOSTATUS_COMERROR;

	ack=GetAck(uart,instruction);

	if(ack!=ISOSTATUS_OK)
		return ack;

	if(UART_Read(uart,dataOut,offset,dataLength)==SUCCESS)
	{
		//DisplayBlock("Read",dataOut+offset,dataLength);
		return ISOExchange_GetStatus(uart);
	}
	else
	{
		//printf("Read Error\n");
		return ISOSTATUS_COMERROR;
	}
}

//Performs the ExchangeIn procedure
ISOStatus ISOExchange_In(int uart,BYTE _class, BYTE instruction, BYTE parm1, BYTE parm2,BYTE* dataIn,int offset, int length)
{
	BYTE dataLength;
	ISOStatus ack;

	if (length > 255||length<0)
		return ISOSTATUS_BADINPUT;

	dataLength=(BYTE)length;

	if(SendCommand(uart,_class,instruction,parm1,parm2,dataLength)!=SUCCESS)
		return ISOSTATUS_COMERROR;

	ack=GetAck(uart,instruction);

	if(ack!=ISOSTATUS_OK)
		return ack;

	//DisplayBlock("Write",dataIn+offset,length);
	if(UART_Write(uart,dataIn,offset,length)==SUCCESS)
		return ISOExchange_GetStatus(uart);
	else
		return ISOSTATUS_COMERROR;
}

//Performs the ExchangeIn/Out procedure
ISOStatus ISOExchange_InOut(int uart,BYTE _class, BYTE instruction, BYTE parm1,BYTE* dataIn,int offsetIn, int lengthIn,BYTE* dataOut,int offsetOut,int lengthOut)
{
	BYTE dataInLength;
	BYTE dataOutLength;
	ISOStatus ack;

	if (lengthIn > 255||lengthIn<0
		||lengthOut > 255||lengthOut<0 )
		return ISOSTATUS_BADINPUT;

	dataInLength=(BYTE)lengthIn;
	dataOutLength=(BYTE)lengthOut;


	if(SendCommand(uart,_class,instruction,parm1,dataOutLength,dataInLength)!=SUCCESS)
		return ISOSTATUS_COMERROR;

	ack=GetAck(uart,instruction);

	if(ack!=ISOSTATUS_OK)
		return ack;

//  DisplayBlock("Write",dataIn+offsetIn,dataInLength);

	if(UART_Write(uart,dataIn,offsetIn,lengthIn)!=SUCCESS)
		return ISOSTATUS_COMERROR;

	ack=GetAck(uart,instruction);

	if(ack!=ISOSTATUS_OK)
		return ack;

	if(UART_Read(uart,dataOut,offsetOut,dataOutLength)==SUCCESS)
	{
		//DisplayBlock("Read",dataOut+offsetOut,dataOutLength);

		return ISOExchange_GetStatus(uart);
	}
	else
	{
		//printf("Read Error\n");
		return ISOSTATUS_COMERROR;
	}

}
