//------------------------------------------------------------------
//
// INSIDE C LIBRARY FOR PICO FAMILY COUPLER
//
// File : 	Com.c
//			IMPLEMENTED VERSION FOR WINDOWS OS / VC++ 6
//		Low level function that manage communication
//		with the coupler and the ISO 7816 T=0
//		protocol.
//
// Documentation :
//		The T=0 protocol is detailed in the coupler
//		Datasheet.
//
// Version :    1.5.8
// Date :       June, 22th 2004
// Author :     Joris JOURDAIN / Frederic CAPASSO
//
//------------------------------------------------------------------
#define WIN32_LEAN_AND_MEAN          
#include <windows.h>

#include "winCOM.h"

// Global variables declaration
//HANDLE	g_hndCOMPort;

//-----------------------------------------------------------------------------
// Function name : v_sendByte
//-----------------------------------------------------------------------------
// Description : Send a byte to the host following the interface defined
//               when a command has been received (parallel or serial).
//
// IN       :   byte        byte to send
// OUT      :   - none -
// RETURN   :   - none -
//
// Notes    :
//-----------------------------------------------------------------------------
void v_sendByte(int *handle,BYTE byte)
{

	BYTE		l_abBuffer[256];
	DWORD		l_iAckLength;
	HANDLE *pHndCOMPort=(HANDLE *)handle;
    //--------------------------------
    // Send byte to RS232 port buffer
    //
	l_abBuffer[0] = byte;
	WriteFile(*pHndCOMPort, l_abBuffer, 1, &l_iAckLength, NULL);
}	



//-----------------------------------------------------------------------------
// Function name :  b_getByte

//-----------------------------------------------------------------------------
// Description : Get a byte from the host.
//
// IN       :
// OUT      :
// RETURN   :   - RXREG -
//
// Notes    :
//-----------------------------------------------------------------------------
int b_getByte(int *handle)
{
	BYTE		l_abBuffer[2];
	DWORD		l_iAckLength = 1;
	int rval=0;
	HANDLE *pHndCOMPort=(HANDLE *)handle;

    //--------------------------------
    // Get byte from RS232 port buffer
    //
	if (ReadFile(*pHndCOMPort, l_abBuffer, 1, &l_iAckLength, NULL))
	{
		if(l_iAckLength==1)
		{
			return l_abBuffer[0];
		}
		else
			return (-1);			
	}
	else
		return(-1);
}






//-----------------------------------------------------------------------------
// Function name : b_InitComHost
//-----------------------------------------------------------------------------
// Description : Initialise the internal UART to the defined baudrate.
//
// IN       :   - none -
// OUT      :   - none -
// RETURN   :   - none -
//
// Notes    :	You will have to launch b_ConnectCoupler after this procedure.
//-----------------------------------------------------------------------------
BOOL b_InitComHost(char *p_szComPortName,int *handle)
{
//	BOOL			l_bConnection;
	COMMTIMEOUTS	CommTimeOuts = {0};
	DCB				CommParameters = {0};
	HANDLE *pHndCOMPort=(HANDLE *)handle;

	//--------------------------------
    // Initialise Serial communication
    //
    CommTimeOuts.ReadIntervalTimeout = 0;
    CommTimeOuts.ReadTotalTimeoutConstant = 20;

	// Open serial port and get handler
	*pHndCOMPort =	CreateFile( 
						p_szComPortName, 
						GENERIC_READ | GENERIC_WRITE,					// In/Out mode
						0,												// exclusive access
						NULL,											// no security attrs
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						NULL);

	// Build DCB
	BuildCommDCB("9600,e,8,2", &CommParameters);
    SetCommState(*pHndCOMPort, &CommParameters);
    SetCommTimeouts(*pHndCOMPort, &CommTimeOuts);

	//// Syncrhonize buffer
	//l_bConnection = b_ConnectCoupler();
	//if (!l_bConnection )
	//	g_hndCOMPort = INVALID_HANDLE_VALUE;
		
    //CommTimeOuts.ReadIntervalTimeout = 100;
    //CommTimeOuts.ReadTotalTimeoutConstant = 100;
    //SetCommTimeouts(g_hndCOMPort, &CommTimeOuts);

	
	if(*pHndCOMPort==INVALID_HANDLE_VALUE)
		return FALSE;
	else
		return TRUE;
}


BOOL b_Disconnect(int *handle)
{
	HANDLE *pHndCOMPort=(HANDLE *)handle;
	BOOL l_bDiscon;
	l_bDiscon = CloseHandle(*pHndCOMPort);
	if (l_bDiscon) *pHndCOMPort = INVALID_HANDLE_VALUE;

	return(l_bDiscon);
}
