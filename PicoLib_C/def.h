// 09Jan09	defines and typedefs for contactless reader prototype program - EM 

typedef unsigned char BYTE;

typedef unsigned int BOOLEAN;

#define TRUE 1
#define FALSE 0

#define SUCCESS 0
#define FAILURE (-1)

// ASCII control char defs
#define STX 0x02
#define ACK 0x06
#define NAK 0x15
#define INV 0x09

// program state defs, for output to LEDs
#define STATE_INIT 0x00
#define STATE_SETUP 0x01
#define STATE_IDLE 0x02
#define STATE_MACHINE_STATUS 0x04
#define STATE_GETSETUP 0x08
#define STATE_GETBLUETOOTH 0x10
#define STATE_GETCARDREADER 0x20
#define STATE_END 0xff

