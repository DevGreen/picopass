#include <stdio.h>
#pragma warning(disable : 4996)
#include <string.h>

#include "def.h"
#include "test.h"
#include "FlashCard.h"
#include "uart.h"
#include "SerialASCIIProtocol.h"
#include "FlashCTSProtocol.h"
#include "FlashCTSReader.h"

#include "WinSleep.h"


BOOLEAN Assert(int rc,char *outputMsg,char *context)
{
	if(rc!=0)
	{
		sprintf(outputMsg,"%s failed. Error code: %i",context,rc);
		return FALSE;
	}
	else
		return TRUE;
}

void FormatHex(char *dst,BYTE *buffer,int offset,int length)
{
	int i;
	int nWritten;

	for(i=0;i<length;i++)
	{
		nWritten = sprintf(dst,"%.2X ",buffer[offset+i]);

		dst+= nWritten;
	}
}
static BYTE buffer[256];

void TestCTSLoop(char *msg)
{
	int rc;

	int picoUart=COM1;
	int ctsUart=COM2;
	
	rc=initUART	(ctsUart,0,0,0,0,0);
	if(!Assert(rc,msg,"initUART cts"))
		return;

	rc=initUART	(picoUart,0,0,0,0,0);
	if(!Assert(rc,msg,"initUART pico"))
		return;


	FlashCTSReader_Init(ctsUart,picoUart);


	FlashCTSReader_Loop();
}


void TestCommand(char *msg)
{
	int uart=COM2;
	int i;
	BYTE serialNumber[]={0xA0,0xB1,0xC2,0xD3,0xE4,0xF5,0xF6,0xF7};
	int timeout;
	unsigned short creditAmount;
	BYTE b1,b2;


	int rc=initUART	(uart,0,0,0,0,0);

	if(!Assert(rc,msg,"initUART"))
		return;

	for(i=0;i<10;i++)
	{
//		rc=SerialASCII_ReadCommand(uart,buffer,buffer+100,&nParms);

		rc=FlashCTS_GetCommand(uart,&timeout,serialNumber,&creditAmount);

		switch(rc)
		{
		case FLASHCTS_CMD_READ:
			printf("Received Read Command. Timeout: %d\n",timeout);
			rc=FlashCTS_SendReadResponse(uart,FLASHCARD_SUCCESS,serialNumber,0xFF36,"GREENWALD",357);

			if(rc==SUCCESS)
				printf("Sent Response\n");
			else
				printf("Send Response failed\n");

			break;
		case FLASHCTS_CMD_ADDVALUE:
			printf("Received Add Value Command. Timeout: %d\n",timeout);

			b1=serialNumber[0];
			b2=serialNumber[1];
			printf("Serial Nubmer: %.2X%.2X%.2X%.2X%.2X%.2X%.2X%.2X\n",b1,b2,serialNumber[2],serialNumber[3],serialNumber[4],serialNumber[5],serialNumber[6],serialNumber[7]);

			printf("Credit Amount: %d \n",creditAmount);

			rc=FlashCTS_SendAddValueResponse(uart,FLASHCARD_SUCCESS,0);

			if(rc==SUCCESS)
				printf("Sent Response\n");
			else
				printf("Send Response failed\n");

			break;

		case FLASHCTS_CMD_ERROR:
			printf("Error\n");
			break;
		case FLASHCTS_CMD_TIMEOUT:
			printf("Timeout\n");
			break;
		}
			
		WinSleep(500);

	}

	sprintf(msg,"DONE");

	closeUART(uart);
}
void TestRead(char *msg)
{
	FlashCardInfo cardInfo;
	char snString[1024];
	char locString[1024];
	int uart=COM1;
	int i;


	int rc=initUART	(uart,0,0,0,0,0);

	if(!Assert(rc,msg,"initUART"))
		return;

	rc=FlashCard_Initialize(uart);

	if(!Assert(rc,msg,"FlashCard_Initialize"))
		return;


	rc=FlashCard_ReadCard(uart,&cardInfo);


	if(rc==FLASHCARD_NO_CARD)
	{
		sprintf(msg,"No Card");
		return;
	}

	if(rc==FLASHCARD_INVALID_CARD)
	{
		sprintf(msg,"Invalid Card");
		return;
	}

	if(!Assert(rc,msg,"FlashCard_ReadCard"))
		return;


	FormatHex(snString,cardInfo.SerialNumber,0,8);


	for(i=0;i<10;i++)
	{
		if(cardInfo.LocationID[i]==0x20)
			locString[i]=0x00;
		else
			locString[i]=cardInfo.LocationID[i];
	}
	locString[10]=0x00;

//	FormatHex(locString,cardInfo.LocationID,0,10);


	sprintf(msg,"READ SUCCESS\n Card[%s]\nCustomer: %.4X\nLocation: |%s|\nPurse: $ %i.%.2i",snString,cardInfo.CustomerID,locString,cardInfo.PurseValue/100,cardInfo.PurseValue%100);

	closeUART(1);
}


void TestVend(char *msg)
{
	FlashCardInfo cardInfo;
	char snString[1024];
	int uart=1;
	unsigned short purse;
	

	int rc=initUART	(uart,0,0,0,0,0);

	if(!Assert(rc,msg,"initUART"))
		return;

	rc=FlashCard_Initialize(uart);

	if(!Assert(rc,msg,"FlashCard_Initialize"))
		return;

	cardInfo.SerialNumber[0]=0x6C;
	cardInfo.SerialNumber[1]=0xA4;
	cardInfo.SerialNumber[2]=0x63;
	cardInfo.SerialNumber[3]=0x01;
	cardInfo.SerialNumber[4]=0x08;
	cardInfo.SerialNumber[5]=0x00;
	cardInfo.SerialNumber[6]=0x12;
	cardInfo.SerialNumber[7]=0xE0;
	

	FormatHex(snString,cardInfo.SerialNumber,0,8);


	rc=FlashCard_VendCard(uart,cardInfo.SerialNumber,500,&purse);

	if(!Assert(rc,msg,"FlashCard_VendCard"))
		return;
	
	sprintf(msg,"VEND SUCCESS Card[%s]\nNew Purse: $ %i.%.2i",snString,purse/100,purse%100);

	closeUART(1);
}