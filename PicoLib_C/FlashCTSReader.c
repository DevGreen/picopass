#include <stdio.h>

#include "def.h"
#include "FlashCard.h"
#include "FlashCTSProtocol.h"
#include "FlashCTSReader.h"


static int _ctsUart;
static int _picoUart;

void FlashCTSReader_Init(int ctsUart,int picoUart)
{
	int rc;

	_ctsUart=ctsUart;
	_picoUart=picoUart;

	rc=FlashCard_Initialize(picoUart);

	if(rc!=SUCCESS)
		printf("Error Initializing Pico Uart %i\n",picoUart);

}

void HandleReadRequest(int timeout)
{
	int rc;
	FlashCardInfo cardInfo;

	printf("Received Read Command. Timeout: %d\n",timeout);


	if(timeout<=0)
		timeout=1;

	while(timeout >0)
	{
		rc=FlashCard_ReadCard(_picoUart,&cardInfo);

		if(rc==FLASHCARD_SUCCESS)
		{
			printf("Sending Read Response");

			rc=FlashCTS_SendReadResponse(_ctsUart,FLASHCARD_SUCCESS,cardInfo.SerialNumber,cardInfo.CustomerID,cardInfo.LocationID,cardInfo.PurseValue);
	
			return;
		}
		else if(rc== FLASHCARD_INVALID_CARD)
		{
			rc=FlashCTS_SendReadResponse(_ctsUart,FLASHCARD_INVALID_CARD,0,0,0,0);
			return;
		}

		timeout-=100;
		//delay
	}

	rc=FlashCTS_SendReadResponse(_ctsUart,FLASHCARD_NO_CARD,0,0,0,0);

}

void HandleAddValueRequest(int timeout,BYTE *serialNumber,unsigned short creditAmount)
{
	int rc;
	unsigned short newPurseValue=0;
	
	printf("Received ADDV Command. Timeout: %d\n",timeout);

	if(timeout<=0)
		timeout=1;

	while(timeout >0)
	{
		rc=FlashCard_AddValue(_picoUart,serialNumber,creditAmount,&newPurseValue);

		if(rc==FLASHCARD_SUCCESS)
		{
			printf("ADDV Success");

			rc=FlashCTS_SendAddValueResponse(_ctsUart,FLASHCARD_SUCCESS,newPurseValue);

			return;
		}
		else if(    rc == FLASHCARD_INVALID_CARD
			     || rc == FLASHCARD_WRONG_CARD
			     || rc == FLASHCARD_WRITE_ERROR
			     || rc == FLASHCARD_PURSE_FULL
			     || rc == FLASHCARD_RECHARGE_EXP)

		{
			printf("ADDV Fail: %d",rc);
			rc=FlashCTS_SendAddValueResponse(_ctsUart,rc,newPurseValue);
			return;
		}

		timeout-=100;
		//delay
	}

	rc=FlashCTS_SendAddValueResponse(_ctsUart,FLASHCARD_NO_CARD,0);
}

void FlashCTSReader_Loop()
{
	int rc;
	int timeout;
	BYTE serialNumber[8];
	unsigned short creditAmount;


	while(1)
	{
		
		rc=FlashCTS_GetCommand(_ctsUart,&timeout,serialNumber,&creditAmount);

		printf("GetCommand:%d\n",rc);

		switch(rc)
		{
		case FLASHCTS_CMD_READ:
			HandleReadRequest(timeout);
			break;
		case FLASHCTS_CMD_ADDVALUE:
			HandleAddValueRequest(timeout,serialNumber,creditAmount);
			break;
		}
			
	}
}