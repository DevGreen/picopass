#ifndef FLASHCTSPROTOCOL_H
#define FLASHCTSPROTOCOL_H


#define FLASHCTS_CMD_READ		0x01
#define FLASHCTS_CMD_ADDVALUE	0x02
#define FLASHCTS_CMD_ERROR		0x03
#define FLASHCTS_CMD_TIMEOUT    0x04



// Returns Command type (READ, ADDVALUE, ERROR, or TIMEOUT)
int FlashCTS_GetCommand(int uart,int *timeout,BYTE *serialNumber,unsigned short *creditAmount);

// Returns SUCCESS or FAILURE
int FlashCTS_SendReadResponse(int uart,int flashCardResult,BYTE *serialNumber,unsigned short customerID,char *locationID,unsigned short purseValue);
int FlashCTS_SendAddValueResponse(int uart,int flashCardResult,unsigned short newPurseValue);


#endif //FLASHCTSPROTOCOL_H
